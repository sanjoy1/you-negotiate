@extends('layout.app-header')
@section('content')
<section class="ceoMail">
   <div class="container">
    <div class="table-email">
        <h1>My Accounts</h1>
      <div class="table-responsive2 table-desktop-style">
        <table id="example" class="table" style="width:100%">
        <thead>
            <tr>
                <th>creditor’s name</th>
                <th>total balance</th>
                <th>discount</th>
                <th>discounted balance</th>
                <th>status</th>
                <th>action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <a href="#" class="blue-text">YouNegotiate</a>
                </td>
                <td>$200.00</td>
                <td>$25.00</td>
                <td>$150.00</td>
                <td>Pending</td>
                <td>
                    <a class="btn btn-blue2" href="choose2">Negotiate </a>
                    <a class="btn btn-red" href="#">Dispute</a>
                </td>
            </tr>
             <tr>
                <td>
                    <a href="#" class="blue-text">YouNegotiate</a>
                </td>
                <td>$200.00</td>
                <td>$25.00</td>
                <td>$150.00</td>
                <td>Manage Payment Plan</td>
                <td>
                    <a class="btn btn-blue2" href="choose2">Manage Payment Plan </a>
                  
                </td>
            </tr>
            <tr>
                <td>
                    <a href="#" class="blue-text">YouNegotiate</a>
                </td>
                <td>$200.00</td>
                <td>$25.00</td>
                <td>$150.00</td>
                <td>Ongoing</td>
                <td>
                    <a class="btn btn-blue2" href="choose2">Negotiate</a>
                    <a class="btn btn-green" href="#">Last visited Page</a>
                     <a class="btn btn-red" href="#">Dispute</a>
                  
                </td>
            </tr>
            <tr>
                <td>
                    <a href="#" class="blue-text">YouNegotiate</a>
                </td>
                <td>$200.00</td>
                <td>$25.00</td>
                <td>$150.00</td>
                <td>Pending</td>
                <td>
                    <a class="btn btn-blue2" href="choose2">Negotiate</a>
                   
                     <a class="btn btn-red" href="#">Dispute</a>
                  
                </td>
            </tr>
             <tr>
                <td>
                    <a href="#" class="blue-text">YouNegotiate</a>
                </td>
                <td>$200.00</td>
                <td>$25.00</td>
                <td>$150.00</td>
                <td>Ongoing</td>
                <td>
                    <a class="btn btn-blue2" href="choose2">Negotiate</a>
                    <a class="btn btn-green" href="#">Last visited Page</a>
                    <a class="btn btn-red" href="#">Dispute</a>
                  
                </td>
            </tr>
            <tr>
                <td>
                    <a href="#" class="blue-text">YouNegotiate</a>
                </td>
                <td>$200.00</td>
                <td>$25.00</td>
                <td>$150.00</td>
                <td>Ongoing</td>
                <td>
                    <a class="btn btn-blue2" href="choose2">Negotiate</a>
                    <a class="btn btn-green" href="#">Last visited Page</a>
                     <a class="btn btn-red" href="#">Dispute</a>
                  
                </td>
            </tr>
            <tr>
                <td>
                    <a href="#" class="blue-text">YouNegotiate</a>
                </td>
                <td>$200.00</td>
                <td>$25.00</td>
                <td>$150.00</td>
                <td>Manage Payment Plan</td>
                <td>
                    <a class="btn btn-blue2" href="choose2">Manage Payment Plan</a>
                   
                </td>
            </tr>
<tr>
                <td>
                    <a href="#" class="blue-text">YouNegotiate</a>
                </td>
                <td>$200.00</td>
                <td>$25.00</td>
                <td>$150.00</td>
                <td>Manage Payment Plan</td>
                <td>
                    <a class="btn btn-blue2" href="choose2">Manage Payment Plan</a>
                   
                </td>
            </tr>
             <tr>
                <td>
                    <a href="#" class="blue-text">YouNegotiate</a>
                </td>
                <td>$200.00</td>
                <td>$25.00</td>
                <td>$150.00</td>
                <td>Pending</td>
                <td>
                    <a class="btn btn-blue2" href="choose2">Negotiate</a>
                   
                     <a class="btn btn-red" href="#">Dispute</a>
                  
                </td>
            </tr>
            <tr>
                <td>
                    <a href="#" class="blue-text">YouNegotiate</a>
                </td>
                <td>$200.00</td>
                <td>$25.00</td>
                <td>$150.00</td>
                <td>Manage Payment Plan</td>
                <td>
                    <a class="btn btn-blue2" href="choose2">Manage Payment Plan</a>
                   
                </td>
            </tr>
        </tbody>
        
    </table>
</div>
    </div>
      
   </div>
</section>
@endsection
@section('script')

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            language: {
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });
    });
</script>
@endsection