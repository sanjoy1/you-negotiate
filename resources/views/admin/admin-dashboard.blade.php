@extends('layout.admin.app')
@section('admin-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Superadmin: Manage Creditors</div>
      </h1>
      <div class="section-body">
         
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Superadmin: Manage Creditors</h4>
                     
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table" id="example">
                           <thead>
                              <tr>
                                 <th>
                                    Company name
                                 </th>
                                 <th>Status</th>
                                 <th>Action</th>
                                
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>


                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>

                             
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Manage Archived Creditors </h4>
                 
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table" id="example2">
                           <thead>
                              <tr>
                                 <th>
                                    Company name
                                 </th>
                                 <th>Status</th>
                                 <th>Action</th>
                                
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>


                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>

                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr><tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="blue-text">AmitCompany</a>
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </td>
                                 <td>
                                    <a href="manage-creditors" class="btn btn-green">View</a> 
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#configure">Configure</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#set-commission" >Commission  </a>
                                    <a href="#" class="btn btn-view">Access Account  </a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#set-commission">Master Terms</a>
                                    <a href="#" class="btn btn-border-line" data-toggle="modal" data-target="#set-d">Deactivate account </a>
                                    <a href="#" class="btn btn-green">Export List</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Delete</a>
                                 </td>
                              </tr>
                              
                              
                             
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
        
        
      </div>
   </section>
</div>
<footer class="main-footer">
   <div class="footer-bg">
      <div class="footer-left">
         <a href="index"><img alt="logo" src="{{asset('admin/img/logo.png')}}"></a>
      </div>
      <div class="footer-right">
         © 2020 YouNegotiate ™ | All rights reserved
      </div>
   </div>
</footer>
</div>
</div>
<!-- Modal -->
<div class="modal modal-new fade" id="configure">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Configure</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <form>
              <div class="form-row">
                  <div class="form-group col-md-6">
                     <label>Contract Date</label>
                     <div id="datepicker" class="input-group date" data-date-format="mm-dd-yyyy">
                        <input class="form-control" type="text" readonly />
                        <span class="input-group-addon"><img alt="image" src="{{asset('admin/img/calendar.svg')}}" /></span>
                     </div>
                  </div>
                  <div class="form-group col-md-6">
                     <label>SMS Rate <i class="icon-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="At vero eos et">!</i></label>
                     <input type="text" class="form-control" placeholder="">
                  </div>
               </div>

               <div class="form-row">
                  <div class="form-group col-md-6">
                     <label>Email Rate</label>
                    <input type="text" class="form-control" placeholder="Select...">
                  </div>
                  <div class="form-group col-md-6">
                     <label>E-Letter Rate</label>
                     <input type="text" class="form-control" placeholder="">
                  </div>
               </div>

               <div class="form-row">
                   <div class="form-group col-md-6">
                     <label>H2H Share(%) <i class="icon-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="At vero eos et">!</i></label>
                     <input type="text" class="form-control" placeholder="">
                  </div>
                  <div class="form-group col-md-6">
                     <label>Billing Schedule</label>
                     <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                  </div>
               </div>

               <div class="form-row">
                   <div class="form-group col-md-6">
                     <label>Licence Fees</label>
                     <input type="text" class="form-control" placeholder="">
                  </div>
                 
               </div>



               <div class="text-right">
                  <a href="#" class="btn border-btn">Reject</a>
                  <button type="submit" class="btn blue-btn">Approve</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<div class="modal modal-new fade" id="set-commission">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Master Negotiation Terms</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <form>
              
                  <div class="form-group">
                     <label>Pay in Full Balance Discount % <sup>*</sup> <i class="icon-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="At vero eos et">!</i></label>
                    <input type="text" class="form-control" placeholder="">
                  </div>
                 

                <div class="form-group">
                     <label>Payment Plan Balance Discount % <sup>*</sup> <i class="icon-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="At vero eos et">!</i></label>
                     <input type="text" class="form-control" placeholder="">
                  </div>

                  <div class="form-group">
                     <label>Minimum Monthly Payment % of Payment Plan Balance <sup>*</sup> <i class="icon-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="At vero eos et">!</i></label>
                      <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                  </div>

                    <div class="form-group">
                     <label>Maximum Days to Receive 1st Payment Plan Payment  <sup>*</sup> <i class="icon-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="At vero eos et">!</i></label>
                      <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                  </div>

              




               <div class="text-right">
                  <a href="#" class="btn border-btn">Close</a>
                  <button type="submit" class="btn blue-btn">Save</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>

<div class="modal modal-new fade" id="set-d">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Set Commission</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <form>
              
                
<div class="form-group">
                     <label>Name </label>
                      <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                  </div>
                  <div class="form-group">
                     <label>H2H User Type</label>
                      <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                  </div>

                    <div class="form-group">
                     <label>Commission on H2H Share</label>
                      <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                  </div>

              




               <div class="text-right">
                  <a href="#" class="btn border-btn">Close</a>
                  <button type="submit" class="btn blue-btn">Save</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>


<div class="modal modal-new md-sm fade" id="small-modal" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Delete Creditor</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Do You Want to Delete ?</h2>
            <p>You sure you want to delete this creditor</p>
            <div class="text-right mt-4">
               <button type="submit" class="btn border-btn">Cancel</button>
               <button type="submit" class="btn blue-btn">Yes</button>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection
@section('script')
<script src="{{asset('admin/modules/jquery.min.js')}}"></script>
<script src="{{asset('admin/modules/popper.js')}}"></script>
<script src="{{asset('admin/modules/tooltip.js')}}"></script>
<script src="{{asset('admin/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('admin/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('admin/js/sa-functions.js')}}"></script>
<script src="{{asset('admin/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('admin/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('admin/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('admin/modules/modules-datatables.js')}}"></script>
<script src="{{asset('admin/modules/chart.min.js')}}"></script>
<script src="{{asset('admin/modules/summernote/summernote-lite.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<script src="{{asset('admin/js/scripts.js')}}"></script>
<script src="{{asset('admin/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
       $('#example').DataTable({
           language: {
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
        $('#example2').DataTable({
           language: {
            
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   

      $("#datepicker").datepicker({ 
            autoclose: true, 
            todayHighlight: true
      }).datepicker('update', new Date());
    
  
   });
</script>
@endsection