@extends('layout.admin.app')
@section('admin-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Consumer profile</div>
      </h1>

      <div class="tab-section">
        <ul class="list-tab">
            <li><a href="basic-info" class="active">Basic Information</a></li>
            <li><a href="pay-history">Payment History</a></li>
        </ul>
                    <div>
                     <a href="#" class="btn btn-navy">Counter</a>
                     <a href="#" class="btn btn-yellow">Ask My Client</a>
                     <a href="#" class="btn btn-black">Contact Customer</a>
                     <a href="#" class="btn btn-deactive">Decline</a>
                     <a href="#" class="btn btn-green">Accept</a>
                    </div>
      </div>

      <div class="section-body">
         <div class="row">
            <div class="col-12">
              <div class="card">
                  <div class="card-header flex-hd border-btm">
                     <h4>Basic Information</h4>
                    
                  </div>
                  <div class="card-body">
                          <div class="row mb-4">
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="ac-profile mb-4">
                                    <h2 class="bold-roy">Roy Mustang</h2>
                        <h4>Happy Finance</h4>

                        <div class="d-flex gap-20 mt-4">
                            <div class="col-t">
                                <div class="lbl-text">Account Number </div>
                                <h5>1321456565656</h5>
                            </div>
                             <div class="col-t">
                                <div class="lbl-text">Last 4 SSN</div>
                               <h5>1321</h5>
                            </div>
                            <div class="col-t">
                                <div class="lbl-text">Join Link</div>
                               <h5><a href="#">https://yng.link/LO147368TB</a></h5>
                            </div>
                            
                        </div>

                     </div>

                            </div>
                       
                        <div class="col-sm-12 col-md-3 col-lg-3">
                             <a href="#" class="common-bx inline-block">
                        <div><img alt="user" src="http://127.0.0.1:8000/admin/img/users.svg"></div>
                        <p>Original Balance</p>
                        <h4>$1240.00</h4>
                        </a>
                        </div>
                       
                       <div class="col-sm-12 col-md-3 col-lg-3">
                             <a href="#" class="common-bx inline-block">
                        <div><img alt="user" src="http://127.0.0.1:8000/admin/img/users.svg"></div>
                        <p>Current Balance </p>
                        <h4>$12340.00</h4>
                        </a>
                        </div>
                             
                        
                     </div>
                     <div class="ac-profile mb-4">
                        <h4>Contact Profile</h4>

                        <div class="d-flex gap-20 mt-4">
                            <div class="col-t">
                                <div class="lbl-text">Mobile 1</div>
                                <h5>+91 934949955</h5>
                            </div>
                             <div class="col-t">
                                <div class="lbl-text">Mobile 2</div>
                               <h5>+943243435</h5>
                            </div>
                            <div class="col-t">
                                <div class="lbl-text">Email 1</div>
                               <h5>hello@gmaill.com</h5>
                            </div>
                            <div class="col-t">
                                <div class="lbl-text">Email 2</div>
                               <h5>hi@yahoo.in</h5>
                            </div>
                        </div>

                     </div>

                     <div class="ac-profile mb-4">
                        <h4>Communication Profile</h4>

                        <div class="d-flex gap-20 mt-4">
                            <div class="col-t">
                                <div class="custom-control custom-radio custom-control-inline cp-lbl">
                                    <input type="radio" id="c1" name="c1" class="custom-control-input">
                                    <label class="custom-control-label" for="c1">Email Communication</label>
                                 </div>
                                <h5>hi@gmail.com</h5>
                            </div>
                             <div class="col-t">
                                <div class="custom-control custom-radio custom-control-inline cp-lbl">
                                    <input type="radio" id="c2" name="c2" class="custom-control-input">
                                    <label class="custom-control-label" for="c2">Text</label>
                                 </div>
                               <h5>+91 934949955</h5>
                            </div>
                            <div class="col-t">
                                <div class="custom-control custom-radio custom-control-inline cp-lbl">
                                    <input type="radio" id="c3" name="c3" class="custom-control-input">
                                    <label class="custom-control-label" for="c3">Receive Call On Mobile</label>
                                 </div>
                               <h5>+91 934949955</h5>
                            </div>
                            <div class="col-t">
                                 <div class="custom-control custom-radio custom-control-inline cp-lbl">
                                    <input type="radio" id="c4" name="c4" class="custom-control-input">
                                    <label class="custom-control-label" for="c4">Receive Call On Landline</label>
                                 </div>
                               <h5>+91 934949955</h5>
                            </div>
                        </div>

                     </div>

                <div class="ac-profile mb-4">
                        <h4>Consumer Resonse</h4>

                        <div class="d-flex gap-20 mt-4">
                            <div class="col-t">
                                <label class="container-checkbox">Prefer to Manage By Phone
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                                </label>
                            </div>

                            <div class="col-t">
                                <label class="container-checkbox">Never Plan To Pay
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="col-t">
                                <label class="container-checkbox">Disputed
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="col-t">
                                <label class="container-checkbox">Bankruptcy
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="col-t">
                                <label class="container-checkbox">No response to date
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                                </label>
                            </div>
                            
                        </div>

                     </div>
                    

                  </div>

                 

               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd border-btm">
                     <h4>Account Information</h4>
                    
                  </div>
                  <div class="card-body">
                          <div class="row mb-4">
                            <div class="col-sm-12">
                                <div class="ac-profile mb-4">
                                    <h2 class="bold-roy">Monthly Payment Plan</h2>
                                    <h4>Pay monthly</h4>

                                    <div class="d-flex gap-20 mt-4">
                                        <div class="col-t">
                                            <div class="lbl-text">Status</div>
                                            <span class="tag tag-green">Approved</span>
                                        </div>
                                        <div class="col-t">
                                            <div class="lbl-text">Approved By</div>
                                        <h5>1321</h5>
                                        </div>
                                        <div class="col-t">
                                            <div class="lbl-text">Discounted Pay of</div>
                                        <h5>$1244.00</h5>
                                        </div>
                                        <div class="col-t">
                                            <div class="lbl-text">AMT</div>
                                        <h5>10$ / Month</h5>
                                        </div>
                                        
                                    </div>

                     </div>

                            </div>
                       
                       
                       
                     
                             
                        
                     </div>
                    


             
            </div>
            </div>

            <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Set Terms</h4>
                  </div>
                  <div class="card-body">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Payterms For</label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">US</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                 </select>
                              </div>
                              <div class="form-group col-md-6">
                              </div>
                           </div>
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Pay in Full Balance Discount % <sup>*</sup></label>
                                 <p>Surveyed consumers said they are more likely to pay off a balance if they can get a discount, and if they don’t they do nothing or go to a debt negotiation service!<br>If my customer can pay in full, I would like to discount their balance by this %. Example: Account Balance $500.00, My PIF Balance Discount 20% ($100.00), My Customer can pay off this account for $400.00!</p>
                                 <input type="text" class="form-control">
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Payments Plan Balance Discount % <sup>*</sup></label>
                                 <p>
                                    account.98% of consumers surveyed said they will not call a creditor to set up payments, so we want your customer to start making payments on You Negotiate! Increase your chances by discounting the balance if they set up and stick with a payment plan!<br>If my customer commits to a Payment Plan (I approve), I would like to discount their pay off balance by this %. Example: Account Balance $500.00, My Payment Plan Balance Discount is 10% ($50.00), If my customer sets up a payment plan, My Customer will make a total of $450.00 in payments to pay off this account.
                                 </p>
                                 <input type="text" class="form-control">
                              </div>
                           </div>
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Minimum Monthly Payments % of Balance <sup>*</sup></label>
                                 <p>99% of consumers surveyed said they will not set up a plan if they can’t afford it and don’t call because they feel too much pressure from a collector. The goal here is to get a payment plan they can afford in place as quickly as possible before they throw in the towel with the attitude of – do what you need to do!<br>% of Balance Minimum % of balance that must be paid per month to digitally approve. Example: account balance $500.00, minimum monthly payment % is 10%, which means the minimum monthly payment offered is $50.00 (it will take 10 months for the Customer to pay their balance in full)</p>
                                 <input type="text" class="form-control">
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Maximum Days to Receive 1st Payment Plan Payment <sup>*</sup></label>
                                 <p>
                                    99% of consumers surveyed want to pay off the debt and make a payment as soon as they can. Life comes up with other pressing bills to pay, so we want to give them enough time to make the payment, yet not let them stretch it out to eternity!<br>From the date the Customer sets up a payment plan, what is the # of days you allow to schedule their first payment. Example: Customer Agrees to Payment Plan on May 1, My Max days is set at 30 days, Customer must schedule this first payment on or before June 1st, otherwise requires them to submit an offer.
                                 </p>
                                 <input type="text" class="form-control">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-12"> <label>Please note the customer will make up to 3 negotiation attempts. If their attempts do not meet your minimums above, we will provide them a final offer based on your term requirements. If they cannot afford your final offer, they will be directed to send you an offer they can afford. Be on the lookout in the Customer Offers</label></div>
                        </div>
                        <div class="text-right col-sm-12">
                           <button type="submit" class="btn blue-btn">Save My Master Terms</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </div>

         
   </section>
   </div>
  
</div>
</div>


@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $('#summernote').summernote({
      placeholder: 'Hello stand alone ui',
      tabsize: 2,
      height: 100,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
      ]
    });
   $(document).ready(function () {

       $('#example2').DataTable({
          language: {
            
              'paginate': {
                  'previous': '<span class="fa fa-angle-left"></span>',
                  'next': '<span class="fa fa-angle-right"></span>'
              }
              
          }
      });
   
   });
</script>
@endsection