@extends('layout.admin.app')
@section('admin-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Manage H2H Users</div>
      </h1>
      <div class="section-body">
         
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Create New User</h4>
                  </div>
                  <div class="card-body">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Name <sup>*</sup> <i class="icon-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="At vero eos et">!</i></label>
                                 <input type="text" class="form-control">
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Email <sup>*</sup> <i class="icon-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="At vero eos et">!</i></label>
                                 <input type="text" class="form-control">
                              </div>
                           </div>
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Phone <i class="icon-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="At vero eos et">!</i></label>
                                 <input type="text" class="form-control">
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Select H2H User Type <i class="icon-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="At vero eos et">!</i></label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">US</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="text-right col-sm-12">
                           <button type="submit" class="btn blue-btn">Submit</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Current Users </h4>
                   
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-200" id="example2">
                           <thead>
                              <tr>
                                 <th>
                                    Name
                                 </th>
                                 <th>Status</th>
                                 <th>Email</th>
                                 <th>Company</th>
                                 <th>Fed ID or SSN</th>
                                 <th>Type</th>
                                 <th>Total creditors</th>
                                 <th>Consumer payments to date</th>
                                 <th>YN revenue to date</th>
                                 <th>Channel Rev to date</th>
                                 <th>Action</th>
                                
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                   Peter Williams
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                   
                                 </td>
                                 <td>
                                    <a href="#" class="text-black">pwilliams@gmail.com</a> 
                                 </td>
                                 <td>
                                    <select class="custom-select select-min-width">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                                 </td>
                                 <td>4444</td>
                                 <td>Sales rep</td>
                                 <td class="blue-text">245</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>
                                     <a href="#" class="btn btn-view">Open Profile</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#small-d">Deactivate</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#small-b" >Export  </a>
                                  
                                 </td>
                              </tr>

                                <tr>
                                 <td>
                                   Peter Williams
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                   
                                 </td>
                                 <td>
                                    <a href="#" class="text-black">pwilliams@gmail.com</a> 
                                 </td>
                                 <td>
                                    <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                                 </td>
                                 <td>4444</td>
                                 <td>Sales rep</td>
                                 <td class="blue-text">245</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>
                                    <a href="#" class="btn btn-view">Open Profile</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#small-d">Deactivate</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#small-b" >Export  </a>
                                  
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                   Peter Williams
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                   
                                 </td>
                                 <td>
                                    <a href="#" class="text-black">pwilliams@gmail.com</a> 
                                 </td>
                                 <td>
                                    <select class="custom-select select-min-width">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                                 </td>
                                 <td>4444</td>
                                 <td>Sales rep</td>
                                 <td class="blue-text">245</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>
                                     <a href="#" class="btn btn-view">Open Profile</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#small-d">Deactivate</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#small-b" >Export  </a>
                                  
                                 </td>
                              </tr>

                                <tr>
                                 <td>
                                   Peter Williams
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                   
                                 </td>
                                 <td>
                                    <a href="#" class="text-black">pwilliams@gmail.com</a> 
                                 </td>
                                 <td>
                                    <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                                 </td>
                                 <td>4444</td>
                                 <td>Sales rep</td>
                                 <td class="blue-text">245</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>
                                    <a href="#" class="btn btn-view">Open Profile</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#small-d">Deactivate</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#small-b" >Export  </a>
                                  
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                   Peter Williams
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                   
                                 </td>
                                 <td>
                                    <a href="#" class="text-black">pwilliams@gmail.com</a> 
                                 </td>
                                 <td>
                                    <select class="custom-select select-min-width">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                                 </td>
                                 <td>4444</td>
                                 <td>Sales rep</td>
                                 <td class="blue-text">245</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>
                                     <a href="#" class="btn btn-view">Open Profile</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#small-d">Deactivate</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#small-b" >Export  </a>
                                  
                                 </td>
                              </tr>

                                <tr>
                                 <td>
                                   Peter Williams
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                   
                                 </td>
                                 <td>
                                    <a href="#" class="text-black">pwilliams@gmail.com</a> 
                                 </td>
                                 <td>
                                    <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                                 </td>
                                 <td>4444</td>
                                 <td>Sales rep</td>
                                 <td class="blue-text">245</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>
                                    <a href="#" class="btn btn-view">Open Profile</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#small-d">Deactivate</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#small-b" >Export  </a>
                                  
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                   Peter Williams
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                   
                                 </td>
                                 <td>
                                    <a href="#" class="text-black">pwilliams@gmail.com</a> 
                                 </td>
                                 <td>
                                    <select class="custom-select select-min-width">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                                 </td>
                                 <td>4444</td>
                                 <td>Sales rep</td>
                                 <td class="blue-text">245</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>
                                     <a href="#" class="btn btn-view">Open Profile</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#small-d">Deactivate</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#small-b" >Export  </a>
                                  
                                 </td>
                              </tr>

                                <tr>
                                 <td>
                                   Peter Williams
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                   
                                 </td>
                                 <td>
                                    <a href="#" class="text-black">pwilliams@gmail.com</a> 
                                 </td>
                                 <td>
                                    <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                                 </td>
                                 <td>4444</td>
                                 <td>Sales rep</td>
                                 <td class="blue-text">245</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>
                                    <a href="#" class="btn btn-view">Open Profile</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#small-d">Deactivate</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#small-b" >Export  </a>
                                  
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                   Peter Williams
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                   
                                 </td>
                                 <td>
                                    <a href="#" class="text-black">pwilliams@gmail.com</a> 
                                 </td>
                                 <td>
                                    <select class="custom-select select-min-width">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                                 </td>
                                 <td>4444</td>
                                 <td>Sales rep</td>
                                 <td class="blue-text">245</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>
                                     <a href="#" class="btn btn-view">Open Profile</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#small-d">Deactivate</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#small-b" >Export  </a>
                                  
                                 </td>
                              </tr>

                                <tr>
                                 <td>
                                   Peter Williams
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                   
                                 </td>
                                 <td>
                                    <a href="#" class="text-black">pwilliams@gmail.com</a> 
                                 </td>
                                 <td>
                                    <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                                 </td>
                                 <td>4444</td>
                                 <td>Sales rep</td>
                                 <td class="blue-text">245</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>
                                    <a href="#" class="btn btn-view">Open Profile</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#small-d">Deactivate</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#small-b" >Export  </a>
                                  
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                   Peter Williams
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                   
                                 </td>
                                 <td>
                                    <a href="#" class="text-black">pwilliams@gmail.com</a> 
                                 </td>
                                 <td>
                                    <select class="custom-select select-min-width">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                                 </td>
                                 <td>4444</td>
                                 <td>Sales rep</td>
                                 <td class="blue-text">245</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>
                                     <a href="#" class="btn btn-view">Open Profile</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#small-d">Deactivate</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#small-b" >Export  </a>
                                  
                                 </td>
                              </tr>

                                <tr>
                                 <td>
                                   Peter Williams
                                 </td>
                                 <td>
                                    <span class="tag tag-blue">Active</span>
                                   
                                 </td>
                                 <td>
                                    <a href="#" class="text-black">pwilliams@gmail.com</a> 
                                 </td>
                                 <td>
                                    <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                                 </td>
                                 <td>4444</td>
                                 <td>Sales rep</td>
                                 <td class="blue-text">245</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>$12345.00</td>
                                 <td>
                                    <a href="#" class="btn btn-view">Open Profile</a>
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#small-d">Deactivate</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#small-b" >Export  </a>
                                  
                                 </td>
                              </tr>
                              
                              
                             
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
        
        
      </div>
   </section>
</div>

</div>
</div>
<!-- Modal -->


<div class="modal modal-new md-sm fade" id="small-b" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Block User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Do you want to block the user?</h2>
            <p>Sed ut perspiciatis unde omnis iste natus error</p>
            <div class="text-right mt-4">
               <button type="submit" class="btn border-btn">Cancel</button>
               <button type="submit" class="btn blue-btn">Yes</button>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal modal-new md-sm fade" id="small-d" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Edit User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Are you sure?</h2>
            <p>Do you want to edit the user details?</p>
            <div class="text-right mt-4">
               <button type="submit" class="btn border-btn">Cancel</button>
               <button type="submit" class="btn blue-btn">Yes</button>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection
@section('script')
<script src="{{asset('admin/modules/jquery.min.js')}}"></script>
<script src="{{asset('admin/modules/popper.js')}}"></script>
<script src="{{asset('admin/modules/tooltip.js')}}"></script>
<script src="{{asset('admin/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('admin/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('admin/js/sa-functions.js')}}"></script>
<script src="{{asset('admin/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('admin/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('admin/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('admin/modules/modules-datatables.js')}}"></script>
<script src="{{asset('admin/modules/chart.min.js')}}"></script>
<script src="{{asset('admin/modules/summernote/summernote-lite.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<script src="{{asset('admin/js/scripts.js')}}"></script>
<script src="{{asset('admin/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
       $('#example').DataTable({
           language: {
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
        $('#example2').DataTable({
           language: {
            
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   

      $("#datepicker").datepicker({ 
            autoclose: true, 
            todayHighlight: true
      }).datepicker('update', new Date());
    
  
   });
</script>
@endsection