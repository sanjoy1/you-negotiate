@extends('layout.admin.app')
@section('admin-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Report</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Generate Reports</h4>
                  </div>
                  <div class="card-body">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-row">
                              <div class="form-group col-md-10">
                                 <label>Select Creditor</label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                              </div>

                              <div class="form-group col-md-2">
                                 <label>&nbsp</label><br>
                                <button type="submit" class="btn blue-btn w-100">Generate Report</button>
                              </div>
                             
                           </div>
                          
                         
                         
                          
                        </div>
                    
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Report</h4>
                     <a href="#" class="btn btn-black">Download Report</a>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-200" id="example2">
                           <thead>
                              <tr>
                                 <th>creditor name</th>
                                 <th>channel name</th>
                                 <th>Invoice Date</th>
                                 <th>Invoice number</th>
                                 <th>total</th>
                                 <th>Monthly licensing fee</th>
                                 <th>Annual licensing fee</th>
                                 <th>YN  % billed</th>
                                 <th>sMS bill</th>
                                 <th>email bill</th>
                                 <th>LETTER bill</th>
                                 <th>pdf bill</th>
                                 <th>Total Consumer payments</th>
                                 <th>YN Merchant Fees</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">Happy client</a>
                                 </td>
                                 <td>Goodfellas</td>
                                 <td>2022-04-12</td>
                                 <td>121234235</td>
                                <td>$170.00</td>
                                <td>$4</td>
                                <td>$4</td>
                                <td>5%</td>
                                <td>$4</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1400.00</td>
                                


                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">Happy client</a>
                                 </td>
                                 <td>Goodfellas</td>
                                 <td>2022-04-12</td>
                                 <td>121234235</td>
                                <td>$170.00</td>
                                <td>$4</td>
                                <td>$4</td>
                                <td>5%</td>
                                <td>$4</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1400.00</td>
                                


                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">Happy client</a>
                                 </td>
                                 <td>Goodfellas</td>
                                 <td>2022-04-12</td>
                                 <td>121234235</td>
                                <td>$170.00</td>
                                <td>$4</td>
                                <td>$4</td>
                                <td>5%</td>
                                <td>$4</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1400.00</td>
                                


                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">Happy client</a>
                                 </td>
                                 <td>Goodfellas</td>
                                 <td>2022-04-12</td>
                                 <td>121234235</td>
                                <td>$170.00</td>
                                <td>$4</td>
                                <td>$4</td>
                                <td>5%</td>
                                <td>$4</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1400.00</td>
                                


                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">Happy client</a>
                                 </td>
                                 <td>Goodfellas</td>
                                 <td>2022-04-12</td>
                                 <td>121234235</td>
                                <td>$170.00</td>
                                <td>$4</td>
                                <td>$4</td>
                                <td>5%</td>
                                <td>$4</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1400.00</td>
                                


                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">Happy client</a>
                                 </td>
                                 <td>Goodfellas</td>
                                 <td>2022-04-12</td>
                                 <td>121234235</td>
                                <td>$170.00</td>
                                <td>$4</td>
                                <td>$4</td>
                                <td>5%</td>
                                <td>$4</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1400.00</td>
                                


                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">Happy client</a>
                                 </td>
                                 <td>Goodfellas</td>
                                 <td>2022-04-12</td>
                                 <td>121234235</td>
                                <td>$170.00</td>
                                <td>$4</td>
                                <td>$4</td>
                                <td>5%</td>
                                <td>$4</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1400.00</td>
                                


                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">Happy client</a>
                                 </td>
                                 <td>Goodfellas</td>
                                 <td>2022-04-12</td>
                                 <td>121234235</td>
                                <td>$170.00</td>
                                <td>$4</td>
                                <td>$4</td>
                                <td>5%</td>
                                <td>$4</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1400.00</td>
                                


                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">Happy client</a>
                                 </td>
                                 <td>Goodfellas</td>
                                 <td>2022-04-12</td>
                                 <td>121234235</td>
                                <td>$170.00</td>
                                <td>$4</td>
                                <td>$4</td>
                                <td>5%</td>
                                <td>$4</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1400.00</td>
                                


                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">Happy client</a>
                                 </td>
                                 <td>Goodfellas</td>
                                 <td>2022-04-12</td>
                                 <td>121234235</td>
                                <td>$170.00</td>
                                <td>$4</td>
                                <td>$4</td>
                                <td>5%</td>
                                <td>$4</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1400.00</td>
                                


                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">Happy client</a>
                                 </td>
                                 <td>Goodfellas</td>
                                 <td>2022-04-12</td>
                                 <td>121234235</td>
                                <td>$170.00</td>
                                <td>$4</td>
                                <td>$4</td>
                                <td>5%</td>
                                <td>$4</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1400.00</td>
                                


                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">Happy client</a>
                                 </td>
                                 <td>Goodfellas</td>
                                 <td>2022-04-12</td>
                                 <td>121234235</td>
                                <td>$170.00</td>
                                <td>$4</td>
                                <td>$4</td>
                                <td>5%</td>
                                <td>$4</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1400.00</td>
                                


                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">Happy client</a>
                                 </td>
                                 <td>Goodfellas</td>
                                 <td>2022-04-12</td>
                                 <td>121234235</td>
                                <td>$170.00</td>
                                <td>$4</td>
                                <td>$4</td>
                                <td>5%</td>
                                <td>$4</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1400.00</td>
                                


                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">Happy client</a>
                                 </td>
                                 <td>Goodfellas</td>
                                 <td>2022-04-12</td>
                                 <td>121234235</td>
                                <td>$170.00</td>
                                <td>$4</td>
                                <td>$4</td>
                                <td>5%</td>
                                <td>$4</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1400.00</td>
                                


                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">Happy client</a>
                                 </td>
                                 <td>Goodfellas</td>
                                 <td>2022-04-12</td>
                                 <td>121234235</td>
                                <td>$170.00</td>
                                <td>$4</td>
                                <td>$4</td>
                                <td>5%</td>
                                <td>$4</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1400.00</td>
                                


                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">Happy client</a>
                                 </td>
                                 <td>Goodfellas</td>
                                 <td>2022-04-12</td>
                                 <td>121234235</td>
                                <td>$170.00</td>
                                <td>$4</td>
                                <td>$4</td>
                                <td>5%</td>
                                <td>$4</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1200</td>
                                <td>$1400.00</td>
                                


                              </tr>

                              
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </section>
   </div>
  
</div>
</div>
<!-- Modal -->

@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>

   $(document).ready(function () {

       $('#example2').DataTable({
          language: {
            
              'paginate': {
                  'previous': '<span class="fa fa-angle-left"></span>',
                  'next': '<span class="fa fa-angle-right"></span>'
              }
              
          }
      });
   
   });
</script>
@endsection