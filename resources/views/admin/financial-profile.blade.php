@extends('layout.admin.app')
@section('admin-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Profile</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                        <div class="card-header flex-hd">
                           <ul class="step-list">
                            <li class="select active"><a href="profile"><i class="nav-icon text-14b i-Yes"></i> Company Profile</a></li>
                            <li><i class="fa fa-angles-right"></i></li>
                            <li class="select active"><a href="bank-details"><i class="nav-icon text-14b i-Yes"></i> Bank Details</a></li>
                            <li><i class="fa fa-angles-right"></i></li>
                            <li class="select"><a href="financial-profile"><i class="nav-icon text-14b i-Yes"></i> Financial Profile</a></li>
                            <li><i class="fa fa-angles-right"></i></li>
                            <li><a href="paymentsetup"><i class="nav-icon text-14b i-Yes"></i> Paymentsetup</a></li>
                            <li><i class="fa fa-angles-right"></i></li>
                            <li><a href="my-logo"><i class="nav-icon text-14b i-Yes"></i> Logo</a></li>
                            <li><i class="fa fa-angles-right"></i></li>
                            <li><a href="contact-profile"><i class="nav-icon text-14b i-Yes"></i> Contact</a></li>
                            <li><a href="contact-profile"><i class="nav-icon text-14b i-Yes"></i> Contact</a></li>


                           </ul>
                           <div>
                            <a href="bank-details" class="btn btn-black">Back</a>
                            <a href="paymentsetup" class="btn btn-primary">Next</a>
                           
                           </div>
                        </div>
                        <hr>
                        <div class="card-body form-area">
                            <h3 class="mb-4">Financial Profile</h3>
                                <div class="form-row">
                                            <div class="form-group col-md-6">
                                            <label>% of YN revenue </label>
                                            <input type="text" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6">
                                            <label>Creditor Pricing Plans</label>
                                            <div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c1" name="c1" class="custom-control-input">
                                       <label class="custom-control-label" for="c1">Default to YN plans</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c2" name="c2" class="custom-control-input">
                                       <label class="custom-control-label" for="c2">Customize plan</label>
                                    </div>
                                   
                                 </div>
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="form-group col-md-6">
                                            <label>Frequency</label>
                                            <select class="custom-select">
                                                    <option selected="">Select</option>
                                                    <option value="1">US</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                            <label>% payments</label>
                                            <input type="text" class="form-control">
                                            </div>
                                </div>
                              
                                <div class="form-row">
                                            <div class="form-group col-md-6">
                                            <label>Text Rate</label>
                                             <input type="text" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6">
                                            <label>Email Rate</label>
                                            <input type="text" class="form-control">
                                            </div>
                                </div>
                                
                                <div class="form-row">
                                            <div class="form-group col-md-6">
                                            <label>eLetter</label>
                                             <input type="text" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6">
                                            <label>PDF</label>
                                            <input type="text" class="form-control">
                                            </div>
                                </div>
                              

                                <div class="text-right">
                             <a href="bank-details" class="btn btn-black">Back</a>
                            <a href="paymentsetup" class="btn btn-primary">Next</a>
                           
                           </div>


                        </div>
                     </div>
            </div>
         </div>
        
   </section>
   </div>
  
</div>
</div>

@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $('#summernote').summernote({
      placeholder: 'Hello stand alone ui',
      tabsize: 2,
      height: 100,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
      ]
    });
   $(document).ready(function () {

       $('#example2').DataTable({
          language: {
            
              'paginate': {
                  'previous': '<span class="fa fa-angle-left"></span>',
                  'next': '<span class="fa fa-angle-right"></span>'
              }
              
          }
      });
   
   });
</script>
@endsection