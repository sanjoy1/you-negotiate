@extends('layout.admin.app')
@section('admin-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Creditor PRofile</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd border-btm">
                     <h4>Basic Information</h4>
                     <div>
                        <a href="#" class="btn btn-navy">Counter</a>
                     <a href="#" class="btn btn-yellow">Ask My Client</a>
                     <a href="#" class="btn btn-black">Contact Customer</a>
                     <a href="#" class="btn btn-deactive">Decline</a>
                     <a href="#" class="btn btn-green">Accept</a>
                    </div>
                  </div>
                  <div class="card-body">
                     <div class="ac-profile mb-4">
                        <h4>Account Profile</h4>

                        <div class="row mt-4">
                            <div class="col-sm-12 col-md-6">
                                <div class="lbl-text">Owner’s Name</div>
                                <h5>XD Finance</h5>
                            </div>
                             <div class="col-sm-12 col-md-6">
                                <div class="lbl-text">Status</div>
                                <div class="d-flex">
                                    <span class="tag tag-blue">Active</span>
                                    <span class="tag tag-green">Approved</span>
                                 </div>
                            </div>
                        </div>

                     </div>

                     <div class="flex-prime mb-4">
                        <a href="#" class="common-bx">
                        <div><img alt="user" src="{{asset('admin/img/users.svg')}}"></div>
                        <p>Total Accounts</p>
                        <h4>1240</h4>
                        </a>
                        <a href="#" class="common-bx">
                        <div><img alt="user" src="{{asset('admin/img/users.svg')}}"></div>
                        <p>$balance value</p>
                        <h4>$12340.00</h4>
                        </a>
                        <a href="#" class="common-bx">
                        <div><img alt="user" src="{{asset('admin/img/users.svg')}}"></div>
                        <p>Last payment amount</p>
                        <h4>$12340.00</h4>
                        </a>
                        <a href="#" class="common-bx">
                        <div><img alt="user" src="{{asset('admin/img/users.svg')}}"></div>
                        <p>Payments made to date</p>
                        <h4>$12340.00</h4>
                        </a>
                        <a href="#" class="common-bx">
                        <div><img alt="user" src="{{asset('admin/img/users.svg')}}"></div>
                        <p>Total YN revenue to date</p>
                        <h4>$12340.00</h4>
                        </a>
                             
                        
                     </div>
                     <div class="ac-profile mb-4">
                        <h4>Fees Structure</h4>

                        <div class="row mt-4">
                            <div class="col-sm-12 col-md-4 col-lg-2">
                                <div class="lbl-text">Share Type </div>
                                <h5><strong>Monthly</strong></h5>
                            </div>
                             <div class="col-sm-12 col-md-4 col-lg-2">
                                <div class="lbl-text">Value 10 %</div>
                                <h5><strong>2%</strong></h5>
                            </div>
                        </div>

                         <div class="radius-border">
                        <table class="table tb-border ">
                           <thead>
                              <tr>
                                 <th>Type</th>
                                 <th>Rate</th>
                                
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>SMS</td>
                                 <td>$70.00</td>
                               
                              </tr>
                               <tr>
                                 <td>Txt</td>
                                 <td>$70.00</td>
                               
                              </tr>

                                <tr>
                                 <td>eLetter </td>
                                 <td>$70.00</td>
                               
                              </tr>
                              
                             <tr>
                                 <td>PDF </td>
                                 <td>$70.00</td>
                               
                              </tr>
                           </tbody>
                        </table>
                     </div>

                     </div>


                  </div>

                  <div class="card-ft-end">
                    
                     <div>
                        <a href="#" class="btn btn-navy">Counter</a>
                     <a href="#" class="btn btn-yellow">Ask My Client</a>
                     <a href="#" class="btn btn-black">Contact Customer</a>
                     <a href="#" class="btn btn-deactive">Decline</a>
                     <a href="#" class="btn btn-green">Accept</a>
                    </div>
</div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Current Customers</h4>
                     
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-150" id="example">
                           <thead>
                              <tr>
                                 <th>account</th>
                                 <th>first name</th>
                                 <th>last name</th>
                                 <th>last 4 ssn</th>
                                 <th>email</th>
                                 <th>mobile</th>
                                 <th>invitATION LINK</th>
                                 <th>Status</th>
                                 <th>Action</th>
                                
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">32435666661</a>
                                 </td>
                                 <td>
                                    Edward
                                 </td>
                                 <td>Elric</td>
                                <td>2455</td>
                                <td>sunny11106@yopmail.com</td>      
                                <td>9787456113</td>  
                                <td><a href="#" class="blue-text">https://yng.link/TESTod6045Rl</a></td>  
                                <td>Joined</td>  
                                
                                 <td>
                                    <a href="offer-details" class="btn btn-view">Profile</a> 
                                    <a href="#" class="btn btn-green">Subscribe</a>
                                    
                                    <a href="#" class="btn btn-yellow">Cancel All Payment</a>
                                  
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>


                            <tr>
                                 <td>
                                    <a href="#" class="text-black">32435666661</a>
                                 </td>
                                 <td>
                                    Edward
                                 </td>
                                 <td>Elric</td>
                                <td>2455</td>
                                <td>sunny11106@yopmail.com</td>      
                                <td>9787456113</td>  
                                <td><a href="#" class="blue-text">https://yng.link/TESTod6045Rl</a></td>  
                                <td>Joined</td>  
                                
                                 <td>
                                    <a href="offer-details" class="btn btn-view">Profile</a> 
                                    <a href="#" class="btn btn-green">Subscribe</a>
                                    
                                    <a href="#" class="btn btn-yellow">Cancel All Payment</a>
                                  
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">32435666661</a>
                                 </td>
                                 <td>
                                    Edward
                                 </td>
                                 <td>Elric</td>
                                <td>2455</td>
                                <td>sunny11106@yopmail.com</td>      
                                <td>9787456113</td>  
                                <td><a href="#" class="blue-text">https://yng.link/TESTod6045Rl</a></td>  
                                <td>Joined</td>  
                                
                                 <td>
                                    <a href="offer-details" class="btn btn-view">Profile</a> 
                                    <a href="#" class="btn btn-green">Subscribe</a>
                                    
                                    <a href="#" class="btn btn-yellow">Cancel All Payment</a>
                                  
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">32435666661</a>
                                 </td>
                                 <td>
                                    Edward
                                 </td>
                                 <td>Elric</td>
                                <td>2455</td>
                                <td>sunny11106@yopmail.com</td>      
                                <td>9787456113</td>  
                                <td><a href="#" class="blue-text">https://yng.link/TESTod6045Rl</a></td>  
                                <td>Joined</td>  
                                
                                 <td>
                                    <a href="offer-details" class="btn btn-view">Profile</a> 
                                    <a href="#" class="btn btn-green">Subscribe</a>
                                    
                                    <a href="#" class="btn btn-yellow">Cancel All Payment</a>
                                  
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">32435666661</a>
                                 </td>
                                 <td>
                                    Edward
                                 </td>
                                 <td>Elric</td>
                                <td>2455</td>
                                <td>sunny11106@yopmail.com</td>      
                                <td>9787456113</td>  
                                <td><a href="#" class="blue-text">https://yng.link/TESTod6045Rl</a></td>  
                                <td>Joined</td>  
                                
                                 <td>
                                    <a href="offer-details" class="btn btn-view">Profile</a> 
                                    <a href="#" class="btn btn-green">Subscribe</a>
                                    
                                    <a href="#" class="btn btn-yellow">Cancel All Payment</a>
                                  
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">32435666661</a>
                                 </td>
                                 <td>
                                    Edward
                                 </td>
                                 <td>Elric</td>
                                <td>2455</td>
                                <td>sunny11106@yopmail.com</td>      
                                <td>9787456113</td>  
                                <td><a href="#" class="blue-text">https://yng.link/TESTod6045Rl</a></td>  
                                <td>Joined</td>  
                                
                                 <td>
                                    <a href="offer-details" class="btn btn-view">Profile</a> 
                                    <a href="#" class="btn btn-green">Subscribe</a>
                                    
                                    <a href="#" class="btn btn-yellow">Cancel All Payment</a>
                                  
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">32435666661</a>
                                 </td>
                                 <td>
                                    Edward
                                 </td>
                                 <td>Elric</td>
                                <td>2455</td>
                                <td>sunny11106@yopmail.com</td>      
                                <td>9787456113</td>  
                                <td><a href="#" class="blue-text">https://yng.link/TESTod6045Rl</a></td>  
                                <td>Joined</td>  
                                
                                 <td>
                                    <a href="offer-details" class="btn btn-view">Profile</a> 
                                    <a href="#" class="btn btn-green">Subscribe</a>
                                    
                                    <a href="#" class="btn btn-yellow">Cancel All Payment</a>
                                  
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">32435666661</a>
                                 </td>
                                 <td>
                                    Edward
                                 </td>
                                 <td>Elric</td>
                                <td>2455</td>
                                <td>sunny11106@yopmail.com</td>      
                                <td>9787456113</td>  
                                <td><a href="#" class="blue-text">https://yng.link/TESTod6045Rl</a></td>  
                                <td>Joined</td>  
                                
                                 <td>
                                    <a href="offer-details" class="btn btn-view">Profile</a> 
                                    <a href="#" class="btn btn-green">Subscribe</a>
                                    
                                    <a href="#" class="btn btn-yellow">Cancel All Payment</a>
                                  
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">32435666661</a>
                                 </td>
                                 <td>
                                    Edward
                                 </td>
                                 <td>Elric</td>
                                <td>2455</td>
                                <td>sunny11106@yopmail.com</td>      
                                <td>9787456113</td>  
                                <td><a href="#" class="blue-text">https://yng.link/TESTod6045Rl</a></td>  
                                <td>Joined</td>  
                                
                                 <td>
                                    <a href="offer-details" class="btn btn-view">Profile</a> 
                                    <a href="#" class="btn btn-green">Subscribe</a>
                                    
                                    <a href="#" class="btn btn-yellow">Cancel All Payment</a>
                                  
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">32435666661</a>
                                 </td>
                                 <td>
                                    Edward
                                 </td>
                                 <td>Elric</td>
                                <td>2455</td>
                                <td>sunny11106@yopmail.com</td>      
                                <td>9787456113</td>  
                                <td><a href="#" class="blue-text">https://yng.link/TESTod6045Rl</a></td>  
                                <td>Joined</td>  
                                
                                 <td>
                                    <a href="offer-details" class="btn btn-view">Profile</a> 
                                    <a href="#" class="btn btn-green">Subscribe</a>
                                    
                                    <a href="#" class="btn btn-yellow">Cancel All Payment</a>
                                  
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">32435666661</a>
                                 </td>
                                 <td>
                                    Edward
                                 </td>
                                 <td>Elric</td>
                                <td>2455</td>
                                <td>sunny11106@yopmail.com</td>      
                                <td>9787456113</td>  
                                <td><a href="#" class="blue-text">https://yng.link/TESTod6045Rl</a></td>  
                                <td>Joined</td>  
                                
                                 <td>
                                    <a href="offer-details" class="btn btn-view">Profile</a> 
                                    <a href="#" class="btn btn-green">Subscribe</a>
                                    
                                    <a href="#" class="btn btn-yellow">Cancel All Payment</a>
                                  
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">32435666661</a>
                                 </td>
                                 <td>
                                    Edward
                                 </td>
                                 <td>Elric</td>
                                <td>2455</td>
                                <td>sunny11106@yopmail.com</td>      
                                <td>9787456113</td>  
                                <td><a href="#" class="blue-text">https://yng.link/TESTod6045Rl</a></td>  
                                <td>Joined</td>  
                                
                                 <td>
                                    <a href="offer-details" class="btn btn-view">Profile</a> 
                                    <a href="#" class="btn btn-green">Subscribe</a>
                                    
                                    <a href="#" class="btn btn-yellow">Cancel All Payment</a>
                                  
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">32435666661</a>
                                 </td>
                                 <td>
                                    Edward
                                 </td>
                                 <td>Elric</td>
                                <td>2455</td>
                                <td>sunny11106@yopmail.com</td>      
                                <td>9787456113</td>  
                                <td><a href="#" class="blue-text">https://yng.link/TESTod6045Rl</a></td>  
                                <td>Joined</td>  
                                
                                 <td>
                                    <a href="offer-details" class="btn btn-view">Profile</a> 
                                    <a href="#" class="btn btn-green">Subscribe</a>
                                    
                                    <a href="#" class="btn btn-yellow">Cancel All Payment</a>
                                  
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>


                             
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Creditor Payments </h4>
                 
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table" id="example2">
                           <thead>
                              <tr>
                                 <th>Date</th>
                                 <th>frequency</th>
                                  <th>SMS Rate</th>
                                   <th>TXT sms</th>
                                    <th>Eletter rate</th>
                                     <th>PDF Rate</th>
                                     <th>Total payment made</th>
                                     <th>Status</th>
                                 <th>Action</th>
                                
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">2222-06-08</a>
                                 </td>
                                 <td>Monthly</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$12400</td>
                                 <td><span class="tag tag-green">Approved</span></td>
                                 <td>
                                    <a href="#" class="btn btn-view">View</a> 
                                   <a href="#" class="btn btn-green">Download</a>
                                 
                                 </td>
                              </tr>


                             <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">2222-06-08</a>
                                 </td>
                                 <td>Monthly</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$12400</td>
                                 <td><span class="tag tag-green">Approved</span></td>
                                 <td>
                                    <a href="#" class="btn btn-view">View</a> 
                                   <a href="#" class="btn btn-green">Download</a>
                                 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">2222-06-08</a>
                                 </td>
                                 <td>Monthly</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$12400</td>
                                 <td><span class="tag tag-green">Approved</span></td>
                                 <td>
                                    <a href="#" class="btn btn-view">View</a> 
                                   <a href="#" class="btn btn-green">Download</a>
                                 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">2222-06-08</a>
                                 </td>
                                 <td>Monthly</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$12400</td>
                                 <td><span class="tag tag-green">Approved</span></td>
                                 <td>
                                    <a href="#" class="btn btn-view">View</a> 
                                   <a href="#" class="btn btn-green">Download</a>
                                 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">2222-06-08</a>
                                 </td>
                                 <td>Monthly</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$12400</td>
                                 <td><span class="tag tag-green">Approved</span></td>
                                 <td>
                                    <a href="#" class="btn btn-view">View</a> 
                                   <a href="#" class="btn btn-green">Download</a>
                                 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">2222-06-08</a>
                                 </td>
                                 <td>Monthly</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$12400</td>
                                 <td><span class="tag tag-green">Approved</span></td>
                                 <td>
                                    <a href="#" class="btn btn-view">View</a> 
                                   <a href="#" class="btn btn-green">Download</a>
                                 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">2222-06-08</a>
                                 </td>
                                 <td>Monthly</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$12400</td>
                                 <td><span class="tag tag-green">Approved</span></td>
                                 <td>
                                    <a href="#" class="btn btn-view">View</a> 
                                   <a href="#" class="btn btn-green">Download</a>
                                 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">2222-06-08</a>
                                 </td>
                                 <td>Monthly</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$12400</td>
                                 <td><span class="tag tag-green">Approved</span></td>
                                 <td>
                                    <a href="#" class="btn btn-view">View</a> 
                                   <a href="#" class="btn btn-green">Download</a>
                                 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">2222-06-08</a>
                                 </td>
                                 <td>Monthly</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$12400</td>
                                 <td><span class="tag tag-green">Approved</span></td>
                                 <td>
                                    <a href="#" class="btn btn-view">View</a> 
                                   <a href="#" class="btn btn-green">Download</a>
                                 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">2222-06-08</a>
                                 </td>
                                 <td>Monthly</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$12400</td>
                                 <td><span class="tag tag-green">Approved</span></td>
                                 <td>
                                    <a href="#" class="btn btn-view">View</a> 
                                   <a href="#" class="btn btn-green">Download</a>
                                 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">2222-06-08</a>
                                 </td>
                                 <td>Monthly</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$2</td>
                                <td>$12400</td>
                                 <td><span class="tag tag-green">Approved</span></td>
                                 <td>
                                    <a href="#" class="btn btn-view">View</a> 
                                   <a href="#" class="btn btn-green">Download</a>
                                 
                                 </td>
                              </tr>
                              
                              
                              
                             
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
        
        
      </div>
   </section>
</div>

</div>
</div>
<!-- Modal -->
<div class="modal modal-new fade" id="configure">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Configure</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <form>
              <div class="form-row">
                  <div class="form-group col-md-6">
                     <label>Contract Date</label>
                     <div id="datepicker" class="input-group date" data-date-format="mm-dd-yyyy">
                        <input class="form-control" type="text" readonly />
                        <span class="input-group-addon"><img alt="image" src="{{asset('admin/img/calendar.svg')}}" /></span>
                     </div>
                  </div>
                  <div class="form-group col-md-6">
                     <label>SMS Rate <i class="icon-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="At vero eos et">!</i></label>
                     <input type="text" class="form-control" placeholder="">
                  </div>
               </div>

               <div class="form-row">
                  <div class="form-group col-md-6">
                     <label>Email Rate</label>
                    <input type="text" class="form-control" placeholder="Select...">
                  </div>
                  <div class="form-group col-md-6">
                     <label>E-Letter Rate</label>
                     <input type="text" class="form-control" placeholder="">
                  </div>
               </div>

               <div class="form-row">
                   <div class="form-group col-md-6">
                     <label>H2H Share(%) <i class="icon-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="At vero eos et">!</i></label>
                     <input type="text" class="form-control" placeholder="">
                  </div>
                  <div class="form-group col-md-6">
                     <label>Billing Schedule</label>
                     <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                  </div>
               </div>

               <div class="form-row">
                   <div class="form-group col-md-6">
                     <label>Licence Fees</label>
                     <input type="text" class="form-control" placeholder="">
                  </div>
                 
               </div>



               <div class="text-right">
                  <a href="#" class="btn border-btn">Reject</a>
                  <button type="submit" class="btn blue-btn">Approve</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<div class="modal modal-new fade" id="set-commission">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Master Negotiation Terms</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <form>
              
                  <div class="form-group">
                     <label>Pay in Full Balance Discount % <sup>*</sup> <i class="icon-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="At vero eos et">!</i></label>
                    <input type="text" class="form-control" placeholder="">
                  </div>
                 

                <div class="form-group">
                     <label>Payment Plan Balance Discount % <sup>*</sup> <i class="icon-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="At vero eos et">!</i></label>
                     <input type="text" class="form-control" placeholder="">
                  </div>

                  <div class="form-group">
                     <label>Minimum Monthly Payment % of Payment Plan Balance <sup>*</sup> <i class="icon-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="At vero eos et">!</i></label>
                      <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                  </div>

                    <div class="form-group">
                     <label>Maximum Days to Receive 1st Payment Plan Payment  <sup>*</sup> <i class="icon-info" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="At vero eos et">!</i></label>
                      <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                  </div>

              




               <div class="text-right">
                  <a href="#" class="btn border-btn">Close</a>
                  <button type="submit" class="btn blue-btn">Save</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>

<div class="modal modal-new fade" id="set-d">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Set Commission</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <form>
              
                
<div class="form-group">
                     <label>Name </label>
                      <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                  </div>
                  <div class="form-group">
                     <label>H2H User Type</label>
                      <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                  </div>

                    <div class="form-group">
                     <label>Commission on H2H Share</label>
                      <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                  </div>

              




               <div class="text-right">
                  <a href="#" class="btn border-btn">Close</a>
                  <button type="submit" class="btn blue-btn">Save</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>


<div class="modal modal-new md-sm fade" id="small-modal" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Delete Creditor</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Do You Want to Delete ?</h2>
            <p>You sure you want to delete this creditor</p>
            <div class="text-right mt-4">
               <button type="submit" class="btn border-btn">Cancel</button>
               <button type="submit" class="btn blue-btn">Yes</button>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection
@section('script')
<script src="{{asset('admin/modules/jquery.min.js')}}"></script>
<script src="{{asset('admin/modules/popper.js')}}"></script>
<script src="{{asset('admin/modules/tooltip.js')}}"></script>
<script src="{{asset('admin/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('admin/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('admin/js/sa-functions.js')}}"></script>
<script src="{{asset('admin/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('admin/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('admin/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('admin/modules/modules-datatables.js')}}"></script>
<script src="{{asset('admin/modules/chart.min.js')}}"></script>
<script src="{{asset('admin/modules/summernote/summernote-lite.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<script src="{{asset('admin/js/scripts.js')}}"></script>
<script src="{{asset('admin/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
       $('#example').DataTable({
           language: {
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
        $('#example2').DataTable({
           language: {
            
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   

      $("#datepicker").datepicker({ 
            autoclose: true, 
            todayHighlight: true
      }).datepicker('update', new Date());
    
  
   });
</script>
@endsection