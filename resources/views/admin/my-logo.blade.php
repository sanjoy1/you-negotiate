@extends('layout.admin.app')
@section('admin-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Profile</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                        <div class="card-header flex-hd">
                           <ul class="step-list">
                            <li class="select active"><a href="profile"><i class="nav-icon text-14b i-Yes"></i> Company Profile</a></li>
                            <li><i class="fa fa-angles-right"></i></li>
                            <li class="select active"><a href="bank-details"><i class="nav-icon text-14b i-Yes"></i> Bank Details</a></li>
                            <li><i class="fa fa-angles-right"></i></li>
                            <li class="select active"><a href="financial-profile"><i class="nav-icon text-14b i-Yes"></i> Financial Profile</a></li>
                            <li><i class="fa fa-angles-right"></i></li>
                            <li class="select active"><a href="paymentsetup"><i class="nav-icon text-14b i-Yes"></i> Paymentsetup</a></li>
                            <li><i class="fa fa-angles-right"></i></li>
                            <li class="select"><a href="my-logo"><i class="nav-icon text-14b i-Yes"></i> Logo</a></li>
                            <li><i class="fa fa-angles-right"></i></li>
                            <li><a href="contact-profile"><i class="nav-icon text-14b i-Yes"></i> Contact</a></li>


                           </ul>
                           <div>
                            <a href="paymentsetup" class="btn btn-black">Back</a>
                            <a href="contact-profile" class="btn btn-primary">Next</a>
                           
                           </div>
                        </div>
                        <hr>
                        <div class="card-body form-area">
                            <h3 class="">Set Up Master Account Merchant Processor</h3>
                            <div class="mb-5">
                                <p class="sm-text">Please select the color that matches your website theme or where you are planing to use it</p>
</div>
                               
                              <div class="row">
                        <div class="col-sm-12 col-md-8 col-lg-8 right-padding-80">
                          <div class="row color-picker">
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>Select Primary Color <sup>*</sup></label>
                                    <img alt="image" src="http://127.0.0.1:8000/dist/img/picker.svg" class="w-100">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>Select Secondary Color</label>
                                    <img alt="image" src="http://127.0.0.1:8000/dist/img/picker2.svg" class="w-100">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>Select Background Color</label>
                                    <img alt="image" src="http://127.0.0.1:8000/dist/img/picker3.svg" class="w-100">
                                </div>
                            </div>
                          </div>
                          <div class="mb-4">
                           <button type="submit" class="btn btn-reset">Reset Default</button>
                           <button type="submit" class="btn blue-btn">Preview</button>
                           <button type="submit" class="btn save-disable">Save</button>
                        </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 photo-preview">
                          
                          <h5>Preview</h5>
                          <div class="logo-preview-box height-200">
                            <img alt="image" src="http://127.0.0.1:8000/dist/img/YouNegotiate.png" class="img-fluid">
                          </div>
                        </div>
                     </div>

                     <div class="row mt-2">
                        <div class="col-sm-12 col-md-6 col-lg-6 link-form">
                          <h4 class="mb-2">Your Links</h4>
                          <p class="sm-text">Manage your link that will help your customer to connect with our portal</p>

                          <div class="form-group mt-2">
                           <label>Customer Communications</label>
                           <div class="input-group mb-2">
                              <div class="input-group-prepend">
                                 <div class="input-group-text">https://</div>
                              </div>
                              <input type="text" class="form-control" placeholder="happycompany">
                              <div class="input-group-prepend">
                                 <div class="input-group-text radius-l">younegotiate.com</div>
                              </div>
                           </div>
                          </div>

                          <div class="form-group mt-4">
                           <label>Embed Code for Your Website</label>

                              <textarea class="form-control h-auto" rows="3" value="http://www.acidfanatic.com/index.php?section=loops"></textarea>
                              
                          </div>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-4 offset-lg-2 photo-preview">
                           <h5>QR Code</h5>
                          <div class="logo-preview-box height-200">
                            <img alt="image" src="http://127.0.0.1:8000/dist/img/qr-code.png" class="img-fluid">
                          </div>
                        </div>
                     </div>


                                <div class="text-right">
                             <a href="paymentsetup" class="btn btn-black">Back</a>
                            <a href="contact-profile" class="btn btn-primary">Next</a>
                           
                           </div>

                        </div>
                     </div>
            </div>
         </div>
        
   </section>
   </div>
  
</div>
</div>


@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $('#summernote').summernote({
      placeholder: 'Hello stand alone ui',
      tabsize: 2,
      height: 100,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
      ]
    });
   $(document).ready(function () {

       $('#example2').DataTable({
          language: {
            
              'paginate': {
                  'previous': '<span class="fa fa-angle-left"></span>',
                  'next': '<span class="fa fa-angle-right"></span>'
              }
              
          }
      });
   
   });
</script>
@endsection