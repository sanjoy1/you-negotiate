@extends('layout.admin.app')
@section('admin-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Consumer profile</div>
      </h1>
      <div class="tab-section">
         <ul class="list-tab">
            <li><a href="basic-info" >Basic Information</a></li>
            <li><a href="pay-history" class="active">Payment History</a></li>
         </ul>
         <div>
            <a href="#" class="btn btn-navy">Counter</a>
            <a href="#" class="btn btn-yellow">Ask My Client</a>
            <a href="#" class="btn btn-black">Contact Customer</a>
            <a href="#" class="btn btn-deactive">Decline</a>
            <a href="#" class="btn btn-green">Accept</a>
         </div>
      </div>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Transaction History</h4>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <div class="table-responsive">
                           <table class="table" id="example2">
                              <thead>
                                 <tr>
                                    <th>date & time</th>
                                    <th>consumer name</th>
                                    <th>Transaction ID</th>
                                    <th>Payment method</th>
                                    <th>Transaction amount</th>
                                    <th>failed reason</th>
                                    <th>status</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>Edward Elric</td>
                                    <td>13245</td>
                                    <td>Amazon Pay</td>
                                    <td>$10.00</td>
                                    <td>Bad interent</td>
                                    <td><span class="red-text">Failed</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>Edward Elric</td>
                                    <td>13245</td>
                                    <td>Amazon Pay</td>
                                    <td>$10.00</td>
                                    <td>Bad interent</td>
                                    <td><span class="green-text">Failed</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>Edward Elric</td>
                                    <td>13245</td>
                                    <td>Amazon Pay</td>
                                    <td>$10.00</td>
                                    <td>Bad interent</td>
                                    <td><span class="green-text">Failed</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>Edward Elric</td>
                                    <td>13245</td>
                                    <td>Amazon Pay</td>
                                    <td>$10.00</td>
                                    <td>Bad interent</td>
                                    <td><span class="green-text">Failed</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>Edward Elric</td>
                                    <td>13245</td>
                                    <td>Amazon Pay</td>
                                    <td>$10.00</td>
                                    <td>Bad interent</td>
                                    <td><span class="green-text">Failed</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>Edward Elric</td>
                                    <td>13245</td>
                                    <td>Amazon Pay</td>
                                    <td>$10.00</td>
                                    <td>Bad interent</td>
                                    <td><span class="green-text">Failed</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>Edward Elric</td>
                                    <td>13245</td>
                                    <td>Amazon Pay</td>
                                    <td>$10.00</td>
                                    <td>Bad interent</td>
                                    <td><span class="green-text">Failed</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>Edward Elric</td>
                                    <td>13245</td>
                                    <td>Amazon Pay</td>
                                    <td>$10.00</td>
                                    <td>Bad interent</td>
                                    <td><span class="red-text">Failed</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>Edward Elric</td>
                                    <td>13245</td>
                                    <td>Amazon Pay</td>
                                    <td>$10.00</td>
                                    <td>Bad interent</td>
                                    <td><span class="red-text">Failed</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>Edward Elric</td>
                                    <td>13245</td>
                                    <td>Amazon Pay</td>
                                    <td>$10.00</td>
                                    <td>Bad interent</td>
                                    <td><span class="red-text">Failed</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>Edward Elric</td>
                                    <td>13245</td>
                                    <td>Amazon Pay</td>
                                    <td>$10.00</td>
                                    <td>Bad interent</td>
                                    <td><span class="red-text">Failed</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>Edward Elric</td>
                                    <td>13245</td>
                                    <td>Amazon Pay</td>
                                    <td>$10.00</td>
                                    <td>Bad interent</td>
                                    <td><span class="red-text">Failed</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>Edward Elric</td>
                                    <td>13245</td>
                                    <td>Amazon Pay</td>
                                    <td>$10.00</td>
                                    <td>Bad interent</td>
                                    <td><span class="red-text">Failed</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>Edward Elric</td>
                                    <td>13245</td>
                                    <td>Amazon Pay</td>
                                    <td>$10.00</td>
                                    <td>Bad interent</td>
                                    <td><span class="red-text">Failed</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>Edward Elric</td>
                                    <td>13245</td>
                                    <td>Amazon Pay</td>
                                    <td>$10.00</td>
                                    <td>Bad interent</td>
                                    <td><span class="red-text">Failed</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>Edward Elric</td>
                                    <td>13245</td>
                                    <td>Amazon Pay</td>
                                    <td>$10.00</td>
                                    <td>Bad interent</td>
                                    <td><span class="red-text">Failed</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>Edward Elric</td>
                                    <td>13245</td>
                                    <td>Amazon Pay</td>
                                    <td>$10.00</td>
                                    <td>Bad interent</td>
                                    <td><span class="red-text">Failed</span></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Scheduled Payment Details</h4>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <div class="table-responsive">
                           <table class="table" id="example1">
                              <thead>
                                 <tr>
                                    <th>date & time</th>
                                    <th>Transaction amount</th>
                                    <th>Card/cah</th>
                                    <th>status</th>
                                    <th>action</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="orange-text">Upcoming</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="orange-text">Upcoming</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="orange-text">Upcoming</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="orange-text">Upcoming</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="orange-text">Upcoming</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="orange-text">Upcoming</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="orange-text">Upcoming</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="orange-text">Upcoming</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="orange-text">Upcoming</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="orange-text">Upcoming</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="orange-text">Upcoming</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="orange-text">Upcoming</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="orange-text">Upcoming</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="orange-text">Upcoming</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="orange-text">Upcoming</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="orange-text">Upcoming</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="orange-text">Upcoming</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="orange-text">Upcoming</span></td>
                                    <td>-</td>
                                 </tr>
                                 
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Scheduled Cancelled Payment Details</h4>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <div class="table-responsive">
                           <table class="table" id="example3">
                              <thead>
                                 <tr>
                                    <th>date & time</th>
                                    <th>Transaction amount</th>
                                    <th>Card/cah</th>
                                    <th>status</th>
                                    <th>action</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr><tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       2222-06-08 06:01
                                    </td>
                                    <td>$10.00</td>
                                    <td>Payment Savvy</td>
                                    <td><span class="red-text">Cancelled</span></td>
                                    <td>-</td>
                                 </tr>
                                
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Communication History</h4>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <div class="table-responsive">
                           <table class="table" id="example4">
                              <thead>
                                 <tr>
                                    <th>sent at</th>
                                    <th>email id</th>
                                    <th>phone number</th>
                                    <th>sms segment</th>
                                    <th>group</th>
                                    <th>template</th>
                                    <th>template type</th>
                                    <th>Action</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 
                                 <tr>
                                    <td>
                                      2222-06-08 06:01
                                    </td>
                                    <td>hello@younegotiate.com</td>
                                    <td>84895685690</td>
                                    <td>-</td>
                                    <td>Alpha</td>
                                    <td>New Template</td>
                                    <td>Email</td>
                                    <td><a href="#" class="btn btn-green">View</a></td>
                                 </tr>
                                  <tr>
                                    <td>
                                      2222-06-08 06:01
                                    </td>
                                    <td>hello@younegotiate.com</td>
                                    <td>84895685690</td>
                                    <td>-</td>
                                    <td>Alpha</td>
                                    <td>New Template</td>
                                    <td>Email</td>
                                    <td><a href="#" class="btn btn-green">View</a></td>
                                 </tr>
                                  <tr>
                                    <td>
                                      2222-06-08 06:01
                                    </td>
                                    <td>hello@younegotiate.com</td>
                                    <td>84895685690</td>
                                    <td>-</td>
                                    <td>Alpha</td>
                                    <td>New Template</td>
                                    <td>Email</td>
                                    <td><a href="#" class="btn btn-green">View</a></td>
                                 </tr>
                                  <tr>
                                    <td>
                                      2222-06-08 06:01
                                    </td>
                                    <td>hello@younegotiate.com</td>
                                    <td>84895685690</td>
                                    <td>-</td>
                                    <td>Alpha</td>
                                    <td>New Template</td>
                                    <td>Email</td>
                                    <td><a href="#" class="btn btn-green">View</a></td>
                                 </tr>

                                  <tr>
                                    <td>
                                      2222-06-08 06:01
                                    </td>
                                    <td>hello@younegotiate.com</td>
                                    <td>84895685690</td>
                                    <td>-</td>
                                    <td>Alpha</td>
                                    <td>New Template</td>
                                    <td>Email</td>
                                    <td><a href="#" class="btn btn-green">View</a></td>
                                 </tr>
                                  <tr>
                                    <td>
                                      2222-06-08 06:01
                                    </td>
                                    <td>hello@younegotiate.com</td>
                                    <td>84895685690</td>
                                    <td>-</td>
                                    <td>Alpha</td>
                                    <td>New Template</td>
                                    <td>Email</td>
                                    <td><a href="#" class="btn btn-green">View</a></td>
                                 </tr>
                                  <tr>
                                    <td>
                                      2222-06-08 06:01
                                    </td>
                                    <td>hello@younegotiate.com</td>
                                    <td>84895685690</td>
                                    <td>-</td>
                                    <td>Alpha</td>
                                    <td>New Template</td>
                                    <td>Email</td>
                                    <td><a href="#" class="btn btn-green">View</a></td>
                                 </tr>
                                  <tr>
                                    <td>
                                      2222-06-08 06:01
                                    </td>
                                    <td>hello@younegotiate.com</td>
                                    <td>84895685690</td>
                                    <td>-</td>
                                    <td>Alpha</td>
                                    <td>New Template</td>
                                    <td>Email</td>
                                    <td><a href="#" class="btn btn-green">View</a></td>
                                 </tr>
                                  <tr>
                                    <td>
                                      2222-06-08 06:01
                                    </td>
                                    <td>hello@younegotiate.com</td>
                                    <td>84895685690</td>
                                    <td>-</td>
                                    <td>Alpha</td>
                                    <td>New Template</td>
                                    <td>Email</td>
                                    <td><a href="#" class="btn btn-green">View</a></td>
                                 </tr>
                                  <tr>
                                    <td>
                                      2222-06-08 06:01
                                    </td>
                                    <td>hello@younegotiate.com</td>
                                    <td>84895685690</td>
                                    <td>-</td>
                                    <td>Alpha</td>
                                    <td>New Template</td>
                                    <td>Email</td>
                                    <td><a href="#" class="btn btn-green">View</a></td>
                                 </tr>
                                  <tr>
                                    <td>
                                      2222-06-08 06:01
                                    </td>
                                    <td>hello@younegotiate.com</td>
                                    <td>84895685690</td>
                                    <td>-</td>
                                    <td>Alpha</td>
                                    <td>New Template</td>
                                    <td>Email</td>
                                    <td><a href="#" class="btn btn-green">View</a></td>
                                 </tr>
                                  <tr>
                                    <td>
                                      2222-06-08 06:01
                                    </td>
                                    <td>hello@younegotiate.com</td>
                                    <td>84895685690</td>
                                    <td>-</td>
                                    <td>Alpha</td>
                                    <td>New Template</td>
                                    <td>Email</td>
                                    <td><a href="#" class="btn btn-green">View</a></td>
                                 </tr>
                                  <tr>
                                    <td>
                                      2222-06-08 06:01
                                    </td>
                                    <td>hello@younegotiate.com</td>
                                    <td>84895685690</td>
                                    <td>-</td>
                                    <td>Alpha</td>
                                    <td>New Template</td>
                                    <td>Email</td>
                                    <td><a href="#" class="btn btn-green">View</a></td>
                                 </tr>
                                  <tr>
                                    <td>
                                      2222-06-08 06:01
                                    </td>
                                    <td>hello@younegotiate.com</td>
                                    <td>84895685690</td>
                                    <td>-</td>
                                    <td>Alpha</td>
                                    <td>New Template</td>
                                    <td>Email</td>
                                    <td><a href="#" class="btn btn-green">View</a></td>
                                 </tr>
                                 
                                 
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
</div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $('#summernote').summernote({
      placeholder: 'Hello stand alone ui',
      tabsize: 2,
      height: 100,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
      ]
    });
   $(document).ready(function () {
   $('#example1').DataTable({
          language: {
            
              'paginate': {
                  'previous': '<span class="fa fa-angle-left"></span>',
                  'next': '<span class="fa fa-angle-right"></span>'
              }
              
          }
      });
       $('#example2').DataTable({
          language: {
            
              'paginate': {
                  'previous': '<span class="fa fa-angle-left"></span>',
                  'next': '<span class="fa fa-angle-right"></span>'
              }
              
          }
      });

       $('#example3').DataTable({
          language: {
            
              'paginate': {
                  'previous': '<span class="fa fa-angle-left"></span>',
                  'next': '<span class="fa fa-angle-right"></span>'
              }
              
          }
      });
       $('#example4').DataTable({
          language: {
            
              'paginate': {
                  'previous': '<span class="fa fa-angle-left"></span>',
                  'next': '<span class="fa fa-angle-right"></span>'
              }
              
          }
      });
   
   });
</script>
@endsection