@extends('layout.admin.app')
@section('admin-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Payments</div>
      </h1>
      <div class="section-body">
         
        
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Payment Configuration</h4>
                   
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table" id="example2">
                           <thead>
                              <tr>
                                 <th>
                                    MERCHANT NAME
                                 </th>
                                 <th>sub account name</th>
                                 <th>Payment Method</th>
                                 <th>Payment Type.</th>
                                 <th>Status</th>
                                 <th>Action</th>
                                
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                   <a href="#" class="text-black">Peter Williams</a>
                                 </td>
                                 <td>
                                   <a href="#" class="text-black">David</a>
                                 </td>
                                 <td>
                                    Stripe
                                 </td>
                                 <td>
                                   CC
                                 </td>
                                 <td class="red-text"> Not verified</td>
                              
                                 <td>
                                   
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#config" >Configure  </a>
                                  
                                 </td>
                              </tr>

                              <tr>
                                 <td>
                                   <a href="#" class="text-black">Peter Williams</a>
                                 </td>
                                 <td>
                                   <a href="#" class="text-black">David</a>
                                 </td>
                                 <td>
                                    Stripe
                                 </td>
                                 <td>
                                   CC
                                 </td>
                                 <td class="red-text"> Not verified</td>
                              
                                 <td>
                                   
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#config" >Configure  </a>
                                  
                                 </td>
                              </tr>

                              <tr>
                                 <td>
                                   <a href="#" class="text-black">Peter Williams</a>
                                 </td>
                                 <td>
                                   <a href="#" class="text-black">David</a>
                                 </td>
                                 <td>
                                    Stripe
                                 </td>
                                 <td>
                                   CC
                                 </td>
                                   <td class="text-green"> Approved</td>
                              
                                 <td>
                                   
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#config" >Configure  </a>
                                  
                                 </td>
                              </tr>

                              <tr>
                                 <td>
                                   <a href="#" class="text-black">Peter Williams</a>
                                 </td>
                                 <td>
                                   <a href="#" class="text-black">David</a>
                                 </td>
                                 <td>
                                    Stripe
                                 </td>
                                 <td>
                                   CC
                                 </td>
                                  <td class="text-green"> Approved</td>
                              
                                 <td>
                                   
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#config" >Configure  </a>
                                  
                                 </td>
                              </tr>

                              <tr>
                                 <td>
                                   <a href="#" class="text-black">Peter Williams</a>
                                 </td>
                                 <td>
                                   <a href="#" class="text-black">David</a>
                                 </td>
                                 <td>
                                    Stripe
                                 </td>
                                 <td>
                                   CC
                                 </td>
                                   <td class="text-green"> Approved</td>
                              
                                 <td>
                                   
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#config" >Configure  </a>
                                  
                                 </td>
                              </tr>

                              <tr>
                                 <td>
                                   <a href="#" class="text-black">Peter Williams</a>
                                 </td>
                                 <td>
                                   <a href="#" class="text-black">David</a>
                                 </td>
                                 <td>
                                    Stripe
                                 </td>
                                 <td>
                                   CC
                                 </td>
                                   <td class="text-green"> Approved</td>
                              
                                 <td>
                                   
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#config" >Configure  </a>
                                  
                                 </td>
                              </tr>

                              <tr>
                                 <td>
                                   <a href="#" class="text-black">Peter Williams</a>
                                 </td>
                                 <td>
                                   <a href="#" class="text-black">David</a>
                                 </td>
                                 <td>
                                    Stripe
                                 </td>
                                 <td>
                                   CC
                                 </td>
                                  <td class="text-green"> Approved</td>
                              
                                 <td>
                                   
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#config" >Configure  </a>
                                  
                                 </td>
                              </tr>

                              <tr>
                                 <td>
                                   <a href="#" class="text-black">Peter Williams</a>
                                 </td>
                                 <td>
                                   <a href="#" class="text-black">David</a>
                                 </td>
                                 <td>
                                    Stripe
                                 </td>
                                 <td>
                                   CC
                                 </td>
                                 <td class="text-green"> Approved</td>
                              
                                 <td>
                                   
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#config" >Configure  </a>
                                  
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                   <a href="#" class="text-black">Peter Williams</a>
                                 </td>
                                 <td>
                                   <a href="#" class="text-black">David</a>
                                 </td>
                                 <td>
                                    Stripe
                                 </td>
                                 <td>
                                   CC
                                 </td>
                                   <td class="text-green"> Approved</td>
                              
                                 <td>
                                   
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#config" >Configure  </a>
                                  
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                   <a href="#" class="text-black">Peter Williams</a>
                                 </td>
                                 <td>
                                   <a href="#" class="text-black">David</a>
                                 </td>
                                 <td>
                                    Stripe
                                 </td>
                                 <td>
                                   CC
                                 </td>
                                  <td class="text-green"> Approved</td>
                              
                                 <td>
                                   
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#config" >Configure  </a>
                                  
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                   <a href="#" class="text-black">Peter Williams</a>
                                 </td>
                                 <td>
                                   <a href="#" class="text-black">David</a>
                                 </td>
                                 <td>
                                    Stripe
                                 </td>
                                 <td>
                                   CC
                                 </td>
                                 <td class="text-green"> Approved</td>
                              
                                 <td>
                                   
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#config" >Configure  </a>
                                  
                                 </td>
                              </tr>


                              
                             
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
        
        
      </div>
   </section>
</div>

</div>
</div>
<!-- Modal -->


<div class="modal modal-new md-sm fade" id="small-b" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Block User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Do you want to block the user?</h2>
            <p>Sed ut perspiciatis unde omnis iste natus error</p>
            <div class="text-right mt-4">
               <button type="submit" class="btn border-btn">Cancel</button>
               <button type="submit" class="btn blue-btn">Yes</button>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal modal-new fade" id="config">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Configure</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <form>
                <div class="form-group">
                                 <label>Type</label>
                                 <div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c1" name="c1" class="custom-control-input">
                                       <label class="custom-control-label" for="c1">Stripe</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c2" name="c2" class="custom-control-input">
                                       <label class="custom-control-label" for="c2">Authorize.net</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c3" name="c3" class="custom-control-input">
                                       <label class="custom-control-label" for="c3">Repay</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c4" name="c4" class="custom-control-input">
                                       <label class="custom-control-label" for="c4">USAePay</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c5" name="c5" class="custom-control-input">
                                       <label class="custom-control-label" for="c5">PaidYet</label>
                                    </div>
                                 </div>
                              </div>

                              <div class="form-group">
                     <label>Payment Type</label>
                     <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                  </div>
                      <div class="form-group">
                     <label>Public Key</label>
                     <input class="form-control" type="text" />
                  </div>

                   <div class="form-group">
                     <label>Secret Key</label>
                     <input class="form-control" type="text" />
                  </div>
              <div class="form-row">
                  <div class="form-group col-md-6">
                     <label>Convenience Fee Percentage</label>
                    <input class="form-control" type="text" />
                  </div>
                  <div class="form-group col-md-6">
                     <label>Convenience Fee Amount</label>
                    <input class="form-control" type="text" />
                  </div>
               </div>

              <div class="form-row">
                  <div class="form-group col-md-6">
                     <label>Min Convenience Fee Amount</label>
                    <input class="form-control" type="text" />
                  </div>
                  <div class="form-group col-md-6">
                     <label>Max Convenience Fee Amount</label>
                    <input class="form-control" type="text" />
                  </div>
               </div>

             


               <div class="text-right">
                  <a href="#" class="btn border-btn">Cancel</a>
                  <button type="submit" class="btn blue-btn">Save</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script src="{{asset('admin/modules/jquery.min.js')}}"></script>
<script src="{{asset('admin/modules/popper.js')}}"></script>
<script src="{{asset('admin/modules/tooltip.js')}}"></script>
<script src="{{asset('admin/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('admin/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('admin/js/sa-functions.js')}}"></script>
<script src="{{asset('admin/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('admin/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('admin/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('admin/modules/modules-datatables.js')}}"></script>
<script src="{{asset('admin/modules/chart.min.js')}}"></script>
<script src="{{asset('admin/modules/summernote/summernote-lite.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<script src="{{asset('admin/js/scripts.js')}}"></script>
<script src="{{asset('admin/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
       $('#example').DataTable({
           language: {
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
        $('#example2').DataTable({
           language: {
            
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   

      $("#datepicker").datepicker({ 
            autoclose: true, 
            todayHighlight: true
      }).datepicker('update', new Date());
    
  
   });
</script>
@endsection