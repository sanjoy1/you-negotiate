@extends('layout.admin.app')
@section('admin-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Profile</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                        <div class="card-header flex-hd">
                           <ul class="step-list">
                            <li class="select active"><a href="profile"><i class="nav-icon text-14b i-Yes"></i> Company Profile</a></li>
                            <li><i class="fa fa-angles-right"></i></li>
                            <li class="select active"><a href="bank-details"><i class="nav-icon text-14b i-Yes"></i> Bank Details</a></li>
                            <li><i class="fa fa-angles-right"></i></li>
                            <li class="select active"><a href="financial-profile"><i class="nav-icon text-14b i-Yes"></i> Financial Profile</a></li>
                            <li><i class="fa fa-angles-right"></i></li>
                            <li class="select"><a href="paymentsetup"><i class="nav-icon text-14b i-Yes"></i> Paymentsetup</a></li>
                            <li><i class="fa fa-angles-right"></i></li>
                            <li><a href="my-logo"><i class="nav-icon text-14b i-Yes"></i> Logo</a></li>
                            <li><i class="fa fa-angles-right"></i></li>
                            <li><a href="contact-profile"><i class="nav-icon text-14b i-Yes"></i> Contact</a></li>


                           </ul>
                           <div>
                            <a href="financial-profile" class="btn btn-black">Back</a>
                            <a href="my-logo" class="btn btn-primary">Next</a>
                           
                           </div>
                        </div>
                        <hr>
                        <div class="card-body form-area">
                            <h3 class="mb-4">Set Up Master Account Merchant Processor</h3>
                               
                                            <div class="form-group">
                                            <label>Set credit type card info</label>
                                            <div class="flex-align">
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c1" name="c1" class="custom-control-input">
                                       <label class="custom-control-label" for="c1"><img alt="epay" src="http://127.0.0.1:8000/dist/img/epay1.png"></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c2" name="c1" class="custom-control-input">
                                       <label class="custom-control-label" for="c2"><img alt="epay" src="http://127.0.0.1:8000/dist/img/epay2.png"></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c3" name="c1" class="custom-control-input">
                                       <label class="custom-control-label" for="c3"><img alt="epay" src="http://127.0.0.1:8000/dist/img/epay3.png"></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c4" name="c1" class="custom-control-input">
                                       <label class="custom-control-label" for="c4"><img alt="epay" src="http://127.0.0.1:8000/dist/img/epay4.png"></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c5" name="c1" class="custom-control-input">
                                       <label class="custom-control-label" for="c5"><img alt="epay" src="http://127.0.0.1:8000/dist/img/epay5.png"></label>
                                    </div>


                                 </div>
                                            </div>
                                            
                                <div class="form-row">
                                            <div class="form-group col-md-6">
                                            <label>Key</label>
                                            <input type="text" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6">
                                            <label>PIN</label>
                                            <input type="text" class="form-control">
                                            </div>
                                </div>
  <div class="form-group">
                                            <label>Set ACH type card info</label>
                                           <div class="flex-align">
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c6" name="c6" class="custom-control-input">
                                       <label class="custom-control-label" for="c6"><img alt="epay" src="http://127.0.0.1:8000/dist/img/epay1.png"></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c7" name="c6" class="custom-control-input">
                                       <label class="custom-control-label" for="c7"><img alt="epay" src="http://127.0.0.1:8000/dist/img/epay2.png"></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c8" name="c6" class="custom-control-input">
                                       <label class="custom-control-label" for="c8"><img alt="epay" src="http://127.0.0.1:8000/dist/img/epay3.png"></label>
                                    </div>
                                    </div>
                                            </div>

                              
                                <div class="form-row">
                                            <div class="form-group col-md-6">
                                            <label>Key</label>
                                             <input type="text" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6">
                                            <label>PIN</label>
                                            <input type="text" class="form-control">
                                            </div>
                                </div>
                                
                               

                                <div class="text-right">
                             <a href="financial-profile" class="btn btn-black">Back</a>
                            <a href="my-logo" class="btn btn-primary">Next</a>
                           
                           </div>


                        </div>
                     </div>
            </div>
         </div>
        
   </section>
   </div>
  
</div>
</div>

@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $('#summernote').summernote({
      placeholder: 'Hello stand alone ui',
      tabsize: 2,
      height: 100,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
      ]
    });
   $(document).ready(function () {

       $('#example2').DataTable({
          language: {
            
              'paginate': {
                  'previous': '<span class="fa fa-angle-left"></span>',
                  'next': '<span class="fa fa-angle-right"></span>'
              }
              
          }
      });
   
   });
</script>
@endsection