@extends('layout.admin.app')
@section('admin-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>My Campaign Schedules</div>
      </h1>
      <div class="section-body">
      
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Scheduled Campaigns</h4>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                         <table class="table" id="example2">
                        <thead>
                           <tr>
                              <th>
                                 status
                              </th>
                              <th>description</th>
                              <th>email template</th>
                              <th>sms template</th>
                              <th>frequency</th>
                              <th>action</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <td>
                                 C-1
                              </td>
                              <td>Did not click on link</td>
                              <td>
                                 C-1 Email Did not click on link
                              </td>
                              <td>C-1 SMS Did not click on link</td>
                              <td>Once at 0:00Hrs</td>
                              <td>
                                 <div class="tg-list-item">
                                    
                                    <input class="tgl tgl-skewed" id="cb1" type="checkbox"/>
                                    <label class="tgl-btn" data-tg-off="OFF" data-tg-on="ON" for="cb1"></label>
                                </div>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 C-1
                              </td>
                              <td>Did not click on link</td>
                              <td>
                                 C-1 Email Did not click on link
                              </td>
                              <td>C-1 SMS Did not click on link</td>
                              <td>Once at 0:00Hrs</td>
                              <td>
                                 <div class="tg-list-item">
                                    
                                    <input class="tgl tgl-skewed" id="cb2" type="checkbox"/>
                                    <label class="tgl-btn" data-tg-off="OFF" data-tg-on="ON" for="cb2"></label>
                                </div>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 C-1
                              </td>
                              <td>Did not click on link</td>
                              <td>
                                 C-1 Email Did not click on link
                              </td>
                              <td>C-1 SMS Did not click on link</td>
                              <td>Once at 0:00Hrs</td>
                              <td>
                                 <div class="tg-list-item">
                                    
                                    <input class="tgl tgl-skewed" id="cb3" type="checkbox"/>
                                    <label class="tgl-btn" data-tg-off="OFF" data-tg-on="ON" for="cb3"></label>
                                </div>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 C-1
                              </td>
                              <td>Did not click on link</td>
                              <td>
                                 C-1 Email Did not click on link
                              </td>
                              <td>C-1 SMS Did not click on link</td>
                              <td>Once at 0:00Hrs</td>
                              <td>
                                 <div class="tg-list-item">
                                    
                                    <input class="tgl tgl-skewed" id="cb4" type="checkbox"/>
                                    <label class="tgl-btn" data-tg-off="OFF" data-tg-on="ON" for="cb4"></label>
                                </div>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 C-1
                              </td>
                              <td>Did not click on link</td>
                              <td>
                                 C-1 Email Did not click on link
                              </td>
                              <td>C-1 SMS Did not click on link</td>
                              <td>Once at 0:00Hrs</td>
                              <td>
                                 <div class="tg-list-item">
                                    
                                    <input class="tgl tgl-skewed" id="cb5" type="checkbox"/>
                                    <label class="tgl-btn" data-tg-off="OFF" data-tg-on="ON" for="cb5"></label>
                                </div>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 C-1
                              </td>
                              <td>Did not click on link</td>
                              <td>
                                 C-1 Email Did not click on link
                              </td>
                              <td>C-1 SMS Did not click on link</td>
                              <td>Once at 0:00Hrs</td>
                              <td>
                                 <div class="tg-list-item">
                                    
                                    <input class="tgl tgl-skewed" id="cb6" type="checkbox"/>
                                    <label class="tgl-btn" data-tg-off="OFF" data-tg-on="ON" for="cb6"></label>
                                </div>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 C-1
                              </td>
                              <td>Did not click on link</td>
                              <td>
                                 C-1 Email Did not click on link
                              </td>
                              <td>C-1 SMS Did not click on link</td>
                              <td>Once at 0:00Hrs</td>
                              <td>
                                 <div class="tg-list-item">
                                    
                                    <input class="tgl tgl-skewed" id="cb7" type="checkbox"/>
                                    <label class="tgl-btn" data-tg-off="OFF" data-tg-on="ON" for="cb7"></label>
                                </div>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 C-1
                              </td>
                              <td>Did not click on link</td>
                              <td>
                                 C-1 Email Did not click on link
                              </td>
                              <td>C-1 SMS Did not click on link</td>
                              <td>Once at 0:00Hrs</td>
                              <td>
                                 <div class="tg-list-item">
                                    
                                    <input class="tgl tgl-skewed" id="cb8" type="checkbox"/>
                                    <label class="tgl-btn" data-tg-off="OFF" data-tg-on="ON" for="cb8"></label>
                                </div>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 C-1
                              </td>
                              <td>Did not click on link</td>
                              <td>
                                 C-1 Email Did not click on link
                              </td>
                              <td>C-1 SMS Did not click on link</td>
                              <td>Once at 0:00Hrs</td>
                              <td>
                                 <div class="tg-list-item">
                                    
                                    <input class="tgl tgl-skewed" id="cb9" type="checkbox"/>
                                    <label class="tgl-btn" data-tg-off="OFF" data-tg-on="ON" for="cb9"></label>
                                </div>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 C-1
                              </td>
                              <td>Did not click on link</td>
                              <td>
                                 C-1 Email Did not click on link
                              </td>
                              <td>C-1 SMS Did not click on link</td>
                              <td>Once at 0:00Hrs</td>
                              <td>
                                 <div class="tg-list-item">
                                    
                                    <input class="tgl tgl-skewed" id="cb10" type="checkbox"/>
                                    <label class="tgl-btn" data-tg-off="OFF" data-tg-on="ON" for="cb10"></label>
                                </div>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 C-1
                              </td>
                              <td>Did not click on link</td>
                              <td>
                                 C-1 Email Did not click on link
                              </td>
                              <td>C-1 SMS Did not click on link</td>
                              <td>Once at 0:00Hrs</td>
                              <td>
                                 <div class="tg-list-item">
                                    
                                    <input class="tgl tgl-skewed" id="cb11" type="checkbox"/>
                                    <label class="tgl-btn" data-tg-off="OFF" data-tg-on="ON" for="cb11"></label>
                                </div>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 C-1
                              </td>
                              <td>Did not click on link</td>
                              <td>
                                 C-1 Email Did not click on link
                              </td>
                              <td>C-1 SMS Did not click on link</td>
                              <td>Once at 0:00Hrs</td>
                              <td>
                                 <div class="tg-list-item">
                                    
                                    <input class="tgl tgl-skewed" id="cb12" type="checkbox"/>
                                    <label class="tgl-btn" data-tg-off="OFF" data-tg-on="ON" for="cb12"></label>
                                </div>
                              </td>
                           </tr>

                        </tbody>
                     </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </section>
   </div>
  
</div>
</div>
<!-- Modal -->

<div class="modal modal-new md-sm fade" id="delete" >
   <div class="modal-dialog" >
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Delete Campaign</h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Are you sure?</h2>
            <p> Do you want to delete this campaign </p>
            <div class="text-right mt-4">
               <button type="submit" class="btn border-btn" data-dismiss="modal" aria-label="Close">Cancel</button>
               <button type="submit" class="btn blue-btn">Yes</button>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $('#summernote').summernote({
      placeholder: 'Hello stand alone ui',
      tabsize: 2,
      height: 100,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
      ]
    });
   $(document).ready(function () {

       $('#example2').DataTable({
          language: {
            
              'paginate': {
                  'previous': '<span class="fa fa-angle-left"></span>',
                  'next': '<span class="fa fa-angle-right"></span>'
              }
              
          }
      });
   
   });
</script>
@endsection