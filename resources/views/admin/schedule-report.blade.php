@extends('layout.admin.app')
@section('admin-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Report</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Schedule Report</h4>
                  </div>
                  <div class="card-body">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Select Report Type</label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                              </div>

                              <div class="form-group col-md-6">
                                 <label>Select Client</label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                              </div>
                             
                           </div>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Select Frequency</label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                              </div>

                              <div class="form-group col-md-6">
                                 <label>Type</label>
                                 <div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c1" name="c1" class="custom-control-input">
                                       <label class="custom-control-label" for="c1">Email</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c2" name="c2" class="custom-control-input">
                                       <label class="custom-control-label" for="c2">SFTP</label>
                                    </div>
                                  
                                 </div>
                              </div>
                             
                           </div>
                        
                           

                         
                          
                        </div>
                        <div class="text-right col-sm-12">
                           <button type="submit" class="btn blue-btn">Add To Schedule Exports</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Your Current Scheduled Exports</h4>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table" id="example2">
                           <thead>
                              <tr>
                                 <th>
                                  created on
                                 </th>
                                 <th>Report Type</th>
                                 <th>Frequency</th>
                                 <th>client</th>
                                 <th>export type</th>
                                 <th>export location</th>
                                 <th>action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">2222-06-08 06:01</a>
                                 </td>
                                 <td>Commission_Reporting_RNN</td>
                                 <td>Monthly</td>
                                 <td>Happy Company</td>
                                <td>Email</td>
                                  <td>-</td>
                                   
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit Schedule</a>
                                    <a href="#" class="btn btn-view">Pause Export</a>
                                  
                                  <a href="#" class="btn btn-deactive">Delete</a>
                                   
                                 </td>
                              </tr>
                             <tr>
                                 <td>
                                    <a href="#" class="text-black">2222-06-08 06:01</a>
                                 </td>
                                 <td>Commission_Reporting_RNN</td>
                                 <td>Monthly</td>
                                 <td>Happy Company</td>
                                <td>Email</td>
                                  <td>-</td>
                                   
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit Schedule</a>
                                    <a href="#" class="btn btn-view">Pause Export</a>
                                  
                                  <a href="#" class="btn btn-deactive">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">2222-06-08 06:01</a>
                                 </td>
                                 <td>Commission_Reporting_RNN</td>
                                 <td>Monthly</td>
                                 <td>Happy Company</td>
                                <td>Email</td>
                                  <td>-</td>
                                   
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit Schedule</a>
                                    <a href="#" class="btn btn-view">Pause Export</a>
                                  
                                  <a href="#" class="btn btn-deactive">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">2222-06-08 06:01</a>
                                 </td>
                                 <td>Commission_Reporting_RNN</td>
                                 <td>Monthly</td>
                                 <td>Happy Company</td>
                                <td>Email</td>
                                  <td>-</td>
                                   
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit Schedule</a>
                                    <a href="#" class="btn btn-view">Pause Export</a>
                                  
                                  <a href="#" class="btn btn-deactive">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">2222-06-08 06:01</a>
                                 </td>
                                 <td>Commission_Reporting_RNN</td>
                                 <td>Monthly</td>
                                 <td>Happy Company</td>
                                <td>Email</td>
                                  <td>-</td>
                                   
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit Schedule</a>
                                    <a href="#" class="btn btn-view">Pause Export</a>
                                  
                                  <a href="#" class="btn btn-deactive">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">2222-06-08 06:01</a>
                                 </td>
                                 <td>Commission_Reporting_RNN</td>
                                 <td>Monthly</td>
                                 <td>Happy Company</td>
                                <td>Email</td>
                                  <td>-</td>
                                   
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit Schedule</a>
                                    <a href="#" class="btn btn-view">Pause Export</a>
                                  
                                  <a href="#" class="btn btn-deactive">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">2222-06-08 06:01</a>
                                 </td>
                                 <td>Commission_Reporting_RNN</td>
                                 <td>Monthly</td>
                                 <td>Happy Company</td>
                                <td>Email</td>
                                  <td>-</td>
                                   
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit Schedule</a>
                                    <a href="#" class="btn btn-view">Pause Export</a>
                                  
                                  <a href="#" class="btn btn-deactive">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">2222-06-08 06:01</a>
                                 </td>
                                 <td>Commission_Reporting_RNN</td>
                                 <td>Monthly</td>
                                 <td>Happy Company</td>
                                <td>Email</td>
                                  <td>-</td>
                                   
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit Schedule</a>
                                    <a href="#" class="btn btn-view">Pause Export</a>
                                  
                                  <a href="#" class="btn btn-deactive">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">2222-06-08 06:01</a>
                                 </td>
                                 <td>Commission_Reporting_RNN</td>
                                 <td>Monthly</td>
                                 <td>Happy Company</td>
                                <td>Email</td>
                                  <td>-</td>
                                   
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit Schedule</a>
                                    <a href="#" class="btn btn-view">Pause Export</a>
                                  
                                  <a href="#" class="btn btn-deactive">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">2222-06-08 06:01</a>
                                 </td>
                                 <td>Commission_Reporting_RNN</td>
                                 <td>Monthly</td>
                                 <td>Happy Company</td>
                                <td>Email</td>
                                  <td>-</td>
                                   
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit Schedule</a>
                                    <a href="#" class="btn btn-view">Pause Export</a>
                                  
                                  <a href="#" class="btn btn-deactive">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">2222-06-08 06:01</a>
                                 </td>
                                 <td>Commission_Reporting_RNN</td>
                                 <td>Monthly</td>
                                 <td>Happy Company</td>
                                <td>Email</td>
                                  <td>-</td>
                                   
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit Schedule</a>
                                    <a href="#" class="btn btn-view">Pause Export</a>
                                  
                                  <a href="#" class="btn btn-deactive">Delete</a>
                                   
                                 </td>
                              </tr>
                            
                              
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </section>
   </div>
  
</div>
</div>
<!-- Modal -->

<div class="modal modal-new md-sm fade" id="delete" >
   <div class="modal-dialog" >
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Delete Report</h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Are you sure?</h2>
            <p> Do you want to delete this Report </p>
            <div class="text-right mt-4">
               <button type="submit" class="btn border-btn" data-dismiss="modal" aria-label="Close">Cancel</button>
               <button type="submit" class="btn blue-btn">Yes</button>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $('#summernote').summernote({
      placeholder: 'Hello stand alone ui',
      tabsize: 2,
      height: 100,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
      ]
    });
   $(document).ready(function () {

       $('#example2').DataTable({
          language: {
            
              'paginate': {
                  'previous': '<span class="fa fa-angle-left"></span>',
                  'next': '<span class="fa fa-angle-right"></span>'
              }
              
          }
      });
   
   });
</script>
@endsection