@extends('layout.admin.app')
@section('admin-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Campaign Scheduler</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Schedule A Campaign</h4>
                  </div>
                  <div class="card-body">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Select Company <sup>*</sup></label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                              </div>

                              <div class="form-group col-md-6">
                                 <label>Select Template <sup>*</sup></label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                              </div>
                             
                           </div>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Select Group <sup>*</sup></label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                              </div>

                              <div class="form-group col-md-6">
                                 <label>Select Frequency <sup>*</sup></label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                              </div>
                             
                           </div>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Select Time From (EST Only) <sup>*</sup></label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                              </div>

                              <div class="form-group col-md-6">
                                 <label>Select Time To(EST Only) <sup>*</sup></label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                              </div>
                             
                           </div>
                           

                         
                          
                        </div>
                        <div class="text-right col-sm-12">
                           <button type="submit" class="btn blue-btn">Add Schedule Campaign</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Scheduled Campaigns</h4>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table" id="example2">
                           <thead>
                              <tr>
                                 <th>
                                  template
                                 </th>
                                 <th>company name</th>
                                 <th>type</th>
                                 <th>group</th>
                                 <th>company</th>
                                 <th>Frequency</th>
                                 <th>action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">1st Header</a>
                                 </td>
                                 <td>Good company</td>
                                 <td>Email</td>
                                 <td>3rdJune2021 - Not Joined</td>
                                <td>Good company</td>
                                <td>Daily at 1:30Hrs</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-reset">Preview</a>
                                    <a href="#" class="btn btn-view">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">1st Header</a>
                                 </td>
                                 <td>Good company</td>
                                 <td>Email</td>
                                 <td>3rdJune2021 - Not Joined</td>
                                <td>Good company</td>
                                <td>Daily at 1:30Hrs</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-reset">Preview</a>
                                    <a href="#" class="btn btn-view">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">1st Header</a>
                                 </td>
                                 <td>Good company</td>
                                 <td>Email</td>
                                 <td>3rdJune2021 - Not Joined</td>
                                <td>Good company</td>
                                <td>Daily at 1:30Hrs</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-reset">Preview</a>
                                    <a href="#" class="btn btn-view">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">1st Header</a>
                                 </td>
                                 <td>Good company</td>
                                 <td>Email</td>
                                 <td>3rdJune2021 - Not Joined</td>
                                <td>Good company</td>
                                <td>Daily at 1:30Hrs</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-reset">Preview</a>
                                    <a href="#" class="btn btn-view">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">1st Header</a>
                                 </td>
                                 <td>Good company</td>
                                 <td>Email</td>
                                 <td>3rdJune2021 - Not Joined</td>
                                <td>Good company</td>
                                <td>Daily at 1:30Hrs</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-reset">Preview</a>
                                    <a href="#" class="btn btn-view">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">1st Header</a>
                                 </td>
                                 <td>Good company</td>
                                 <td>Email</td>
                                 <td>3rdJune2021 - Not Joined</td>
                                <td>Good company</td>
                                <td>Daily at 1:30Hrs</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-reset">Preview</a>
                                    <a href="#" class="btn btn-view">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">1st Header</a>
                                 </td>
                                 <td>Good company</td>
                                 <td>Email</td>
                                 <td>3rdJune2021 - Not Joined</td>
                                <td>Good company</td>
                                <td>Daily at 1:30Hrs</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-reset">Preview</a>
                                    <a href="#" class="btn btn-view">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">1st Header</a>
                                 </td>
                                 <td>Good company</td>
                                 <td>Email</td>
                                 <td>3rdJune2021 - Not Joined</td>
                                <td>Good company</td>
                                <td>Daily at 1:30Hrs</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-reset">Preview</a>
                                    <a href="#" class="btn btn-view">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">1st Header</a>
                                 </td>
                                 <td>Good company</td>
                                 <td>Email</td>
                                 <td>3rdJune2021 - Not Joined</td>
                                <td>Good company</td>
                                <td>Daily at 1:30Hrs</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-reset">Preview</a>
                                    <a href="#" class="btn btn-view">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">1st Header</a>
                                 </td>
                                 <td>Good company</td>
                                 <td>Email</td>
                                 <td>3rdJune2021 - Not Joined</td>
                                <td>Good company</td>
                                <td>Daily at 1:30Hrs</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-reset">Preview</a>
                                    <a href="#" class="btn btn-view">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">1st Header</a>
                                 </td>
                                 <td>Good company</td>
                                 <td>Email</td>
                                 <td>3rdJune2021 - Not Joined</td>
                                <td>Good company</td>
                                <td>Daily at 1:30Hrs</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-reset">Preview</a>
                                    <a href="#" class="btn btn-view">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">1st Header</a>
                                 </td>
                                 <td>Good company</td>
                                 <td>Email</td>
                                 <td>3rdJune2021 - Not Joined</td>
                                <td>Good company</td>
                                <td>Daily at 1:30Hrs</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-reset">Preview</a>
                                    <a href="#" class="btn btn-view">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">1st Header</a>
                                 </td>
                                 <td>Good company</td>
                                 <td>Email</td>
                                 <td>3rdJune2021 - Not Joined</td>
                                <td>Good company</td>
                                <td>Daily at 1:30Hrs</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-reset">Preview</a>
                                    <a href="#" class="btn btn-view">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">1st Header</a>
                                 </td>
                                 <td>Good company</td>
                                 <td>Email</td>
                                 <td>3rdJune2021 - Not Joined</td>
                                <td>Good company</td>
                                <td>Daily at 1:30Hrs</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-reset">Preview</a>
                                    <a href="#" class="btn btn-view">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">1st Header</a>
                                 </td>
                                 <td>Good company</td>
                                 <td>Email</td>
                                 <td>3rdJune2021 - Not Joined</td>
                                <td>Good company</td>
                                <td>Daily at 1:30Hrs</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-reset">Preview</a>
                                    <a href="#" class="btn btn-view">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                              
                              

                              
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </section>
   </div>
  
</div>
</div>
<!-- Modal -->

<div class="modal modal-new md-sm fade" id="delete" >
   <div class="modal-dialog" >
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Delete Campaign</h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Are you sure?</h2>
            <p> Do you want to delete this campaign </p>
            <div class="text-right mt-4">
               <button type="submit" class="btn border-btn" data-dismiss="modal" aria-label="Close">Cancel</button>
               <button type="submit" class="btn blue-btn">Yes</button>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $('#summernote').summernote({
      placeholder: 'Hello stand alone ui',
      tabsize: 2,
      height: 100,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
      ]
    });
   $(document).ready(function () {

       $('#example2').DataTable({
          language: {
            
              'paginate': {
                  'previous': '<span class="fa fa-angle-left"></span>',
                  'next': '<span class="fa fa-angle-right"></span>'
              }
              
          }
      });
   
   });
</script>
@endsection