@extends('layout.admin.app')
@section('admin-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Schedule A Campaign</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Schedule A Campaign</h4>
                  </div>
                  <div class="card-body">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Select Status</label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                              </div>

                              <div class="form-group col-md-6">
                                 <label>Select Frequency: </label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                              </div>
                             
                           </div>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Select Time From (EST Only)</label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                              </div>

                              <div class="form-group col-md-6">
                                 <label>Select Time To (EST Only)</label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                 </select>
                              </div>
                             
                           </div>
                         

                         
                          
                        </div>
                        <div class="text-right col-sm-12">
                           <button type="submit" class="btn blue-btn">Add Schedule Campaign</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Scheduled Campaigns</h4>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-150" id="example2">
                           <thead>
                              <tr>
                                 <th>status</th>
                                 <th>description</th>
                                 <th>email template</th>
                                 <th>sms template</th>
                                 <th>frequency</th>
                                 <th>created at</th>
                                 <th>action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <a href="#" class="text-black">C-1</a>
                                 </td>
                                 <td>	Did not click on link</td>
                                 <td>C-1 Email Did not click on link</td>
                                 <td>C-1 SMS Did not click on link</td>
                                <td>Once at 0:00Hrs</td>
                                <td>2022-06-06 06:12:19</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-navy">Active(0)</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#preview">Email</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#sms-preview">SMS</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-yellow">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                    <a href="#" class="text-black">C-1</a>
                                 </td>
                                 <td>	Did not click on link</td>
                                 <td>C-1 Email Did not click on link</td>
                                 <td>C-1 SMS Did not click on link</td>
                                <td>Once at 0:00Hrs</td>
                                <td>2022-06-06 06:12:19</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-navy">Active(0)</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#preview">Email</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#sms-preview">SMS</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-yellow">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                    <a href="#" class="text-black">C-1</a>
                                 </td>
                                 <td>	Did not click on link</td>
                                 <td>C-1 Email Did not click on link</td>
                                 <td>C-1 SMS Did not click on link</td>
                                <td>Once at 0:00Hrs</td>
                                <td>2022-06-06 06:12:19</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-navy">Active(0)</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#preview">Email</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#sms-preview">SMS</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-yellow">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                    <a href="#" class="text-black">C-1</a>
                                 </td>
                                 <td>	Did not click on link</td>
                                 <td>C-1 Email Did not click on link</td>
                                 <td>C-1 SMS Did not click on link</td>
                                <td>Once at 0:00Hrs</td>
                                <td>2022-06-06 06:12:19</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-navy">Active(0)</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#preview">Email</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#sms-preview">SMS</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-yellow">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                    <a href="#" class="text-black">C-1</a>
                                 </td>
                                 <td>	Did not click on link</td>
                                 <td>C-1 Email Did not click on link</td>
                                 <td>C-1 SMS Did not click on link</td>
                                <td>Once at 0:00Hrs</td>
                                <td>2022-06-06 06:12:19</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-navy">Active(0)</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#preview">Email</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#sms-preview">SMS</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-yellow">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                    <a href="#" class="text-black">C-1</a>
                                 </td>
                                 <td>	Did not click on link</td>
                                 <td>C-1 Email Did not click on link</td>
                                 <td>C-1 SMS Did not click on link</td>
                                <td>Once at 0:00Hrs</td>
                                <td>2022-06-06 06:12:19</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-navy">Active(0)</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#preview">Email</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#sms-preview">SMS</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-yellow">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                    <a href="#" class="text-black">C-1</a>
                                 </td>
                                 <td>	Did not click on link</td>
                                 <td>C-1 Email Did not click on link</td>
                                 <td>C-1 SMS Did not click on link</td>
                                <td>Once at 0:00Hrs</td>
                                <td>2022-06-06 06:12:19</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-navy">Active(0)</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#preview">Email</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#sms-preview">SMS</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-yellow">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                    <a href="#" class="text-black">C-1</a>
                                 </td>
                                 <td>	Did not click on link</td>
                                 <td>C-1 Email Did not click on link</td>
                                 <td>C-1 SMS Did not click on link</td>
                                <td>Once at 0:00Hrs</td>
                                <td>2022-06-06 06:12:19</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-navy">Active(0)</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#preview">Email</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#sms-preview">SMS</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-yellow">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                    <a href="#" class="text-black">C-1</a>
                                 </td>
                                 <td>	Did not click on link</td>
                                 <td>C-1 Email Did not click on link</td>
                                 <td>C-1 SMS Did not click on link</td>
                                <td>Once at 0:00Hrs</td>
                                <td>2022-06-06 06:12:19</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-navy">Active(0)</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#preview">Email</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#sms-preview">SMS</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-yellow">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                    <a href="#" class="text-black">C-1</a>
                                 </td>
                                 <td>	Did not click on link</td>
                                 <td>C-1 Email Did not click on link</td>
                                 <td>C-1 SMS Did not click on link</td>
                                <td>Once at 0:00Hrs</td>
                                <td>2022-06-06 06:12:19</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-navy">Active(0)</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#preview">Email</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#sms-preview">SMS</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-yellow">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                    <a href="#" class="text-black">C-1</a>
                                 </td>
                                 <td>	Did not click on link</td>
                                 <td>C-1 Email Did not click on link</td>
                                 <td>C-1 SMS Did not click on link</td>
                                <td>Once at 0:00Hrs</td>
                                <td>2022-06-06 06:12:19</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-navy">Active(0)</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#preview">Email</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#sms-preview">SMS</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-yellow">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                    <a href="#" class="text-black">C-1</a>
                                 </td>
                                 <td>	Did not click on link</td>
                                 <td>C-1 Email Did not click on link</td>
                                 <td>C-1 SMS Did not click on link</td>
                                <td>Once at 0:00Hrs</td>
                                <td>2022-06-06 06:12:19</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-navy">Active(0)</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#preview">Email</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#sms-preview">SMS</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-yellow">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                    <a href="#" class="text-black">C-1</a>
                                 </td>
                                 <td>	Did not click on link</td>
                                 <td>C-1 Email Did not click on link</td>
                                 <td>C-1 SMS Did not click on link</td>
                                <td>Once at 0:00Hrs</td>
                                <td>2022-06-06 06:12:19</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-navy">Active(0)</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#preview">Email</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#sms-preview">SMS</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-yellow">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                    <a href="#" class="text-black">C-1</a>
                                 </td>
                                 <td>	Did not click on link</td>
                                 <td>C-1 Email Did not click on link</td>
                                 <td>C-1 SMS Did not click on link</td>
                                <td>Once at 0:00Hrs</td>
                                <td>2022-06-06 06:12:19</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-navy">Active(0)</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#preview">Email</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#sms-preview">SMS</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-yellow">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                               <tr>
                                 <td>
                                    <a href="#" class="text-black">C-1</a>
                                 </td>
                                 <td>	Did not click on link</td>
                                 <td>C-1 Email Did not click on link</td>
                                 <td>C-1 SMS Did not click on link</td>
                                <td>Once at 0:00Hrs</td>
                                <td>2022-06-06 06:12:19</td>
                                 <td>
                                  
                                    <a href="#" class="btn btn-navy">Active(0)</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#preview">Email</a>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#sms-preview">SMS</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-yellow">Pause</a>
                                  <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a>
                                   
                                 </td>
                              </tr>
                            
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </section>
   </div>
  
</div>
</div>
<!-- Modal -->
<div class="modal modal-new fade" id="preview" >
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Email Preview</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <div class="row">
               <div class="col-sm-12">
                 
                  <h5><strong>Section 1.10.33 of "de Finibus Bonorum et Malorum", 
written by Cicero in 45 BC</strong>
                  </h5>
               </div>
               <div class="col-sm-12">
                
                  <p>"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal modal-new fade" id="sms-preview" >
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">SMS Preview</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <div class="row">
               <div class="col-sm-12">
                 
                  <h5><strong>Section 1.10.33 of "de Finibus Bonorum et Malorum", 
written by Cicero in 45 BC</strong>
                  </h5>
               </div>
               <div class="col-sm-12">
                
                  <p>"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal modal-new md-sm fade" id="delete" >
   <div class="modal-dialog" >
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Edit User</h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Are you sure?</h2>
            <p> Do you want to edit the group? </p>
            <div class="text-right mt-4">
               <button type="submit" class="btn border-btn" data-dismiss="modal" aria-label="Close">Cancel</button>
               <button type="submit" class="btn blue-btn">Yes</button>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection
@section('script')
<script src="{{asset('admin/modules/jquery.min.js')}}"></script>
<script src="{{asset('admin/modules/popper.js')}}"></script>
<script src="{{asset('admin/modules/tooltip.js')}}"></script>
<script src="{{asset('admin/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('admin/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('admin/js/sa-functions.js')}}"></script>
<script src="{{asset('admin/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('admin/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('admin/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('admin/modules/modules-datatables.js')}}"></script>
<script src="{{asset('admin/modules/chart.min.js')}}"></script>
<script src="{{asset('admin/modules/summernote/summernote-lite.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<script src="{{asset('admin/js/scripts.js')}}"></script>
<script src="{{asset('admin/js/custom.js')}}"></script>
<script>

   $(document).ready(function () {

       $('#example2').DataTable({
          language: {
            
              'paginate': {
                  'previous': '<span class="fa fa-angle-left"></span>',
                  'next': '<span class="fa fa-angle-right"></span>'
              }
              
          }
      });
   
   });
</script>
@endsection