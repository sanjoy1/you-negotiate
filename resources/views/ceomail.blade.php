@extends('layout.app-header')
@section('content')
<section class="ceoMail">
   <div class="container">
      <div class="welcome-nehotitate">
         <h1>Welcome to YouNegotiate!</h1>
         <p>YouNegotiate has uploaded a collection account into YouNegotiate so you can access online & Negotiate REPLACE WITH ABC Collections has posted your account on YouNegotiate to provide you instant access to view your discounts, negotiate and send offers.</p>
         <p><strong>Please enter the last 4 digits of your social security for secure access!</strong></p>
         <div class="row justify-content-center">
            <div class="col-sm-12 col-md-6 col-lg-4">
               <div id="otp" class="inputs d-flex flex-row justify-content-center mt-2"> 
                  <input class="m-2 text-center form-control rounded" type="text" id="first" maxlength="1" /> 
                  <input class="m-2 text-center form-control rounded" type="text" id="second" maxlength="1" /> 
                  <input class="m-2 text-center form-control rounded" type="text" id="third" maxlength="1" /> 
                  <input class="m-2 text-center form-control rounded" type="text" id="fourth" maxlength="1" /> 
               </div>
            </div>
         </div>
         <div class="mt-4"> <a href="choose" class="btn btn-blue px-4">Validate My Account</a> </div>
      </div>
      <div class="dv-tag">
         <h5>Your information is protected</h5>
         <p>YouNegotiate, the first application to enable consumers to manage all of their collection account easily!
            This app has been built by consumers with debt, for consumers with debt and we are dedicated to helping you maintain the highest credit score possible to live a Big Life!! We will be adding functionality every day to support you!
         </p>
         <div class="mb-5">
            <img alt="Logo" src="{{asset('assets/img/rsv.png')}}">
         </div>
         <h5>How to Access Your Account(s)</h5>
         <p>Security and privacy are very important. If you recognize this creditor(s), we appreciate one final level of confirmation to provide you access. Please enter the last four of your social.</p>
         <p>Security and privacy are very important. If you recognize this creditor(s), we appreciate one final level of confirmation to provide you access. Please enter the last four of your social.</p>
      </div>
   </div>
</section>
@endsection
@section('script')
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
@endsection