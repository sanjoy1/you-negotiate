@extends('layout.app-header')
@section('content')
<section class="ceoMail">
   <div class="container">
      <div class="check-step">
         <div class="w-650">
            <a href="choose" class="w-25-1 select">
               <div><span><img src="{{asset('assets/img/check-white.svg')}}" alt="check"></span></div>
               <h4>Discounted Payoff</h4>
            </a>
            <a href="choose1" class="w-25-1">
               <div><span><img src="{{asset('assets/img/check-white.svg')}}" alt="check"></span></div>
               <h4>Negotiation</h4>
            </a>
            <a href="choose3" class="w-25-1">
               <div><span><img src="{{asset('assets/img/check-white.svg')}}" alt="check"></span></div>
               <h4>Payment setup</h4>
            </a>
            <a href="choose3" class="w-25-1">
               <div><span><img src="{{asset('assets/img/check-white.svg')}}" alt="check"></span></div>
               <h4>Confirmation</h4>
            </a>
         </div>
      </div>
      <div class="afford-to-pay">
         <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-5 pr-5">
               <div class="balance-box">
                  <h3>Balance</h3>
                  <div class="sm-text">Pay off balance including duscount offer</div>
                  <div class="d-flex align-items-center justify-content-between mt-3"><span>Creditor name</span> <strong>YouNegotiate</strong></div>
                  <div class="d-flex align-items-center justify-content-between mt-3"><span>Acccount</span> <strong>5454666</strong></div>
                  <div class="d-flex align-items-center justify-content-between mt-3"><span>Current Balance</span> <strong class="blue-text">$100.00</strong></div>
                  <div class="d-flex align-items-center justify-content-between mt-3"><span>Payoff Discount</span> <strong class="red-text">-$30.00</strong></div>
                  <div class="d-flex align-items-center justify-content-between mt-3"><span>Payoff Balance</span> <strong class="green-text">$70.00</strong></div>
               </div>
            </div>
            <div class="col-sm-12 col-md-8 col-lg-7 right-bx pl-5">
               <h3>Can you afford to pay <span>$70.00</span> today & say bye-bye to this debt?</h3>
               <div class="yes-can">
                  <div class="media-flex">
                     <img src="{{asset('assets/img/pay1.svg')}}" alt="check">
                     <div>
                        <h4>Yes I can</h4>
                        <p>I am excited to pay this off and say bye bye</p>
                        <a href="choose3" class="btn btn-blue position-right">Continue</a>
                     </div>
                  </div>
               </div>
               <div class="yes-can">
                  <div class="media-flex">
                     <img src="{{asset('assets/img/pay2.svg')}}" alt="check">
                     <div>
                        <h4>No! I cant</h4>
                        <p>I can’t afford to pay it off today please help me to find other option to stop collection call stress</p>
                        <a href="choose1" class="btn btn-blue position-right">Continue</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
@section('script')
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
@endsection