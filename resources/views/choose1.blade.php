@extends('layout.app-header') @section('content')
<section class="ceoMail">
    <div class="container">
        <div class="check-step">
            <div class="w-650">
                <a href="choose" class="w-25-1 select active">
                    <div>
                        <span><img src="{{asset('assets/img/check-white.svg')}}" alt="check" /></span>
                    </div>
                    <h4>Discounted Payoff</h4>
                </a>
                <a href="choose1" class="w-25-1 select">
                    <div>
                        <span><img src="{{asset('assets/img/check-white.svg')}}" alt="check" /></span>
                    </div>
                    <h4>Negotiation</h4>
                </a>
                <a href="choose3" class="w-25-1">
                    <div>
                        <span><img src="{{asset('assets/img/check-white.svg')}}" alt="check" /></span>
                    </div>
                    <h4>Payment setup</h4>
                </a>
                <a href="choose3" class="w-25-1">
                    <div>
                        <span><img src="{{asset('assets/img/check-white.svg')}}" alt="check" /></span>
                    </div>
                    <h4>Confirmation</h4>
                </a>
            </div>
        </div>
        <div class="balance-box d-flex justify-content-between mb-4">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <h3>
                    Set up a payment plan today and enjoy <br />
                    <strong class="blue-text">$25.00 discount</strong>
                </h3>
                <div class="sm-text">Let's start negotiating!</div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-5">
                <div class="d-flex align-items-center justify-content-between"><span>Current Balance</span> <strong class="blue-text">$100.00</strong></div>
                <div class="d-flex align-items-center justify-content-between mt-3"><span>Payoff Discount</span> <strong class="red-text">-$30.00</strong></div>
                <div class="d-flex align-items-center justify-content-between mt-3"><span>Payoff Balance</span> <strong class="green-text">$70.00</strong></div>
            </div>
        </div>
        <div class="afford-to-pay">
            <div class="row">
                <div class="col-sm-12 col-md-6 pr-5 setup-plan form-style1">
                    <h5>Setup your Negotiation plan</h5>
                    <form>
                        <div class="form-group">
                            <label>Payment Frequency</label><br />
                            <small>Select a payment schedule to pay your debt</small>
                            <select class="custom-select">
                                <option selected>Choose...</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Amount Paying </label><br />
                            <small>How much you can pay frequently</small>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="12" />
                                <div class="input-group-prepend">
                                    <div class="input-group-text">$</div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="balance-box mt-4">
                        <div class="d-flex align-items-center justify-content-between "><span>Current Balance</span> <strong class="blue-text">$100.00</strong></div>
                        <div class="d-flex align-items-center justify-content-between mt-3"><span>Your Payment Plan Payoff Balance is</span> <strong>$75.00</strong></div>
                        <div class="d-flex align-items-center justify-content-between mt-3">
                            <span>You will be making <strong>5 payment of</strong></span> <strong class="green-text">$12.00</strong>
                        </div>
                        <div class="d-flex align-items-center justify-content-between mt-3">
                            <span>You will be making <strong>1 payment of</strong></span> <strong class="green-text">$15.00</strong>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 right-bx">
                    <div class="calenderSec">
                        <table id="calendar-demo" class="calendar"></table>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center mt-5 mb-5">
            <button type="submit" class="btn btn-blue" data-toggle="modal" data-target="#tryAgain">
                Submit My 1st Offer
            </button>
        </div>
        <div class="text-center help-i">
            <h2>Help! I’ve change my mind</h2>
            <div>
                <a href="choose" class="btn border-btn"> Back to Discount Payoff </a>
                <a href="choose2" class="btn btn-blue"> I Want to Send My Own Offer </a>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade modal-new" id="tryAgain" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Lets Try Again</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-content">
                    <h4>Dran it! looks like the creditor coulnt diligtally approve yoir offer</h4>
                    <p>
                        YouNegotiate strives to provide first class technology solutions that empower consumers to manage their accounts online, without the pressure of collection calls. It is our passion to provide a stress-free online
                        experience that incorporates dynamic options that facilitate the amicable resolution of debt.
                    </p>
                    <div class="text-right pt-4"><a href="#" class="btn btn-blue">Ttry Again</a></div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection @section('script')
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/date-calender.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
<script>
    $("#calendar-demo").dcalendar();
</script>
@endsection
