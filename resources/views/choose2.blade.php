@extends('layout.app-header')
@section('content')
<section class="ceoMail">
   <div class="container">
      <div class="check-step">
         <div class="w-650">
            <a href="choose" class="w-25-1 select active">
               <div><span><img src="{{asset('assets/img/check-white.svg')}}" alt="check"></span></div>
               <h4>Discounted Payoff</h4>
            </a>
            <a href="choose1" class="w-25-1 select active">
               <div><span><img src="{{asset('assets/img/check-white.svg')}}" alt="check"></span></div>
               <h4>Negotiation</h4>
            </a>
            <a href="choose3" class="w-25-1 select">
               <div><span><img src="{{asset('assets/img/check-white.svg')}}" alt="check"></span></div>
               <h4>Payment setup</h4>
            </a>
            <a href="choose3" class="w-25-1">
               <div><span><img src="{{asset('assets/img/check-white.svg')}}" alt="check"></span></div>
               <h4>Confirmation</h4>
            </a>
         </div>
      </div>
      <h2 class="text-center pp-bold-text">Set up a payment plan today and enjoy <span class="blue-text">25.00 discount</span></h2>
      <p class="text-center text-are">Are you ready to start negotiating with the creditor?</p>
      <div class="afford-to-pay">
         <div class="row">
            <div class="col-sm-12 col-md-6 pr-5 setup-plan form-style1">
               <h5>Setup your Negotiation plan</h5>
               <form>
                  <div class="form-group">
                     <label>Payment Frequency</label><br>
                     <small>Select a payment schedule to pay your debt</small>
                     <select class="custom-select">
                        <option selected>Select</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                     </select>
                  </div>
                  <div class="form-group">
                     <label>Amount Paying </label><br>
                     <small>How much you can pay frequently</small>
                     <div class="input-group">
                        <input type="text" class="form-control" placeholder="12">
                        <div class="input-group-prepend">
                           <div class="input-group-text">$</div>
                        </div>
                     </div>
                  </div>
                  <div class="balance-box mt-5 mb-5">
                     <div class="d-flex align-items-center justify-content-between mt-3"><span>Current Balance</span> <strong class="blue-text">$100.00</strong></div>
                     <div class="d-flex align-items-center justify-content-between mt-3"><span>Your Payment Plan Payoff Balance is</span> <strong>$75.00</strong></div>
                     <div class="d-flex align-items-center justify-content-between mt-3"><span>You are trying to  <strong>settle for</strong></span> <strong class="green-text">$60.00</strong></div>
                  </div>
                  <div class="form-group">
                     <label>Contact Preference </label>
                     <small>How you want to communicate</small>
                     <div class="custom-control custom-radio flex-radio">
                        <div>
                           <input type="radio" class="custom-control-input" id="cc1" name="radio-stacked" required>
                           <label class="custom-control-label" for="cc1">Via YouNegotiate</label>
                        </div>
                        <div>
                           <input type="radio" class="custom-control-input" id="cc2" name="radio-stacked" required>
                           <label class="custom-control-label" for="cc2">Email</label>
                        </div>
                        <div>
                           <input type="radio" class="custom-control-input" id="cc3" name="radio-stacked" required>
                           <label class="custom-control-label" for="cc3">SMS</label>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="mb-2">Email</label>
                     <input type="email" class="form-control" placeholder="">
                  </div>
                  <div class="form-group">
                     <label class="mb-2">Phone</label>
                     <input type="text" class="form-control" placeholder="">
                  </div>
               </form>
            </div>
            <div class="col-sm-12 col-md-6 right-bx">
               <div class="calenderSec">
                  <table
                     id="calendar-demo"
                     class="calendar"
                     ></table>
               </div>
               <div class="setup-plan form-style1 mt-5">
                  <form>
                     <div class="form-group">
                        <label>Payment Frequency</label><br>
                        <small>Select a payment schedule to pay your debt</small>
                        <select class="custom-select">
                           <option selected>Select</option>
                           <option value="1">One</option>
                           <option value="2">Two</option>
                           <option value="3">Three</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <label class="mb-2">Note (We will send this along with your custom offer)</label>
                        <textarea class="form-control" rows="5"></textarea>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <div class="text-center help-i mt-5">
         <div>
            <a href="choose" class="btn border-btn"> Back to Discount Payoff </a>
            <button type="submit" class="btn btn-blue"> I Want to Send My Own Offer </button>
         </div>
      </div>
   </div>
</section>
@endsection
@section('script')
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/date-calender.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
<script>
   $("#calendar-demo").dcalendar();
</script>
@endsection