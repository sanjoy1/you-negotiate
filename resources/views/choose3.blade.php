@extends('layout.app-header')
@section('content')
<section class="ceoMail">
   <div class="container">
      <div class="check-step">
         <div class="w-650">
            <a href="choose" class="w-25-1 select active">
               <div><span><img src="{{asset('assets/img/check-white.svg')}}" alt="check"></span></div>
               <h4>Discounted Payoff</h4>
            </a>
            <a href="choose1" class="w-25-1 select active">
               <div><span><img src="{{asset('assets/img/check-white.svg')}}" alt="check"></span></div>
               <h4>Negotiation</h4>
            </a>
            <a href="choose3" class="w-25-1 select active">
               <div><span><img src="{{asset('assets/img/check-white.svg')}}" alt="check"></span></div>
               <h4>Payment setup</h4>
            </a>
            <a href="choose3" class="w-25-1 select">
               <div><span><img src="{{asset('assets/img/check-white.svg')}}" alt="check"></span></div>
               <h4>Confirmation</h4>
            </a>
         </div>
      </div>
      <div class="afford-to-pay">
         <div class="row">
            <div class="col-sm-12 col-md-6 pr-5 ">
               <div class="balance-box">
                  <h3>Payment Schedule</h3>
                  <div class="sm-text">Pay off balance including duscount offer</div>
                  <div class="d-flex align-items-center justify-content-between mt-3"><span>Payment Date</span> <strong>Jun  19, 2022</strong></div>
                  <div class="d-flex align-items-center justify-content-between mt-3"><span>Current Balance</span> <strong class="blue-text">$100.00</strong></div>
                  <div class="d-flex align-items-center justify-content-between mt-3"><span>Payoff Discount</span> <strong class="red-text">-$30.00</strong></div>
                  <div class="d-flex align-items-center justify-content-between mt-3"><span>Payoff Balance</span> <strong class="green-text">$70.00</strong></div>
               </div>
            </div>
            <div class="col-sm-12 col-md-6 right-bx">
               <div class="setup-plan form-style1">
                  <form>
                     <h5>Billing Address</h5>
                     <div class="form-group">
                        <label class="mb-2">Name</label>
                        <input type="text" class="form-control" placeholder="">
                     </div>
                     <div class="form-group">
                        <label class="mb-2">State</label>
                        <input type="text" class="form-control" placeholder="">
                     </div>
                     <div class="form-group">
                        <label class="mb-2">Zip</label>
                        <input type="text" class="form-control" placeholder="">
                     </div>
                     <h5>Payment Option</h5>
                     <div class="form-group">
                        <label class="font18">Credit or Debit Card</label><br>
                        <small>We only accept following credit / dabit cards</small>
                     </div>
                     <div class="form-group">
                        <label class="mb-2">Card Numnber</label>
                        <input type="text" class="form-control" placeholder="">
                     </div>
                     <div class="form-group">
                        <label class="mb-2">Card Holder Name</label>
                        <input type="text" class="form-control" placeholder="">
                     </div>
                     <div class="form-row">
                        <div class="form-group col-md-6">
                           <label >Expiry</label>
                           <input type="text" class="form-control" >
                        </div>
                        <div class="form-group col-md-6">
                           <label >CVV</label>
                           <input type="text" class="form-control" >
                        </div>
                     </div>
                     <div class="form-group mt-4">
                        <label class="font18">UPI Payment apps</label><br>
                        <small>We only accept following credit / dabit cards</small>
                     </div>
                     <div class="form-group ">
                        <label >Card Numnber</label>
                        <input type="text" class="form-control" >
                     </div>
                     <div class="form-group ">
                        <label >Card Holder Name</label>
                        <input type="text" class="form-control" >
                     </div>
                     <div class="form-row">
                        <div class="form-group col-md-6">
                           <label >Expiry</label>
                           <input type="text" class="form-control" >
                        </div>
                        <div class="form-group col-md-6">
                           <label >CVV</label>
                           <input type="text" class="form-control" >
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <div class="text-center help-i mt-5">
         <div class="custom-control custom-checkbox mb-3">
            <input type="checkbox" class="custom-control-input" id="customCheck1">
            <label class="custom-control-label label-relative" for="customCheck1">By entering this site, you are agreeing to the <a href="privacy" class="blue-text">Privacy</a> and <a href="terms" class="blue-text">Terms of Use</a></label>
         </div>
         <div>
            <a href="choose" class="btn border-btn"> Cancel</a>
            <button type="submit" class="btn btn-blue"> Pay </button>
         </div>
      </div>
   </div>
</section>
@endsection
@section('script')
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/date-calender.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
<script>
   $("#calendar-demo").dcalendar();
</script>
@endsection