@extends('layout.app')
@section('content')
<section class="white-common">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="how-it">
               <h2 class="h2-text">Contact Us</h2>
               <p class="artile-1">Should you have questions about this debt, including 
                  collection letter or calls, contact us:
               </p>
               <h5 class="small-heading">Address</h5>
               <p class="sige18"><strong>8400 E Prentice Ave, Penthouse Suite, Greenwood 
                  Village, CO 80111</strong>
               </p>
               <h5 class="small-heading">Phone</h5>
               <p class="sige18"><strong>(866) 679-0839</strong></p>
            </div>
         </div>
         <div class="col-sm-12 col-md-5 col-lg-5 offset-md-1">
            <div class="login-card form-style1">
               <h3>Write to us</h3>
               <form>
                  <div class="form-group">
                     <label>Name <sup>*</sup></label>
                     <input type="text" class="form-control">
                  </div>
                  <div class="form-group">
                     <label>Email <sup>*</sup></label>
                     <input type="text" class="form-control">
                  </div>
                  <div class="form-group">
                     <label>Subject</label>
                     <input type="text" class="form-control">
                  </div>
                  <div class="form-group">
                     <label>Message</label>
                     <textarea rows="4" class="form-control"></textarea>
                  </div>
                  <div class="text-right"><button type="submit" class="btn btn-blue">Submit</button></div>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
@section('script')
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
@endsection