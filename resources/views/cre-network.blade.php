@extends('layout.app')
@section('content')
<section class="home-banner bg-gray-box">
   <div class="container text-center">
      <h1>Consumers are now in control of their <br><span>collection accounts</span></h1>
      <p>With utmost care and respect, we are dedicated to helping  100 million people <br>
         pay their bills and build their dreams
      </p>
      <a href="#" class="btn btn-blue">I’m a consumer</a>
      <div class="video-frame2">
         <iframe width="100%" height="420" src="https://www.youtube.com/embed/6GGEP0Dn0is" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
   </div>
</section>
<section class="white-common">
   <div class="container">
      <div class="row align-items-center">
         <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="how-it">
               <h2 class="h2-text">How it works</h2>
               <p class="artile-1">YouNegotiate® accommodates each creditor environment and provides consumers a centralized platform
                  to settle their debts. We’re fixing the consumer debt problem and increasing your receivables
                  one consumer at a time, every second of the day.
               </p>
               <ul>
                  <li><img src="{{asset('assets/img/check.svg')}}" alt="a" > Creditor Self Enrolls</li>
                  <li><img src="{{asset('assets/img/check.svg')}}" alt="a" > Quickly & Easily Set Up Your Account</li>
                  <li><img src="{{asset('assets/img/check.svg')}}" alt="a" > Upload Consumer Accounts in Any Format</li>
                  <li><img src="{{asset('assets/img/check.svg')}}" alt="a" > Add Your Company URL & the YouNegotiate® Logo in Your Colors to Your Website</li>
                  <li><img src="{{asset('assets/img/check.svg')}}" alt="a" > Consumers Create an Account & Start Paying or Asking for Terms They Can Meet</li>
               </ul>
            </div>
         </div>
         <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="">
               <img src="{{asset('assets/img/Laptop.png')}}" alt="a" class="img-fluid" >
            </div>
         </div>
      </div>
   </div>
</section>
<section class="white-common bg-gray-box over-hidded">
   <div class="container">
      <h2 class="h2-text text-center">Testimonials</h2>
      <p class="artile-1 text-center">What Consumers are saying about YouNegotiate®!</p>
      <section id="testimonial" class="">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div id="testimonialCarousel" class="carousel slide" data-ride="carousel">
                     <div class="carousel-inner" role="listbox">
                        <!-- Slide 1 -->
                        <div class="carousel-item active">
                           <div class="carousel-content">
                              <div class="client-img"><img src="{{asset('assets/img/u1.png')}}" alt="ss"></div>
                              <p class="col-md-10 offset-md-1">
                                 YouNegotiate® offers this easy-to-use service free of charge to all consumers in order to help resolve debts in the way that works best for each individual. No need to speak to a collector or creditor, ever again. Instead, just manage your account with us and get out of debt at your pace.
                              </p>
                              <figure><img src="{{asset('assets/img/star.svg')}}" alt="review"></figure>
                              <h3>Tom Holand</h3>
                           </div>
                        </div>
                        <!-- Slide 2 -->
                        <div class="carousel-item ">
                           <div class="carousel-content">
                              <div class="client-img"><img src="{{asset('assets/img/u1.png')}}" alt="ss"></div>
                              <p class="col-md-10 offset-md-1">
                                 YouNegotiate® offers this easy-to-use service free of charge to all consumers in order to help resolve debts in the way that works best for each individual. No need to speak to a collector or creditor, ever again. Instead, just manage your account with us and get out of debt at your pace.
                              </p>
                              <figure><img src="{{asset('assets/img/star.svg')}}" alt="review"></figure>
                              <h3>Tom Holand</h3>
                           </div>
                        </div>
                        <!-- Slide 3 -->
                        <div class="carousel-item ">
                           <div class="carousel-content">
                              <div class="client-img"><img src="{{asset('assets/img/u1.png')}}" alt="ss"></div>
                              <p class="col-md-10 offset-md-1">
                                 YouNegotiate® offers this easy-to-use service free of charge to all consumers in order to help resolve debts in the way that works best for each individual. No need to speak to a collector or creditor, ever again. Instead, just manage your account with us and get out of debt at your pace.
                              </p>
                              <figure><img src="{{asset('assets/img/star.svg')}}" alt="review"></figure>
                              <h3>Tom Holand</h3>
                           </div>
                        </div>
                        <!-- Slider pre and next arrow -->
                        <a class="carousel-control-prev text-white" href="#testimonialCarousel" role="button" data-slide="prev">
                        <i class=""><img src="{{asset('assets/img/arw-left.svg')}}" alt="arrow"></i>
                        </a>
                        <a class="carousel-control-next text-white" href="#testimonialCarousel" role="button" data-slide="next">
                        <i class=""><img src="{{asset('assets/img/arrow-right.svg')}}" alt="arrow"></i>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </div>
</section>
<section class="white-common">
   <div class="container">
      <h2 class="h2-text text-center">Security is Our Priority</h2>
      <p class="artile-1 text-center">
         This site uses state-of-the-art cryptographic algorithms during data transmission<br>
         (HTTPS with RSA key exchange mechanism and SHA1 128-bit encryption) and in our<br>
         databases (encryption with unique keys). Data is hosted on Amazon AWS infrastructure<br>
         configured to meet SAS70, ISO 27001 and PCI DSS Level 1 certification with 24×7 monitoring.
      </p>
      <div class="row mt-5 text-center mt-5">
         <div class="col-sm-12 col-md-4 col-lg-4">
            <img src="{{asset('assets/img/security03.png')}}" alt="amazon" class="img-fluid">
         </div>
         <div class="col-sm-12 col-md-4 col-lg-4">
            <img src="{{asset('assets/img/download-7.png')}}" alt="ac" class="img-fluid">
         </div>
         <div class="col-sm-12 col-md-4 col-lg-4">
            <img src="{{asset('assets/img/download-8.png')}}" alt="ds" class="img-fluid">
         </div>
      </div>
   </div>
</section>
@endsection
@section('script')
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
@endsection