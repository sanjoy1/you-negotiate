@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <div class="section-body">
         <div class="row">
            <div class="col-12 col-md-3 col-lg-3 mb-4">
               <a href="{{url('dashboard/transaction')}}" class="icon-card-box">
                  <div><img alt="user" src="{{asset('dist/img/credit-card.svg')}}"></div>
                  <h4>View Transaction</h4>
               </a>
            </div>
            <div class="col-12 col-md-3 col-lg-3 mb-4">
               <a href="{{url('dashboard/current-customer')}}" class="icon-card-box">
                  <div><img alt="user" src="{{asset('dist/img/users.svg')}}"></div>
                  <h4>Current Customers</h4>
               </a>
            </div>
            <div class="col-12 col-md-3 col-lg-3 mb-4">
               <a href="{{url('dashboard/deactive-customer')}}" class="icon-card-box">
                  <div><img alt="user" src="{{asset('dist/img/user-x.svg')}}"></div>
                  <h4>Deactivated Customers</h4>
               </a>
            </div>
            <div class="col-12 col-md-3 col-lg-3 mb-4">
               <a href="{{url('dashboard/unsubscribed-list')}}" class="icon-card-box">
                  <div><img alt="user" src="{{asset('dist/img/user-minus.svg')}}"></div>
                  <h4>Unsubscribed List</h4>
               </a>
            </div>
            <div class="col-12 col-md-3 col-lg-3 mb-4">
               <a href="{{url('dashboard/communication-history')}}" class="icon-card-box">
                  <div><img alt="user" src="{{asset('dist/img/message-square.svg')}}"></div>
                  <h4>Communication History</h4>
               </a>
            </div>
            <div class="col-12 col-md-3 col-lg-3 mb-4">
               <a href="{{url('dashboard/cumulative-report')}}" class="icon-card-box">
                  <div><img alt="user" src="{{asset('dist/img/file-text.svg')}}"></div>
                  <h4>Cumulative Report</h4>
               </a>
            </div>
            <div class="col-12 col-md-3 col-lg-3 mb-4">
               <a href="{{url('dashboard/invoice-report')}}" class="icon-card-box">
                  <div><img alt="user" src="{{asset('dist/img/file-minus.svg')}}"></div>
                  <h4>Invoice Report</h4>
               </a>
            </div>
            <div class="col-12 col-md-3 col-lg-3 mb-4">
               <a href="{{url('dashboard/dispute-consumers')}}" class="icon-card-box">
                  <div><img alt="user" src="{{asset('dist/img/meh.svg')}}"></div>
                  <h4>Dispute Consumers</h4>
               </a>
            </div>
         </div>
      </div>
   </section>
</div>
<footer class="main-footer">
   <div class="footer-bg">
      <div class="footer-left">
         <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
      </div>
      <div class="footer-right">
         © 2020 YouNegotiate ™ | All rights reserved
      </div>
   </div>
</footer>
</div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
       $('#example').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
        $('#example2').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
        $('#example3').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
        $('#example4').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
       $('#example5').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
        $('#example6').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   });
</script>
@endsection