@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Dashboard</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12 col-md-3 col-lg-3">
               <a href="{{url('dashboard/current-customer')}}" class="icon-card-box">
                  <div><img alt="user" src="{{asset('dist/img/users.svg')}}"></div>
                  <p>Total Accounts in YN</p>
                  <h4>325</h4>
               </a>
            </div>
            <div class="col-12 col-md-3 col-lg-3">
               <a href="{{url('dashboard/customer-payment')}}" class="icon-card-box">
                  <div><img alt="user" src="{{asset('dist/img/file-text.svg')}}"></div>
                  <p>Customers on Payment Plans</p>
                  <h4>325</h4>
               </a>
            </div>
            <div class="col-12 col-md-3 col-lg-3">
               <a href="{{url('dashboard/30-days')}}" class="icon-card-box">
                  <div><img alt="user" src="{{asset('dist/img/calendar.svg')}}"></div>
                  <p>30-day Forecast Scheduled Payment $</p>
                  <h4>$325.00</h4>
               </a>
            </div>
            <div class="col-12 col-md-3 col-lg-3">
               <a href="{{url('dashboard/30-days')}}" class="icon-card-box">
                  <div><img alt="user" src="{{asset('dist/img/check-circle.svg')}}"></div>
                  <p>Last 30-Day Successful Transactions</p>
                  <h4>$325.00</h4>
               </a>
            </div>
         </div>
         <div class="row mt-5">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Open Negotiation Offers!</h4>
                     <a href="#" class="btn btn-primary">Download Report</a>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive" id="example">
                           <thead>
                              <tr>
                                 <th>
                                    account
                                 </th>
                                 <th>customer name</th>
                                 <th>client</th>
                                 <th>account balance</th>
                                 <th>negotiation date</th>
                                 <th>payment profile</th>
                                 <th>status</th>
                                 <th>action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Yes</strong>
                                 </td>
                                 <td><a href="#" class="link-blue-text">Custom Offer Received! Action Needed!	</a></td>
                                 <td>
                                    <a href="offer-details" class="btn btn-view">View</a> 
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#Block">Reset</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Deactivate</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Yes</strong>
                                 </td>
                                 <td><a href="#" class="link-blue-text">Custom Offer Received! Action Needed!	</a></td>
                                 <td>
                                    <a href="offer-details" class="btn btn-view">View</a> 
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#Block">Reset</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Deactivate</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Yes</strong>
                                 </td>
                                 <td><a href="#" class="link-blue-text">Custom Offer Received! Action Needed!	</a></td>
                                 <td>
                                    <a href="offer-details" class="btn btn-view">View</a> 
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#Block">Reset</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Deactivate</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Yes</strong>
                                 </td>
                                 <td><a href="#" class="link-blue-text">Custom Offer Received! Action Needed!	</a></td>
                                 <td>
                                    <a href="offer-details" class="btn btn-view">View</a> 
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#Block">Reset</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Deactivate</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Yes</strong>
                                 </td>
                                 <td><a href="#" class="link-blue-text">Custom Offer Received! Action Needed!	</a></td>
                                 <td>
                                    <a href="offer-details" class="btn btn-view">View</a> 
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#Block">Reset</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Deactivate</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Yes</strong>
                                 </td>
                                 <td><a href="#" class="link-blue-text">Custom Offer Received! Action Needed!	</a></td>
                                 <td>
                                    <a href="offer-details" class="btn btn-view">View</a> 
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#Block">Reset</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Deactivate</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Yes</strong>
                                 </td>
                                 <td><a href="#" class="link-blue-text">Custom Offer Received! Action Needed!	</a></td>
                                 <td>
                                    <a href="offer-details" class="btn btn-view">View</a> 
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#Block">Reset</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Deactivate</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Yes</strong>
                                 </td>
                                 <td><a href="#" class="link-blue-text">Custom Offer Received! Action Needed!	</a></td>
                                 <td>
                                    <a href="offer-details" class="btn btn-view">View</a> 
                                    <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#Block">Reset</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Deactivate</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Yes</strong>
                                 </td>
                                 <td><a href="#" class="link-blue-text">Custom Offer Received! Action Needed!	</a></td>
                                 <td>
                                    <a href="offer-details" class="btn btn-view">View</a>  <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#Block">Reset</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Deactivate</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Yes</strong>
                                 </td>
                                 <td><a href="#" class="link-blue-text">Custom Offer Received! Action Needed!	</a></td>
                                 <td>
                                    <a href="offer-details" class="btn btn-view">View</a>  <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#Block">Reset</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Deactivate</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Yes</strong>
                                 </td>
                                 <td><a href="#" class="link-blue-text">Custom Offer Received! Action Needed!	</a></td>
                                 <td>
                                    <a href="offer-details" class="btn btn-view">View</a>  <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#Block">Reset</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Deactivate</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Yes</strong>
                                 </td>
                                 <td><a href="#" class="link-blue-text">Custom Offer Received! Action Needed!	</a></td>
                                 <td>
                                    <a href="offer-details" class="btn btn-view">View</a>  <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#Block">Reset</a>
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#small-modal">Deactivate</a>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Successfully Negotiated but Pending Payment Setup </h4>
                     <a href="#" class="btn btn-primary">Download Report</a>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive" id="example2">
                           <thead>
                              <tr>
                                 <th>
                                    account
                                 </th>
                                 <th>customer name</th>
                                 <th>client</th>
                                 <th>Sub Account 1 id</th>
                                 <th>sub account 2 id</th>
                                 <th>current balance</th>
                                 <th>acton</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Monthly</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Monthly</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Monthly</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Monthly</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Monthly</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Monthly</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Monthly</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Monthly</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Monthly</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Monthly</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Monthly</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Monthly</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>Addidas</strong>
                                 </td>
                                 <td>
                                    $200.00
                                 </td>
                                 <td>2021-11-17</td>
                                 <td>
                                    <strong>Monthly</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a> 
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Counter Offers! </h4>
                     <a href="#" class="btn btn-primary">Download Report</a>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive" id="example3">
                           <thead>
                              <tr>
                                 <th>
                                    account
                                 </th>
                                 <th>custmer name</th>
                                 <th>mobile number</th>
                                 <th>email</th>
                                 <th>current balance</th>
                                 <th>see counter offer</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>9844455511</strong>
                                 </td>
                                 <td>
                                    <strong>roymustang@gmail.com</strong>
                                 </td>
                                 <td>
                                    <strong>$1324.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#cf">View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>9844455511</strong>
                                 </td>
                                 <td>
                                    <strong>roymustang@gmail.com</strong>
                                 </td>
                                 <td>
                                    <strong>$1324.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#cf">View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>9844455511</strong>
                                 </td>
                                 <td>
                                    <strong>roymustang@gmail.com</strong>
                                 </td>
                                 <td>
                                    <strong>$1324.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#cf">View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>9844455511</strong>
                                 </td>
                                 <td>
                                    <strong>roymustang@gmail.com</strong>
                                 </td>
                                 <td>
                                    <strong>$1324.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#cf">View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>9844455511</strong>
                                 </td>
                                 <td>
                                    <strong>roymustang@gmail.com</strong>
                                 </td>
                                 <td>
                                    <strong>$1324.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#cf">View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>9844455511</strong>
                                 </td>
                                 <td>
                                    <strong>roymustang@gmail.com</strong>
                                 </td>
                                 <td>
                                    <strong>$1324.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#cf">View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>9844455511</strong>
                                 </td>
                                 <td>
                                    <strong>roymustang@gmail.com</strong>
                                 </td>
                                 <td>
                                    <strong>$1324.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#cf">View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>9844455511</strong>
                                 </td>
                                 <td>
                                    <strong>roymustang@gmail.com</strong>
                                 </td>
                                 <td>
                                    <strong>$1324.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#cf">View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>9844455511</strong>
                                 </td>
                                 <td>
                                    <strong>roymustang@gmail.com</strong>
                                 </td>
                                 <td>
                                    <strong>$1324.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#cf">View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>9844455511</strong>
                                 </td>
                                 <td>
                                    <strong>roymustang@gmail.com</strong>
                                 </td>
                                 <td>
                                    <strong>$1324.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#cf">View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>9844455511</strong>
                                 </td>
                                 <td>
                                    <strong>roymustang@gmail.com</strong>
                                 </td>
                                 <td>
                                    <strong>$1324.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#cf">View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>9844455511</strong>
                                 </td>
                                 <td>
                                    <strong>roymustang@gmail.com</strong>
                                 </td>
                                 <td>
                                    <strong>$1324.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#cf">View</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a href="consumer-profile" class="text-black">32435666661</a>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>9844455511</strong>
                                 </td>
                                 <td>
                                    <strong>roymustang@gmail.com</strong>
                                 </td>
                                 <td>
                                    <strong>$1324.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#cf">View</a> 
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Recent Transactions <i class="info-style" data-toggle="tooltip" data-placement="top" title="" data-original-title="Run A Report">!</i> </h4>
                     <a href="#" class="btn btn-primary">Download Report</a>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive" id="example4">
                           <thead>
                              <tr>
                                 <th>
                                    date time
                                 </th>
                                 <th>account</th>
                                 <th>customer name</th>
                                 <th>transaction id</th>
                                 <th>transaction ammount</th>
                                 <th>status</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>d45d2a63-aae3-4b21-a6c9-2670223f693d</strong>
                                 </td>
                                 <td>
                                    <strong>$200.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">Processed</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>d45d2a63-aae3-4b21-a6c9-2670223f693d</strong>
                                 </td>
                                 <td>
                                    <strong>$200.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">Processed</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>d45d2a63-aae3-4b21-a6c9-2670223f693d</strong>
                                 </td>
                                 <td>
                                    <strong>$200.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">Processed</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>d45d2a63-aae3-4b21-a6c9-2670223f693d</strong>
                                 </td>
                                 <td>
                                    <strong>$200.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">Processed</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>d45d2a63-aae3-4b21-a6c9-2670223f693d</strong>
                                 </td>
                                 <td>
                                    <strong>$200.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">Processed</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>d45d2a63-aae3-4b21-a6c9-2670223f693d</strong>
                                 </td>
                                 <td>
                                    <strong>$200.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">Processed</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>d45d2a63-aae3-4b21-a6c9-2670223f693d</strong>
                                 </td>
                                 <td>
                                    <strong>$200.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">Processed</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>d45d2a63-aae3-4b21-a6c9-2670223f693d</strong>
                                 </td>
                                 <td>
                                    <strong>$200.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">Processed</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>d45d2a63-aae3-4b21-a6c9-2670223f693d</strong>
                                 </td>
                                 <td>
                                    <strong>$200.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">Processed</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>d45d2a63-aae3-4b21-a6c9-2670223f693d</strong>
                                 </td>
                                 <td>
                                    <strong>$200.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">Processed</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>d45d2a63-aae3-4b21-a6c9-2670223f693d</strong>
                                 </td>
                                 <td>
                                    <strong>$200.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">Processed</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>d45d2a63-aae3-4b21-a6c9-2670223f693d</strong>
                                 </td>
                                 <td>
                                    <strong>$200.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">Processed</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>d45d2a63-aae3-4b21-a6c9-2670223f693d</strong>
                                 </td>
                                 <td>
                                    <strong>$200.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">Processed</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>d45d2a63-aae3-4b21-a6c9-2670223f693d</strong>
                                 </td>
                                 <td>
                                    <strong>$200.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">Processed</a> 
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Upcoming Transactions  <i class="info-style" data-toggle="tooltip" data-placement="top" title="" data-original-title="Run A Report">!</i> </h4>
                     <a href="#" class="btn btn-primary">Download Report</a>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive" id="example5">
                           <thead>
                              <tr>
                                 <th>
                                    date time
                                 </th>
                                 <th>account</th>
                                 <th>customer name</th>
                                 <th>client name</th>
                                 <th>transaction ammount</th>
                                 <th>Card/ACH</th>
                                 <th>Customer notes</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-03-09 06:19:44</strong>
                                 </td>
                                 <td><strong>32354551</strong></td>
                                 <td>
                                    <strong>Roy Mustang</strong>
                                 </td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="text-green">CC</a> 
                                 </td>
                                 <td>
                                    <strong class="mr-2">-</strong>
                                    <a href="consumer-profile" class="btn btn-view">Account Profile</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#re-sc">Re-Schedule</a>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Disputed Records </h4>
                     <a href="#" class="btn btn-primary">Download Report</a>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive" id="example6">
                           <thead>
                              <tr>
                                 <th>
                                    account number
                                 </th>
                                 <th>consumer name</th>
                                 <th>current balance</th>
                                 <th>status</th>
                                 <th>dispute date</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <strong>4353566661</strong>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>$400.00</strong>
                                 </td>
                                 <td>
                                    <strong style="color:red;">Disputed</strong>
                                 </td>
                                 <td>
                                    <strong>2022-02-01 09:00:04</strong>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>4353566661</strong>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>$400.00</strong>
                                 </td>
                                 <td>
                                    <strong style="color:red;">Disputed</strong>
                                 </td>
                                 <td>
                                    <strong>2022-02-01 09:00:04</strong>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>4353566661</strong>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>$400.00</strong>
                                 </td>
                                 <td>
                                    <strong style="color:red;">Disputed</strong>
                                 </td>
                                 <td>
                                    <strong>2022-02-01 09:00:04</strong>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>4353566661</strong>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>$400.00</strong>
                                 </td>
                                 <td>
                                    <strong style="color:red;">Disputed</strong>
                                 </td>
                                 <td>
                                    <strong>2022-02-01 09:00:04</strong>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>4353566661</strong>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>$400.00</strong>
                                 </td>
                                 <td>
                                    <strong style="color:red;">Disputed</strong>
                                 </td>
                                 <td>
                                    <strong>2022-02-01 09:00:04</strong>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>4353566661</strong>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>$400.00</strong>
                                 </td>
                                 <td>
                                    <strong style="color:red;">Disputed</strong>
                                 </td>
                                 <td>
                                    <strong>2022-02-01 09:00:04</strong>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>4353566661</strong>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>$400.00</strong>
                                 </td>
                                 <td>
                                    <strong style="color:red;">Disputed</strong>
                                 </td>
                                 <td>
                                    <strong>2022-02-01 09:00:04</strong>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>4353566661</strong>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>$400.00</strong>
                                 </td>
                                 <td>
                                    <strong style="color:red;">Disputed</strong>
                                 </td>
                                 <td>
                                    <strong>2022-02-01 09:00:04</strong>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>4353566661</strong>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>$400.00</strong>
                                 </td>
                                 <td>
                                    <strong style="color:red;">Disputed</strong>
                                 </td>
                                 <td>
                                    <strong>2022-02-01 09:00:04</strong>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>4353566661</strong>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>$400.00</strong>
                                 </td>
                                 <td>
                                    <strong style="color:red;">Disputed</strong>
                                 </td>
                                 <td>
                                    <strong>2022-02-01 09:00:04</strong>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>4353566661</strong>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>$400.00</strong>
                                 </td>
                                 <td>
                                    <strong style="color:red;">Disputed</strong>
                                 </td>
                                 <td>
                                    <strong>2022-02-01 09:00:04</strong>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>4353566661</strong>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>$400.00</strong>
                                 </td>
                                 <td>
                                    <strong style="color:red;">Disputed</strong>
                                 </td>
                                 <td>
                                    <strong>2022-02-01 09:00:04</strong>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>4353566661</strong>
                                 </td>
                                 <td><strong>Roy Mustang</strong></td>
                                 <td>
                                    <strong>$400.00</strong>
                                 </td>
                                 <td>
                                    <strong style="color:red;">Disputed</strong>
                                 </td>
                                 <td>
                                    <strong>2022-02-01 09:00:04</strong>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
<footer class="main-footer">
   <div class="footer-bg">
      <div class="footer-left">
         <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
      </div>
      <div class="footer-right">
         © 2020 YouNegotiate ™ | All rights reserved
      </div>
   </div>
</footer>
</div>
</div>
<!-- Modal -->
<div class="modal modal-new fade" id="exampleModal">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Soumabha Pal</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <div class="row">
               <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                  <h4>Account</h4>
                  <h5>1571724</h5>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                  <h4>Client Name</h4>
                  <h5>YouNegotiate</h5>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                  <h4>Account Balance</h4>
                  <h5>$2.00</h5>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                  <h4>Negotiation Term</h4>
                  <h5>Single Payment</h5>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                  <h4>Approved By</h4>
                  <h5>Auto Approve</h5>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                  <h4>Plan</h4>
                  <h5>1 Time Payment</h5>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                  <h4>Negotiation Completed On</h4>
                  <h5>2020-09-15 06:30:01</h5>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                  <h4>First Payment Date</h4>
                  <h5>2020-09-17</h5>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                  <h4>Payment Profile Setup</h4>
                  <h5>Yes</h5>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Modal counter offer -->
<div class="modal modal-new fade" id="cf">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Counter Offer</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <div class="table-border-st">
               <table class="table width-responsive">
                  <thead>
                     <tr>
                        <th>Offer Description</th>
                        <th>Original Offer </th>
                        <th>Counter Offer</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>Discounted PIF Amount</td>
                        <td>$5,250.00</td>
                        <td>$4,250.00</td>
                     </tr>
                     <tr>
                        <td>Discounted Settlement Amount</td>
                        <td>$5,250.00</td>
                        <td>$4,250.00</td>
                     </tr>
                     <tr>
                        <td>Min.Monthly Payment</td>
                        <td>$5,250.00</td>
                        <td>$4,250.00</td>
                     </tr>
                     <tr>
                        <td>First Payment Date</td>
                        <td>$5,250.00</td>
                        <td>$4,250.00</td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="row mt-4">
               <div class="col-sm-12">
                  <h4>Note</h4>
                  <h5>Sed ut perspiciatis unde omnis iste natus error sit voluptatem 
                     accusantium doloremque laudantium
                  </h5>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Modal re-schedule -->
<div class="modal modal-new fade" id="re-sc">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Need to change the date slightly? No Problem!</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <div class="form-area">
               <div class="form-group">
                  <label>Change Date</label>
                  <input type="date" class="form-control" value="DD/MM/YYYY">
               </div>
            </div>
            <div class="row mt-4">
               <div class="col-sm-12 mb-2">
                  <h4>Name</h4>
                  <h5>Prateek Gupta</h5>
               </div>
               <div class="col-sm-12 mb-2">
                  <h4>Existing Schedule Date</h4>
                  <h5>2020-10-28</h5>
               </div>
            </div>
            <div class="text-right">
               <a href="#" class="btn border-btn">Cancel</a>
               <a href="#" class="btn btn-view">Submit</a>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal modal-new md-sm fade" id="small-modal" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Reset Alart</h5>
            <button type="button" class="close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Do you want to reset this ?</h2>
            <div class="text-right mt-4">
               <button type="submit" class="btn border-btn">Cancel</button>
               <button type="submit" class="btn blue-btn">Yes</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal modal-new md-sm fade" id="Block" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Block</h5>
            <button type="button" class="close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Do you want to block this user?</h2>
            <div class="text-right mt-4">
               <button type="submit" class="btn border-btn">Cancel</button>
               <button type="submit" class="btn blue-btn">Yes</button>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
       $('#example').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
        $('#example2').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
        $('#example3').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
        $('#example4').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
       $('#example5').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
        $('#example6').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   });
</script>
@endsection