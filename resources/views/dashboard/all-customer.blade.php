@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>My Customer's YouNegotiate Experience</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>All Customers</h4>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive" id="example2">
                           <thead>
                              <tr>
                                 <th>
                                    Event login
                                 </th>
                                 <th>account</th>
                                 <th>consumer name</th>
                                 <th>current balance</th>
                                 <th>status</th>
                                 <th>total pages</th>
                                 <th>action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <strong>2022-06-13 21:32:05</strong>
                                 </td>
                                 <td><strong>245661</strong></td>
                                 <td>
                                    <strong>Edward Elric</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td><a class="text-org">Offer Accepted-Pending Payment Setup</a></td>
                                 <td>
                                    <strong>3</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#act-details">View Session</a>
                                    <a href="#" class="btn btn-view">Watch Video</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-06-13 21:32:05</strong>
                                 </td>
                                 <td><strong>245661</strong></td>
                                 <td>
                                    <strong>Edward Elric</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td><a class="text-org">Offer Accepted-Pending Payment Setup</a></td>
                                 <td>
                                    <strong>3</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#act-details">View Session</a>
                                    <a href="#" class="btn btn-view">Watch Video</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-06-13 21:32:05</strong>
                                 </td>
                                 <td><strong>245661</strong></td>
                                 <td>
                                    <strong>Edward Elric</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td><a class="text-org">Offer Accepted-Pending Payment Setup</a></td>
                                 <td>
                                    <strong>3</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#act-details">View Session</a>
                                    <a href="#" class="btn btn-view">Watch Video</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-06-13 21:32:05</strong>
                                 </td>
                                 <td><strong>245661</strong></td>
                                 <td>
                                    <strong>Edward Elric</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td><a class="text-org">Offer Accepted-Pending Payment Setup</a></td>
                                 <td>
                                    <strong>3</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#act-details">View Session</a>
                                    <a href="#" class="btn btn-view">Watch Video</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-06-13 21:32:05</strong>
                                 </td>
                                 <td><strong>245661</strong></td>
                                 <td>
                                    <strong>Edward Elric</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td><a class="text-org">Offer Accepted-Pending Payment Setup</a></td>
                                 <td>
                                    <strong>3</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#act-details">View Session</a>
                                    <a href="#" class="btn btn-view">Watch Video</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-06-13 21:32:05</strong>
                                 </td>
                                 <td><strong>245661</strong></td>
                                 <td>
                                    <strong>Edward Elric</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td><a class="text-org">Offer Accepted-Pending Payment Setup</a></td>
                                 <td>
                                    <strong>3</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#act-details">View Session</a>
                                    <a href="#" class="btn btn-view">Watch Video</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-06-13 21:32:05</strong>
                                 </td>
                                 <td><strong>245661</strong></td>
                                 <td>
                                    <strong>Edward Elric</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td><a class="text-org">Offer Accepted-Pending Payment Setup</a></td>
                                 <td>
                                    <strong>3</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#act-details">View Session</a>
                                    <a href="#" class="btn btn-view">Watch Video</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-06-13 21:32:05</strong>
                                 </td>
                                 <td><strong>245661</strong></td>
                                 <td>
                                    <strong>Edward Elric</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td><a class="text-org">Offer Accepted-Pending Payment Setup</a></td>
                                 <td>
                                    <strong>3</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#act-details">View Session</a>
                                    <a href="#" class="btn btn-view">Watch Video</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-06-13 21:32:05</strong>
                                 </td>
                                 <td><strong>245661</strong></td>
                                 <td>
                                    <strong>Edward Elric</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td><a class="text-org">Offer Accepted-Pending Payment Setup</a></td>
                                 <td>
                                    <strong>3</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#act-details">View Session</a>
                                    <a href="#" class="btn btn-view">Watch Video</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-06-13 21:32:05</strong>
                                 </td>
                                 <td><strong>245661</strong></td>
                                 <td>
                                    <strong>Edward Elric</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td><a class="text-org">Offer Accepted-Pending Payment Setup</a></td>
                                 <td>
                                    <strong>3</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#act-details">View Session</a>
                                    <a href="#" class="btn btn-view">Watch Video</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-06-13 21:32:05</strong>
                                 </td>
                                 <td><strong>245661</strong></td>
                                 <td>
                                    <strong>Edward Elric</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td><a class="text-org">Offer Accepted-Pending Payment Setup</a></td>
                                 <td>
                                    <strong>3</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#act-details">View Session</a>
                                    <a href="#" class="btn btn-view">Watch Video</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-06-13 21:32:05</strong>
                                 </td>
                                 <td><strong>245661</strong></td>
                                 <td>
                                    <strong>Edward Elric</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td><a class="text-org">Offer Accepted-Pending Payment Setup</a></td>
                                 <td>
                                    <strong>3</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#act-details">View Session</a>
                                    <a href="#" class="btn btn-view">Watch Video</a>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </section>
   </div>
   <footer class="main-footer">
      <div class="footer-bg">
         <div class="footer-left">
            <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
            © 2020 YouNegotiate ™ | All rights reserved
         </div>
      </div>
   </footer>
</div>
</div>
<!-- Modal -->
<div class="modal modal-new fade" id="act-details" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Activity Details</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <div class="row">
               <div class="col-sm-12">
                  <h4>Name & Account No</h4>
                  <h5><strong>Edward Elric</strong><br>
                  566661242</h5>

               </div>
               <div class="col-sm-12">
                 <div class="table-border-st mt-4">
               <table class="table width-responsive w-100" id="ex1">
                  <thead>
                     <tr>
                        <th>time</th>
                        <th>event</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>2022-06-14 00:32:41</td>
                        <td>Consumer opted for Custom Offer</td>
                     </tr>
                     <tr>
                        <td>2022-06-14 00:32:41</td>
                        <td>Consumer opted for Custom Offer</td>
                     </tr>
                     <tr>
                        <td>2022-06-14 00:32:41</td>
                        <td>Consumer opted for Custom Offer</td>
                     </tr>
                     <tr>
                        <td>2022-06-14 00:32:41</td>
                        <td>Consumer opted for Custom Offer</td>
                     </tr>
                     <tr>
                        <td>2022-06-14 00:32:41</td>
                        <td>Consumer opted for Custom Offer</td>
                     </tr>

                    
                  </tbody>
               </table>
            </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
   
   $('#ex1').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
        $('#example2').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
   });
</script>
@endsection