@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Profile</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <ul class="step-list">
                        <li class="select active"><a href="profile"><i class="nav-icon text-14b i-Yes"></i> Company Profile</a></li>
                        <li class="select active"><i class="fa fa-angles-right"></i></li>
                        <li class="select"><a href="bank-details"><i class="nav-icon text-14b i-Yes"></i> Bank Details</a></li>
                        <li><i class="fa fa-angles-right"></i></li>
                        <li><a href="master-account"><i class="nav-icon text-14b i-Yes"></i> Master Merchant Account</a></li>
                        <li><i class="fa fa-angles-right"></i></li>
                        <li><a href="contact-details"><i class="nav-icon text-14b i-Yes"></i> Contact Details</a></li>
                     </ul>
                     <div>
                        <a href="profile" class="btn btn-reset">Back</a>
                        <a href="master-account" class="btn btn-primary">Next</a>
                     </div>
                  </div>
                  <hr>
                  <div class="card-body form-area">
                     <h3>Bank Account Details</h3>
                     <div class="form-row">
                        <div class="form-group col-md-6">
                           <label>Bank Name <sup>*</sup></label>
                           <input type="text" class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                           <label>Bank Address <sup>*</sup></label>
                           <input type="text" class="form-control">
                        </div>
                     </div>
                     <div class="form-row">
                        <div class="form-group col-md-6">
                           <label>Bank City <sup>*</sup></label>
                           <input type="text" class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                           <label>Bank State <sup>*</sup></label>
                           <input type="text" class="form-control">
                        </div>
                     </div>
                     <div class="form-row">
                        <div class="form-group col-md-6">
                           <label>Bank Zip <sup>*</sup></label>
                           <input type="text" class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                           <label>Company Name (Per Bank Account) <sup>*</sup></label>
                           <input type="text" class="form-control">
                        </div>
                     </div>
                     <div class="form-row">
                        <div class="form-group col-md-6">
                           <label>Company Address (Per Bank Account) <sup>*</sup></label>
                           <input type="text" class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                           <label>Company City (Per Bank Account) <sup>*</sup></label>
                           <input type="text" class="form-control">
                        </div>
                     </div>
                     <div class="form-row">
                        <div class="form-group col-md-6">
                           <label>Company State (Per Bank Account) <sup>*</sup></label>
                           <select class="custom-select">
                              <option selected="">Select</option>
                              <option value="1">US</option>
                              <option value="2">Two</option>
                              <option value="3">Three</option>
                           </select>
                        </div>
                        <div class="form-group col-md-6">
                           <label>Company Zip (Per Bank Account) <sup>*</sup></label>
                           <input type="text" class="form-control">
                        </div>
                     </div>
                     <div class="form-row">
                        <div class="form-group col-md-6">
                           <label>Bank Account Type <sup>*</sup></label>
                           <select class="custom-select">
                              <option selected="">Select</option>
                              <option value="1">US</option>
                              <option value="2">Two</option>
                              <option value="3">Three</option>
                           </select>
                        </div>
                        <div class="form-group col-md-6">
                           <label>Bank Routing <sup>*</sup></label>
                           <input type="text" class="form-control">
                        </div>
                     </div>
                     <div class="form-row">
                        <div class="form-group col-md-6">
                           <label>Bank Account Number <sup>*</sup></label>
                           <select class="custom-select">
                              <option selected="">Select</option>
                              <option value="1">US</option>
                              <option value="2">Two</option>
                              <option value="3">Three</option>
                           </select>
                        </div>
                     </div>
                     <div class="text-right">
                        <a href="profile" class="btn btn-reset">Back</a>
                        <a href="master-account" class="btn btn-view">Next</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </section>
   </div>
   <footer class="main-footer">
      <div class="footer-bg">
         <div class="footer-left">
            <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
            © 2020 YouNegotiate ™ | All rights reserved
         </div>
      </div>
   </footer>
</div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
       $('#example').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
        $('#example2').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
        $('#example3').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
        $('#example4').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
       $('#example5').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
        $('#example6').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
        $('#example7').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   });
</script>
@endsection