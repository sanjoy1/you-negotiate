@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Campaign Tracker (Review all your previous communication campaigns)</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>My Campaign Tracker</h4>
                     <a href="#" class="btn btn-reset">Export CSV</a>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive fil-width-big" id="example2">
                           <thead>
                              <tr>
                                 <th>
                                    Sent on
                                 </th>
                                 <th>type</th>
                                 <th>template name</th>
                                 <th>group name</th>
                                 <th>subject line</th>
                                 <th>Unique accounts</th>
                                 <th>Total sent</th>
                                 <th>Total delevered</th>
                                 <th>% Delivered</th>
                                 <th>% Clicked Link</th>
                                 <th>% Entered PII</th>
                                 <th>PIF %</th>
                                 <th>PPL %</th>
                                 <th>Sent Custom Offer %</th>
                                 <th>action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <strong>2022-05-31 03:38:30</strong>
                                 </td>
                                 <td><strong>Email</strong></td>
                                 <td>
                                    <strong>Intro Template</strong>
                                 </td>
                                 <td><strong>Happy Group</strong></td>
                                 <td><strong>Welcome to younegotiate</strong></td>
                                 <td>
                                    <strong>324</strong>
                                 </td>
                                 <td><strong>318</strong></td>
                                 <td><strong>4</strong></td>
                                 <td>
                                    <strong>14%</strong>
                                 </td>
                                 <td><strong>20%</strong></td>
                                 <td><strong>10%</strong></td>
                                 <td>
                                    <strong>5%</strong>
                                 </td>
                                 <td><strong>2%</strong></td>
                                 <td>
                                    <strong>12%</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view">Re-Run</a>
                                    <a href="#" class="btn btn-b">Export Consumers</a>
                                    <a href="#" class="btn btn-deactive">Export Failed List</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#failed-list">Create Group From Failed List</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-05-31 03:38:30</strong>
                                 </td>
                                 <td><strong>Email</strong></td>
                                 <td>
                                    <strong>Intro Template</strong>
                                 </td>
                                 <td><strong>Happy Group</strong></td>
                                 <td><strong>Welcome to younegotiate</strong></td>
                                 <td>
                                    <strong>324</strong>
                                 </td>
                                 <td><strong>318</strong></td>
                                 <td><strong>4</strong></td>
                                 <td>
                                    <strong>14%</strong>
                                 </td>
                                 <td><strong>20%</strong></td>
                                 <td><strong>10%</strong></td>
                                 <td>
                                    <strong>5%</strong>
                                 </td>
                                 <td><strong>2%</strong></td>
                                 <td>
                                    <strong>12%</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view">Re-Run</a>
                                    <a href="#" class="btn btn-b">Export Consumers</a>
                                    <a href="#" class="btn btn-deactive">Export Failed List</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#failed-list">Create Group From Failed List</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-05-31 03:38:30</strong>
                                 </td>
                                 <td><strong>Email</strong></td>
                                 <td>
                                    <strong>Intro Template</strong>
                                 </td>
                                 <td><strong>Happy Group</strong></td>
                                 <td><strong>Welcome to younegotiate</strong></td>
                                 <td>
                                    <strong>324</strong>
                                 </td>
                                 <td><strong>318</strong></td>
                                 <td><strong>4</strong></td>
                                 <td>
                                    <strong>14%</strong>
                                 </td>
                                 <td><strong>20%</strong></td>
                                 <td><strong>10%</strong></td>
                                 <td>
                                    <strong>5%</strong>
                                 </td>
                                 <td><strong>2%</strong></td>
                                 <td>
                                    <strong>12%</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view">Re-Run</a>
                                    <a href="#" class="btn btn-b">Export Consumers</a>
                                    <a href="#" class="btn btn-deactive">Export Failed List</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#failed-list">Create Group From Failed List</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-05-31 03:38:30</strong>
                                 </td>
                                 <td><strong>Email</strong></td>
                                 <td>
                                    <strong>Intro Template</strong>
                                 </td>
                                 <td><strong>Happy Group</strong></td>
                                 <td><strong>Welcome to younegotiate</strong></td>
                                 <td>
                                    <strong>324</strong>
                                 </td>
                                 <td><strong>318</strong></td>
                                 <td><strong>4</strong></td>
                                 <td>
                                    <strong>14%</strong>
                                 </td>
                                 <td><strong>20%</strong></td>
                                 <td><strong>10%</strong></td>
                                 <td>
                                    <strong>5%</strong>
                                 </td>
                                 <td><strong>2%</strong></td>
                                 <td>
                                    <strong>12%</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view">Re-Run</a>
                                    <a href="#" class="btn btn-b">Export Consumers</a>
                                    <a href="#" class="btn btn-deactive">Export Failed List</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#failed-list">Create Group From Failed List</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-05-31 03:38:30</strong>
                                 </td>
                                 <td><strong>Email</strong></td>
                                 <td>
                                    <strong>Intro Template</strong>
                                 </td>
                                 <td><strong>Happy Group</strong></td>
                                 <td><strong>Welcome to younegotiate</strong></td>
                                 <td>
                                    <strong>324</strong>
                                 </td>
                                 <td><strong>318</strong></td>
                                 <td><strong>4</strong></td>
                                 <td>
                                    <strong>14%</strong>
                                 </td>
                                 <td><strong>20%</strong></td>
                                 <td><strong>10%</strong></td>
                                 <td>
                                    <strong>5%</strong>
                                 </td>
                                 <td><strong>2%</strong></td>
                                 <td>
                                    <strong>12%</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view">Re-Run</a>
                                    <a href="#" class="btn btn-b">Export Consumers</a>
                                    <a href="#" class="btn btn-deactive">Export Failed List</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#failed-list">Create Group From Failed List</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-05-31 03:38:30</strong>
                                 </td>
                                 <td><strong>Email</strong></td>
                                 <td>
                                    <strong>Intro Template</strong>
                                 </td>
                                 <td><strong>Happy Group</strong></td>
                                 <td><strong>Welcome to younegotiate</strong></td>
                                 <td>
                                    <strong>324</strong>
                                 </td>
                                 <td><strong>318</strong></td>
                                 <td><strong>4</strong></td>
                                 <td>
                                    <strong>14%</strong>
                                 </td>
                                 <td><strong>20%</strong></td>
                                 <td><strong>10%</strong></td>
                                 <td>
                                    <strong>5%</strong>
                                 </td>
                                 <td><strong>2%</strong></td>
                                 <td>
                                    <strong>12%</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view">Re-Run</a>
                                    <a href="#" class="btn btn-b">Export Consumers</a>
                                    <a href="#" class="btn btn-deactive">Export Failed List</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#failed-list">Create Group From Failed List</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-05-31 03:38:30</strong>
                                 </td>
                                 <td><strong>Email</strong></td>
                                 <td>
                                    <strong>Intro Template</strong>
                                 </td>
                                 <td><strong>Happy Group</strong></td>
                                 <td><strong>Welcome to younegotiate</strong></td>
                                 <td>
                                    <strong>324</strong>
                                 </td>
                                 <td><strong>318</strong></td>
                                 <td><strong>4</strong></td>
                                 <td>
                                    <strong>14%</strong>
                                 </td>
                                 <td><strong>20%</strong></td>
                                 <td><strong>10%</strong></td>
                                 <td>
                                    <strong>5%</strong>
                                 </td>
                                 <td><strong>2%</strong></td>
                                 <td>
                                    <strong>12%</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view">Re-Run</a>
                                    <a href="#" class="btn btn-b">Export Consumers</a>
                                    <a href="#" class="btn btn-deactive">Export Failed List</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#failed-list">Create Group From Failed List</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-05-31 03:38:30</strong>
                                 </td>
                                 <td><strong>Email</strong></td>
                                 <td>
                                    <strong>Intro Template</strong>
                                 </td>
                                 <td><strong>Happy Group</strong></td>
                                 <td><strong>Welcome to younegotiate</strong></td>
                                 <td>
                                    <strong>324</strong>
                                 </td>
                                 <td><strong>318</strong></td>
                                 <td><strong>4</strong></td>
                                 <td>
                                    <strong>14%</strong>
                                 </td>
                                 <td><strong>20%</strong></td>
                                 <td><strong>10%</strong></td>
                                 <td>
                                    <strong>5%</strong>
                                 </td>
                                 <td><strong>2%</strong></td>
                                 <td>
                                    <strong>12%</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view">Re-Run</a>
                                    <a href="#" class="btn btn-b">Export Consumers</a>
                                    <a href="#" class="btn btn-deactive">Export Failed List</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#failed-list">Create Group From Failed List</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-05-31 03:38:30</strong>
                                 </td>
                                 <td><strong>Email</strong></td>
                                 <td>
                                    <strong>Intro Template</strong>
                                 </td>
                                 <td><strong>Happy Group</strong></td>
                                 <td><strong>Welcome to younegotiate</strong></td>
                                 <td>
                                    <strong>324</strong>
                                 </td>
                                 <td><strong>318</strong></td>
                                 <td><strong>4</strong></td>
                                 <td>
                                    <strong>14%</strong>
                                 </td>
                                 <td><strong>20%</strong></td>
                                 <td><strong>10%</strong></td>
                                 <td>
                                    <strong>5%</strong>
                                 </td>
                                 <td><strong>2%</strong></td>
                                 <td>
                                    <strong>12%</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view">Re-Run</a>
                                    <a href="#" class="btn btn-b">Export Consumers</a>
                                    <a href="#" class="btn btn-deactive">Export Failed List</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#failed-list">Create Group From Failed List</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-05-31 03:38:30</strong>
                                 </td>
                                 <td><strong>Email</strong></td>
                                 <td>
                                    <strong>Intro Template</strong>
                                 </td>
                                 <td><strong>Happy Group</strong></td>
                                 <td><strong>Welcome to younegotiate</strong></td>
                                 <td>
                                    <strong>324</strong>
                                 </td>
                                 <td><strong>318</strong></td>
                                 <td><strong>4</strong></td>
                                 <td>
                                    <strong>14%</strong>
                                 </td>
                                 <td><strong>20%</strong></td>
                                 <td><strong>10%</strong></td>
                                 <td>
                                    <strong>5%</strong>
                                 </td>
                                 <td><strong>2%</strong></td>
                                 <td>
                                    <strong>12%</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view">Re-Run</a>
                                    <a href="#" class="btn btn-b">Export Consumers</a>
                                    <a href="#" class="btn btn-deactive">Export Failed List</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#failed-list">Create Group From Failed List</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-05-31 03:38:30</strong>
                                 </td>
                                 <td><strong>Email</strong></td>
                                 <td>
                                    <strong>Intro Template</strong>
                                 </td>
                                 <td><strong>Happy Group</strong></td>
                                 <td><strong>Welcome to younegotiate</strong></td>
                                 <td>
                                    <strong>324</strong>
                                 </td>
                                 <td><strong>318</strong></td>
                                 <td><strong>4</strong></td>
                                 <td>
                                    <strong>14%</strong>
                                 </td>
                                 <td><strong>20%</strong></td>
                                 <td><strong>10%</strong></td>
                                 <td>
                                    <strong>5%</strong>
                                 </td>
                                 <td><strong>2%</strong></td>
                                 <td>
                                    <strong>12%</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view">Re-Run</a>
                                    <a href="#" class="btn btn-b">Export Consumers</a>
                                    <a href="#" class="btn btn-deactive">Export Failed List</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#failed-list">Create Group From Failed List</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>2022-05-31 03:38:30</strong>
                                 </td>
                                 <td><strong>Email</strong></td>
                                 <td>
                                    <strong>Intro Template</strong>
                                 </td>
                                 <td><strong>Happy Group</strong></td>
                                 <td><strong>Welcome to younegotiate</strong></td>
                                 <td>
                                    <strong>324</strong>
                                 </td>
                                 <td><strong>318</strong></td>
                                 <td><strong>4</strong></td>
                                 <td>
                                    <strong>14%</strong>
                                 </td>
                                 <td><strong>20%</strong></td>
                                 <td><strong>10%</strong></td>
                                 <td>
                                    <strong>5%</strong>
                                 </td>
                                 <td><strong>2%</strong></td>
                                 <td>
                                    <strong>12%</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view">Re-Run</a>
                                    <a href="#" class="btn btn-b">Export Consumers</a>
                                    <a href="#" class="btn btn-deactive">Export Failed List</a>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#failed-list">Create Group From Failed List</a>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </section>
   </div>
   <footer class="main-footer">
      <div class="footer-bg">
         <div class="footer-left">
            <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
            © 2020 YouNegotiate ™ | All rights reserved
         </div>
      </div>
   </footer>
</div>
</div>
<div class="modal modal-new fade" id="failed-list" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Templates</h5>
            <button type="button" class="close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <form>
               <div class="form-group">
                  <label>Number of consumers in the group</label>
                  <input type="text" class="form-control">
               </div>
               <div class="form-group">
                  <label>Group Name <sup>*</sup></label>
                  <input type="text" class="form-control">
               </div>
               <div class="form-group">
                  <label>Group Descriptio</label>
                  <textarea class="form-control h-auto" rows="4"></textarea>
               </div>
               <div class="text-right">
                  <a href="#" class="btn border-btn">Cancel</a>
                  <button type="submit" class="btn blue-btn">Submit</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
   
   
   
        $('#example2').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
   });
</script>
@endsection