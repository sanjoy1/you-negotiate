@extends('layout.dashboard.app')
@section('dashboard-content')

      <div class="main-content">
         <section class="section">
            <h1 class="section-header">
               <div>Password</div>
            </h1>
            <div class="section-body">
              
               <div class="row">
                  <div class="col-12">
                     <div class="card">
                       
                        <div class="card-body form-area">
                            <h3>Change My Password</h3>
                                <div class="form-row">
                                            <div class="form-group col-md-6">
                                            <label class="flex-between">Current Password <sup>*</sup></label>
                                            <div class="grp-area">
                                                <input type="password" id="txtPassword" class="form-control" name="txtPassword" />
                                                <button type="button" id="btnToggle" class="toggle-view"><i id="eyeIcon"
                                                class="fa fa-eye"></i></button>
                                            </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                            <label>New Password <sup>*</sup></label>
                                             <div class="grp-area">
                                                <input type="password" id="txtPassword2" class="form-control" name="txtPassword2" />
                                                <button type="button" id="btnToggle2" class="toggle-view"><i id="eyeIcon2"
                                                class="fa fa-eye"></i></button>
                                            </div>
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="form-group col-md-6">
                                            <label>Confirm New Password <sup>*</sup></label>
                                           <div class="grp-area">
                                                <input type="password" id="txtPassword3" class="form-control" name="txtPassword3" />
                                                <button type="button" id="btnToggle3" class="toggle-view"><i id="eyeIcon3"
                                                class="fa fa-eye"></i></button>
                                            </div>
                                            </div>
                                            
                                </div>
                             
                              <div class="text-right">
                            
                            <a href="#" class="btn btn-view">Save</a>
                           
                           </div>
                              
                                </div>
                               

                                


                        </div>
                     </div>
                  </div>
               </div>

            

           
         </section>
      </div>
      <footer class="main-footer">
         <div class="footer-bg">
         <div class="footer-left">
           <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
             © 2020 YouNegotiate ™ | All rights reserved
         </div>
         </div>
      </footer>
   </div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
       let passwordInput = document.getElementById("txtPassword"),
           toggle = document.getElementById("btnToggle"),
           icon = document.getElementById("eyeIcon");
   
       function togglePassword() {
           if (passwordInput.type === "password") {
           passwordInput.type = "text";
           icon.classList.add("fa-eye-slash");
           //toggle.innerHTML = 'hide';
           } else {
           passwordInput.type = "password";
           icon.classList.remove("fa-eye-slash");
           //toggle.innerHTML = 'show';
           }
       }
   
       function checkInput() {}
   
       toggle.addEventListener("click", togglePassword, false);
       passwordInput.addEventListener("keyup", checkInput, false);
   });
   
   $(document).ready(function () {
       let passwordInput = document.getElementById("txtPassword2"),
           toggle = document.getElementById("btnToggle2"),
           icon = document.getElementById("eyeIcon2");
   
       function togglePassword() {
           if (passwordInput.type === "password") {
           passwordInput.type = "text";
           icon.classList.add("fa-eye-slash");
           //toggle.innerHTML = 'hide';
           } else {
           passwordInput.type = "password";
           icon.classList.remove("fa-eye-slash");
           //toggle.innerHTML = 'show';
           }
       }
   
       function checkInput() {}
   
       toggle.addEventListener("click", togglePassword, false);
       passwordInput.addEventListener("keyup", checkInput, false);
   });

   $(document).ready(function () {
       let passwordInput = document.getElementById("txtPassword3"),
           toggle = document.getElementById("btnToggle3"),
           icon = document.getElementById("eyeIcon3");
   
       function togglePassword() {
           if (passwordInput.type === "password") {
           passwordInput.type = "text";
           icon.classList.add("fa-eye-slash");
           //toggle.innerHTML = 'hide';
           } else {
           passwordInput.type = "password";
           icon.classList.remove("fa-eye-slash");
           //toggle.innerHTML = 'show';
           }
       }
   
       function checkInput() {}
   
       toggle.addEventListener("click", togglePassword, false);
       passwordInput.addEventListener("keyup", checkInput, false);
   });
</script>
@endsection