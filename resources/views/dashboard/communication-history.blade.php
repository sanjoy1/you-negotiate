@extends('layout.dashboard.app')
@section('dashboard-content')

      <div class="main-content">
         <section class="section">
            <h1 class="section-header">
               <div>Download Communication History</div>
            </h1>
            <div class="section-body">
              
            

               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Communication History</h4>

                          <div>
                            <div class="form-inline">
                                <div class="form-group">
                                    <label class="mr-2">From</label>
                                    <input type="date" class="form-control mr-2" value="DD / MM / YYYY">
                                </div>
                                <div class="form-group mr-4">
                                    <label class="mr-2">To</label>
                                    <input type="date" class="form-control" value="DD / MM / YYYY">
                                </div>

                                 <button type="submit" class="btn btn-reset mr-2">Apply</button>
                             <a href="#" class="btn btn-view">Export CSV</a>
                            </div>
                            
                          </div>
                          
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table width-responsive w-100" id="example2">
                                 <thead>
                                    <tr>
                                       <th>
                                         Sent at
                                       </th>
                                       <th>account</th>
                                       <th>consumer name</th>
                                       <th>email</th>
                                       <th>phone</th>
                                       <th>sms segment</th>
                                       <th>group</th>
                                       <th>template</th>
                                       <th>Template type</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <strong>2022-05-31 03:38:30</strong>
                                       </td>
                                       <td><strong>543646</strong></td>
                                       <td>
                                          <strong>Edward</strong>
                                       </td>
                                       <td>
                                        <strong>edelric@gmail.com</strong>
                                       </td>
                                       <td>
                                        <strong>9845166555</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>Alpha</strong>
                                       </td>
                                       <td>
                                          <strong>New Template</strong>
                                       </td>
                                       <td>
                                          <strong>Email</strong>
                                       </td>
                                      
                                    </tr>
                                 <tr>
                                       <td>
                                          <strong>2022-05-31 03:38:30</strong>
                                       </td>
                                       <td><strong>543646</strong></td>
                                       <td>
                                          <strong>Edward</strong>
                                       </td>
                                       <td>
                                        <strong>edelric@gmail.com</strong>
                                       </td>
                                       <td>
                                        <strong>9845166555</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>Alpha</strong>
                                       </td>
                                       <td>
                                          <strong>New Template</strong>
                                       </td>
                                       <td>
                                          <strong>Email</strong>
                                       </td>
                                      
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2022-05-31 03:38:30</strong>
                                       </td>
                                       <td><strong>543646</strong></td>
                                       <td>
                                          <strong>Edward</strong>
                                       </td>
                                       <td>
                                        <strong>edelric@gmail.com</strong>
                                       </td>
                                       <td>
                                        <strong>9845166555</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>Alpha</strong>
                                       </td>
                                       <td>
                                          <strong>New Template</strong>
                                       </td>
                                       <td>
                                          <strong>Email</strong>
                                       </td>
                                      
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2022-05-31 03:38:30</strong>
                                       </td>
                                       <td><strong>543646</strong></td>
                                       <td>
                                          <strong>Edward</strong>
                                       </td>
                                       <td>
                                        <strong>edelric@gmail.com</strong>
                                       </td>
                                       <td>
                                        <strong>9845166555</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>Alpha</strong>
                                       </td>
                                       <td>
                                          <strong>New Template</strong>
                                       </td>
                                       <td>
                                          <strong>Email</strong>
                                       </td>
                                      
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2022-05-31 03:38:30</strong>
                                       </td>
                                       <td><strong>543646</strong></td>
                                       <td>
                                          <strong>Edward</strong>
                                       </td>
                                       <td>
                                        <strong>edelric@gmail.com</strong>
                                       </td>
                                       <td>
                                        <strong>9845166555</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>Alpha</strong>
                                       </td>
                                       <td>
                                          <strong>New Template</strong>
                                       </td>
                                       <td>
                                          <strong>Email</strong>
                                       </td>
                                      
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2022-05-31 03:38:30</strong>
                                       </td>
                                       <td><strong>543646</strong></td>
                                       <td>
                                          <strong>Edward</strong>
                                       </td>
                                       <td>
                                        <strong>edelric@gmail.com</strong>
                                       </td>
                                       <td>
                                        <strong>9845166555</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>Alpha</strong>
                                       </td>
                                       <td>
                                          <strong>New Template</strong>
                                       </td>
                                       <td>
                                          <strong>Email</strong>
                                       </td>
                                      
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2022-05-31 03:38:30</strong>
                                       </td>
                                       <td><strong>543646</strong></td>
                                       <td>
                                          <strong>Edward</strong>
                                       </td>
                                       <td>
                                        <strong>edelric@gmail.com</strong>
                                       </td>
                                       <td>
                                        <strong>9845166555</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>Alpha</strong>
                                       </td>
                                       <td>
                                          <strong>New Template</strong>
                                       </td>
                                       <td>
                                          <strong>Email</strong>
                                       </td>
                                      
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2022-05-31 03:38:30</strong>
                                       </td>
                                       <td><strong>543646</strong></td>
                                       <td>
                                          <strong>Edward</strong>
                                       </td>
                                       <td>
                                        <strong>edelric@gmail.com</strong>
                                       </td>
                                       <td>
                                        <strong>9845166555</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>Alpha</strong>
                                       </td>
                                       <td>
                                          <strong>New Template</strong>
                                       </td>
                                       <td>
                                          <strong>Email</strong>
                                       </td>
                                      
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2022-05-31 03:38:30</strong>
                                       </td>
                                       <td><strong>543646</strong></td>
                                       <td>
                                          <strong>Edward</strong>
                                       </td>
                                       <td>
                                        <strong>edelric@gmail.com</strong>
                                       </td>
                                       <td>
                                        <strong>9845166555</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>Alpha</strong>
                                       </td>
                                       <td>
                                          <strong>New Template</strong>
                                       </td>
                                       <td>
                                          <strong>Email</strong>
                                       </td>
                                      
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2022-05-31 03:38:30</strong>
                                       </td>
                                       <td><strong>543646</strong></td>
                                       <td>
                                          <strong>Edward</strong>
                                       </td>
                                       <td>
                                        <strong>edelric@gmail.com</strong>
                                       </td>
                                       <td>
                                        <strong>9845166555</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>Alpha</strong>
                                       </td>
                                       <td>
                                          <strong>New Template</strong>
                                       </td>
                                       <td>
                                          <strong>Email</strong>
                                       </td>
                                      
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2022-05-31 03:38:30</strong>
                                       </td>
                                       <td><strong>543646</strong></td>
                                       <td>
                                          <strong>Edward</strong>
                                       </td>
                                       <td>
                                        <strong>edelric@gmail.com</strong>
                                       </td>
                                       <td>
                                        <strong>9845166555</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>Alpha</strong>
                                       </td>
                                       <td>
                                          <strong>New Template</strong>
                                       </td>
                                       <td>
                                          <strong>Email</strong>
                                       </td>
                                      
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2022-05-31 03:38:30</strong>
                                       </td>
                                       <td><strong>543646</strong></td>
                                       <td>
                                          <strong>Edward</strong>
                                       </td>
                                       <td>
                                        <strong>edelric@gmail.com</strong>
                                       </td>
                                       <td>
                                        <strong>9845166555</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>Alpha</strong>
                                       </td>
                                       <td>
                                          <strong>New Template</strong>
                                       </td>
                                       <td>
                                          <strong>Email</strong>
                                       </td>
                                      
                                    </tr>
                                    
                                    
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

              
              
         </section>
      </div>
      <footer class="main-footer">
         <div class="footer-bg">
         <div class="footer-left">
           <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
             © 2020 YouNegotiate ™ | All rights reserved
         </div>
         </div>
      </footer>
   </div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>

<script>
    $(document).ready(function () {
     

         $('#example2').DataTable({
            language: {
               searchPlaceholder: "Search",
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });

      
     
    });
</script>
@endsection