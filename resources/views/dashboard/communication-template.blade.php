@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Communication Template</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Create Template</h4>
                  </div>
                  <div class="card-body">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Template Name <sup>*</sup></label>
                                 <input type="text" class="form-control">
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Type <sup>*</sup></label>
                                 <div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c1" name="c1" class="custom-control-input">
                                       <label class="custom-control-label" for="c1">Email</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c2" name="c2" class="custom-control-input">
                                       <label class="custom-control-label" for="c2">SMS</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c3" name="c3" class="custom-control-input">
                                       <label class="custom-control-label" for="c3">Eletter</label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label>Subject <sup>*</sup></label>
                              <input type="text" class="form-control">
                           </div>
                           <div class="form-group">
                              <label>Insert Custom Fields</label>
                              <div>
                                 <code class="code-style">[First Name] [Last Name] [Account Number] [Master Company Name] [Sub Client 1 Name] [Sub Client 2 Name] [Subclient 1 Account Number] [Subclient 2 Account Number] [Account Balance] [Payment Set up Discount %] [Payment Set up Discount Amount] [Pay in Full Discount %] [Pay in Full Discount Amount] [You Negotiate Link] [Pass Through 1] [Pass Through 2] [Pass Through 3] [Pass Through 4] [Pass Through 5] [Account QR Code]
                                 </code>
                              </div>
                           </div>
                           <div class="form-group">
                              <div id="summernote"></div>
                           </div>
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Select Group </label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">US</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="text-right col-sm-12">
                           <button type="submit" class="btn btn-reset" data-toggle="modal" data-target="#preview">Preview</button>   <button type="submit" class="btn blue-btn">Save</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Template Library</h4>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive" id="example2">
                           <thead>
                              <tr>
                                 <th>
                                    template name
                                 </th>
                                 <th>type</th>
                                 <th>Subject</th>
                                 <th>created on</th>
                                 <th>created by</th>
                                 <th>action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <strong>Test demo</strong>
                                 </td>
                                 <td><strong>Eletter</strong></td>
                                 <td>
                                    <strong>Section 1.10.33 of "de Finibus Bonorum</strong>
                                 </td>
                                 <td><strong>	2022-06-06 03:25</strong></td>
                                 <td><strong>Donna Weaver</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#send-template">Send This Template</a>
                                    <a href="#" class="btn btn-reset"  data-toggle="modal" data-target="#preview">View</a>
                                    <a href="#" class="btn btn-yellow">Edit</a>
                                    <a href="#" class="btn btn-green">Export</a>
                                    <a href="#" data-toggle="modal" data-target="#delete-template" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Test demo</strong>
                                 </td>
                                 <td><strong>Eletter</strong></td>
                                 <td>
                                    <strong>Section 1.10.33 of "de Finibus Bonorum</strong>
                                 </td>
                                 <td><strong>	2022-06-06 03:25</strong></td>
                                 <td><strong>Donna Weaver</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#send-template">Send This Template</a>
                                    <a href="#" class="btn btn-reset"  data-toggle="modal" data-target="#preview">View</a>
                                    <a href="#" class="btn btn-yellow">Edit</a>
                                    <a href="#" class="btn btn-green">Export</a>
                                    <a href="#" data-toggle="modal" data-target="#delete-template" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Test demo</strong>
                                 </td>
                                 <td><strong>Eletter</strong></td>
                                 <td>
                                    <strong>Section 1.10.33 of "de Finibus Bonorum</strong>
                                 </td>
                                 <td><strong>	2022-06-06 03:25</strong></td>
                                 <td><strong>Donna Weaver</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#send-template">Send This Template</a>
                                    <a href="#" class="btn btn-reset"  data-toggle="modal" data-target="#preview">View</a>
                                    <a href="#" class="btn btn-yellow">Edit</a>
                                    <a href="#" class="btn btn-green">Export</a>
                                    <a href="#" data-toggle="modal" data-target="#delete-template" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Test demo</strong>
                                 </td>
                                 <td><strong>Eletter</strong></td>
                                 <td>
                                    <strong>Section 1.10.33 of "de Finibus Bonorum</strong>
                                 </td>
                                 <td><strong>	2022-06-06 03:25</strong></td>
                                 <td><strong>Donna Weaver</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#send-template">Send This Template</a>
                                    <a href="#" class="btn btn-reset"  data-toggle="modal" data-target="#preview">View</a>
                                    <a href="#" class="btn btn-yellow">Edit</a>
                                    <a href="#" class="btn btn-green">Export</a>
                                    <a href="#" data-toggle="modal" data-target="#delete-template" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Test demo</strong>
                                 </td>
                                 <td><strong>Eletter</strong></td>
                                 <td>
                                    <strong>Section 1.10.33 of "de Finibus Bonorum</strong>
                                 </td>
                                 <td><strong>	2022-06-06 03:25</strong></td>
                                 <td><strong>Donna Weaver</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#send-template">Send This Template</a>
                                    <a href="#" class="btn btn-reset"  data-toggle="modal" data-target="#preview">View</a>
                                    <a href="#" class="btn btn-yellow">Edit</a>
                                    <a href="#" class="btn btn-green">Export</a>
                                    <a href="#" data-toggle="modal" data-target="#delete-template" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Test demo</strong>
                                 </td>
                                 <td><strong>Eletter</strong></td>
                                 <td>
                                    <strong>Section 1.10.33 of "de Finibus Bonorum</strong>
                                 </td>
                                 <td><strong>	2022-06-06 03:25</strong></td>
                                 <td><strong>Donna Weaver</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#send-template">Send This Template</a>
                                    <a href="#" class="btn btn-reset"  data-toggle="modal" data-target="#preview">View</a>
                                    <a href="#" class="btn btn-yellow">Edit</a>
                                    <a href="#" class="btn btn-green">Export</a>
                                    <a href="#" data-toggle="modal" data-target="#delete-template" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Test demo</strong>
                                 </td>
                                 <td><strong>Eletter</strong></td>
                                 <td>
                                    <strong>Section 1.10.33 of "de Finibus Bonorum</strong>
                                 </td>
                                 <td><strong>	2022-06-06 03:25</strong></td>
                                 <td><strong>Donna Weaver</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#send-template">Send This Template</a>
                                    <a href="#" class="btn btn-reset"  data-toggle="modal" data-target="#preview">View</a>
                                    <a href="#" class="btn btn-yellow">Edit</a>
                                    <a href="#" class="btn btn-green">Export</a>
                                    <a href="#" data-toggle="modal" data-target="#delete-template" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Test demo</strong>
                                 </td>
                                 <td><strong>Eletter</strong></td>
                                 <td>
                                    <strong>Section 1.10.33 of "de Finibus Bonorum</strong>
                                 </td>
                                 <td><strong>	2022-06-06 03:25</strong></td>
                                 <td><strong>Donna Weaver</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#send-template">Send This Template</a>
                                    <a href="#" class="btn btn-reset"  data-toggle="modal" data-target="#preview">View</a>
                                    <a href="#" class="btn btn-yellow">Edit</a>
                                    <a href="#" class="btn btn-green">Export</a>
                                    <a href="#" data-toggle="modal" data-target="#delete-template" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Test demo</strong>
                                 </td>
                                 <td><strong>Eletter</strong></td>
                                 <td>
                                    <strong>Section 1.10.33 of "de Finibus Bonorum</strong>
                                 </td>
                                 <td><strong>	2022-06-06 03:25</strong></td>
                                 <td><strong>Donna Weaver</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#send-template">Send This Template</a>
                                    <a href="#" class="btn btn-reset"  data-toggle="modal" data-target="#preview">View</a>
                                    <a href="#" class="btn btn-yellow">Edit</a>
                                    <a href="#" class="btn btn-green">Export</a>
                                    <a href="#" data-toggle="modal" data-target="#delete-template" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Test demo</strong>
                                 </td>
                                 <td><strong>Eletter</strong></td>
                                 <td>
                                    <strong>Section 1.10.33 of "de Finibus Bonorum</strong>
                                 </td>
                                 <td><strong>	2022-06-06 03:25</strong></td>
                                 <td><strong>Donna Weaver</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#send-template">Send This Template</a>
                                    <a href="#" class="btn btn-reset"  data-toggle="modal" data-target="#preview">View</a>
                                    <a href="#" class="btn btn-yellow">Edit</a>
                                    <a href="#" class="btn btn-green">Export</a>
                                    <a href="#" data-toggle="modal" data-target="#delete-template" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Test demo</strong>
                                 </td>
                                 <td><strong>Eletter</strong></td>
                                 <td>
                                    <strong>Section 1.10.33 of "de Finibus Bonorum</strong>
                                 </td>
                                 <td><strong>	2022-06-06 03:25</strong></td>
                                 <td><strong>Donna Weaver</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#send-template">Send This Template</a>
                                    <a href="#" class="btn btn-reset"  data-toggle="modal" data-target="#preview">View</a>
                                    <a href="#" class="btn btn-yellow">Edit</a>
                                    <a href="#" class="btn btn-green">Export</a>
                                    <a href="#" data-toggle="modal" data-target="#delete-template" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Test demo</strong>
                                 </td>
                                 <td><strong>Eletter</strong></td>
                                 <td>
                                    <strong>Section 1.10.33 of "de Finibus Bonorum</strong>
                                 </td>
                                 <td><strong>	2022-06-06 03:25</strong></td>
                                 <td><strong>Donna Weaver</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#send-template">Send This Template</a>
                                    <a href="#" class="btn btn-reset"  data-toggle="modal" data-target="#preview">View</a>
                                    <a href="#" class="btn btn-yellow">Edit</a>
                                    <a href="#" class="btn btn-green">Export</a>
                                    <a href="#" data-toggle="modal" data-target="#delete-template" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </section>
   </div>
   <footer class="main-footer">
      <div class="footer-bg">
         <div class="footer-left">
            <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
            © 2020 YouNegotiate ™ | All rights reserved
         </div>
      </div>
   </footer>
</div>
</div>
<!-- Modal -->
<div class="modal modal-new fade" id="preview" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Preview</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <div class="row">
               <div class="col-sm-12">
                  <h4>Subject</h4>
                  <h5><strong>Section 1.10.33 of "de Finibus Bonorum et Malorum", written by 
                     Cicero in 45 BC</strong>
                  </h5>
               </div>
               <div class="col-sm-12">
                  <h4>Content</h4>
                  <p>"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal modal-new md-sm fade" id="delete-template" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Delete Template</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Do You Want to delete this?</h2>
            <div class="text-right mt-4">
               <button type="submit" class="btn border-btn" data-dismiss="modal" aria-label="Close">Close</button>
               <button type="submit" class="btn blue-btn">Save</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal modal-new fade" id="send-template" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Select Group to Run Send the Template</h5>
            <button type="button" class="close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <form>
               <div class="form-group">
                  <label>Select Group </label>
                  <select class="custom-select">
                     <option selected="">Select</option>
                     <option value="1">US</option>
                     <option value="2">Two</option>
                     <option value="3">Three</option>
                  </select>
               </div>
               <div class="text-right">
                  <button type="submit" class="btn border-btn" data-dismiss="modal" aria-label="Close">Close</button>
                  <button type="submit" class="btn blue-btn">Save</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $('#summernote').summernote({
      placeholder: 'Hello stand alone ui',
      tabsize: 2,
      height: 100,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
      ]
    });
   $(document).ready(function () {
   
   
   
       $('#example2').DataTable({
          language: {
               searchPlaceholder: "Search",
              'paginate': {
                  'previous': '<span class="fa fa-angle-left"></span>',
                  'next': '<span class="fa fa-angle-right"></span>'
              }
              
          }
      });
   
   });
</script>
@endsection