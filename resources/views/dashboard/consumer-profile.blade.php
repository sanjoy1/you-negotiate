@extends('layout.dashboard.app')
@section('dashboard-content')

      <div class="main-content">
         <section class="section">
            <h1 class="section-header">
               <div>Consumer profile</div>
            </h1>
            <div class="section-body">
              
               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Consumer Information</h4>
                           <div>
                            <a href="#" class="btn btn-primary">Update Info</a>
                            <a href="send-email" class="btn btn-green">Send Email</a>
                            <a href="send-sms" class="btn btn-b">Send Sms</a>
                            <a href="#" class="btn btn-yellow">Reset Negotiation</a>
                            <a href="#" class="btn btn-reset">Download Agreement</a>
                            <a href="#" class="btn btn-deactive">Delete</a>
                           </div>
                        </div>
                        <hr>
                        <div class="card-body">
                          <div class="row">
                            <div class="col-sm-12 col-md-6">
                               <div class="form-row">
                                    <div class="form-group col-md-4">
                                    <label>First Name <sup>*</sup></label>
                                    <input type="text" class="form-control">
                                    </div>
                                    <div class="form-group col-md-4">
                                    <label>Middle Name</label>
                                    <input type="text" class="form-control">
                                    </div>
                                     <div class="form-group col-md-4">
                                    <label>Last Name <sup>*</sup></label>
                                    <input type="text" class="form-control">
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label>Negotiation Term</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Email <sup>*</sup></label>
                                    <input type="email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Original Balance <sup>*</sup></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                        <div class="input-group-text">$</div>
                                        </div>
                                        <input type="text" class="form-control" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Discounted Payoff Balance</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                        <div class="input-group-text">$</div>
                                        </div>
                                        <input type="text" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>DOB <sup>*</sup></label>
                                    <input type="date" class="form-control" value="DD/MM/YYYY">
                                </div>
                                <div class="form-group">
                                    <label>Last 4 SSN <sup>*</sup></label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>First Payment Date (Active Payment Profile)</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label>Client Name <sup>*</sup></label>
                                    <input type="text" class="form-control">
                                    </div>

                                 <div class="form-group">
                                    <label>Account Number <sup>*</sup></label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Approved By</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Mobile Number <sup>*</sup></label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Payment Plan Current Balance <sup>*</sup></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                        <div class="input-group-text">$</div>
                                        </div>
                                        <input type="text" class="form-control" >
                                    </div>
                                </div>

                               
                                <div class="form-group">
                                    <label>Negotiation Completed On</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Invitation Link</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Communication Status</label>
                                    <input type="text" class="form-control">
                                </div>
                                
                            </div>

                            <div class="col-sm-12 text-right mt-3">
                                <a href="#" class="btn blue-btn">Update Info</a>
                            <a href="#" class="btn btn-green">Send Email</a>
                            <a href="#" class="btn btn-b">Send Sms</a>
                            <a href="#" class="btn btn-yellow">Reset Negotiation</a>
                            <a href="#" class="btn btn-reset">Download Agreement</a>
                            <a href="#" class="btn btn-deactive">Delete</a>
                            </div>
                          </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Transaction History</h4>
                          
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table width-responsive" id="example2">
                                 <thead>
                                    <tr>
                                       <th>
                                         date & time
                                       </th>
                                       <th>consumer name</th>
                                       <th>Transaction ID</th>
                                       <th>Payment method</th>
                                       <th>Transaction amount</th>
                                       <th>failed reason</th>
                                       <th>status</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>Edward Elric</stron></td>
                                       <td>
                                          <strong>13245</stron>
                                       </td>
                                       <td>
                                        <strong> Amazon Pay</stron>
                                       </td>
                                       <td>
                                        <strong> $10.00</stron>
                                       </td>
                                       <td>
                                         <strong>Bad interent</strong>
                                       </td>
                                       
                                       <td>
                                          <a href="#" class="red-text">Failed</a> 
                                       </td>
                                       
                                    </tr>
                                <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>Edward Elric</stron></td>
                                       <td>
                                          <strong>13245</stron>
                                       </td>
                                       <td>
                                        <strong> Amazon Pay</stron>
                                       </td>
                                       <td>
                                        <strong> $10.00</stron>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                          <a href="#" class="text-green">Completed</a> 
                                       </td>
                                       
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>Edward Elric</stron></td>
                                       <td>
                                          <strong>13245</stron>
                                       </td>
                                       <td>
                                        <strong> Amazon Pay</stron>
                                       </td>
                                       <td>
                                        <strong> $10.00</stron>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                          <a href="#" class="text-green">Completed</a> 
                                       </td>
                                       
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>Edward Elric</stron></td>
                                       <td>
                                          <strong>13245</stron>
                                       </td>
                                       <td>
                                        <strong> Amazon Pay</stron>
                                       </td>
                                       <td>
                                        <strong> $10.00</stron>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                          <a href="#" class="text-green">Completed</a> 
                                       </td>
                                       
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>Edward Elric</stron></td>
                                       <td>
                                          <strong>13245</stron>
                                       </td>
                                       <td>
                                        <strong> Amazon Pay</stron>
                                       </td>
                                       <td>
                                        <strong> $10.00</stron>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                          <a href="#" class="text-green">Completed</a> 
                                       </td>
                                       
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>Edward Elric</stron></td>
                                       <td>
                                          <strong>13245</stron>
                                       </td>
                                       <td>
                                        <strong> Amazon Pay</stron>
                                       </td>
                                       <td>
                                        <strong> $10.00</stron>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                          <a href="#" class="text-green">Completed</a> 
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>Edward Elric</stron></td>
                                       <td>
                                          <strong>13245</stron>
                                       </td>
                                       <td>
                                        <strong> Amazon Pay</stron>
                                       </td>
                                       <td>
                                        <strong> $10.00</stron>
                                       </td>
                                       <td>
                                         <strong>Bad interent</strong>
                                       </td>
                                       
                                       <td>
                                          <a href="#" class="red-text">Failed</a> 
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>Edward Elric</stron></td>
                                       <td>
                                          <strong>13245</stron>
                                       </td>
                                       <td>
                                        <strong> Amazon Pay</stron>
                                       </td>
                                       <td>
                                        <strong> $10.00</stron>
                                       </td>
                                       <td>
                                         <strong>Bad interent</strong>
                                       </td>
                                       
                                       <td>
                                          <a href="#" class="red-text">Failed</a> 
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>Edward Elric</stron></td>
                                       <td>
                                          <strong>13245</stron>
                                       </td>
                                       <td>
                                        <strong> Amazon Pay</stron>
                                       </td>
                                       <td>
                                        <strong> $10.00</stron>
                                       </td>
                                       <td>
                                         <strong>Bad interent</strong>
                                       </td>
                                       
                                       <td>
                                          <a href="#" class="red-text">Failed</a> 
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>Edward Elric</stron></td>
                                       <td>
                                          <strong>13245</stron>
                                       </td>
                                       <td>
                                        <strong> Amazon Pay</stron>
                                       </td>
                                       <td>
                                        <strong> $10.00</stron>
                                       </td>
                                       <td>
                                         <strong>Bad interent</strong>
                                       </td>
                                       
                                       <td>
                                          <a href="#" class="red-text">Failed</a> 
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>Edward Elric</stron></td>
                                       <td>
                                          <strong>13245</stron>
                                       </td>
                                       <td>
                                        <strong> Amazon Pay</stron>
                                       </td>
                                       <td>
                                        <strong> $10.00</stron>
                                       </td>
                                       <td>
                                         <strong>Bad interent</strong>
                                       </td>
                                       
                                       <td>
                                          <a href="#" class="red-text">Failed</a> 
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>Edward Elric</stron></td>
                                       <td>
                                          <strong>13245</stron>
                                       </td>
                                       <td>
                                        <strong> Amazon Pay</stron>
                                       </td>
                                       <td>
                                        <strong> $10.00</stron>
                                       </td>
                                       <td>
                                         <strong>Bad interent</strong>
                                       </td>
                                       
                                       <td>
                                          <a href="#" class="red-text">Failed</a> 
                                       </td>
                                       
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Scheduled Payment Details </h4>
                          
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table width-responsive" id="example3">
                                 <thead>
                                    <tr>
                                       <th>
                                         date & time
                                       </th>
                                       <th>Transaction amount</th>
                                       <th>Card/cah</th>
                                       <th>status</th>
                                       <th>action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:#FF900E">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                 <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:#FF900E">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:#FF900E">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:#FF900E">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:#FF900E">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:#FF900E">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:#FF900E">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:#FF900E">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:#FF900E">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:#FF900E">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:#FF900E">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>

                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
              <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Scheduled Cancelled Payment Details </h4>
                          
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table width-responsive" id="example4">
                                 <thead>
                                    <tr>
                                       <th>
                                         date & time
                                       </th>
                                       <th>Transaction amount</th>
                                       <th>Card/cah</th>
                                       <th>status</th>
                                       <th>action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:red">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                 <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:red">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:red">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:red">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:red">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:red">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:red">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:red">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:red">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:red">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>$10.00</stron></td>
                                       <td>
                                          <strong>Payment Savvy</stron>
                                       </td>
                                       <td>
                                         <strong style="color:red">Upcoming</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>

                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Communication History</h4>
                         
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table width-responsive" id="example5">
                                 <thead>
                                    <tr>
                                       <th>
                                        sent at
                                       </th>
                                       <th>email id</th>
                                       <th>phone number</th>
                                       <th>sms segment</th>
                                       <th>group</th>
                                       <th>template</th>
                                       <th>template type</th>
                                       <th>action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>84895685690</stron>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       
                                       <td>
                                           <strong>New Template</strong>
                                       </td>
                                        <td>
                                         <strong>Email</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">View</a>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>84895685690</stron>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       
                                       <td>
                                           <strong>New Template</strong>
                                       </td>
                                        <td>
                                         <strong>Email</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">View</a>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>84895685690</stron>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       
                                       <td>
                                           <strong>New Template</strong>
                                       </td>
                                        <td>
                                         <strong>Email</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">View</a>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>84895685690</stron>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       
                                       <td>
                                           <strong>New Template</strong>
                                       </td>
                                        <td>
                                         <strong>Email</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">View</a>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>84895685690</stron>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       
                                       <td>
                                           <strong>New Template</strong>
                                       </td>
                                        <td>
                                         <strong>Email</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">View</a>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>84895685690</stron>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       
                                       <td>
                                           <strong>New Template</strong>
                                       </td>
                                        <td>
                                         <strong>Email</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">View</a>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>84895685690</stron>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       
                                       <td>
                                           <strong>New Template</strong>
                                       </td>
                                        <td>
                                         <strong>Email</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">View</a>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>84895685690</stron>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       
                                       <td>
                                           <strong>New Template</strong>
                                       </td>
                                        <td>
                                         <strong>Email</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">View</a>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>84895685690</stron>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       
                                       <td>
                                           <strong>New Template</strong>
                                       </td>
                                        <td>
                                         <strong>Email</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">View</a>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>84895685690</stron>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       
                                       <td>
                                           <strong>New Template</strong>
                                       </td>
                                        <td>
                                         <strong>Email</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">View</a>
                                       </td>
                                    </tr>

                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Eletter History </h4>
                           
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table width-responsive" id="example7">
                                 <thead>
                                    <tr>
                                       <th>
                                       created at
                                       </th>
                                       <th>eletter id</th>
                                       <th>name</th>
                                       <th>Subject</th>
                                       <th>group</th>
                                        <th>read by consumer</th>
                                       <th>enabled</th>
                                       <th>view  message</th>

                                       
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>Troy Boult</stron>
                                       </td>
                                       <td>
                                        <strong>-</stron>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                     
                                    </tr>
                                   <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>Troy Boult</stron>
                                       </td>
                                       <td>
                                        <strong>-</stron>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                     
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>Troy Boult</stron>
                                       </td>
                                       <td>
                                        <strong>-</stron>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                     
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>Troy Boult</stron>
                                       </td>
                                       <td>
                                        <strong>-</stron>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                     
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>Troy Boult</stron>
                                       </td>
                                       <td>
                                        <strong>-</stron>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                     
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>Troy Boult</stron>
                                       </td>
                                       <td>
                                        <strong>-</stron>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                     
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>Troy Boult</stron>
                                       </td>
                                       <td>
                                        <strong>-</stron>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                     
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>Troy Boult</stron>
                                       </td>
                                       <td>
                                        <strong>-</stron>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                     
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>Troy Boult</stron>
                                       </td>
                                       <td>
                                        <strong>-</stron>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                     
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>Troy Boult</stron>
                                       </td>
                                       <td>
                                        <strong>-</stron>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                     
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>hello@younegotiate.com</stron></td>
                                       <td>
                                          <strong>Troy Boult</stron>
                                       </td>
                                       <td>
                                        <strong>-</stron>
                                       </td>
                                       
                                       <td>
                                         <strong>Alpha</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                     
                                    </tr>

                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      <footer class="main-footer">
         <div class="footer-bg">
         <div class="footer-left">
           <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
             © 2020 YouNegotiate ™ | All rights reserved
         </div>
         </div>
      </footer>
   </div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>

<script>
    $(document).ready(function () {
        $('#example').DataTable({
            language: {
               searchPlaceholder: "Search",
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });

         $('#example2').DataTable({
            language: {
               searchPlaceholder: "Search",
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });

         $('#example3').DataTable({
            language: {
               searchPlaceholder: "Search",
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });
         $('#example4').DataTable({
            language: {
               searchPlaceholder: "Search",
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });
        $('#example5').DataTable({
            language: {
               searchPlaceholder: "Search",
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });
         $('#example6').DataTable({
            language: {
               searchPlaceholder: "Search",
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });
         $('#example7').DataTable({
            language: {
               searchPlaceholder: "Search",
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });
    });
</script>
@endsection