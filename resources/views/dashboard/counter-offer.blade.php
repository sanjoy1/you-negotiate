@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Counter offer</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Counter offer</h4>
                     <div>
                        <a href="offer-details" class="btn border-btn">Close</a>
                        <a href="offer-details" class="btn btn-green">Back To Review Offer</a>
                        <button type="submit" class="btn btn-view">Submit My offer</button>
                     
                        
                     </div>
                  </div>
                  <hr>
                  <div class="card-body">
                     <div class="new-radius-box">
                        <h3>Account Profile</h3>
                        <div class="row content-ac-del ">
                           <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                              <h4>Customer's Name</h4>
                              <h5>Edward Elric</h5>
                           </div>
                           <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                              <h4>Account</h4>
                              <h5>1571724</h5>
                           </div>
                           <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                              <h4>My Client</h4>
                              <h5>YouNegotiate</h5>
                           </div>
                           <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                              <h4>Loan Date</h4>
                              <h5>-</h5>
                           </div>
                           <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                              <h4>Placement Date</h4>
                              <h5>-</h5>
                           </div>
                           <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                              <h4>Deadline to Help</h4>
                              <h5>-</h5>
                           </div>
                           <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                              <h4>Account Balance</h4>
                              <h5>$100.00</h5>
                           </div>
                        </div>
                     </div>
                     <div class="new-radius-box">
                        <h3>Account Profile</h3>
                        <div class="table-border-st">
                           <table class="table width-responsive">
                              <thead>
                                 <tr>
                                    <th>Offer Description</th>
                                    <th>Our Offer  </th>
                                    <th>Customer's Offer </th>
                                    <th style="width:250px">counter offer</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>Discounted PayOff Balance</td>
                                    <td>$70.00</td>
                                    <td>$50.00</td>
                                    <td class="form-group"><input type="text" class="form-control" value="$60.00"></td>
                                 </tr>
                                 <tr>
                                    <td>First Payment Date</td>
                                    <td>2022-06-27</td>
                                    <td>2022-06-27</td>
                                     <td class="form-group"><input type="date" value="2022-06-27" class="form-control"></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                      <div class="new-radius-box">
                        <h3>Add Note</h3>
                        <div class="form-group">
                                            <label>Maximum 2000 characters</label>
                                            <textarea rows="5" class="form-control h-auto" placeholder="DD/MM/YYYY"></textarea>
                        </div>
                     </div>
                     
                     <div class="text-right mt-3">
                        <a href="offer-details" class="btn border-btn">Close</a>
                        <a href="offer-details" class="btn btn-green">Back To Review Offer</a>
                        <button type="submit" class="btn btn-view">Submit My offer</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
        
      </div>
   </section>
</div>
<footer class="main-footer">
   <div class="footer-bg">
      <div class="footer-left">
         <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
      </div>
      <div class="footer-right">
         © 2020 YouNegotiate ™ | All rights reserved
      </div>
   </div>
</footer>
</div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>

@endsection