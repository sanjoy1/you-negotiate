@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Counter offer</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Update Customer Terms</h4>
                     <div>
                      
                        <a href="current-master-terms" class="btn btn-green">Save</a>
                        <button type="submit" class="btn btn-view">Save & Update Customer</button>
                     
                        
                     </div>
                  </div>
                  <hr>
                  <div class="card-body">
                     <div class="new-radius-box">
                        <h3>Account Profile</h3>
                        <div class="row content-ac-del ">
                           <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                              <h4>Customer's Name</h4>
                              <h5>Edward Elric</h5>
                           </div>
                           <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                              <h4>Account</h4>
                              <h5>1571724</h5>
                           </div>
                           <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                              <h4>My Client</h4>
                              <h5>YouNegotiate</h5>
                           </div>
                           <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                              <h4>Account Balance</h4>
                              <h5>$100.00</h5>
                           </div>
                           
                        </div>
                     </div>
                     <div class="new-radius-box">
                        <h3>Account Profile</h3>
                        <div class="table-border-st">
                           <table class="table width-responsive">
                              <thead>
                                 <tr>
                                    <th>Descriptions</th>
                                    <th>counter offer</th>
                                    <th>Change to</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td><strong>Discounted PayOff Balance ($)</strong></td>
                                    
                                    <td class="form-group"><input type="text" class="form-control" value="$60.00"></td>
                                    <td class="form-group"><input type="text" class="form-control" value="$60.00"></td>
                                 </tr>
                                 <tr>
                                    <td><strong>PayOff Discount (%)</strong></td>
                                    
                                    <td class="form-group"><input type="text" class="form-control" value="2022-06-27"></td>
                                    <td class="form-group"><input type="text" class="form-control" value="2022-06-27"></td>
                                 </tr>
                                 <tr>
                                    <td><strong>Discounted Payment Plan Balance ($)</strong></td>
                                    
                                    <td class="form-group"><input type="text" class="form-control" value="2022-06-27"></td>
                                    <td class="form-group"><input type="text" class="form-control" value="2022-06-27"></td>
                                 </tr>
                                 <tr>
                                    <td><strong>Payment Plan Discount (%)</strong></td>
                                    
                                    <td class="form-group"><input type="text" class="form-control" value="2022-06-27"></td>
                                    <td class="form-group"><input type="text" class="form-control" value="2022-06-27"></td>
                                 </tr>
                                  <tr>
                                    <td><strong>Min. Monthly Payment ($)</strong></td>
                                    
                                    <td class="form-group"><input type="text" class="form-control" value="2022-06-27"></td>
                                    <td class="form-group"><input type="text" class="form-control" value="2022-06-27"></td>
                                 </tr>
                                 <tr>
                                    <td><strong>Min. Monthly Pay (%)</strong></td>
                                    
                                    <td class="form-group"><input type="text" class="form-control" value="2022-06-27"></td>
                                    <td class="form-group"><input type="text" class="form-control" value="2022-06-27"></td>
                                 </tr>
                                 <tr>
                                    <td><strong>Days to First Pay</strong></td>
                                    
                                    <td class="form-group"><input type="text" class="form-control" value="2022-06-27"></td>
                                    <td class="form-group"><input type="text" class="form-control" value="2022-06-27"></td>
                                 </tr>


                                 
                              </tbody>
                           </table>
                        </div>
                     </div>
                      
                     
                     <div class="text-right mt-3">
                        <a href="current-master-terms" class="btn border-btn">Close</a>
                        <a href="current-master-terms" class="btn btn-green">Save</a>
                        <button type="submit" class="btn btn-view">Save & Update Customer</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
        
      </div>
   </section>
</div>
<footer class="main-footer">
   <div class="footer-bg">
      <div class="footer-left">
         <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
      </div>
      <div class="footer-right">
         © 2020 YouNegotiate ™ | All rights reserved
      </div>
   </div>
</footer>
</div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>

@endsection