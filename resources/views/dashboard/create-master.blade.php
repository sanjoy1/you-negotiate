@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Create Your Master Input File Profile (Upload and Map any CSV Layout)</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <div>
                        <h4 class="mb-2">Create Your Master Input File Profile</h4>
                        <p class="sm-text">Create your input file in this section. Once you map your header fields, you’re ready to upload unlimited files!</p>
                     </div>
                  </div>
                  <div class="card-body">
                     <div class="row">
                        <div class="col-sm-5">
                           <div class="form-group">
                              <label>Upload your header name</label>
                              <input type="text" class="form-control">
                           </div>
                           <div class="form-group">
                              <label>Upload file</label>
                              <div class="button-upload">
                                 <span class="label">
                                 <img alt="upload" src="{{asset('dist/img/upload.svg')}}" class="mr-2"> Upload File
                                 </span>
                                 <input type="file" name="upload" id="upload" class="upload-box" placeholder="Upload File">
                              </div>
                           </div>
                           <div class="text-right mb-4">
                              <button type="submit" class="btn blue-btn">Upload CSV</button>
                           </div>
                           <div class="note-content">
                              <h6>Note</h6>
                              <p>We recommend downloading the Master Header Overview to become an expert</p>
                           </div>
                        </div>
                        <div class="col-sm-7 p-left your-head">
                           <h3>Your Headers</h3>
                           <div class="card-bdr">
                              <div class="card-bdr-header">
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">US</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                 </select>
                                 <div class="pl-2 d-flex">
                                    <button type="submit" class="btn btn-deactive mr-2">Delete</button>
                                    <button type="submit" class="btn blue-btn">Download</button>
                                 </div>
                              </div>
                              <hr>
                              <div class="card-bdr-body">
                                 <div class="table-border-st">
                                    <table class="table width-responsive">
                                       <thead>
                                          <tr>
                                             <th>Your Uploaded Headers</th>
                                             <th>YouNegotiate Data Field</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr>
                                             <td>User Accounts</td>
                                             <td>account_number</td>
                                          </tr>
                                          <tr>
                                             <td>F Name</td>
                                             <td>first_name</td>
                                          </tr>
                                          <tr>
                                             <td>L Name</td>
                                             <td>last_name</td>
                                          </tr>
                                          <tr>
                                             <td>last4ssn</td>
                                             <td>last4ssn</td>
                                          </tr>
                                          <tr>
                                             <td>dob</td>
                                             <td>dob</td>
                                          </tr>
                                          <tr>
                                             <td>account_balance</td>
                                             <td>current_balance</td>
                                          </tr>
                                          <tr>
                                             <td>mobile1</td>
                                             <td>mobile1</td>
                                          </tr>
                                          <tr>
                                             <td>mobile1</td>
                                             <td>email1</td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </section>
   </div>
   <footer class="main-footer">
      <div class="footer-bg">
         <div class="footer-left">
            <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
            © 2020 YouNegotiate ™ | All rights reserved
         </div>
      </div>
   </footer>
</div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
     
   
        $('#example').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
       
   
   });
</script>
@endsection