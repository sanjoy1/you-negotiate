@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Manage Users (Create & Manage Users Accounts to Access YouNegotiate)</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Create New User</h4>
                  </div>
                  <div class="card-body">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Name <sup>*</sup> <i class="icon-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="At vero eos et">!</i></label>
                                 <input type="text" class="form-control">
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Email <sup>*</sup> <i class="icon-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="At vero eos et">!</i></label>
                                 <input type="text" class="form-control">
                              </div>
                           </div>
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Phone <i class="icon-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="At vero eos et">!</i></label>
                                 <input type="text" class="form-control">
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Select Company <sup>*</sup> <i class="icon-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="At vero eos et">!</i></label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">US</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="text-right col-sm-12">
                           <button type="submit" class="btn blue-btn">Submit</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Current Users</h4>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive" id="example">
                           <thead>
                              <tr>
                                 <th class="">
                                    name
                                 </th>
                                 <th>email</th>
                                 <th>phone</th>
                                 <th>company name</th>
                                 <th>company type</th>
                                 <th>action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <strong>Natsu Dragneel</stron>
                                 </td>
                                 <td><strong>ndragneel@younegotiate.com</stron></td>
                                 <td>
                                    <strong>9846564755</stron>
                                 </td>
                                 <td><strong>	Good Company</stron></td>
                                 <td><strong>	Finance</stron></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#re-pass">Reset Password</a>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#block-user">Block</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Natsu Dragneel</stron>
                                 </td>
                                 <td><strong>ndragneel@younegotiate.com</stron></td>
                                 <td>
                                    <strong>9846564755</stron>
                                 </td>
                                 <td><strong>	Good Company</stron></td>
                                 <td><strong>	Finance</stron></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#re-pass">Reset Password</a>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#block-user">Block</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Natsu Dragneel</stron>
                                 </td>
                                 <td><strong>ndragneel@younegotiate.com</stron></td>
                                 <td>
                                    <strong>9846564755</stron>
                                 </td>
                                 <td><strong>	Good Company</stron></td>
                                 <td><strong>	Finance</stron></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#re-pass">Reset Password</a>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#block-user">Block</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Natsu Dragneel</stron>
                                 </td>
                                 <td><strong>ndragneel@younegotiate.com</stron></td>
                                 <td>
                                    <strong>9846564755</stron>
                                 </td>
                                 <td><strong>	Good Company</stron></td>
                                 <td><strong>	Finance</stron></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#re-pass">Reset Password</a>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#block-user">Block</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Natsu Dragneel</stron>
                                 </td>
                                 <td><strong>ndragneel@younegotiate.com</stron></td>
                                 <td>
                                    <strong>9846564755</stron>
                                 </td>
                                 <td><strong>	Good Company</stron></td>
                                 <td><strong>	Finance</stron></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#re-pass">Reset Password</a>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#block-user">Block</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Natsu Dragneel</stron>
                                 </td>
                                 <td><strong>ndragneel@younegotiate.com</stron></td>
                                 <td>
                                    <strong>9846564755</stron>
                                 </td>
                                 <td><strong>	Good Company</stron></td>
                                 <td><strong>	Finance</stron></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#re-pass">Reset Password</a>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#block-user">Block</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Natsu Dragneel</stron>
                                 </td>
                                 <td><strong>ndragneel@younegotiate.com</stron></td>
                                 <td>
                                    <strong>9846564755</stron>
                                 </td>
                                 <td><strong>	Good Company</stron></td>
                                 <td><strong>	Finance</stron></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#re-pass">Reset Password</a>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#block-user">Block</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Natsu Dragneel</stron>
                                 </td>
                                 <td><strong>ndragneel@younegotiate.com</stron></td>
                                 <td>
                                    <strong>9846564755</stron>
                                 </td>
                                 <td><strong>	Good Company</stron></td>
                                 <td><strong>	Finance</stron></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#re-pass">Reset Password</a>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#block-user">Block</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Natsu Dragneel</stron>
                                 </td>
                                 <td><strong>ndragneel@younegotiate.com</stron></td>
                                 <td>
                                    <strong>9846564755</stron>
                                 </td>
                                 <td><strong>	Good Company</stron></td>
                                 <td><strong>	Finance</stron></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#re-pass">Reset Password</a>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#block-user">Block</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Natsu Dragneel</stron>
                                 </td>
                                 <td><strong>ndragneel@younegotiate.com</stron></td>
                                 <td>
                                    <strong>9846564755</stron>
                                 </td>
                                 <td><strong>	Good Company</stron></td>
                                 <td><strong>	Finance</stron></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#re-pass">Reset Password</a>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#block-user">Block</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </section>
   </div>
   <footer class="main-footer">
      <div class="footer-bg">
         <div class="footer-left">
            <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
            © 2020 YouNegotiate ™ | All rights reserved
         </div>
      </div>
   </footer>
</div>
</div>


<div class="modal modal-new md-sm fade" id="re-pass" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Reset Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Do You Want to continue?</h2>
            <div class="text-right mt-4">
               <button type="submit" class="btn border-btn" data-dismiss="modal" aria-label="Close">Cancel</button>
               <button type="submit" class="btn blue-btn">Save</button>
            </div>
         </div>
      </div>
   </div>
</div>


<div class="modal modal-new md-sm fade" id="block-user" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Block User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Do You Want to block this user?</h2>
            <div class="text-right mt-4">
               <button type="submit" class="btn border-btn" data-dismiss="modal" aria-label="Close">Cancel</button>
               <button type="submit" class="btn blue-btn">Save</button>
            </div>
         </div>
      </div>
   </div>
</div>


@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
     
   
        $('#example').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
       
   
   });
</script>
@endsection