@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Cumulative Performance Report</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Cumulative Communication Stats</h4>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive" id="example2">
                           <thead>
                              <tr>
                                 <th>
                                    YN Accounts
                                 </th>
                                 <th>	Unique SMS Sent</th>
                                 <th>SMS Delivered</th>
                                 <th>Unique Email Sent</th>
                                 <th>Email Delivered</th>
                                 <th>Unique Accounts Clicked on Link</th>
                                 <th>Entered PII Data</th>
                                 <th>PII Mismatch</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <strong>325</strong>
                                 </td>
                                 <td><strong>6</strong></td>
                                 <td>
                                    <strong>5</strong>
                                 </td>
                                 <td>
                                    <strong>325</strong>
                                 </td>
                                 <td>
                                    <strong>325</strong>
                                 </td>
                                 <td>
                                    <strong>136</strong>
                                 </td>
                                 <td>
                                    <strong>135</strong>
                                 </td>
                                 <td>
                                    <strong>0.74% of # of entered PII Data</strong>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>100% Accounts in YN</strong>
                                 </td>
                                 <td><strong>1.85% of # of accounts in YN</strong></td>
                                 <td>
                                    <strong>83.33% of # of SMS sent</strong>
                                 </td>
                                 <td>
                                    <strong>100.00% of # of accounts in YN</strong>
                                 </td>
                                 <td>
                                    <strong>100.00% of # of email Sent</strong>
                                 </td>
                                 <td>
                                    <strong>41.85% of # of communication Sent</strong>
                                 </td>
                                 <td>
                                    <strong>41.85% of # of communication</strong>
                                 </td>
                                 <td>
                                    <strong>0.74% of # of entered PII Data</strong>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>100% Accounts in YN</strong>
                                 </td>
                                 <td><strong>1.85% of # of accounts in YN</strong></td>
                                 <td>
                                    <strong>83.33% of # of SMS sent</strong>
                                 </td>
                                 <td>
                                    <strong>100.00% of # of accounts in YN</strong>
                                 </td>
                                 <td>
                                    <strong>100.00% of # of email Sent</strong>
                                 </td>
                                 <td>
                                    <strong>41.85% of # of communication Sent</strong>
                                 </td>
                                 <td>
                                    <strong>41.85% of # of communication</strong>
                                 </td>
                                 <td>
                                    <strong>0.74% of # of entered PII Data</strong>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>100% Accounts in YN</strong>
                                 </td>
                                 <td><strong>1.85% of # of accounts in YN</strong></td>
                                 <td>
                                    <strong>83.33% of # of SMS sent</strong>
                                 </td>
                                 <td>
                                    <strong>100.00% of # of accounts in YN</strong>
                                 </td>
                                 <td>
                                    <strong>100.00% of # of email Sent</strong>
                                 </td>
                                 <td>
                                    <strong>41.85% of # of communication Sent</strong>
                                 </td>
                                 <td>
                                    <strong>41.85% of # of communication</strong>
                                 </td>
                                 <td>
                                    <strong>0.74% of # of entered PII Data</strong>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Cumulative Financial Stats</h4>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive" id="example3">
                           <thead>
                              <tr>
                                 <th>
                                    Successfully Negotiated
                                 </th>
                                 <th>Paid in Full</th>
                                 <th>Neg. PayProfile Set Up</th>
                                 <th>Account Settled</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <strong>37</strong>
                                 </td>
                                 <td><strong>30</strong></td>
                                 <td>
                                    <strong>5</strong>
                                 </td>
                                 <td>
                                    <strong>1</strong>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>27.61% of # of successful entered PII</strong>
                                 </td>
                                 <td><strong>22.39% of # of successful entered PII</strong></td>
                                 <td>
                                    <strong>75.68% of # of successfully negotiated</strong>
                                 </td>
                                 <td>
                                    <strong>10.00% of # of settled in full</strong>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>27.61% of # of successful entered PII</strong>
                                 </td>
                                 <td><strong>22.39% of # of successful entered PII</strong></td>
                                 <td>
                                    <strong>75.68% of # of successfully negotiated</strong>
                                 </td>
                                 <td>
                                    <strong>10.00% of # of settled in full</strong>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>27.61% of # of successful entered PII</strong>
                                 </td>
                                 <td><strong>22.39% of # of successful entered PII</strong></td>
                                 <td>
                                    <strong>75.68% of # of successfully negotiated</strong>
                                 </td>
                                 <td>
                                    <strong>10.00% of # of settled in full</strong>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </section>
   </div>
   <footer class="main-footer">
      <div class="footer-bg">
         <div class="footer-left">
            <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
            © 2020 YouNegotiate ™ | All rights reserved
         </div>
      </div>
   </footer>
</div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
    
   
        $('#example2').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
       $('#example3').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
     
    
   });
</script>
@endsection