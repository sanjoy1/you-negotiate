@extends('layout.dashboard.app')
@section('dashboard-content')

      <div class="main-content">
         <section class="section">
            <h1 class="section-header">
               <div>Current Customers</div>
            </h1>
            <div class="section-body">
              
               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Current Customers</h4>
                          
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table width-responsive" id="example">
                                 <thead>
                                    <tr>
                                       <th>
                                         account
                                       </th>
                                       <th>first name</th>
                                       <th>last name</th>
                                       <th>last 4 ssn</th>
                                       <th>email</th>
                                       <th>mobile</th>
                                       <th>invitATION LINK</th>
                                       <th>status</th>
                                       <th>action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Edward</strong></td>
                                       <td>
                                          <strong>Elric</strong>
                                       </td>
                                       <td><strong>2455</strong></td>
                                       <td>
                                        <strong>sunny11106@yopmail.com</strong>
                                       </td>
                                       <td>
                                         <strong>9787456113</strong>
                                       </td>
                                       <td><a href="#" class="link-blue-text">	https://yng.link/TESTod6045Rl</a></td>
                                       <td>
                                         <strong>Joined</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">Profile</a>
                                          <a href="#" class="btn btn-green">Subscribe</a>
                                          <a href="#" class="btn btn-reset">Cancel All Payments</a>
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                       
                                    </tr>
                                  <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Edward</strong></td>
                                       <td>
                                          <strong>Elric</strong>
                                       </td>
                                       <td><strong>2455</strong></td>
                                       <td>
                                        <strong>sunny11106@yopmail.com</strong>
                                       </td>
                                       <td>
                                         <strong>9787456113</strong>
                                       </td>
                                       <td><a href="#" class="link-blue-text">	https://yng.link/TESTod6045Rl</a></td>
                                       <td>
                                         <strong>Joined</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">Profile</a>
                                          <a href="#" class="btn btn-green">Subscribe</a>
                                          <a href="#" class="btn btn-reset">Cancel All Payments</a>
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Edward</strong></td>
                                       <td>
                                          <strong>Elric</strong>
                                       </td>
                                       <td><strong>2455</strong></td>
                                       <td>
                                        <strong>sunny11106@yopmail.com</strong>
                                       </td>
                                       <td>
                                         <strong>9787456113</strong>
                                       </td>
                                       <td><a href="#" class="link-blue-text">	https://yng.link/TESTod6045Rl</a></td>
                                       <td>
                                         <strong>Joined</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">Profile</a>
                                          <a href="#" class="btn btn-green">Subscribe</a>
                                          <a href="#" class="btn btn-reset">Cancel All Payments</a>
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Edward</strong></td>
                                       <td>
                                          <strong>Elric</strong>
                                       </td>
                                       <td><strong>2455</strong></td>
                                       <td>
                                        <strong>sunny11106@yopmail.com</strong>
                                       </td>
                                       <td>
                                         <strong>9787456113</strong>
                                       </td>
                                       <td><a href="#" class="link-blue-text">	https://yng.link/TESTod6045Rl</a></td>
                                       <td>
                                         <strong>Joined</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">Profile</a>
                                          <a href="#" class="btn btn-green">Subscribe</a>
                                          <a href="#" class="btn btn-reset">Cancel All Payments</a>
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Edward</strong></td>
                                       <td>
                                          <strong>Elric</strong>
                                       </td>
                                       <td><strong>2455</strong></td>
                                       <td>
                                        <strong>sunny11106@yopmail.com</strong>
                                       </td>
                                       <td>
                                         <strong>9787456113</strong>
                                       </td>
                                       <td><a href="#" class="link-blue-text">	https://yng.link/TESTod6045Rl</a></td>
                                       <td>
                                         <strong>Joined</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">Profile</a>
                                          <a href="#" class="btn btn-green">Subscribe</a>
                                          <a href="#" class="btn btn-reset">Cancel All Payments</a>
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Edward</strong></td>
                                       <td>
                                          <strong>Elric</strong>
                                       </td>
                                       <td><strong>2455</strong></td>
                                       <td>
                                        <strong>sunny11106@yopmail.com</strong>
                                       </td>
                                       <td>
                                         <strong>9787456113</strong>
                                       </td>
                                       <td><a href="#" class="link-blue-text">	https://yng.link/TESTod6045Rl</a></td>
                                       <td>
                                         <strong>Joined</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">Profile</a>
                                          <a href="#" class="btn btn-green">Subscribe</a>
                                          <a href="#" class="btn btn-reset">Cancel All Payments</a>
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Edward</strong></td>
                                       <td>
                                          <strong>Elric</strong>
                                       </td>
                                       <td><strong>2455</strong></td>
                                       <td>
                                        <strong>sunny11106@yopmail.com</strong>
                                       </td>
                                       <td>
                                         <strong>9787456113</strong>
                                       </td>
                                       <td><a href="#" class="link-blue-text">	https://yng.link/TESTod6045Rl</a></td>
                                       <td>
                                         <strong>Joined</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">Profile</a>
                                          <a href="#" class="btn btn-green">Subscribe</a>
                                          <a href="#" class="btn btn-reset">Cancel All Payments</a>
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Edward</strong></td>
                                       <td>
                                          <strong>Elric</strong>
                                       </td>
                                       <td><strong>2455</strong></td>
                                       <td>
                                        <strong>sunny11106@yopmail.com</strong>
                                       </td>
                                       <td>
                                         <strong>9787456113</strong>
                                       </td>
                                       <td><a href="#" class="link-blue-text">	https://yng.link/TESTod6045Rl</a></td>
                                       <td>
                                         <strong>Joined</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">Profile</a>
                                          <a href="#" class="btn btn-green">Subscribe</a>
                                          <a href="#" class="btn btn-reset">Cancel All Payments</a>
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Edward</strong></td>
                                       <td>
                                          <strong>Elric</strong>
                                       </td>
                                       <td><strong>2455</strong></td>
                                       <td>
                                        <strong>sunny11106@yopmail.com</strong>
                                       </td>
                                       <td>
                                         <strong>9787456113</strong>
                                       </td>
                                       <td><a href="#" class="link-blue-text">	https://yng.link/TESTod6045Rl</a></td>
                                       <td>
                                         <strong>Joined</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">Profile</a>
                                          <a href="#" class="btn btn-green">Subscribe</a>
                                          <a href="#" class="btn btn-reset">Cancel All Payments</a>
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Edward</strong></td>
                                       <td>
                                          <strong>Elric</strong>
                                       </td>
                                       <td><strong>2455</strong></td>
                                       <td>
                                        <strong>sunny11106@yopmail.com</strong>
                                       </td>
                                       <td>
                                         <strong>9787456113</strong>
                                       </td>
                                       <td><a href="#" class="link-blue-text">	https://yng.link/TESTod6045Rl</a></td>
                                       <td>
                                         <strong>Joined</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">Profile</a>
                                          <a href="#" class="btn btn-green">Subscribe</a>
                                          <a href="#" class="btn btn-reset">Cancel All Payments</a>
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Edward</strong></td>
                                       <td>
                                          <strong>Elric</strong>
                                       </td>
                                       <td><strong>2455</strong></td>
                                       <td>
                                        <strong>sunny11106@yopmail.com</strong>
                                       </td>
                                       <td>
                                         <strong>9787456113</strong>
                                       </td>
                                       <td><a href="#" class="link-blue-text">	https://yng.link/TESTod6045Rl</a></td>
                                       <td>
                                         <strong>Joined</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">Profile</a>
                                          <a href="#" class="btn btn-green">Subscribe</a>
                                          <a href="#" class="btn btn-reset">Cancel All Payments</a>
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                       
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

           

            </div>
         </section>
      </div>
      <footer class="main-footer">
         <div class="footer-bg">
         <div class="footer-left">
           <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
             © 2020 YouNegotiate ™ | All rights reserved
         </div>
         </div>
      </footer>
   </div>
</div>
@endsection

@section('script')
    <script src="{{asset('dist/modules/jquery.min.js')}}"></script>
    <script src="{{asset('dist/modules/popper.js')}}"></script>
    <script src="{{asset('dist/modules/tooltip.js')}}"></script>
    <script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
    <script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
    <script src="{{asset('dist/js/sa-functions.js')}}"></script>
    <script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
    <script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
    <script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
    <script src="{{asset('dist/modules/chart.min.js')}}"></script>
    <script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
    <script src="{{asset('dist/js/scripts.js')}}"></script>
    <script src="{{asset('dist/js/custom.js')}}"></script>
<script>
    $(document).ready(function () {
      

         $('#example').DataTable({
            language: {
               searchPlaceholder: "Search",
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });

    });
</script>
@endsection