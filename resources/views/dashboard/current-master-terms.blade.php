@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Update Customer Terms on the Fly</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Current Master Terms</h4>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive" id="example">
                           <thead>
                              <tr>
                                 <th>
                                    account
                                 </th>
                                 <th>customer name</th>
                                 <th>sub account(S)</th>
                                 <th>current balance</th>
                                 <th>pay in full balance</th>
                                 <th>Payment Plan Balance</th>
                                 <th>Min. Monthly Payment</th>
                                 <th># Days 1st Payment</th>
                                 <th>action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <strong>245661</strong>
                                 </td>
                                 <td><strong>Edward Elric</strong></td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td>
                                    <strong>$600.00</strong>
                                 </td>
                                 <td>
                                    <strong>$700.00</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <a href="counter-offer2" class="btn btn-green">Change Terms</a>
                                    <a href="#" class="btn btn-view">Subscribe</a>
                                    
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>245661</strong>
                                 </td>
                                 <td><strong>Edward Elric</strong></td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td>
                                    <strong>$600.00</strong>
                                 </td>
                                 <td>
                                    <strong>$700.00</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <a href="counter-offer2" class="btn btn-green">Change Terms</a>
                                    <a href="#" class="btn btn-view">Subscribe</a>
                                    
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>245661</strong>
                                 </td>
                                 <td><strong>Edward Elric</strong></td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td>
                                    <strong>$600.00</strong>
                                 </td>
                                 <td>
                                    <strong>$700.00</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <a href="counter-offer2" class="btn btn-green">Change Terms</a>
                                    <a href="#" class="btn btn-view">Subscribe</a>
                                    
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>245661</strong>
                                 </td>
                                 <td><strong>Edward Elric</strong></td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td>
                                    <strong>$600.00</strong>
                                 </td>
                                 <td>
                                    <strong>$700.00</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <a href="counter-offer2" class="btn btn-green">Change Terms</a>
                                    <a href="#" class="btn btn-view">Subscribe</a>
                                    
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>245661</strong>
                                 </td>
                                 <td><strong>Edward Elric</strong></td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td>
                                    <strong>$600.00</strong>
                                 </td>
                                 <td>
                                    <strong>$700.00</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <a href="counter-offer2" class="btn btn-green">Change Terms</a>
                                    <a href="#" class="btn btn-view">Subscribe</a>
                                    
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>245661</strong>
                                 </td>
                                 <td><strong>Edward Elric</strong></td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td>
                                    <strong>$600.00</strong>
                                 </td>
                                 <td>
                                    <strong>$700.00</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <a href="counter-offer2" class="btn btn-green">Change Terms</a>
                                    <a href="#" class="btn btn-view">Subscribe</a>
                                    
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>245661</strong>
                                 </td>
                                 <td><strong>Edward Elric</strong></td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td>
                                    <strong>$600.00</strong>
                                 </td>
                                 <td>
                                    <strong>$700.00</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <a href="counter-offer2" class="btn btn-green">Change Terms</a>
                                    <a href="#" class="btn btn-view">Subscribe</a>
                                    
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>245661</strong>
                                 </td>
                                 <td><strong>Edward Elric</strong></td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td>
                                    <strong>$600.00</strong>
                                 </td>
                                 <td>
                                    <strong>$700.00</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <a href="counter-offer2" class="btn btn-green">Change Terms</a>
                                    <a href="#" class="btn btn-view">Subscribe</a>
                                    
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>245661</strong>
                                 </td>
                                 <td><strong>Edward Elric</strong></td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td>
                                    <strong>$600.00</strong>
                                 </td>
                                 <td>
                                    <strong>$700.00</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <a href="counter-offer2" class="btn btn-green">Change Terms</a>
                                    <a href="#" class="btn btn-view">Subscribe</a>
                                    
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>245661</strong>
                                 </td>
                                 <td><strong>Edward Elric</strong></td>
                                 <td>
                                    <strong>YouNegotiate</strong>
                                 </td>
                                 <td><strong>$1000.00</strong></td>
                                 <td>
                                    <strong>$600.00</strong>
                                 </td>
                                 <td>
                                    <strong>$700.00</strong>
                                 </td>
                                 <td>
                                    <strong>$70.00</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <a href="counter-offer2" class="btn btn-green">Change Terms</a>
                                    <a href="#" class="btn btn-view">Subscribe</a>
                                    
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
<footer class="main-footer">
   <div class="footer-bg">
      <div class="footer-left">
         <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
      </div>
      <div class="footer-right">
         © 2020 YouNegotiate ™ | All rights reserved
      </div>
   </div>
</footer>
</div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
     
   
        $('#example').DataTable({
           language: {
            
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
   });
</script>
@endsection