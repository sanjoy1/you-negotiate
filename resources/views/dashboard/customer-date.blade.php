@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Import Customer Data</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-body">
                     <div class="row">
                        <div class="col-sm-5">
                           <div class="form-group">
                              <label>Select Your Header</label>
                              <select class="custom-select">
                                 <option selected="">Select</option>
                                 <option value="1">US</option>
                                 <option value="2">Two</option>
                                 <option value="3">Three</option>
                              </select>
                           </div>
                           <div class="form-group">
                              <label>Upload file</label>
                              <div class="button-upload">
                                 <span class="label">
                                 <img alt="upload" src="{{asset('dist/img/upload.svg')}}" class="mr-2"> Upload File
                                 </span>
                                 <input type="file" name="upload" id="upload" class="upload-box" placeholder="Upload File">
                              </div>
                           </div>
                           <div class="note-content">
                              <h6>Type</h6>
                              <div>
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="c1" name="c1" class="custom-control-input">
                                    <label class="custom-control-label" for="c1">Add & Send Dunning/Demand Consumer Communication 
                                    (CFPB Reg F Template)</label>
                                 </div>
                              </div>
                              <div>
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="c2" name="c2" class="custom-control-input">
                                    <label class="custom-control-label" for="c2">Add New Consumer Accounts</label>
                                 </div>
                              </div>
                              <div>
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="c3" name="c3" class="custom-control-input">
                                    <label class="custom-control-label" for="c3">Update Balance & Replace/Override Master</label>
                                 </div>
                              </div>
                              <div>
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="c4" name="c4" class="custom-control-input">
                                    <label class="custom-control-label" for="c4">Delete Customers</label>
                                 </div>
                              </div>
                           </div>
                           <div class="text-left mt-4">
                              <button type="submit" class="btn blue-btn">Submit</button>
                           </div>
                        </div>
                        <div class="col-sm-7 p-left your-head">
                           <div class="table-border-st">
                              <table class="table width-responsive">
                                 <thead>
                                    <tr>
                                       <th>Your Uploaded Headers</th>
                                       <th>YouNegotiate Data Field</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>User Accounts</td>
                                       <td>account_number</td>
                                    </tr>
                                    <tr>
                                       <td>F Name</td>
                                       <td>first_name</td>
                                    </tr>
                                    <tr>
                                       <td>L Name</td>
                                       <td>last_name</td>
                                    </tr>
                                    <tr>
                                       <td>last4ssn</td>
                                       <td>last4ssn</td>
                                    </tr>
                                    <tr>
                                       <td>dob</td>
                                       <td>dob</td>
                                    </tr>
                                    <tr>
                                       <td>account_balance</td>
                                       <td>current_balance</td>
                                    </tr>
                                    <tr>
                                       <td>mobile1</td>
                                       <td>mobile1</td>
                                    </tr>
                                    <tr>
                                       <td>mobile1</td>
                                       <td>email1</td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>File Upload History</h4>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive" id="example">
                           <thead>
                              <tr>
                                 <th class="text-left">
                                    file name
                                 </th>
                                 <th>dATE TIME</th>
                                 <th>USER</th>
                                 <th>UPLOAD TYPE</th>
                                 <th>NUMBER OF RECORDS</th>
                                 <th>PROCESSED</th>
                                 <th>FAILED</th>
                                 <th>Status</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <strong>20200622074359Internal_Beta_1.csv</stron>
                                 </td>
                                 <td><strong>2020-06-22 07:43</stron></td>
                                 <td>
                                    <strong>TestCompany</stron>
                                 </td>
                                 <td><a href="#" class="green-text">Add</a></td>
                                 <td><strong>	2</stron></td>
                                 <td>
                                    <strong>2</strong>
                                 </td>
                                 <td>
                                    <strong>1 <a href="#" class="blue-text">Download</a></strong>
                                 </td>
                                 <td><span class="green-text">Complete</span></td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>20200622074359Internal_Beta_1.csv</stron>
                                 </td>
                                 <td><strong>2020-06-22 07:43</stron></td>
                                 <td>
                                    <strong>TestCompany</stron>
                                 </td>
                                 <td><a href="#" class="green-text">Add</a></td>
                                 <td><strong>	2</stron></td>
                                 <td>
                                    <strong>2</strong>
                                 </td>
                                 <td>
                                    <strong>0</strong>
                                 </td>
                                 <td><span class="green-text">Complete</span></td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>20200622074359Internal_Beta_1.csv</stron>
                                 </td>
                                 <td><strong>2020-06-22 07:43</stron></td>
                                 <td>
                                    <strong>TestCompany</stron>
                                 </td>
                                 <td><a href="#" class="green-text">Add</a></td>
                                 <td><strong>	2</stron></td>
                                 <td>
                                    <strong>2</strong>
                                 </td>
                                 <td>
                                    <strong>0</strong>
                                 </td>
                                 <td><span class="green-text">Complete</span></td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>20200622074359Internal_Beta_1.csv</stron>
                                 </td>
                                 <td><strong>2020-06-22 07:43</stron></td>
                                 <td>
                                    <strong>TestCompany</stron>
                                 </td>
                                 <td><a href="#" class="green-text">Add</a></td>
                                 <td><strong>	2</stron></td>
                                 <td>
                                    <strong>2</strong>
                                 </td>
                                 <td>
                                    <strong>0</strong>
                                 </td>
                                 <td><span class="green-text">Complete</span></td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>20200622074359Internal_Beta_1.csv</stron>
                                 </td>
                                 <td><strong>2020-06-22 07:43</stron></td>
                                 <td>
                                    <strong>TestCompany</stron>
                                 </td>
                                 <td><a href="#" class="green-text">Add</a></td>
                                 <td><strong>	2</stron></td>
                                 <td>
                                    <strong>2</strong>
                                 </td>
                                 <td>
                                    <strong>0</strong>
                                 </td>
                                 <td><span class="green-text">Complete</span></td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>20200622074359Internal_Beta_1.csv</stron>
                                 </td>
                                 <td><strong>2020-06-22 07:43</stron></td>
                                 <td>
                                    <strong>TestCompany</stron>
                                 </td>
                                 <td><a href="#" class="green-text">Add</a></td>
                                 <td><strong>	2</stron></td>
                                 <td>
                                    <strong>2</strong>
                                 </td>
                                 <td>
                                    <strong>0</strong>
                                 </td>
                                 <td><span class="green-text">Complete</span></td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>20200622074359Internal_Beta_1.csv</stron>
                                 </td>
                                 <td><strong>2020-06-22 07:43</stron></td>
                                 <td>
                                    <strong>TestCompany</stron>
                                 </td>
                                 <td><a href="#" class="green-text">Add</a></td>
                                 <td><strong>	2</stron></td>
                                 <td>
                                    <strong>2</strong>
                                 </td>
                                 <td>
                                    <strong>0</strong>
                                 </td>
                                 <td><span class="green-text">Complete</span></td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>20200622074359Internal_Beta_1.csv</stron>
                                 </td>
                                 <td><strong>2020-06-22 07:43</stron></td>
                                 <td>
                                    <strong>TestCompany</stron>
                                 </td>
                                 <td><a href="#" class="green-text">Add</a></td>
                                 <td><strong>	2</stron></td>
                                 <td>
                                    <strong>2</strong>
                                 </td>
                                 <td>
                                    <strong>0</strong>
                                 </td>
                                 <td><span class="green-text">Complete</span></td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </section>
   </div>
   <footer class="main-footer">
      <div class="footer-bg">
         <div class="footer-left">
            <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
            © 2020 YouNegotiate ™ | All rights reserved
         </div>
      </div>
   </footer>
</div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
     
   
        $('#example').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
       
   
   });
</script>
@endsection