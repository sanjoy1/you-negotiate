@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Communication Groups</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Create New Group</h4>
                  </div>
                  <div class="card-body">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Group Name <sup>*</sup></label>
                                 <input type="text" class="form-control">
                              </div>
                           </div>
                           <div class="form-group">
                              <label>Select Customer Type</label>
                              <div>
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="c1" name="c1" class="custom-control-input">
                                    <label class="custom-control-label" for="c1">All Customers</label>
                                 </div>
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="c2" name="c2" class="custom-control-input">
                                    <label class="custom-control-label" for="c2">Only Has Email</label>
                                 </div>
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="c3" name="c3" class="custom-control-input">
                                    <label class="custom-control-label" for="c3">Paid In Full</label>
                                 </div>
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="c4" name="c4" class="custom-control-input">
                                    <label class="custom-control-label" for="c4">Not Joined</label>
                                 </div>
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="c5" name="c5" class="custom-control-input">
                                    <label class="custom-control-label" for="c5">Failed Payment</label>
                                 </div>
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="c6" name="c6" class="custom-control-input">
                                    <label class="custom-control-label" for="c6">Only Has SMS</label>
                                 </div>
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="c7" name="c7" class="custom-control-input">
                                    <label class="custom-control-label" for="c7">Open Negotiation</label>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label>Subject <sup>*</sup></label>
                              <textarea class="form-control height-auto"></textarea>
                           </div>
                           <div class="form-group mt-5">
                              <h6>Add Custom Rules</h6>
                           </div>
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Custom Rules to Select Customers </label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">US</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                 </select>
                              </div>
                              <div class="form-group col-md-6">
                                 <div class="row">
                                    <div class="col-md-4">
                                       <label>From </label>
                                       <input type="text" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                       <label>To </label>
                                       <input type="text" class="form-control">
                                    </div>
                                    <div class="col-md-4 text-right">
                                       <a href="#" class="top-minus">
                                          <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                             <path d="M3 6.14062H5H21" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                             <path d="M19 6.14062V20.1406C19 20.6711 18.7893 21.1798 18.4142 21.5548C18.0391 21.9299 17.5304 22.1406 17 22.1406H7C6.46957 22.1406 5.96086 21.9299 5.58579 21.5548C5.21071 21.1798 5 20.6711 5 20.1406V6.14062M8 6.14062V4.14062C8 3.61019 8.21071 3.10148 8.58579 2.72641C8.96086 2.35134 9.46957 2.14063 10 2.14062H14C14.5304 2.14063 15.0391 2.35134 15.4142 2.72641C15.7893 3.10148 16 3.61019 16 4.14062V6.14062" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                             <path d="M10 11.1406V17.1406" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                             <path d="M14 11.1406V17.1406" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                       </a>
                                       </svg>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="text-right col-sm-12">
                              <button type="submit" class="btn blue-btn">Add More Rule</button>
                           </div>
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Attach Email Template (if you want to send now):</label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">US</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                 </select>
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Attach SMS Template (if you want to send now):</label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">US</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="text-right col-sm-12">
                           <button type="submit" class="btn btn-reset">Preview Group Size</button>
                           <button type="submit" class="btn blue-btn">Save</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Current Customer Groups</h4>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive fil-width-big2" id="example2">
                           <thead>
                              <tr>
                                 <th>
                                    GROUP NAME
                                 </th>
                                 <th>CREATED on</th>
                                 <th>created by</th>
                                 <th>total consumer</th>
                                 <th>Note</th>
                                 <th>total balance</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <strong>ABCD group</strong>
                                 </td>
                                 <td><strong>2022-06-06 03:23</strong></td>
                                 <td>
                                    <strong>Donna Weaver</strong>
                                 </td>
                                 <td><strong>12</strong></td>
                                 <td><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</strong></td>
                                 <td><strong>$1,45,000</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" >Send</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-b" >Export</a>
                                    <a href="#" class="btn btn-yellow">Delete Consumers</a>
                                    <a href="#" class="btn btn-view" >Set Pay Terms</a>
                                    <a href="#" class="btn btn-deactive">Disable</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>ABCD group</strong>
                                 </td>
                                 <td><strong>2022-06-06 03:23</strong></td>
                                 <td>
                                    <strong>Donna Weaver</strong>
                                 </td>
                                 <td><strong>12</strong></td>
                                 <td><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</strong></td>
                                 <td><strong>$1,45,000</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" >Send</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-b" >Export</a>
                                    <a href="#" class="btn btn-yellow">Delete Consumers</a>
                                    <a href="#" class="btn btn-view" >Set Pay Terms</a>
                                    <a href="#" class="btn btn-deactive">Disable</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>ABCD group</strong>
                                 </td>
                                 <td><strong>2022-06-06 03:23</strong></td>
                                 <td>
                                    <strong>Donna Weaver</strong>
                                 </td>
                                 <td><strong>12</strong></td>
                                 <td><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</strong></td>
                                 <td><strong>$1,45,000</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" >Send</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-b" >Export</a>
                                    <a href="#" class="btn btn-yellow">Delete Consumers</a>
                                    <a href="#" class="btn btn-view" >Set Pay Terms</a>
                                    <a href="#" class="btn btn-deactive">Disable</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>ABCD group</strong>
                                 </td>
                                 <td><strong>2022-06-06 03:23</strong></td>
                                 <td>
                                    <strong>Donna Weaver</strong>
                                 </td>
                                 <td><strong>12</strong></td>
                                 <td><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</strong></td>
                                 <td><strong>$1,45,000</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" >Send</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-b" >Export</a>
                                    <a href="#" class="btn btn-yellow">Delete Consumers</a>
                                    <a href="#" class="btn btn-view" >Set Pay Terms</a>
                                    <a href="#" class="btn btn-deactive">Disable</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>ABCD group</strong>
                                 </td>
                                 <td><strong>2022-06-06 03:23</strong></td>
                                 <td>
                                    <strong>Donna Weaver</strong>
                                 </td>
                                 <td><strong>12</strong></td>
                                 <td><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</strong></td>
                                 <td><strong>$1,45,000</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" >Send</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-b" >Export</a>
                                    <a href="#" class="btn btn-yellow">Delete Consumers</a>
                                    <a href="#" class="btn btn-view" >Set Pay Terms</a>
                                    <a href="#" class="btn btn-deactive">Disable</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>ABCD group</strong>
                                 </td>
                                 <td><strong>2022-06-06 03:23</strong></td>
                                 <td>
                                    <strong>Donna Weaver</strong>
                                 </td>
                                 <td><strong>12</strong></td>
                                 <td><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</strong></td>
                                 <td><strong>$1,45,000</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" >Send</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-b" >Export</a>
                                    <a href="#" class="btn btn-yellow">Delete Consumers</a>
                                    <a href="#" class="btn btn-view" >Set Pay Terms</a>
                                    <a href="#" class="btn btn-deactive">Disable</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>ABCD group</strong>
                                 </td>
                                 <td><strong>2022-06-06 03:23</strong></td>
                                 <td>
                                    <strong>Donna Weaver</strong>
                                 </td>
                                 <td><strong>12</strong></td>
                                 <td><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</strong></td>
                                 <td><strong>$1,45,000</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" >Send</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-b" >Export</a>
                                    <a href="#" class="btn btn-yellow">Delete Consumers</a>
                                    <a href="#" class="btn btn-view" >Set Pay Terms</a>
                                    <a href="#" class="btn btn-deactive">Disable</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>ABCD group</strong>
                                 </td>
                                 <td><strong>2022-06-06 03:23</strong></td>
                                 <td>
                                    <strong>Donna Weaver</strong>
                                 </td>
                                 <td><strong>12</strong></td>
                                 <td><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</strong></td>
                                 <td><strong>$1,45,000</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" >Send</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-b" >Export</a>
                                    <a href="#" class="btn btn-yellow">Delete Consumers</a>
                                    <a href="#" class="btn btn-view" >Set Pay Terms</a>
                                    <a href="#" class="btn btn-deactive">Disable</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>ABCD group</strong>
                                 </td>
                                 <td><strong>2022-06-06 03:23</strong></td>
                                 <td>
                                    <strong>Donna Weaver</strong>
                                 </td>
                                 <td><strong>12</strong></td>
                                 <td><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</strong></td>
                                 <td><strong>$1,45,000</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" >Send</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-b" >Export</a>
                                    <a href="#" class="btn btn-yellow">Delete Consumers</a>
                                    <a href="#" class="btn btn-view" >Set Pay Terms</a>
                                    <a href="#" class="btn btn-deactive">Disable</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>ABCD group</strong>
                                 </td>
                                 <td><strong>2022-06-06 03:23</strong></td>
                                 <td>
                                    <strong>Donna Weaver</strong>
                                 </td>
                                 <td><strong>12</strong></td>
                                 <td><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</strong></td>
                                 <td><strong>$1,45,000</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" >Send</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-b" >Export</a>
                                    <a href="#" class="btn btn-yellow">Delete Consumers</a>
                                    <a href="#" class="btn btn-view" >Set Pay Terms</a>
                                    <a href="#" class="btn btn-deactive">Disable</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>ABCD group</strong>
                                 </td>
                                 <td><strong>2022-06-06 03:23</strong></td>
                                 <td>
                                    <strong>Donna Weaver</strong>
                                 </td>
                                 <td><strong>12</strong></td>
                                 <td><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</strong></td>
                                 <td><strong>$1,45,000</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view" >Send</a>
                                    <a href="#" class="btn btn-green">Edit</a>
                                    <a href="#" class="btn btn-b" >Export</a>
                                    <a href="#" class="btn btn-yellow">Delete Consumers</a>
                                    <a href="#" class="btn btn-view" >Set Pay Terms</a>
                                    <a href="#" class="btn btn-deactive">Disable</a>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </section>
   </div>
   <footer class="main-footer">
      <div class="footer-bg">
         <div class="footer-left">
            <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
            © 2020 YouNegotiate ™ | All rights reserved
         </div>
      </div>
   </footer>
</div>
</div>
<!-- Modal -->
<div class="modal modal-new fade" id="preview" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Preview</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <div class="row">
               <div class="col-sm-12">
                  <h4>Subject</h4>
                  <h5><strong>Section 1.10.33 of "de Finibus Bonorum et Malorum", written by 
                     Cicero in 45 BC</strong>
                  </h5>
               </div>
               <div class="col-sm-12">
                  <h4>Content</h4>
                  <p>"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal modal-new md-sm fade" id="delete-template" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Delete Template</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Do You Want to delete this?</h2>
            <div class="text-right">
               <button type="submit" class="btn border-btn" data-dismiss="modal" aria-label="Close">Close</button>
               <button type="submit" class="btn blue-btn">Save</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal modal-new fade" id="send-template" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Select Group to Run Send the Template</h5>
            <button type="button" class="close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <form>
               <div class="form-group">
                  <label>Select Group </label>
                  <select class="custom-select">
                     <option selected="">Select</option>
                     <option value="1">US</option>
                     <option value="2">Two</option>
                     <option value="3">Three</option>
                  </select>
               </div>
               <div class="text-right">
                  <button type="submit" class="btn border-btn" data-dismiss="modal" aria-label="Close">Close</button>
                  <button type="submit" class="btn blue-btn">Save</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $('#summernote').summernote({
      placeholder: 'Hello stand alone ui',
      tabsize: 2,
      height: 100,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
      ]
    });
   $(document).ready(function () {
   
   
   
       $('#example2').DataTable({
          language: {
            searchPlaceholder: "Search",
              'paginate': {
                  'previous': '<span class="fa fa-angle-left"></span>',
                  'next': '<span class="fa fa-angle-right"></span>'
              }
          }
      });
   
   });
</script>
@endsection