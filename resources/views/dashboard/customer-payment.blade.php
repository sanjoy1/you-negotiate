@extends('layout.dashboard.app')
@section('dashboard-content')

      <div class="main-content">
         <section class="section">
            <h1 class="section-header">
               <div>Customers on Payment Plans</div>
            </h1>
            <div class="section-body">
              
               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Customers on Payment Plans</h4>
                          
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table width-responsive" id="example">
                                 <thead>
                                    <tr>
                                       <th>
                                         custoemr name
                                       </th>
                                       <th>account</th>
                                       <th>Sub Account</th>
                                       <th>current balance</th>
                                       <th>pay in full</th>
                                       <th>payment plan balance</th>
                                       <th>min monthly payment</th>
                                       <th>profile created on</th>
                                       <th>action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <strong>Roy Mustang</strong>
                                       </td>
                                       <td><strong>32435666661</strong></td>
                                       <td>
                                          <strong>YouNegotiate</strong>
                                       </td>
                                       <td><strong>$100.00</strong></td>
                                       <td>
                                        <strong>$70.00</strong>
                                       </td>
                                       <td>
                                         <strong>$75.00</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$12.00</strong>
                                       </td>
                                       <td>
                                         <strong>1969-12-31</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">View</a>
                                         
                                       </td>
                                       
                                    </tr>
                                  <tr>
                                       <td>
                                          <strong>Roy Mustang</strong>
                                       </td>
                                       <td><strong>32435666661</strong></td>
                                       <td>
                                          <strong>YouNegotiate</strong>
                                       </td>
                                       <td><strong>$100.00</strong></td>
                                       <td>
                                        <strong>$70.00</strong>
                                       </td>
                                       <td>
                                         <strong>$75.00</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$12.00</strong>
                                       </td>
                                       <td>
                                         <strong>1969-12-31</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">View</a>
                                         
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>Roy Mustang</strong>
                                       </td>
                                       <td><strong>32435666661</strong></td>
                                       <td>
                                          <strong>YouNegotiate</strong>
                                       </td>
                                       <td><strong>$100.00</strong></td>
                                       <td>
                                        <strong>$70.00</strong>
                                       </td>
                                       <td>
                                         <strong>$75.00</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$12.00</strong>
                                       </td>
                                       <td>
                                         <strong>1969-12-31</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">View</a>
                                         
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>Roy Mustang</strong>
                                       </td>
                                       <td><strong>32435666661</strong></td>
                                       <td>
                                          <strong>YouNegotiate</strong>
                                       </td>
                                       <td><strong>$100.00</strong></td>
                                       <td>
                                        <strong>$70.00</strong>
                                       </td>
                                       <td>
                                         <strong>$75.00</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$12.00</strong>
                                       </td>
                                       <td>
                                         <strong>1969-12-31</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">View</a>
                                         
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>Roy Mustang</strong>
                                       </td>
                                       <td><strong>32435666661</strong></td>
                                       <td>
                                          <strong>YouNegotiate</strong>
                                       </td>
                                       <td><strong>$100.00</strong></td>
                                       <td>
                                        <strong>$70.00</strong>
                                       </td>
                                       <td>
                                         <strong>$75.00</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$12.00</strong>
                                       </td>
                                       <td>
                                         <strong>1969-12-31</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">View</a>
                                         
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>Roy Mustang</strong>
                                       </td>
                                       <td><strong>32435666661</strong></td>
                                       <td>
                                          <strong>YouNegotiate</strong>
                                       </td>
                                       <td><strong>$100.00</strong></td>
                                       <td>
                                        <strong>$70.00</strong>
                                       </td>
                                       <td>
                                         <strong>$75.00</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$12.00</strong>
                                       </td>
                                       <td>
                                         <strong>1969-12-31</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">View</a>
                                         
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>Roy Mustang</strong>
                                       </td>
                                       <td><strong>32435666661</strong></td>
                                       <td>
                                          <strong>YouNegotiate</strong>
                                       </td>
                                       <td><strong>$100.00</strong></td>
                                       <td>
                                        <strong>$70.00</strong>
                                       </td>
                                       <td>
                                         <strong>$75.00</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$12.00</strong>
                                       </td>
                                       <td>
                                         <strong>1969-12-31</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">View</a>
                                         
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>Roy Mustang</strong>
                                       </td>
                                       <td><strong>32435666661</strong></td>
                                       <td>
                                          <strong>YouNegotiate</strong>
                                       </td>
                                       <td><strong>$100.00</strong></td>
                                       <td>
                                        <strong>$70.00</strong>
                                       </td>
                                       <td>
                                         <strong>$75.00</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$12.00</strong>
                                       </td>
                                       <td>
                                         <strong>1969-12-31</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">View</a>
                                         
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>Roy Mustang</strong>
                                       </td>
                                       <td><strong>32435666661</strong></td>
                                       <td>
                                          <strong>YouNegotiate</strong>
                                       </td>
                                       <td><strong>$100.00</strong></td>
                                       <td>
                                        <strong>$70.00</strong>
                                       </td>
                                       <td>
                                         <strong>$75.00</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$12.00</strong>
                                       </td>
                                       <td>
                                         <strong>1969-12-31</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">View</a>
                                         
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>Roy Mustang</strong>
                                       </td>
                                       <td><strong>32435666661</strong></td>
                                       <td>
                                          <strong>YouNegotiate</strong>
                                       </td>
                                       <td><strong>$100.00</strong></td>
                                       <td>
                                        <strong>$70.00</strong>
                                       </td>
                                       <td>
                                         <strong>$75.00</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$12.00</strong>
                                       </td>
                                       <td>
                                         <strong>1969-12-31</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">View</a>
                                         
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>Roy Mustang</strong>
                                       </td>
                                       <td><strong>32435666661</strong></td>
                                       <td>
                                          <strong>YouNegotiate</strong>
                                       </td>
                                       <td><strong>$100.00</strong></td>
                                       <td>
                                        <strong>$70.00</strong>
                                       </td>
                                       <td>
                                         <strong>$75.00</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$12.00</strong>
                                       </td>
                                       <td>
                                         <strong>1969-12-31</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">View</a>
                                         
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>Roy Mustang</strong>
                                       </td>
                                       <td><strong>32435666661</strong></td>
                                       <td>
                                          <strong>YouNegotiate</strong>
                                       </td>
                                       <td><strong>$100.00</strong></td>
                                       <td>
                                        <strong>$70.00</strong>
                                       </td>
                                       <td>
                                         <strong>$75.00</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$12.00</strong>
                                       </td>
                                       <td>
                                         <strong>1969-12-31</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view">View</a>
                                         
                                       </td>
                                       
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

           

            </div>
         </section>
      </div>
      <footer class="main-footer">
         <div class="footer-bg">
         <div class="footer-left">
           <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
             © 2020 YouNegotiate ™ | All rights reserved
         </div>
         </div>
      </footer>
   </div>
</div>
@endsection

@section('script')
    <script src="{{asset('dist/modules/jquery.min.js')}}"></script>
    <script src="{{asset('dist/modules/popper.js')}}"></script>
    <script src="{{asset('dist/modules/tooltip.js')}}"></script>
    <script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
    <script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
    <script src="{{asset('dist/js/sa-functions.js')}}"></script>
    <script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
    <script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
    <script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
    <script src="{{asset('dist/modules/chart.min.js')}}"></script>
    <script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
    <script src="{{asset('dist/js/scripts.js')}}"></script>
    <script src="{{asset('dist/js/custom.js')}}"></script>
<script>
    $(document).ready(function () {
      

         $('#example').DataTable({
            language: {
               searchPlaceholder: "Search",
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });

    });
</script>
@endsection