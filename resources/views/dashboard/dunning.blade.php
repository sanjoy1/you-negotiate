@extends('layout.dashboard.app')
@section('dashboard-content')

      <div class="main-content">
         <section class="section">
            <h1 class="section-header">
               <div> Dunning/Demand (CFPB Reg F Template)</div>
            </h1>
            <div class="section-body">
              

               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Templates</h4>
                          
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table width-responsive w-100" id="example">
                                 <thead>
                                    <tr>
                                       <th>
                                         file name
                                       </th>
                                       <th>updated date</th>
                                       <th>created by</th>
                                       <th>total consumer</th>
                                       <th>total balance</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <strong>ABCD group</strong>
                                       </td>
                                       <td><strong>	2022-06-06 03:23</strong></td>
                                       <td>
                                          <strong>Donna Weaver</strong>
                                       </td>
                                      
                                       <td>
                                         <strong>12</strong>
                                       </td>
                                        <td>
                                         <strong>$1,45,000</strong>
                                       </td>

                                      
                                       <td>
                                          <a href="#" class="btn btn-view">Email</a> 
                                          <a href="#" class="btn btn-b">Text</a>
                                          <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#export">Export</a>
                                          <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete-template">Delete</a>
                                       </td>
                                       
                                    </tr>
                                  <tr>
                                       <td>
                                          <strong>ABCD group</strong>
                                       </td>
                                       <td><strong>	2022-06-06 03:23</strong></td>
                                       <td>
                                          <strong>Donna Weaver</strong>
                                       </td>
                                      
                                       <td>
                                         <strong>12</strong>
                                       </td>
                                        <td>
                                         <strong>$1,45,000</strong>
                                       </td>

                                      
                                       <td>
                                          <a href="#" class="btn btn-view">Email</a> 
                                          <a href="#" class="btn btn-b">Text</a>
                                          <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#export">Export</a>
                                          <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete-template">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>ABCD group</strong>
                                       </td>
                                       <td><strong>	2022-06-06 03:23</strong></td>
                                       <td>
                                          <strong>Donna Weaver</strong>
                                       </td>
                                      
                                       <td>
                                         <strong>12</strong>
                                       </td>
                                        <td>
                                         <strong>$1,45,000</strong>
                                       </td>

                                      
                                       <td>
                                          <a href="#" class="btn btn-view">Email</a> 
                                          <a href="#" class="btn btn-b">Text</a>
                                          <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#export">Export</a>
                                          <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete-template">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>ABCD group</strong>
                                       </td>
                                       <td><strong>	2022-06-06 03:23</strong></td>
                                       <td>
                                          <strong>Donna Weaver</strong>
                                       </td>
                                      
                                       <td>
                                         <strong>12</strong>
                                       </td>
                                        <td>
                                         <strong>$1,45,000</strong>
                                       </td>

                                      
                                       <td>
                                          <a href="#" class="btn btn-view">Email</a> 
                                          <a href="#" class="btn btn-b">Text</a>
                                          <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#export">Export</a>
                                          <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete-template">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>ABCD group</strong>
                                       </td>
                                       <td><strong>	2022-06-06 03:23</strong></td>
                                       <td>
                                          <strong>Donna Weaver</strong>
                                       </td>
                                      
                                       <td>
                                         <strong>12</strong>
                                       </td>
                                        <td>
                                         <strong>$1,45,000</strong>
                                       </td>

                                      
                                       <td>
                                          <a href="#" class="btn btn-view">Email</a> 
                                          <a href="#" class="btn btn-b">Text</a>
                                          <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#export">Export</a>
                                          <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete-template">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>ABCD group</strong>
                                       </td>
                                       <td><strong>	2022-06-06 03:23</strong></td>
                                       <td>
                                          <strong>Donna Weaver</strong>
                                       </td>
                                      
                                       <td>
                                         <strong>12</strong>
                                       </td>
                                        <td>
                                         <strong>$1,45,000</strong>
                                       </td>

                                      
                                       <td>
                                          <a href="#" class="btn btn-view">Email</a> 
                                          <a href="#" class="btn btn-b">Text</a>
                                          <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#export">Export</a>
                                          <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete-template">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>ABCD group</strong>
                                       </td>
                                       <td><strong>	2022-06-06 03:23</strong></td>
                                       <td>
                                          <strong>Donna Weaver</strong>
                                       </td>
                                      
                                       <td>
                                         <strong>12</strong>
                                       </td>
                                        <td>
                                         <strong>$1,45,000</strong>
                                       </td>

                                      
                                       <td>
                                          <a href="#" class="btn btn-view">Email</a> 
                                          <a href="#" class="btn btn-b">Text</a>
                                          <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#export">Export</a>
                                          <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete-template">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>ABCD group</strong>
                                       </td>
                                       <td><strong>	2022-06-06 03:23</strong></td>
                                       <td>
                                          <strong>Donna Weaver</strong>
                                       </td>
                                      
                                       <td>
                                         <strong>12</strong>
                                       </td>
                                        <td>
                                         <strong>$1,45,000</strong>
                                       </td>

                                      
                                       <td>
                                          <a href="#" class="btn btn-view">Email</a> 
                                          <a href="#" class="btn btn-b">Text</a>
                                          <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#export">Export</a>
                                          <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete-template">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>ABCD group</strong>
                                       </td>
                                       <td><strong>	2022-06-06 03:23</strong></td>
                                       <td>
                                          <strong>Donna Weaver</strong>
                                       </td>
                                      
                                       <td>
                                         <strong>12</strong>
                                       </td>
                                        <td>
                                         <strong>$1,45,000</strong>
                                       </td>

                                      
                                       <td>
                                          <a href="#" class="btn btn-view">Email</a> 
                                          <a href="#" class="btn btn-b">Text</a>
                                          <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#export">Export</a>
                                          <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete-template">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>ABCD group</strong>
                                       </td>
                                       <td><strong>	2022-06-06 03:23</strong></td>
                                       <td>
                                          <strong>Donna Weaver</strong>
                                       </td>
                                      
                                       <td>
                                         <strong>12</strong>
                                       </td>
                                        <td>
                                         <strong>$1,45,000</strong>
                                       </td>

                                      
                                       <td>
                                          <a href="#" class="btn btn-view">Email</a> 
                                          <a href="#" class="btn btn-b">Text</a>
                                          <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#export">Export</a>
                                          <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete-template">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>ABCD group</strong>
                                       </td>
                                       <td><strong>	2022-06-06 03:23</strong></td>
                                       <td>
                                          <strong>Donna Weaver</strong>
                                       </td>
                                      
                                       <td>
                                         <strong>12</strong>
                                       </td>
                                        <td>
                                         <strong>$1,45,000</strong>
                                       </td>

                                      
                                       <td>
                                          <a href="#" class="btn btn-view">Email</a> 
                                          <a href="#" class="btn btn-b">Text</a>
                                          <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#export">Export</a>
                                          <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete-template">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>ABCD group</strong>
                                       </td>
                                       <td><strong>	2022-06-06 03:23</strong></td>
                                       <td>
                                          <strong>Donna Weaver</strong>
                                       </td>
                                      
                                       <td>
                                         <strong>12</strong>
                                       </td>
                                        <td>
                                         <strong>$1,45,000</strong>
                                       </td>

                                      
                                       <td>
                                          <a href="#" class="btn btn-view">Email</a> 
                                          <a href="#" class="btn btn-b">Text</a>
                                          <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#export">Export</a>
                                          <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete-template">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>ABCD group</strong>
                                       </td>
                                       <td><strong>	2022-06-06 03:23</strong></td>
                                       <td>
                                          <strong>Donna Weaver</strong>
                                       </td>
                                      
                                       <td>
                                         <strong>12</strong>
                                       </td>
                                        <td>
                                         <strong>$1,45,000</strong>
                                       </td>

                                      
                                       <td>
                                          <a href="#" class="btn btn-view">Email</a> 
                                          <a href="#" class="btn btn-b">Text</a>
                                          <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#export">Export</a>
                                          <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete-template">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>ABCD group</strong>
                                       </td>
                                       <td><strong>	2022-06-06 03:23</strong></td>
                                       <td>
                                          <strong>Donna Weaver</strong>
                                       </td>
                                      
                                       <td>
                                         <strong>12</strong>
                                       </td>
                                        <td>
                                         <strong>$1,45,000</strong>
                                       </td>

                                      
                                       <td>
                                          <a href="#" class="btn btn-view">Email</a> 
                                          <a href="#" class="btn btn-b">Text</a>
                                          <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#export">Export</a>
                                          <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete-template">Delete</a>
                                       </td>
                                       
                                    </tr>
                                    
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

           
         </section>
      </div>
      <footer class="main-footer">
         <div class="footer-bg">
         <div class="footer-left">
           <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
             © 2020 YouNegotiate ™ | All rights reserved
         </div>
         </div>
      </footer>
   </div>
</div>

<div class="modal modal-new md-sm fade" id="delete-template" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Do You Want to delete this?</h2>
            <div class="text-right mt-3">
               <button type="submit" class="btn border-btn" data-dismiss="modal" aria-label="Close">Close</button>
               <button type="submit" class="btn blue-btn">Save</button>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal modal-new md-sm fade" id="export" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">QR Code</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Do You Want to change it?</h2>
            <div class="text-right mt-3">
               <button type="submit" class="btn border-btn" data-dismiss="modal" aria-label="Close">Close</button>
               <button type="submit" class="btn blue-btn">Save</button>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>

<script>
    $(document).ready(function () {
      

         $('#example').DataTable({
            language: {
               searchPlaceholder: "Search",
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });

    });
</script>
@endsection