@extends('layout.dashboard.app')
@section('dashboard-content')

      <div class="main-content">
         <section class="section">
            <h1 class="section-header">
               <div>Report (Quick access to all important stuffs!)</div>
            </h1>
            <div class="section-body">
              
               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Generate & Download Report Now</h4>
                          
                        </div>
                        
                        <div class="card-body">
                          <div class="row">
                            <div class="col-sm-12">
                               <div class="form-row">
                                    <div class="form-group col-md-6">
                                    <label>Select Report Type <sup>*</sup></label>
                                     <select class="custom-select">
                                        <option selected="">Select</option>
                                        <option value="1">US</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                    <label>Select Client <sup>*</sup></label>
                                    <select class="custom-select">
                                        <option selected="">Select</option>
                                        <option value="1">US</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                    </div>
                                   
                                </div>

                                
                               
                                <div class="form-group">
                                    <label>Type <sup>*</sup></label>
                                    <div class="mt-2">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="c1" name="c1" class="custom-control-input">
                                            <label class="custom-control-label" for="c1">CSV</label>
                                        </div>
                                         <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="c2" name="c2" class="custom-control-input">
                                            <label class="custom-control-label" for="c2">PDF</label>
                                        </div>
                                    </div>
                                </div>
                             
                            </div>

                            <div class="text-right col-sm-12">
                              <button type="submit" class="btn blue-btn">Generate Report</button>
                            </div>
                        
                         
                          </div>
                        </div>
                     </div>
                  </div>
               </div>

         
         </section>
      </div>
      <footer class="main-footer">
         <div class="footer-bg">
         <div class="footer-left">
           <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
             © 2020 YouNegotiate ™ | All rights reserved
         </div>
         </div>
      </footer>
   </div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>


@endsection