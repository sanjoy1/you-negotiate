@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Manage Sub Accounts!</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <ul class="step-list">
                        <li class="select"><a href="manage-sub"><i class="nav-icon text-14b i-Yes"></i> Account Details</a></li>
                        <li><i class="fa fa-angles-right"></i></li>
                        <li><a href="negotiation-terms"><i class="nav-icon text-14b i-Yes"></i> Negotiation Terms</a></li>
                        <li><i class="fa fa-angles-right"></i></li>
                        <li><a href="marchent-account"><i class="nav-icon text-14b i-Yes"></i> Master Merchant Account</a></li>
                        <li><i class="fa fa-angles-right"></i></li>
                        <li><a href="tact-details"><i class="nav-icon text-14b i-Yes"></i> Contact Details</a></li>
                     </ul>
                     <div>
                        <a href="#" class="btn btn-gray">Back</a>
                        <a href="negotiation-terms" class="btn btn-primary">Next</a>
                     </div>
                  </div>
                  <hr>
                  <div class="card-body form-area">
                     <h3>Account Details</h3>
                     <div class="form-row">
                        <div class="form-group col-md-6">
                           <label>Account Name <sup>*</sup></label>
                           <input type="text" class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                           <label>Industry Type <sup>*</sup></label>
                           <select class="custom-select">
                              <option selected="">Select</option>
                              <option value="1">US</option>
                              <option value="2">Two</option>
                              <option value="3">Three</option>
                           </select>
                        </div>
                     </div>
                     <div class="form-row">
                        <div class="form-group col-md-6">
                           <label>Unique Identifier <sup>*</sup></label>
                           <input type="text" class="form-control">
                        </div>
                     </div>
                     <div class="text-right">
                        <a href="#" class="btn btn-gray">Back</a>
                        <a href="negotiation-terms" class="btn btn-view">Next</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     Your Sub Accounts
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive w-100" id="example2">
                           <thead>
                              <tr>
                                 <th>
                                    company name
                                 </th>
                                 <th>unique id to assign consumer</th>
                                 <th>type</th>
                                 <th>assigned company</th>
                                 <th>payment methods</th>
                                 <th>action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
</div>
</section>
</div>
<footer class="main-footer">
   <div class="footer-bg">
      <div class="footer-left">
         <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
      </div>
      <div class="footer-right">
         © 2020 YouNegotiate ™ | All rights reserved
      </div>
   </div>
</footer>
</div>
</div>


<div class="modal modal-new fade" id="edit-terms" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Master Negotiation Terms</h5>
            <button type="button" class="close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <form>
               <div class="form-group">
                  <label>Pay in Full Balance Discount % </label>
                   <input type="text" class="form-control">
               </div>
               <div class="form-group">
                  <label>Payment Plan Balance Discount %</label>
                   <input type="text" class="form-control">
               </div>
               <div class="form-group">
                  <label>Minimum Monthly Payment % of Payment Plan Balance</label>
                   <input type="text" class="form-control">
               </div>
               <div class="form-group">
                  <label>Maximum Days to Receive 1st Payment Plan Payment </label>
                   <input type="text" class="form-control">
               </div>

               <div class="text-right">
                  <button type="submit" class="btn border-btn" data-dismiss="modal" aria-label="Close">Close</button>
                  <button type="submit" class="btn blue-btn">Save</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>








@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
       $('#example').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
        $('#example2').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
        $('#example3').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
        $('#example4').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
       $('#example5').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
        $('#example6').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
        $('#example7').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   });
</script>
@endsection