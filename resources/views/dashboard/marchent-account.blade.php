@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Manage Sub Accounts!</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <ul class="step-list">
                        <li class="select active"><a href="manage-sub"><i class="nav-icon text-14b i-Yes"></i> Account Details</a></li>
                        <li class="select active"><i class="fa fa-angles-right"></i></li>
                        <li class="select active"><a href="negotiation-terms"><i class="nav-icon text-14b i-Yes"></i> Negotiation Terms</a></li>
                        <li class="select active"><i class="fa fa-angles-right"></i></li>
                        <li class="select"><a href="marchent-account"><i class="nav-icon text-14b i-Yes"></i> Master Merchant Account</a></li>
                        <li><i class="fa fa-angles-right"></i></li>
                        <li><a href="tact-details"><i class="nav-icon text-14b i-Yes"></i> Contact Details</a></li>
                     </ul>
                     <div>
                        <a href="negotiation-terms" class="btn btn-reset">Back</a>
                        <a href="tact-details" class="btn btn-primary">Next</a>
                     </div>
                  </div>
                  <hr>
                  <div class="card-body form-area">
                    <h3 class="mb-3">Set Up Sub Account Merchant</h3>

                                <div class="form-group">
                                    <h3>Will this sub account have separate merchant account?</h3>
                                    <div class="flex-align">
                                        <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="a1" name="a1" class="custom-control-input">
                                        <label class="custom-control-label" for="a1">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="a2" name="a1" class="custom-control-input">
                                        <label class="custom-control-label" for="a2">No</label>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="form-group">
                                    <h3>Credit Card</h3>
                                    <div class="flex-align">
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c1" name="c1" class="custom-control-input">
                                       <label class="custom-control-label" for="c1"><img alt="epay" src="{{asset('dist/img/epay1.png')}}"></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c2" name="c1" class="custom-control-input">
                                       <label class="custom-control-label" for="c2"><img alt="epay" src="{{asset('dist/img/epay2.png')}}"></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c3" name="c1" class="custom-control-input">
                                       <label class="custom-control-label" for="c3"><img alt="epay" src="{{asset('dist/img/epay3.png')}}"></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c4" name="c1" class="custom-control-input">
                                       <label class="custom-control-label" for="c4"><img alt="epay" src="{{asset('dist/img/epay4.png')}}"></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c5" name="c1" class="custom-control-input">
                                       <label class="custom-control-label" for="c5"><img alt="epay" src="{{asset('dist/img/epay5.png')}}"></label>
                                    </div>


                                 </div>
                                </div>
                                <div class="form-row">
                                            <div class="form-group col-md-6">
                                            <label>Key</label>
                                            <input type="text" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6">
                                            <label>Pin</label>
                                            <input type="text" class="form-control">
                                            </div>
                                </div>
                                <div class="form-group">
                                    <h3>ACH</h3>
                                    <div class="flex-align">
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c6" name="c2" class="custom-control-input">
                                       <label class="custom-control-label" for="c6"><img alt="epay" src="{{asset('dist/img/epay1.png')}}"></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c7" name="c2" class="custom-control-input">
                                       <label class="custom-control-label" for="c7"><img alt="epay" src="{{asset('dist/img/epay2.png')}}"></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                       <input type="radio" id="c8" name="c2" class="custom-control-input">
                                       <label class="custom-control-label" for="c8"><img alt="epay" src="{{asset('dist/img/epay3.png')}}"></label>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                            <div class="form-group col-md-6">
                                            <label>Key</label>
                                            <input type="text" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6">
                                            <label>Pin</label>
                                            <input type="text" class="form-control">
                                            </div>
                                </div>


                     <div class="text-right">
                         <a href="negotiation-terms" class="btn btn-reset">Back</a>
                        <a href="tact-details" class="btn btn-primary">Next</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     Your Sub Accounts
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive" id="example2">
                           <thead>
                              <tr>
                                 <th>
                                    company name
                                 </th>
                                 <th>unique id to assign consumer</th>
                                 <th>type</th>
                                 <th>assigned company</th>
                                 <th>payment methods</th>
                                 <th>action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Good Company</strong>
                                 </td>
                                 <td><strong>31</strong></td>
                                 <td>
                                    <strong>-</strong>
                                 </td>
                                 <td><strong>YouNegotiate</strong></td>
                                 <td><strong>EasyPay</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-view">Edit</a>
                                    <a href="#" class="btn btn-b" data-toggle="modal" data-target="#edit-terms">Edit Terms</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
</div>
</section>
</div>
<footer class="main-footer">
   <div class="footer-bg">
      <div class="footer-left">
         <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
      </div>
      <div class="footer-right">
         © 2020 YouNegotiate ™ | All rights reserved
      </div>
   </div>
</footer>
</div>
</div>


<div class="modal modal-new fade" id="edit-terms" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Master Negotiation Terms</h5>
            <button type="button" class="close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <form>
               <div class="form-group">
                  <label>Pay in Full Balance Discount % </label>
                   <input type="text" class="form-control">
               </div>
               <div class="form-group">
                  <label>Payment Plan Balance Discount %</label>
                   <input type="text" class="form-control">
               </div>
               <div class="form-group">
                  <label>Minimum Monthly Payment % of Payment Plan Balance</label>
                   <input type="text" class="form-control">
               </div>
               <div class="form-group">
                  <label>Maximum Days to Receive 1st Payment Plan Payment </label>
                   <input type="text" class="form-control">
               </div>

               <div class="text-right">
                  <button type="submit" class="btn border-btn" data-dismiss="modal" aria-label="Close">Close</button>
                  <button type="submit" class="btn blue-btn">Save</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>








@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
       $('#example').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
        $('#example2').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
        $('#example3').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
        $('#example4').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
       $('#example5').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
        $('#example6').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
        $('#example7').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   });
</script>
@endsection