@extends('layout.dashboard.app')
@section('dashboard-content')

      <div class="main-content">
         <section class="section">
            <h1 class="section-header">
               <div>My Customer Made a Payment Offer They Can Afford</div>
            </h1>
            <div class="section-body">
              
               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>My Customer’s Offers</h4>
                          
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table width-responsive" id="example">
                                 <thead>
                                    <tr>
                                       <th>
                                         account
                                       </th>
                                       <th>custoMEr name</th>
                                       <th>client</th>
                                       <th>account balance</th>
                                       <th>status</th>
                                       <th>payment profile</th>
                                       <th>payment profile on</th>
                                       <th>action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Custom Offer Received! Action Needed!</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2022-06-22</strong>
                                       </td>
                                       <td>
                                          <a href="offer-details" class="btn btn-view">View Offer</a>
                                        
                                          <a href="#" class="btn btn-deactive">Deactivate</a>
                                       </td>
                                      
                                    </tr>
                                  <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Custom Offer Received! Action Needed!</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2022-06-22</strong>
                                       </td>
                                       <td>
                                          <a href="offer-details" class="btn btn-view">View Offer</a>
                                        
                                          <a href="#" class="btn btn-deactive">Deactivate</a>
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Custom Offer Received! Action Needed!</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2022-06-22</strong>
                                       </td>
                                       <td>
                                          <a href="offer-details" class="btn btn-view">View Offer</a>
                                        
                                          <a href="#" class="btn btn-deactive">Deactivate</a>
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Custom Offer Received! Action Needed!</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2022-06-22</strong>
                                       </td>
                                       <td>
                                          <a href="offer-details" class="btn btn-view">View Offer</a>
                                        
                                          <a href="#" class="btn btn-deactive">Deactivate</a>
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Custom Offer Received! Action Needed!</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2022-06-22</strong>
                                       </td>
                                       <td>
                                          <a href="offer-details" class="btn btn-view">View Offer</a>
                                        
                                          <a href="#" class="btn btn-deactive">Deactivate</a>
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Custom Offer Received! Action Needed!</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2022-06-22</strong>
                                       </td>
                                       <td>
                                          <a href="offer-details" class="btn btn-view">View Offer</a>
                                        
                                          <a href="#" class="btn btn-deactive">Deactivate</a>
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Custom Offer Received! Action Needed!</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2022-06-22</strong>
                                       </td>
                                       <td>
                                          <a href="offer-details" class="btn btn-view">View Offer</a>
                                        
                                          <a href="#" class="btn btn-deactive">Deactivate</a>
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Custom Offer Received! Action Needed!</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2022-06-22</strong>
                                       </td>
                                       <td>
                                          <a href="offer-details" class="btn btn-view">View Offer</a>
                                        
                                          <a href="#" class="btn btn-deactive">Deactivate</a>
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Custom Offer Received! Action Needed!</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2022-06-22</strong>
                                       </td>
                                       <td>
                                          <a href="offer-details" class="btn btn-view">View Offer</a>
                                        
                                          <a href="#" class="btn btn-deactive">Deactivate</a>
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Custom Offer Received! Action Needed!</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2022-06-22</strong>
                                       </td>
                                       <td>
                                          <a href="offer-details" class="btn btn-view">View Offer</a>
                                        
                                          <a href="#" class="btn btn-deactive">Deactivate</a>
                                       </td>
                                      
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

           
               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Recently Completed Negotiations</h4>
                          
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table width-responsive" id="example2">
                                 <thead>
                                    <tr>
                                       <th>
                                         account
                                       </th>
                                       <th>custoMEr name</th>
                                       <th>client</th>
                                       <th>account balance</th>
                                       <th>negotiation date</th>
                                       <th>negotiation term</th>
                                       <th>status</th>
                                       <th>payment profile</th>
                                        <th>payment on</th>
                                        <th>action</th>
                                       
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td><strong>2021-11-19 10:38:25</strong></td>
                                       <td><strong>Discounted Payoff Plan</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Negotiation Pending</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2 021-11-19 10:38:25</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a>
                                       
                                       </td>
                                      
                                    </tr>
                                 <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td><strong>2021-11-19 10:38:25</strong></td>
                                       <td><strong>Discounted Payoff Plan</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Negotiation Pending</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2 021-11-19 10:38:25</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a>
                                       
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td><strong>2021-11-19 10:38:25</strong></td>
                                       <td><strong>Discounted Payoff Plan</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Negotiation Pending</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2 021-11-19 10:38:25</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a>
                                       
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td><strong>2021-11-19 10:38:25</strong></td>
                                       <td><strong>Discounted Payoff Plan</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Negotiation Pending</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2 021-11-19 10:38:25</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a>
                                       
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td><strong>2021-11-19 10:38:25</strong></td>
                                       <td><strong>Discounted Payoff Plan</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Negotiation Pending</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2 021-11-19 10:38:25</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a>
                                       
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td><strong>2021-11-19 10:38:25</strong></td>
                                       <td><strong>Discounted Payoff Plan</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Negotiation Pending</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2 021-11-19 10:38:25</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a>
                                       
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td><strong>2021-11-19 10:38:25</strong></td>
                                       <td><strong>Discounted Payoff Plan</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Negotiation Pending</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2 021-11-19 10:38:25</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a>
                                       
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td><strong>2021-11-19 10:38:25</strong></td>
                                       <td><strong>Discounted Payoff Plan</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Negotiation Pending</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2 021-11-19 10:38:25</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a>
                                       
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td><strong>2021-11-19 10:38:25</strong></td>
                                       <td><strong>Discounted Payoff Plan</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Negotiation Pending</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2 021-11-19 10:38:25</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a>
                                       
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td><strong>2021-11-19 10:38:25</strong></td>
                                       <td><strong>Discounted Payoff Plan</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Negotiation Pending</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2 021-11-19 10:38:25</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a>
                                       
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td><strong>2021-11-19 10:38:25</strong></td>
                                       <td><strong>Discounted Payoff Plan</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Negotiation Pending</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2 021-11-19 10:38:25</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a>
                                       
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>32435666661</strong>
                                       </td>
                                       <td><strong>Roy Mustang</strong></td>
                                       <td>
                                          <strong>Addidas</strong>
                                       </td>
                                       <td><strong>$200.00</strong></td>
                                       <td><strong>2021-11-19 10:38:25</strong></td>
                                       <td><strong>Discounted Payoff Plan</strong></td>
                                       <td>
                                        <a href="#" class="text-org">Negotiation Pending</a>
                                       </td>
                                       <td>
                                         <strong>No</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>2 021-11-19 10:38:25</strong>
                                       </td>
                                       <td>
                                          <a href="#" class="btn btn-view" data-toggle="modal" data-target="#exampleModal">Quick View</a>
                                       
                                       </td>
                                      
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      <footer class="main-footer">
         <div class="footer-bg">
         <div class="footer-left">
           <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
             © 2020 YouNegotiate ™ | All rights reserved
         </div>
         </div>
      </footer>
   </div>
</div>


<!-- Modal -->
<div class="modal modal-new fade" id="exampleModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Delete Template</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
        </button>
      </div>
      <div class="modal-body content-ac-del">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <h4>Account</h4>
                <h5>146017</h5>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6">
                <h4>Client Name</h4>
                <h5>YouNegotiate</h5>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6">
                <h4>Account Balance</h4>
                <h5>$4,000.00</h5>
            </div>
            
        </div>
      </div>
      
    </div>
  </div>
</div>

@endsection



@section('script')
    <script src="{{asset('dist/modules/jquery.min.js')}}"></script>
    <script src="{{asset('dist/modules/popper.js')}}"></script>
    <script src="{{asset('dist/modules/tooltip.js')}}"></script>
    <script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
    <script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
    <script src="{{asset('dist/js/sa-functions.js')}}"></script>
    <script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
    <script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
    <script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
    <script src="{{asset('dist/modules/chart.min.js')}}"></script>
    <script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
    <script src="{{asset('dist/js/scripts.js')}}"></script>
    <script src="{{asset('dist/js/custom.js')}}"></script>
<script>
    $(document).ready(function () {
      

         $('#example').DataTable({
            language: {
               searchPlaceholder: "Search",
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });
         $('#example2').DataTable({
            language: {
               searchPlaceholder: "Search",
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });

    });
</script>
@endsection