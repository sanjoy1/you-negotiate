@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Offer Details</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Offer Information</h4>
                     <div>
                        <a href="counter-offer" class="btn btn-b">Counter</a>
                         <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#ask-client">Ask My Client</a>
                        <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#counter">Contact Customer</a>
                        <a href="#" class="btn btn-deactive">Declline</a>
                        <a href="#" class="btn btn-green">Accept</a>
                     </div>
                  </div>
                  <hr>
                  <div class="card-body">
                     <div class="new-radius-box">
                        <h3>Account Profile</h3>
                        <div class="row content-ac-del ">
                           <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                              <h4>Customer's Name</h4>
                              <h5>Edward Elric</h5>
                           </div>
                           <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                              <h4>Account</h4>
                              <h5>1571724</h5>
                           </div>
                           <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                              <h4>My Client</h4>
                              <h5>YouNegotiate</h5>
                           </div>
                           <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                              <h4>Loan Date</h4>
                              <h5>-</h5>
                           </div>
                           <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                              <h4>Placement Date</h4>
                              <h5>-</h5>
                           </div>
                           <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                              <h4>Deadline to Help</h4>
                              <h5>-</h5>
                           </div>
                           <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                              <h4>Account Balance</h4>
                              <h5>$100.00</h5>
                           </div>
                        </div>
                     </div>
                     <div class="new-radius-box">
                        <h3>Account Profile</h3>
                        <div class="table-border-st">
                           <table class="table width-responsive">
                              <thead>
                                 <tr>
                                    <th>Offer Description</th>
                                    <th>Our Offer  </th>
                                    <th>Customer's Offer </th>
                                    <th>counter offer</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>Discounted PayOff Balance</td>
                                    <td>$70.00</td>
                                    <td>$50.00</td>
                                    <td>-</td>
                                 </tr>
                                 <tr>
                                    <td>First Payment Date</td>
                                    <td>2022-06-27</td>
                                    <td>2022-06-27</td>
                                    <td>-</td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                     <div class="new-radius-box">
                        <h3 class="mb-2"><strong>Payment Profile</strong></h3>
                        <p>Not Yet Setup, waiting for customer to setup their payment profile</p>
                     </div>
                     <div class="new-radius-box">
                        <h3><strong>Notes & Reason</strong></h3>
                        <div class="row content-ac-del ">
                           <div class="col-sm-12 mb-3">
                              <h4>Reason for requesting a custom offer</h4>
                              <h5>Unemployed </h5>
                           </div>
                           <div class="col-sm-12 mb-3">
                              <h4>Notes</h4>
                              <h5>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </h5>
                           </div>
                        </div>
                     </div>
                     <div class="text-right mt-3">
                        <a href="counter-offer" class="btn btn-b">Counter</a>
                        <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#ask-client">Ask My Client</a>
                        <a href="#" class="btn btn-reset" data-toggle="modal" data-target="#counter">Contact Customer</a>
                        <a href="#" class="btn btn-deactive">Declline</a>
                        <a href="#" class="btn btn-green">Accept</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
        
      </div>
   </section>
</div>
<footer class="main-footer">
   <div class="footer-bg">
      <div class="footer-left">
         <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
      </div>
      <div class="footer-right">
         © 2020 YouNegotiate ™ | All rights reserved
      </div>
   </div>
</footer>
</div>
</div>


<!-- Modal ask my client -->
<div class="modal modal-new fade" id="ask-client">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Ask My Client</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
           
            <div class="row">
               <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                  <h4>Customer's Name</h4>
                  <h5>Sayan Adhikary</h5>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                  <h4>Account</h4>
                  <h5>2555666661 </h5>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                  <h4>Sub Account</h4>
                  <h5>- </h5>
               </div>

            </div>
            <div class="text-right">
               <a href="#" class="btn border-btn">Close</a>
               <a href="#" class="btn btn-green">Back To Offer Review</a>
               <a href="#" class="btn btn-yellow">Contact Customer</a>
               <a href="#" class="btn btn-view">Accept</a>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- Modal Counter -->
<div class="modal modal-new fade" id="counter">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Contact Customer</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
           
            <div class="row">
               <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                  <h4>Customer's Name</h4>
                  <h5>Sayan Adhikary</h5>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                  <h4>Account</h4>
                  <h5>2555666661 </h5>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                  <h4>My Client</h4>
                  <h5>YouNegotiate</h5>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                  <h4>Account Balance</h4>
                  <h5>$100.00</h5>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                  <h4>Email</h4>
                  <h5>sadhikari@yopmail.com</h5>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-6 mb-3">
                  <h4>Phone Number</h4>
                  <h5>9458589914</h5>
               </div>

            </div>
            <h3 class="mt-4 h3-heading">Current Offer Summary</h3>

            <div class="table-border-st mb-4">
               <table class="table width-responsive">
                  <thead>
                     <tr>
                        <th>Offer Description</th>
                        <th>Our Offer </th>
                        <th>Customer's Offer</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>First Payment Date</td>
                        <td>$70.00</td>
                        <td>$60.00</td>
                     </tr>
                     
                  </tbody>
               </table>
            </div>
            <div class="text-right">
               <a href="#" class="btn border-btn">Close</a>
               <a href="#" class="btn btn-green">Back To Offer Review</a>
               <a href="#" class="btn btn-yellow">Ask My Client</a>
               <a href="#" class="btn btn-reset">Counter</a>
               <a href="#" class="btn btn-view">Accept</a>
            </div>
         </div>
      </div>
   </div>
</div>





@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>

@endsection