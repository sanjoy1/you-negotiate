@extends('layout.dashboard.app')
@section('dashboard-content')

      <div class="main-content">
         <section class="section">
            <h1 class="section-header">
               <div> Profile Permission Report</div>
            </h1>
            <div class="section-body">
              

               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Profile Permission</h4>
                          
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table width-responsive" id="example2">
                                 <thead>
                                    <tr>
                                       <th>first name</th>
                                       <th>last name</th>
                                       <th>company</th>
                                       <th>date of birth</th>
                                       <th>Social security</th>
                                       <th>Email Permission</th>
                                       <th>Email permission address</th>
                                       <th>Text</th>
                                       <th>Text Number</th>
                                       <th>Call Mobile</th>
                                       <th>Mobile Number</th>
                                       <th>Mail</th>
                                       <th>address</th>
                                       <th>City</th>
                                       <th>stATE</th>
                                       <th>zip</th>


                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <strong>Natsu</stron>
                                       </td>
                                       <td><strong>Dragneel</stron></td>
                                       <td>
                                          <strong>YouNegotiate</stron>
                                       </td>
                                       <td>
                                        <strong> 1957-05-01</stron>
                                       </td>
                                       <td>
                                        <strong> 4444</stron>
                                       </td>
                                       <td>
                                         <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>hello@ndragneel.com</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>4242353671</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>9845613721</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                        <td>
                                         <strong>1/A Beverly Hills California</strong>
                                       </td>
                                       <td>
                                          <strong>California</strong>
                                       </td>

                                       <td>
                                         <strong>US</strong>
                                       </td>
                                       <td>
                                          <strong>4356614</strong>
                                       </td>
                                    </tr>
                                 <tr>
                                       <td>
                                          <strong>Natsu</stron>
                                       </td>
                                       <td><strong>Dragneel</stron></td>
                                       <td>
                                          <strong>YouNegotiate</stron>
                                       </td>
                                       <td>
                                        <strong> 1957-05-01</stron>
                                       </td>
                                       <td>
                                        <strong> 4444</stron>
                                       </td>
                                       <td>
                                         <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>hello@ndragneel.com</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>4242353671</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>9845613721</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                        <td>
                                         <strong>1/A Beverly Hills California</strong>
                                       </td>
                                       <td>
                                          <strong>California</strong>
                                       </td>

                                       <td>
                                         <strong>US</strong>
                                       </td>
                                       <td>
                                          <strong>4356614</strong>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>Natsu</stron>
                                       </td>
                                       <td><strong>Dragneel</stron></td>
                                       <td>
                                          <strong>YouNegotiate</stron>
                                       </td>
                                       <td>
                                        <strong> 1957-05-01</stron>
                                       </td>
                                       <td>
                                        <strong> 4444</stron>
                                       </td>
                                       <td>
                                         <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>hello@ndragneel.com</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>4242353671</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>9845613721</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                        <td>
                                         <strong>1/A Beverly Hills California</strong>
                                       </td>
                                       <td>
                                          <strong>California</strong>
                                       </td>

                                       <td>
                                         <strong>US</strong>
                                       </td>
                                       <td>
                                          <strong>4356614</strong>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>Natsu</stron>
                                       </td>
                                       <td><strong>Dragneel</stron></td>
                                       <td>
                                          <strong>YouNegotiate</stron>
                                       </td>
                                       <td>
                                        <strong> 1957-05-01</stron>
                                       </td>
                                       <td>
                                        <strong> 4444</stron>
                                       </td>
                                       <td>
                                         <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>hello@ndragneel.com</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>4242353671</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>9845613721</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                        <td>
                                         <strong>1/A Beverly Hills California</strong>
                                       </td>
                                       <td>
                                          <strong>California</strong>
                                       </td>

                                       <td>
                                         <strong>US</strong>
                                       </td>
                                       <td>
                                          <strong>4356614</strong>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>Natsu</stron>
                                       </td>
                                       <td><strong>Dragneel</stron></td>
                                       <td>
                                          <strong>YouNegotiate</stron>
                                       </td>
                                       <td>
                                        <strong> 1957-05-01</stron>
                                       </td>
                                       <td>
                                        <strong> 4444</stron>
                                       </td>
                                       <td>
                                         <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>hello@ndragneel.com</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>4242353671</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>9845613721</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                        <td>
                                         <strong>1/A Beverly Hills California</strong>
                                       </td>
                                       <td>
                                          <strong>California</strong>
                                       </td>

                                       <td>
                                         <strong>US</strong>
                                       </td>
                                       <td>
                                          <strong>4356614</strong>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>Natsu</stron>
                                       </td>
                                       <td><strong>Dragneel</stron></td>
                                       <td>
                                          <strong>YouNegotiate</stron>
                                       </td>
                                       <td>
                                        <strong> 1957-05-01</stron>
                                       </td>
                                       <td>
                                        <strong> 4444</stron>
                                       </td>
                                       <td>
                                         <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>hello@ndragneel.com</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>4242353671</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>9845613721</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                        <td>
                                         <strong>1/A Beverly Hills California</strong>
                                       </td>
                                       <td>
                                          <strong>California</strong>
                                       </td>

                                       <td>
                                         <strong>US</strong>
                                       </td>
                                       <td>
                                          <strong>4356614</strong>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>Natsu</stron>
                                       </td>
                                       <td><strong>Dragneel</stron></td>
                                       <td>
                                          <strong>YouNegotiate</stron>
                                       </td>
                                       <td>
                                        <strong> 1957-05-01</stron>
                                       </td>
                                       <td>
                                        <strong> 4444</stron>
                                       </td>
                                       <td>
                                         <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>hello@ndragneel.com</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>4242353671</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>9845613721</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                        <td>
                                         <strong>1/A Beverly Hills California</strong>
                                       </td>
                                       <td>
                                          <strong>California</strong>
                                       </td>

                                       <td>
                                         <strong>US</strong>
                                       </td>
                                       <td>
                                          <strong>4356614</strong>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>Natsu</stron>
                                       </td>
                                       <td><strong>Dragneel</stron></td>
                                       <td>
                                          <strong>YouNegotiate</stron>
                                       </td>
                                       <td>
                                        <strong> 1957-05-01</stron>
                                       </td>
                                       <td>
                                        <strong> 4444</stron>
                                       </td>
                                       <td>
                                         <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>hello@ndragneel.com</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>4242353671</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>9845613721</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                        <td>
                                         <strong>1/A Beverly Hills California</strong>
                                       </td>
                                       <td>
                                          <strong>California</strong>
                                       </td>

                                       <td>
                                         <strong>US</strong>
                                       </td>
                                       <td>
                                          <strong>4356614</strong>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>Natsu</stron>
                                       </td>
                                       <td><strong>Dragneel</stron></td>
                                       <td>
                                          <strong>YouNegotiate</stron>
                                       </td>
                                       <td>
                                        <strong> 1957-05-01</stron>
                                       </td>
                                       <td>
                                        <strong> 4444</stron>
                                       </td>
                                       <td>
                                         <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>hello@ndragneel.com</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>4242353671</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>9845613721</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                        <td>
                                         <strong>1/A Beverly Hills California</strong>
                                       </td>
                                       <td>
                                          <strong>California</strong>
                                       </td>

                                       <td>
                                         <strong>US</strong>
                                       </td>
                                       <td>
                                          <strong>4356614</strong>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>Natsu</stron>
                                       </td>
                                       <td><strong>Dragneel</stron></td>
                                       <td>
                                          <strong>YouNegotiate</stron>
                                       </td>
                                       <td>
                                        <strong> 1957-05-01</stron>
                                       </td>
                                       <td>
                                        <strong> 4444</stron>
                                       </td>
                                       <td>
                                         <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>hello@ndragneel.com</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>4242353671</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>9845613721</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                        <td>
                                         <strong>1/A Beverly Hills California</strong>
                                       </td>
                                       <td>
                                          <strong>California</strong>
                                       </td>

                                       <td>
                                         <strong>US</strong>
                                       </td>
                                       <td>
                                          <strong>4356614</strong>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>Natsu</stron>
                                       </td>
                                       <td><strong>Dragneel</stron></td>
                                       <td>
                                          <strong>YouNegotiate</stron>
                                       </td>
                                       <td>
                                        <strong> 1957-05-01</stron>
                                       </td>
                                       <td>
                                        <strong> 4444</stron>
                                       </td>
                                       <td>
                                         <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>hello@ndragneel.com</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>4242353671</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>9845613721</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                        <td>
                                         <strong>1/A Beverly Hills California</strong>
                                       </td>
                                       <td>
                                          <strong>California</strong>
                                       </td>

                                       <td>
                                         <strong>US</strong>
                                       </td>
                                       <td>
                                          <strong>4356614</strong>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>Natsu</stron>
                                       </td>
                                       <td><strong>Dragneel</stron></td>
                                       <td>
                                          <strong>YouNegotiate</stron>
                                       </td>
                                       <td>
                                        <strong> 1957-05-01</stron>
                                       </td>
                                       <td>
                                        <strong> 4444</stron>
                                       </td>
                                       <td>
                                         <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>hello@ndragneel.com</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>4242353671</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>9845613721</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                        <td>
                                         <strong>1/A Beverly Hills California</strong>
                                       </td>
                                       <td>
                                          <strong>California</strong>
                                       </td>

                                       <td>
                                         <strong>US</strong>
                                       </td>
                                       <td>
                                          <strong>4356614</strong>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>Natsu</stron>
                                       </td>
                                       <td><strong>Dragneel</stron></td>
                                       <td>
                                          <strong>YouNegotiate</stron>
                                       </td>
                                       <td>
                                        <strong> 1957-05-01</stron>
                                       </td>
                                       <td>
                                        <strong> 4444</stron>
                                       </td>
                                       <td>
                                         <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>hello@ndragneel.com</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>4242353671</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>9845613721</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                        <td>
                                         <strong>1/A Beverly Hills California</strong>
                                       </td>
                                       <td>
                                          <strong>California</strong>
                                       </td>

                                       <td>
                                         <strong>US</strong>
                                       </td>
                                       <td>
                                          <strong>4356614</strong>
                                       </td>
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>Natsu</stron>
                                       </td>
                                       <td><strong>Dragneel</stron></td>
                                       <td>
                                          <strong>YouNegotiate</stron>
                                       </td>
                                       <td>
                                        <strong> 1957-05-01</stron>
                                       </td>
                                       <td>
                                        <strong> 4444</stron>
                                       </td>
                                       <td>
                                         <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>hello@ndragneel.com</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>4242353671</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                       <td>
                                         <strong>9845613721</strong>
                                       </td>
                                       <td>
                                          <strong>Yes</strong>
                                       </td>
                                        <td>
                                         <strong>1/A Beverly Hills California</strong>
                                       </td>
                                       <td>
                                          <strong>California</strong>
                                       </td>

                                       <td>
                                         <strong>US</strong>
                                       </td>
                                       <td>
                                          <strong>4356614</strong>
                                       </td>
                                    </tr>
                                    
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

           
         </section>
      </div>
      <footer class="main-footer">
         <div class="footer-bg">
         <div class="footer-left">
           <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
             © 2020 YouNegotiate ™ | All rights reserved
         </div>
         </div>
      </footer>
   </div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>

<script>
    $(document).ready(function () {
      

         $('#example2').DataTable({
            language: {
               searchPlaceholder: "Search",
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });

    });
</script>
@endsection