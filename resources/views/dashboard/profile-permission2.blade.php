@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Profile Permission</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Group Master Negotiation Terms</h4>
                  </div>
                  <div class="card-body">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Payterms For</label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">US</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                 </select>
                              </div>
                              <div class="form-group col-md-6">
                                <label>Select Group</label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">US</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                 </select>
                              </div>
                           </div>
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Pay in Full Balance Discount %</label>
                                 <p>Surveyed consumers said they are more likely to pay off a balance if they can get a discount, and if they don’t they do nothing or go to a debt negotiation service!<br>If my customer can pay in full, I would like to discount their balance by this %. Example: Account Balance $500.00, My PIF Balance Discount 20% ($100.00), My Customer can pay off this account for $400.00!</p>
                                 <input type="text" class="form-control">
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Payment Plan Balance Discount %</label>
                                 <p>
                                    account.98% of consumers surveyed said they will not call a creditor to set up payments, so we want your customer to start making payments on You Negotiate! Increase your chances by discounting the balance if they set up and stick with a payment plan!<br>If my customer commits to a Payment Plan (I approve), I would like to discount their pay off balance by this %. Example: Account Balance $500.00, My Payment Plan Balance Discount is 10% ($50.00), If my customer sets up a payment plan, My Customer will make a total of $450.00 in payments to pay off this account.
                                 </p>
                                 <input type="text" class="form-control">
                              </div>
                           </div>
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Minimum Monthly Payment % of Balance</label>
                                 <p>99% of consumers surveyed said they will not set up a plan if they can’t afford it and don’t call because they feel too much pressure from a collector. The goal here is to get a payment plan they can afford in place as quickly as possible before they throw in the towel with the attitude of – do what you need to do!<br>% of Balance Minimum % of balance that must be paid per month to digitally approve. Example: account balance $500.00, minimum monthly payment % is 10%, which means the minimum monthly payment offered is $50.00 (it will take 10 months for the Customer to pay their balance in full)</p>
                                 <input type="text" class="form-control">
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Maximum Days to Receive 1st Payment Plan Payment</label>
                                 <p>
                                    99% of consumers surveyed want to pay off the debt and make a payment as soon as they can. Life comes up with other pressing bills to pay, so we want to give them enough time to make the payment, yet not let them stretch it out to eternity!<br>From the date the Customer sets up a payment plan, what is the # of days you allow to schedule their first payment. Example: Customer Agrees to Payment Plan on May 1, My Max days is set at 30 days, Customer must schedule this first payment on or before June 1st, otherwise requires them to submit an offer.
                                 </p>
                                 <input type="text" class="form-control">
                              </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-sm-12"> <label>Please note the customer will make up to 3 negotiation attempts. If their attempts do not meet your minimums above, we will provide them a final offer based on your term requirements. If they cannot afford your final offer, they will be directed to send you an offer they can afford. Be on the lookout in the Customer Offers</label></div>
                        </div>
                        <div class="text-right col-sm-12">
                           <button type="submit" class="btn blue-btn">Save My Master Terms</button>
                        </div>
                     </div>
                  </div>
               </div>
 <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Group Terms </h4>
                   
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive w-100" id="example">
                           <thead>
                              <tr>
                                 <th>
                                    group
                                 </th>
                                 <th>PIF</th>
                                 <th>PPA</th>
                                 <th>Min monthly %</th>
                                 <th>Max days first pay</th>
                                 <th>action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <strong>Alpha</strong>
                                 </td>
                                 <td><strong>24</strong></td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>20</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#edit">Edit</a> 
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Alpha</strong>
                                 </td>
                                 <td><strong>24</strong></td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>20</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#edit">Edit</a> 
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Alpha</strong>
                                 </td>
                                 <td><strong>24</strong></td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>20</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#edit">Edit</a> 
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Alpha</strong>
                                 </td>
                                 <td><strong>24</strong></td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>20</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#edit">Edit</a> 
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Alpha</strong>
                                 </td>
                                 <td><strong>24</strong></td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>20</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#edit">Edit</a> 
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Alpha</strong>
                                 </td>
                                 <td><strong>24</strong></td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>20</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#edit">Edit</a> 
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Alpha</strong>
                                 </td>
                                 <td><strong>24</strong></td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>20</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#edit">Edit</a> 
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Alpha</strong>
                                 </td>
                                 <td><strong>24</strong></td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>20</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#edit">Edit</a> 
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Alpha</strong>
                                 </td>
                                 <td><strong>24</strong></td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>20</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#edit">Edit</a> 
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Alpha</strong>
                                 </td>
                                 <td><strong>24</strong></td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>20</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#edit">Edit</a> 
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Alpha</strong>
                                 </td>
                                 <td><strong>24</strong></td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>20</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#edit">Edit</a> 
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Alpha</strong>
                                 </td>
                                 <td><strong>24</strong></td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>20</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#edit">Edit</a> 
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Alpha</strong>
                                 </td>
                                 <td><strong>24</strong></td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>20</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#edit">Edit</a> 
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Alpha</strong>
                                 </td>
                                 <td><strong>24</strong></td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>20</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#edit">Edit</a> 
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a> 
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>Alpha</strong>
                                 </td>
                                 <td><strong>24</strong></td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>10</strong>
                                 </td>
                                 <td>
                                    <strong>20</strong>
                                 </td>
                                 <td>
                                    <a href="#" class="btn btn-view" data-toggle="modal" data-target="#edit">Edit</a> 
                                    <a href="#" class="btn btn-deactive" data-toggle="modal" data-target="#delete">Delete</a> 
                                 </td>
                              </tr>

                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
            </div>

           
         </div>

   </section>
   </div>
   <footer class="main-footer">
      <div class="footer-bg">
         <div class="footer-left">
            <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
            © 2020 YouNegotiate ™ | All rights reserved
         </div>
      </div>
   </footer>
</div>
</div>

<div class="modal modal-new md-sm fade" id="edit" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Edit Group</h5>
            <button type="button" class="close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Do you want to edit this group?</h2>
            <div class="text-right mt-4">
               <button type="submit" class="btn border-btn">Cancel</button>
               <button type="submit" class="btn blue-btn">Yes</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal modal-new md-sm fade" id="delete" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Delete</h5>
            <button type="button" class="close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>You sure you want to delete this?</h2>
            <div class="text-right mt-4">
               <button type="submit" class="btn border-btn">Cancel</button>
               <button type="submit" class="btn blue-btn">Yes</button>
            </div>
         </div>
      </div>
   </div>
</div>


@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
     
   
        $('#example').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
       
   
   });
</script>
@endsection