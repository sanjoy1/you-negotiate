@extends('layout.dashboard.app')
@section('dashboard-content')

      <div class="main-content">
         <section class="section">
            <h1 class="section-header">
               <div>Report (Schedule Automated Exports of Data That You Need)</div>
            </h1>
            <div class="section-body">
              
               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Schedule A New Report</h4>
                          
                        </div>
                        
                        <div class="card-body">
                          <div class="row">
                            <div class="col-sm-12">
                               <div class="form-row">
                                    <div class="form-group col-md-6">
                                    <label>Select Report Type <sup>*</sup></label>
                                     <select class="custom-select">
                                        <option selected="">Select</option>
                                        <option value="1">US</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                    <label>Select Sub Account <sup>*</sup></label>
                                    <select class="custom-select">
                                        <option selected="">Select</option>
                                        <option value="1">US</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                    </div>
                                   
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                    <label>Select Frequency <sup>*</sup></label>
                                     <select class="custom-select">
                                        <option selected="">Select</option>
                                        <option value="1">US</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                    <label>Delivery Type <sup>*</sup></label>
                                    <div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="c1" name="c1" class="custom-control-input">
                                            <label class="custom-control-label" for="c1">Email</label>
                                        </div>
                                         <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="c2" name="c2" class="custom-control-input">
                                            <label class="custom-control-label" for="c2">SFTP</label>
                                        </div>
                                    </div>
                                    </div>
                                   
                                </div>

                                
                               
                              
                            </div>

                            <div class="text-right col-sm-12">
                              <button type="submit" class="btn blue-btn">Add To Schedule Exports</button>
                            </div>
                        
                         
                          </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Your Current Scheduled Exports</h4>
                          
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table width-responsive" id="example">
                                 <thead>
                                    <tr>
                                       <th>
                                         created by
                                       </th>
                                       <th>created on</th>
                                       <th>Report Type</th>
                                       <th>Frequency</th>
                                       <th>client</th>
                                       <th>export type</th>
                                       <th>export location</th>
                                       <th>action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <strong>David Williams</stron>
                                       </td>
                                       <td><strong>2222-06-08 06:01</stron></td>
                                       <td>
                                          <strong>Commission_Reporting_RNN</stron>
                                       </td>
                                       <td><strong>	Hourly Updates</stron></td>
                                       <td><strong>	YouNegotiate</stron></td>
                                       <td>
                                         <strong>Email</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>report@younegotiate.com</strong>
                                       </td>
                                       <td>
                                        <a href="#" class="btn btn-green">Edit Schedule</a>
                                          <a href="#" class="btn btn-view">Pause Export</a>
                                        
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                      
                                    </tr>
                                 <tr>
                                       <td>
                                          <strong>David Williams</stron>
                                       </td>
                                       <td><strong>2222-06-08 06:01</stron></td>
                                       <td>
                                          <strong>Commission_Reporting_RNN</stron>
                                       </td>
                                       <td><strong>	Hourly Updates</stron></td>
                                       <td><strong>	YouNegotiate</stron></td>
                                       <td>
                                         <strong>Email</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>report@younegotiate.com</strong>
                                       </td>
                                       <td>
                                        <a href="#" class="btn btn-green">Edit Schedule</a>
                                          <a href="#" class="btn btn-view">Pause Export</a>
                                        
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>David Williams</stron>
                                       </td>
                                       <td><strong>2222-06-08 06:01</stron></td>
                                       <td>
                                          <strong>Commission_Reporting_RNN</stron>
                                       </td>
                                       <td><strong>	Hourly Updates</stron></td>
                                       <td><strong>	YouNegotiate</stron></td>
                                       <td>
                                         <strong>Email</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>report@younegotiate.com</strong>
                                       </td>
                                       <td>
                                        <a href="#" class="btn btn-green">Edit Schedule</a>
                                          <a href="#" class="btn btn-view">Pause Export</a>
                                        
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>David Williams</stron>
                                       </td>
                                       <td><strong>2222-06-08 06:01</stron></td>
                                       <td>
                                          <strong>Commission_Reporting_RNN</stron>
                                       </td>
                                       <td><strong>	Hourly Updates</stron></td>
                                       <td><strong>	YouNegotiate</stron></td>
                                       <td>
                                         <strong>Email</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>report@younegotiate.com</strong>
                                       </td>
                                       <td>
                                        <a href="#" class="btn btn-green">Edit Schedule</a>
                                          <a href="#" class="btn btn-view">Pause Export</a>
                                        
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>David Williams</stron>
                                       </td>
                                       <td><strong>2222-06-08 06:01</stron></td>
                                       <td>
                                          <strong>Commission_Reporting_RNN</stron>
                                       </td>
                                       <td><strong>	Hourly Updates</stron></td>
                                       <td><strong>	YouNegotiate</stron></td>
                                       <td>
                                         <strong>Email</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>report@younegotiate.com</strong>
                                       </td>
                                       <td>
                                        <a href="#" class="btn btn-green">Edit Schedule</a>
                                          <a href="#" class="btn btn-view">Pause Export</a>
                                        
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>David Williams</stron>
                                       </td>
                                       <td><strong>2222-06-08 06:01</stron></td>
                                       <td>
                                          <strong>Commission_Reporting_RNN</stron>
                                       </td>
                                       <td><strong>	Hourly Updates</stron></td>
                                       <td><strong>	YouNegotiate</stron></td>
                                       <td>
                                         <strong>Email</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>report@younegotiate.com</strong>
                                       </td>
                                       <td>
                                        <a href="#" class="btn btn-green">Edit Schedule</a>
                                          <a href="#" class="btn btn-view">Pause Export</a>
                                        
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>David Williams</stron>
                                       </td>
                                       <td><strong>2222-06-08 06:01</stron></td>
                                       <td>
                                          <strong>Commission_Reporting_RNN</stron>
                                       </td>
                                       <td><strong>	Hourly Updates</stron></td>
                                       <td><strong>	YouNegotiate</stron></td>
                                       <td>
                                         <strong>Email</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>report@younegotiate.com</strong>
                                       </td>
                                       <td>
                                        <a href="#" class="btn btn-green">Edit Schedule</a>
                                          <a href="#" class="btn btn-view">Pause Export</a>
                                        
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>David Williams</stron>
                                       </td>
                                       <td><strong>2222-06-08 06:01</stron></td>
                                       <td>
                                          <strong>Commission_Reporting_RNN</stron>
                                       </td>
                                       <td><strong>	Hourly Updates</stron></td>
                                       <td><strong>	YouNegotiate</stron></td>
                                       <td>
                                         <strong>Email</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>report@younegotiate.com</strong>
                                       </td>
                                       <td>
                                        <a href="#" class="btn btn-green">Edit Schedule</a>
                                          <a href="#" class="btn btn-view">Pause Export</a>
                                        
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>David Williams</stron>
                                       </td>
                                       <td><strong>2222-06-08 06:01</stron></td>
                                       <td>
                                          <strong>Commission_Reporting_RNN</stron>
                                       </td>
                                       <td><strong>	Hourly Updates</stron></td>
                                       <td><strong>	YouNegotiate</stron></td>
                                       <td>
                                         <strong>Email</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>report@younegotiate.com</strong>
                                       </td>
                                       <td>
                                        <a href="#" class="btn btn-green">Edit Schedule</a>
                                          <a href="#" class="btn btn-view">Pause Export</a>
                                        
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>David Williams</stron>
                                       </td>
                                       <td><strong>2222-06-08 06:01</stron></td>
                                       <td>
                                          <strong>Commission_Reporting_RNN</stron>
                                       </td>
                                       <td><strong>	Hourly Updates</stron></td>
                                       <td><strong>	YouNegotiate</stron></td>
                                       <td>
                                         <strong>Email</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>report@younegotiate.com</strong>
                                       </td>
                                       <td>
                                        <a href="#" class="btn btn-green">Edit Schedule</a>
                                          <a href="#" class="btn btn-view">Pause Export</a>
                                        
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                      
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>David Williams</stron>
                                       </td>
                                       <td><strong>2222-06-08 06:01</stron></td>
                                       <td>
                                          <strong>Commission_Reporting_RNN</stron>
                                       </td>
                                       <td><strong>Hourly Updates</stron></td>
                                       <td><strong>YouNegotiate</stron></td>
                                       <td>
                                         <strong>Email</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>report@younegotiate.com</strong>
                                       </td>
                                       <td>
                                        <a href="#" class="btn btn-green">Edit Schedule</a>
                                          <a href="#" class="btn btn-view">Pause Export</a>
                                        
                                          <a href="#" class="btn btn-deactive">Delete</a>
                                       </td>
                                      
                                    </tr>
                                    
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

         
         </section>
      </div>
      <footer class="main-footer">
         <div class="footer-bg">
         <div class="footer-left">
           <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
             © 2020 YouNegotiate ™ | All rights reserved
         </div>
         </div>
      </footer>
   </div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>

<script>
    $(document).ready(function () {
      

         $('#example').DataTable({
            language: {
               searchPlaceholder: "Search",
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });
        

    });
</script>
@endsection