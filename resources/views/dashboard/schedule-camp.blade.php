@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Campaign Scheduler</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Schedule A Campaign</h4>
                  </div>
                  <div class="card-body">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Select Template <sup>*</sup></label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">US</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                 </select>
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Select Group <sup>*</sup></label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">US</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                 </select>
                              </div>
                           </div>
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Select Frequency <sup>*</sup></label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">US</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                 </select>
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Start Date <sup>*</sup></label>
                                 <input type="date" class="form-control" value="DD/MM/YYYY">
                              </div>
                           </div>
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Select Time From (EST Only) <sup>*</sup></label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">US</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="text-right">
                        <button type="submit" class="btn blue-btn">Add Scheduled Campaign</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header flex-hd">
                  <h4>Scheduled Campaigns</h4>
               </div>
               <div class="card-body">
                  <div class="table-responsive">
                     <table class="table width-responsive w-100" id="example2">
                        <thead>
                           <tr>
                              <th>
                                 template
                              </th>
                              <th>type</th>
                              <th>group</th>
                              <th>Frequency</th>
                              <th>start date</th>
                              <th>action</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <td>
                                 <strong>1st Header</strong>
                              </td>
                              <td><strong>Email</strong></td>
                              <td>
                                 <strong>3rdJune2021 - Not Joined</strong>
                              </td>
                              <td><strong>Daily at 1:30Hrs</strong></td>
                              <td><strong>12-06-2021</strong></td>
                              <td>
                                 <a href="#" class="btn btn-green">Edit</a>
                                 <a href="#" class="btn btn-view">Resume</a>
                                 <a href="#" class="btn btn-deactive">Delete</a>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <strong>1st Header</strong>
                              </td>
                              <td><strong>Email</strong></td>
                              <td>
                                 <strong>3rdJune2021 - Not Joined</strong>
                              </td>
                              <td><strong>Daily at 1:30Hrs</strong></td>
                              <td><strong>12-06-2021</strong></td>
                              <td>
                                 <a href="#" class="btn btn-green">Edit</a>
                                 <a href="#" class="btn btn-view">Resume</a>
                                 <a href="#" class="btn btn-deactive">Delete</a>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <strong>1st Header</strong>
                              </td>
                              <td><strong>Email</strong></td>
                              <td>
                                 <strong>3rdJune2021 - Not Joined</strong>
                              </td>
                              <td><strong>Daily at 1:30Hrs</strong></td>
                              <td><strong>12-06-2021</strong></td>
                              <td>
                                 <a href="#" class="btn btn-green">Edit</a>
                                 <a href="#" class="btn btn-view">Resume</a>
                                 <a href="#" class="btn btn-deactive">Delete</a>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <strong>1st Header</strong>
                              </td>
                              <td><strong>Email</strong></td>
                              <td>
                                 <strong>3rdJune2021 - Not Joined</strong>
                              </td>
                              <td><strong>Daily at 1:30Hrs</strong></td>
                              <td><strong>12-06-2021</strong></td>
                              <td>
                                 <a href="#" class="btn btn-green">Edit</a>
                                 <a href="#" class="btn btn-view">Resume</a>
                                 <a href="#" class="btn btn-deactive">Delete</a>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <strong>1st Header</strong>
                              </td>
                              <td><strong>Email</strong></td>
                              <td>
                                 <strong>3rdJune2021 - Not Joined</strong>
                              </td>
                              <td><strong>Daily at 1:30Hrs</strong></td>
                              <td><strong>12-06-2021</strong></td>
                              <td>
                                 <a href="#" class="btn btn-green">Edit</a>
                                 <a href="#" class="btn btn-view">Resume</a>
                                 <a href="#" class="btn btn-deactive">Delete</a>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <strong>1st Header</strong>
                              </td>
                              <td><strong>Email</strong></td>
                              <td>
                                 <strong>3rdJune2021 - Not Joined</strong>
                              </td>
                              <td><strong>Daily at 1:30Hrs</strong></td>
                              <td><strong>12-06-2021</strong></td>
                              <td>
                                 <a href="#" class="btn btn-green">Edit</a>
                                 <a href="#" class="btn btn-view">Resume</a>
                                 <a href="#" class="btn btn-deactive">Delete</a>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <strong>1st Header</strong>
                              </td>
                              <td><strong>Email</strong></td>
                              <td>
                                 <strong>3rdJune2021 - Not Joined</strong>
                              </td>
                              <td><strong>Daily at 1:30Hrs</strong></td>
                              <td><strong>12-06-2021</strong></td>
                              <td>
                                 <a href="#" class="btn btn-green">Edit</a>
                                 <a href="#" class="btn btn-view">Resume</a>
                                 <a href="#" class="btn btn-deactive">Delete</a>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <strong>1st Header</strong>
                              </td>
                              <td><strong>Email</strong></td>
                              <td>
                                 <strong>3rdJune2021 - Not Joined</strong>
                              </td>
                              <td><strong>Daily at 1:30Hrs</strong></td>
                              <td><strong>12-06-2021</strong></td>
                              <td>
                                 <a href="#" class="btn btn-green">Edit</a>
                                 <a href="#" class="btn btn-view">Resume</a>
                                 <a href="#" class="btn btn-deactive">Delete</a>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <strong>1st Header</strong>
                              </td>
                              <td><strong>Email</strong></td>
                              <td>
                                 <strong>3rdJune2021 - Not Joined</strong>
                              </td>
                              <td><strong>Daily at 1:30Hrs</strong></td>
                              <td><strong>12-06-2021</strong></td>
                              <td>
                                 <a href="#" class="btn btn-green">Edit</a>
                                 <a href="#" class="btn btn-view">Resume</a>
                                 <a href="#" class="btn btn-deactive">Delete</a>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <strong>1st Header</strong>
                              </td>
                              <td><strong>Email</strong></td>
                              <td>
                                 <strong>3rdJune2021 - Not Joined</strong>
                              </td>
                              <td><strong>Daily at 1:30Hrs</strong></td>
                              <td><strong>12-06-2021</strong></td>
                              <td>
                                 <a href="#" class="btn btn-green">Edit</a>
                                 <a href="#" class="btn btn-view">Resume</a>
                                 <a href="#" class="btn btn-deactive">Delete</a>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <strong>1st Header</strong>
                              </td>
                              <td><strong>Email</strong></td>
                              <td>
                                 <strong>3rdJune2021 - Not Joined</strong>
                              </td>
                              <td><strong>Daily at 1:30Hrs</strong></td>
                              <td><strong>12-06-2021</strong></td>
                              <td>
                                 <a href="#" class="btn btn-green">Edit</a>
                                 <a href="#" class="btn btn-view">Resume</a>
                                 <a href="#" class="btn btn-deactive">Delete</a>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <strong>1st Header</strong>
                              </td>
                              <td><strong>Email</strong></td>
                              <td>
                                 <strong>3rdJune2021 - Not Joined</strong>
                              </td>
                              <td><strong>Daily at 1:30Hrs</strong></td>
                              <td><strong>12-06-2021</strong></td>
                              <td>
                                 <a href="#" class="btn btn-green">Edit</a>
                                 <a href="#" class="btn btn-view">Resume</a>
                                 <a href="#" class="btn btn-deactive">Delete</a>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <strong>1st Header</strong>
                              </td>
                              <td><strong>Email</strong></td>
                              <td>
                                 <strong>3rdJune2021 - Not Joined</strong>
                              </td>
                              <td><strong>Daily at 1:30Hrs</strong></td>
                              <td><strong>12-06-2021</strong></td>
                              <td>
                                 <a href="#" class="btn btn-green">Edit</a>
                                 <a href="#" class="btn btn-view">Resume</a>
                                 <a href="#" class="btn btn-deactive">Delete</a>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <strong>1st Header</strong>
                              </td>
                              <td><strong>Email</strong></td>
                              <td>
                                 <strong>3rdJune2021 - Not Joined</strong>
                              </td>
                              <td><strong>Daily at 1:30Hrs</strong></td>
                              <td><strong>12-06-2021</strong></td>
                              <td>
                                 <a href="#" class="btn btn-green">Edit</a>
                                 <a href="#" class="btn btn-view">Resume</a>
                                 <a href="#" class="btn btn-deactive">Delete</a>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <strong>1st Header</strong>
                              </td>
                              <td><strong>Email</strong></td>
                              <td>
                                 <strong>3rdJune2021 - Not Joined</strong>
                              </td>
                              <td><strong>Daily at 1:30Hrs</strong></td>
                              <td><strong>12-06-2021</strong></td>
                              <td>
                                 <a href="#" class="btn btn-green">Edit</a>
                                 <a href="#" class="btn btn-view">Resume</a>
                                 <a href="#" class="btn btn-deactive">Delete</a>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <strong>1st Header</strong>
                              </td>
                              <td><strong>Email</strong></td>
                              <td>
                                 <strong>3rdJune2021 - Not Joined</strong>
                              </td>
                              <td><strong>Daily at 1:30Hrs</strong></td>
                              <td><strong>12-06-2021</strong></td>
                              <td>
                                 <a href="#" class="btn btn-green">Edit</a>
                                 <a href="#" class="btn btn-view">Resume</a>
                                 <a href="#" class="btn btn-deactive">Delete</a>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
<footer class="main-footer">
   <div class="footer-bg">
      <div class="footer-left">
         <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
      </div>
      <div class="footer-right">
         © 2020 YouNegotiate ™ | All rights reserved
      </div>
   </div>
</footer>
</div>
</div>
<!-- Modal -->
<div class="modal modal-new fade" id="preview" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Preview</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <div class="row">
               <div class="col-sm-12">
                  <h4>Subject</h4>
                  <h5><strong>Section 1.10.33 of "de Finibus Bonorum et Malorum", written by 
                     Cicero in 45 BC</strong>
                  </h5>
               </div>
               <div class="col-sm-12">
                  <h4>Content</h4>
                  <p>"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal modal-new md-sm fade" id="delete-template" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Delete Template</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h2>Do You Want to delete this?</h2>
            <div class="text-right mt-4">
               <button type="submit" class="btn border-btn">Cancel</button>
               <button type="submit" class="btn blue-btn">Yes</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal modal-new fade" id="send-template" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Select Group to Run Send the Template</h5>
            <button type="button" class="close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <form>
               <div class="form-group">
                  <label>Select Group </label>
                  <select class="custom-select">
                     <option selected="">Select</option>
                     <option value="1">US</option>
                     <option value="2">Two</option>
                     <option value="3">Three</option>
                  </select>
               </div>
               <div class="text-right">
                  <button type="submit" class="btn border-btn" data-dismiss="modal" aria-label="Close">Close</button>
                  <button type="submit" class="btn blue-btn">Save</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $(document).ready(function () {
   
   
   
        $('#example2').DataTable({
           language: {
            searchPlaceholder: "Search",
               'paginate': {
                   'previous': '<span class="fa fa-angle-left"></span>',
                   'next': '<span class="fa fa-angle-right"></span>'
               }
           }
       });
   
   });
</script>
@endsection