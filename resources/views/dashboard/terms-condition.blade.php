@extends('layout.dashboard.app')
@section('dashboard-content')
<div class="main-content">
   <section class="section">
      <h1 class="section-header">
         <div>Terms and Conditions</div>
      </h1>
      <div class="section-body">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>Enter Your Terms & Conditions (Will appear on Consumers Payment Page)</h4>
                  </div>
                  <div class="card-body">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label>Apply To <sup>*</sup></label>
                                 <select class="custom-select">
                                    <option selected="">Select</option>
                                    <option value="1">US</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label>Content <sup>*</sup></label>
                              <div id="summernote"></div>
                           </div>
                        </div>
                        <div class="text-right col-sm-12">
                           <button type="submit" class="btn blue-btn">Submit</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header flex-hd">
                     <h4>All Terms & Conditions</h4>
                  </div>
                  <div class="card-body">
                     <div class="table-responsive">
                        <table class="table width-responsive w-100" id="example2">
                           <thead>
                              <tr>
                                 <th>
                                    company name
                                 </th>
                                 <th>sub account name</th>
                                 <th>action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <strong>YouNegotiate </strong>
                                 </td>
                                 <td><strong>-</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#pre-content">Preview</a>
                                    <a href="#" class="btn btn-view" >Edit</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>YouNegotiate </strong>
                                 </td>
                                 <td><strong>-</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#pre-content">Preview</a>
                                    <a href="#" class="btn btn-view" >Edit</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>YouNegotiate </strong>
                                 </td>
                                 <td><strong>-</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#pre-content">Preview</a>
                                    <a href="#" class="btn btn-view" >Edit</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>YouNegotiate </strong>
                                 </td>
                                 <td><strong>-</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#pre-content">Preview</a>
                                    <a href="#" class="btn btn-view" >Edit</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>YouNegotiate </strong>
                                 </td>
                                 <td><strong>-</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#pre-content">Preview</a>
                                    <a href="#" class="btn btn-view" >Edit</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>YouNegotiate </strong>
                                 </td>
                                 <td><strong>-</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#pre-content">Preview</a>
                                    <a href="#" class="btn btn-view" >Edit</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>YouNegotiate </strong>
                                 </td>
                                 <td><strong>-</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#pre-content">Preview</a>
                                    <a href="#" class="btn btn-view" >Edit</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>YouNegotiate </strong>
                                 </td>
                                 <td><strong>-</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#pre-content">Preview</a>
                                    <a href="#" class="btn btn-view" >Edit</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>YouNegotiate </strong>
                                 </td>
                                 <td><strong>-</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#pre-content">Preview</a>
                                    <a href="#" class="btn btn-view" >Edit</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>YouNegotiate </strong>
                                 </td>
                                 <td><strong>-</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#pre-content">Preview</a>
                                    <a href="#" class="btn btn-view" >Edit</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <strong>YouNegotiate </strong>
                                 </td>
                                 <td><strong>-</strong></td>
                                 <td>
                                    <a href="#" class="btn btn-green" data-toggle="modal" data-target="#pre-content">Preview</a>
                                    <a href="#" class="btn btn-view" >Edit</a>
                                    <a href="#" class="btn btn-deactive">Delete</a>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </section>
   </div>
   <footer class="main-footer">
      <div class="footer-bg">
         <div class="footer-left">
            <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
            © 2020 YouNegotiate ™ | All rights reserved
         </div>
      </div>
   </footer>
</div>
</div>
<!-- Modal -->


<div class="modal modal-new fade" id="pre-content" >
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Preview</h5>
            <button type="button" class="close">
            <span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
            </button>
         </div>
         <div class="modal-body content-ac-del">
            <h5 class="bold-text">Section 1.10.33 of "de Finibus Bonorum et Malorum", 
written by Cicero in 45 BC</h5>
<p>"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>
<script>
   $('#summernote').summernote({
      placeholder: 'Hello stand alone ui',
      tabsize: 2,
      height: 100,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
      ]
    });
   $(document).ready(function () {
   
   
   
       $('#example2').DataTable({
          language: {
            searchPlaceholder: "Search",
              'paginate': {
                  'previous': '<span class="fa fa-angle-left"></span>',
                  'next': '<span class="fa fa-angle-right"></span>'
              }
          }
      });
   
   });
</script>
@endsection