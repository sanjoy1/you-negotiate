@extends('layout.dashboard.app')
@section('dashboard-content')

      <div class="main-content">
         <section class="section">
            <h1 class="section-header">
               <div>Transactions (Quick access to all consumer transactions!)</div>
            </h1>
            <div class="section-body">
              
               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Filter Transactions</h4>
                           
                        </div>
                        <hr>
                        <div class="card-body">
                           <div class="form-row">
                                    <div class="form-group col-md-6">
                                    <label>Select Sub Account <sup>*</sup></label>
                                     <select class="custom-select">
                                        <option selected="">Select</option>
                                        <option value="1">US</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                     </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                    <label>Start Date <sup>*</sup></label>
                                     <input type="date" class="form-control" value="DD/MM/YYYY">
                                    </div>
                                  <div class="form-group col-md-3">
                                    <label>End Date <sup>*</sup></label>
                                     <input type="date" class="form-control" value="DD/MM/YYYY">
                                    </div>
                                </div>
                                <div class="text-right">
                           <button type="submit" class="btn blue-btn">Submit</button>
                        </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Successfull Transactions</h4>
                          
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table width-responsive w-100" id="example2">
                                 <thead>
                                    <tr>
                                       <th>
                                         date & time
                                       </th>
                                       <th>account</th>
                                       <th>customer name</th>
                                       <th>Transaction Type</th>
                                       <th>Transaction ID</th>
                                       <th>Payment mode</th>
                                       <th>Transaction amount</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                        <strong> Online</stron>
                                       </td>
                                       <td>
                                        <strong>Email</stron>
                                       </td>
                                       <td>
                                         <strong>cc</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>$10.00</strong>
                                       </td>
                                       
                                    </tr>
                               <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                        <strong> Online</stron>
                                       </td>
                                       <td>
                                        <strong>Email</stron>
                                       </td>
                                       <td>
                                         <strong>cc</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>$10.00</strong>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                        <strong> Online</stron>
                                       </td>
                                       <td>
                                        <strong>Email</stron>
                                       </td>
                                       <td>
                                         <strong>cc</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>$10.00</strong>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                        <strong> Online</stron>
                                       </td>
                                       <td>
                                        <strong>Email</stron>
                                       </td>
                                       <td>
                                         <strong>cc</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>$10.00</strong>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                        <strong> Online</stron>
                                       </td>
                                       <td>
                                        <strong>Email</stron>
                                       </td>
                                       <td>
                                         <strong>cc</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>$10.00</strong>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                        <strong> Online</stron>
                                       </td>
                                       <td>
                                        <strong>Email</stron>
                                       </td>
                                       <td>
                                         <strong>cc</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>$10.00</strong>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                        <strong> Online</stron>
                                       </td>
                                       <td>
                                        <strong>Email</stron>
                                       </td>
                                       <td>
                                         <strong>cc</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>$10.00</strong>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                        <strong> Online</stron>
                                       </td>
                                       <td>
                                        <strong>Email</stron>
                                       </td>
                                       <td>
                                         <strong>cc</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>$10.00</strong>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                        <strong> Online</stron>
                                       </td>
                                       <td>
                                        <strong>Email</stron>
                                       </td>
                                       <td>
                                         <strong>cc</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>$10.00</strong>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                        <strong> Online</stron>
                                       </td>
                                       <td>
                                        <strong>Email</stron>
                                       </td>
                                       <td>
                                         <strong>cc</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>$10.00</strong>
                                       </td>
                                       
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                        <strong> Online</stron>
                                       </td>
                                       <td>
                                        <strong>Email</stron>
                                       </td>
                                       <td>
                                         <strong>cc</strong>
                                       </td>
                                       
                                       <td>
                                          <strong>$10.00</strong>
                                       </td>
                                       
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Failed Transactions </h4>
                          
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table width-responsive" id="example3">
                                 <thead>
                                    <tr>
                                       <th>
                                         date & time
                                       </th>
                                       <th>account</th>
                                       <th>customer name</th>
                                       <th>Transaction Type</th>
                                       <th>Transaction amount</th>
                                       <th>gateway failed reason</th>
                                       <th>action</th>


                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Online</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>Merchant does not currently support check transactions.</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">Reschedule Failed Payment</a>
                                       </td>
                                       
                                     
                                    </tr>
                                 <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Online</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>Merchant does not currently support check transactions.</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">Reschedule Failed Payment</a>
                                       </td>
                                       
                                     
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Online</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>Merchant does not currently support check transactions.</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">Reschedule Failed Payment</a>
                                       </td>
                                       
                                     
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Online</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>Merchant does not currently support check transactions.</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">Reschedule Failed Payment</a>
                                       </td>
                                       
                                     
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Online</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>Merchant does not currently support check transactions.</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">Reschedule Failed Payment</a>
                                       </td>
                                       
                                     
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Online</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>Merchant does not currently support check transactions.</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">Reschedule Failed Payment</a>
                                       </td>
                                       
                                     
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Online</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>Merchant does not currently support check transactions.</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">Reschedule Failed Payment</a>
                                       </td>
                                       
                                     
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Online</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>Merchant does not currently support check transactions.</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">Reschedule Failed Payment</a>
                                       </td>
                                       
                                     
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Online</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>Merchant does not currently support check transactions.</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">Reschedule Failed Payment</a>
                                       </td>
                                       
                                     
                                    </tr>
                                     <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Online</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>Merchant does not currently support check transactions.</strong>
                                       </td>
                                       <td>
                                         <a href="#" class="btn btn-green">Reschedule Failed Payment</a>
                                       </td>
                                       
                                     
                                    </tr>

                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
              <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-header flex-hd">
                           <h4>Upcoming Transactions</h4>
                          
                        </div>
                        <div class="card-body">
                           <div class="table-responsive">
                              <table class="table width-responsive w-100" id="example4">
                                 <thead>
                                    <tr>
                                       <th>
                                         date & time
                                       </th>
                                       <th>account</th>
                                       <th>customer name</th>
                                       <th>client name</th>
                                       <th>Transaction amount</th>
                                        <th>Card / ACH</th>
                                       <th>status</th>
                                       <th>action</th>

                                       
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Roy Mustang</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>ACH</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                 <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Roy Mustang</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>ACH</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Roy Mustang</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>ACH</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Roy Mustang</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>ACH</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Roy Mustang</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>ACH</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Roy Mustang</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>ACH</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Roy Mustang</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>ACH</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Roy Mustang</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>ACH</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Roy Mustang</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>ACH</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Roy Mustang</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>ACH</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Roy Mustang</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>ACH</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Roy Mustang</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>ACH</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>
                                    <tr>
                                       <td>
                                          <strong>2222-06-08 06:01</stron>
                                       </td>
                                       <td><strong>435551</stron></td>
                                       <td>
                                          <strong>Edward Elric</stron>
                                       </td>
                                       <td>
                                         <strong>Roy Mustang</strong>
                                       </td>
                                       
                                       <td>
                                         <strong>$10.00</strong>
                                       </td>
                                       <td>
                                         <strong>ACH</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       <td>
                                         <strong>-</strong>
                                       </td>
                                       
                                     
                                    </tr>

                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               

               
            </div>
         </section>
      </div>
      <footer class="main-footer">
         <div class="footer-bg">
         <div class="footer-left">
           <a href="index"><img alt="logo" src="{{asset('dist/img/logo.png')}}"></a>
         </div>
         <div class="footer-right">
             © 2020 YouNegotiate ™ | All rights reserved
         </div>
         </div>
      </footer>
   </div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/modules/jquery.min.js')}}"></script>
<script src="{{asset('dist/modules/popper.js')}}"></script>
<script src="{{asset('dist/modules/tooltip.js')}}"></script>
<script src="{{asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('dist/js/sa-functions.js')}}"></script>
<script src="{{asset('dist/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('dist/modules/jquery-ui.min.js')}}"></script>
<script src="{{asset('dist/modules/modules-datatables.js')}}"></script>
<script src="{{asset('dist/modules/chart.min.js')}}"></script>
<script src="{{asset('dist/modules/summernote/summernote-lite.js')}}"></script>
<script src="{{asset('dist/js/scripts.js')}}"></script>
<script src="{{asset('dist/js/custom.js')}}"></script>

<script>
    $(document).ready(function () {
     

         $('#example2').DataTable({
            language: {
               searchPlaceholder: "Search",
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });

         $('#example3').DataTable({
            language: {
               searchPlaceholder: "Search",
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });
         $('#example4').DataTable({
            language: {
               searchPlaceholder: "Search",
                'paginate': {
                    'previous': '<span class="fa fa-angle-left"></span>',
                    'next': '<span class="fa fa-angle-right"></span>'
                }
            }
        });
     
    });
</script>
@endsection