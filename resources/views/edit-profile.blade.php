@extends('layout.app-header') @section('content')
<section class="white-common">
    <div class="container">
        <h2 class="h2-text text-center mb-5">My Profile</h2>
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-3">
                <div class="faq-nav">
                    <ul>
                        <li>
                            <a href="edit-profile" class="active">My Profile Details <i class="fa fa-angle-right"></i></a>
                        </li>
                        <li>
                            <a href="notification">Notification Preference <i class="fa fa-angle-right"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-12 col-md-8 col-lg-8 offset-lg-1">
                <div class="from-content-box">
                    <div class="d-flex align-items-center justify-content-between">
                        <h4>Personal Information</h4>
                        <a href="save-profile " class="btn border-btn" style="font-size: 14px;">Edit Profile</a>
                    </div>

                    <div class="form-style1 mt-5">
                        <form>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" value="Dominic" />
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" value="Cooper" />
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Mobile</label>
                                    <input type="text" class="form-control" value="9845123311" />
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Landline</label>
                                    <input type="text" class="form-control" value="243566666123" />
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Email</label>
                                    <input type="email" class="form-control" value="dcooper@gmail.com" />
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Address</label>
                                    <input type="text" class="form-control" value="1/a beverly hills california, United states" />
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>City</label>
                                    <input type="text" class="form-control" value="cc" />
                                </div>
                                <div class="form-group col-md-6">
                                    <label>State</label>
                                    <select class="custom-select">
                                        <option selected="">US</option>
                                        <option value="1">US</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Pin</label>
                                    <input type="text" class="form-control" value="345551" />
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Date of birth</label>
                                    <input type="date" class="form-control" value="DD/MM/YYYY" />
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>SSN</label>
                                    <input type="text" class="form-control" value="4444444" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        
                        <div class="from-content-box html-show">
                            <div class="d-flex align-items-center justify-content-between">
                                <h4>Password</h4>
                                <button class="btn border-btn click-1" style="font-size: 14px;" id="change-Password">Change Password</button>
                            </div>

                            <div class="form-style1 mt-3">
                                <form>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <div class="grp-area">
                                            <input type="password" id="txtPassword" class="form-control" name="txtPassword" />
                                            <button type="button" id="btnToggle" class="toggle-view"><i id="eyeIcon" class="fa fa-eye"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

<div class="from-content-box password-box1" style="display:none">
                            <div class="d-flex align-items-center justify-content-between">
                                <h4>Change Password</h4>
                                <button class="btn border-btn" style="font-size: 14px;" id="change">Change</button>
                            </div>

                            <div class="form-style1 mt-3">
                                <form>
                                    <div class="form-group">
                                        <label>Current Password</label>
                                        <div class="grp-area">
                                            <input type="password" id="txtPassword" class="form-control" name="Current Password" />
                                            <button type="button" id="btnToggle" class="toggle-view"><i id="eyeIcon" class="fa fa-eye"></i></button>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>New Password </label>
                                        <div class="grp-area">
                                            <input type="password" id="txtPassword1" class="form-control" name="New Password" />
                                            <button type="button" id="btnToggle1" class="toggle-view"><i id="eyeIcon1" class="fa fa-eye"></i></button>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Confirm New Password </label>
                                        <div class="grp-area">
                                            <input type="password" id="txtPassword2" class="form-control" name="Confirm New Password" />
                                            <button type="button" id="btnToggle2" class="toggle-view"><i id="eyeIcon2" class="fa fa-eye"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection @section('script')
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>

<script>
    $(document).ready(function () {
        let passwordInput = document.getElementById("txtPassword"),
            toggle = document.getElementById("btnToggle"),
            icon = document.getElementById("eyeIcon");

        function togglePassword() {
            if (passwordInput.type === "password") {
                passwordInput.type = "text";
                icon.classList.add("fa-eye-slash");
                //toggle.innerHTML = 'hide';
            } else {
                passwordInput.type = "password";
                icon.classList.remove("fa-eye-slash");
                //toggle.innerHTML = 'show';
            }
        }

        function checkInput() {}

        toggle.addEventListener("click", togglePassword, false);
        passwordInput.addEventListener("keyup", checkInput, false);

       $("#change-Password").click(function(){
            $(".html-show").hide();
            $(".password-box1").show();
        });
        $("#change").click(function(){
            $(".password-box1").hide();
             $(".html-show").show();
        });
    });
  
   


</script>
@endsection
