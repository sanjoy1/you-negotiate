@extends('layout.app')
@section('content')
<section class="white-common">
   <div class="container">
      <h2 class="h2-text text-center mb-5">Frequently Ask Question</h2>
      <div class="row">
         <div class="col-sm-12 col-md-4 col-lg-3">
            <div class="faq-nav">
               <ul>
                  <li><a href="#" class="active">Loging In <i class="fa fa-angle-right"></i></a></li>
                  <li><a href="#">My Resolution Options <i class="fa fa-angle-right"></i></a></li>
               </ul>
            </div>
         </div>
         <div class="col-sm-12 col-md-8 col-lg-8 offset-lg-1">
            <div class="faq-content-box">
               <h3>Where do I find the Reference Number for my account?</h3>
               <p>The Reference Number should be clearly identified in our invitation to resolve your debt online. If you cannot 
                  locate or do not know the Reference Number, please
               </p>
            </div>
            <div class="faq-content-box">
               <h3>What if my Reference Number and SSN don’t work? Confirm you have</h3>
               <p>Confirm you have correctly entered the Reference Number and the last 4 digits of your SSN. If you still 
                  cannot log in, your information may be incorrect or the special opportunity may
               </p>
            </div>
            <div class="faq-content-box">
               <h3>What if my Reference Number and SSN don’t work? Confirm you have</h3>
               <p>Confirm you have correctly entered the Reference Number and the last 4 digits of your SSN. If you still 
                  cannot log in, your information may be incorrect or the special opportunity may
               </p>
            </div>
            <div class="faq-content-box">
               <h3>What if my Reference Number and SSN don’t work? Confirm you have</h3>
               <p>Confirm you have correctly entered the Reference Number and the last 4 digits of your SSN. If you still 
                  cannot log in, your information may be incorrect or the special opportunity may
               </p>
            </div>
            <div class="faq-content-box">
               <h3>What if my Reference Number and SSN don’t work? Confirm you have</h3>
               <p>Confirm you have correctly entered the Reference Number and the last 4 digits of your SSN. If you still 
                  cannot log in, your information may be incorrect or the special opportunity may
               </p>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
@section('script')
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
@endsection