@extends('layout.app')
@section('content')
<section class="home-banner">
   <div class="container text-center">
      <h1>The fastest & easiest <span>way to pay bills</span></h1>
      <p>With utmost care and respect, we are dedicated to helping  100 million people <br>
         pay their bills and build their dreams
      </p>
      <a href="login" class="btn btn-blue">I’m a consumer</a>
      <figure>
         <img src="{{asset('assets/img/home.png')}}" alt="home" class="img-fluid">
      </figure>
   </div>
</section>
<section class="fastest-section">
   <div class="container">
      <div class="row align-items-center">
         <div class="col-sm-12 col-md-4 col-lg-4">
            <h2 class="h2-text">The fastest & easiest way to pay bills</h2>
            <p class="artile-1">With utmost care and respect, we are [dedicated to helping  100 million people pay their bills and build their dreams</p>
         </div>
         <div class="col-sm-12 col-md-8 col-lg-8">
            <div class="row">
               <div class="col-sm-12 col-md-6 col-lg-6 mb-4">
                  <div class="cd-Box">
                     <figure>
                        <img src="{{asset('assets/img/f1.svg')}}" alt="icon" class="">
                     </figure>
                     <h3>Creditors Upload Account Data</h3>
                     <p>Upload and connect in any format quickly and easily.</p>
                  </div>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-6 mb-4">
                  <div class="cd-Box">
                     <figure>
                        <img src="{{asset('assets/img/f2.svg')}}" alt="icon" class="">
                     </figure>
                     <h3>Consumers Create Their Profile</h3>
                     <p>One login to connect to ALL collection 
                        accounts. You can easily invite any 
                        creditors that haven’t uploaded your 
                        account data.
                     </p>
                  </div>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-6 mb-4">
                  <div class="cd-Box">
                     <figure>
                        <img src="{{asset('assets/img/f3.svg')}}" alt="icon" class="">
                     </figure>
                     <h3>Consumers Manage Accounts</h3>
                     <p>Manage all of your digital payments, 
                        negotiations, and offers on one platform 
                        and know where you stand at all times
                     </p>
                  </div>
               </div>
               <div class="ccol-sm-12 col-md-6 col-lg-6 mb-4">
                  <div class="cd-Box">
                     <figure>
                        <img src="{{asset('assets/img/f4.svg')}}" alt="icon" class="">
                     </figure>
                     <h3>Consumers Adjust Payments as Needed</h3>
                     <p>In your account you can postpone 
                        payments or change scheduled payments in the event of a life change. You are in control of paying your accounts and have the flexibility you need to make all of your payments.
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="white-common">
   <div class="container">
      <h2 class="h2-text text-center">Finally, a free bill payment solution that makes sense</h2>
      <p class="artile-1 text-center">YouNegotiate® offers this easy-to-use service free of charge to all consumers in order to help resolve debts in the way that works best for each individual. No need to speak to a collector or creditor, ever again. Instead, just manage your account with us and get out of debt at your pace.</p>
      <div class="row mt-5">
         <div class="col-sm-12 col-md-4 col-lg-4">
            <div class="quick-box">
               <figure>
                  <img src="{{asset('assets/img/mail.svg')}}" alt="mail" class="">
               </figure>
               <h3>Quick & Secure Login</h3>
               <p>Ability to login without a reference number</p>
            </div>
         </div>
         <div class="col-sm-12 col-md-4 col-lg-4">
            <div class="quick-box">
               <figure>
                  <img src="{{asset('assets/img/pay.svg')}}" alt="mail" class="">
               </figure>
               <h3>Easily Make Payments</h3>
               <p>Ability to login without a reference number</p>
            </div>
         </div>
         <div class="col-sm-12 col-md-4 col-lg-4">
            <div class="quick-box">
               <figure>
                  <img src="{{asset('assets/img/cm.svg')}}" alt="mail" class="">
               </figure>
               <h3>Receive Immediate Confirmation</h3>
               <p>Receive an email confirmation of all payments 
                  and see it instantly reflected within your 
                  account.
               </p>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="white-common bg-gray-box over-hidded">
   <div class="container">
      <h2 class="h2-text text-center">Testimonials</h2>
      <p class="artile-1 text-center">What Consumers are saying about YouNegotiate®!</p>
      <section id="testimonial" class="">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div id="testimonialCarousel" class="carousel slide" data-ride="carousel">
                     <div class="carousel-inner" role="listbox">
                        <!-- Slide 1 -->
                        <div class="carousel-item active">
                           <div class="carousel-content">
                              <div class="client-img"><img src="{{asset('assets/img/u1.png')}}" alt="ss"></div>
                              <p class="col-md-10 offset-md-1">
                                 YouNegotiate® offers this easy-to-use service free of charge to all consumers in order to help resolve debts in the way that works best for each individual. No need to speak to a collector or creditor, ever again. Instead, just manage your account with us and get out of debt at your pace.
                              </p>
                              <figure><img src="{{asset('assets/img/star.svg')}}" alt="review"></figure>
                              <h3>Tom Holand</h3>
                           </div>
                        </div>
                        <!-- Slide 2 -->
                        <div class="carousel-item ">
                           <div class="carousel-content">
                              <div class="client-img"><img src="{{asset('assets/img/u1.png')}}" alt="ss"></div>
                              <p class="col-md-10 offset-md-1">
                                 YouNegotiate® offers this easy-to-use service free of charge to all consumers in order to help resolve debts in the way that works best for each individual. No need to speak to a collector or creditor, ever again. Instead, just manage your account with us and get out of debt at your pace.
                              </p>
                              <figure><img src="{{asset('assets/img/star.svg')}}" alt="review"></figure>
                              <h3>Tom Holand</h3>
                           </div>
                        </div>
                        <!-- Slide 3 -->
                        <div class="carousel-item ">
                           <div class="carousel-content">
                              <div class="client-img"><img src="{{asset('assets/img/u1.png')}}" alt="ss"></div>
                              <p class="col-md-10 offset-md-1">
                                 YouNegotiate® offers this easy-to-use service free of charge to all consumers in order to help resolve debts in the way that works best for each individual. No need to speak to a collector or creditor, ever again. Instead, just manage your account with us and get out of debt at your pace.
                              </p>
                              <figure><img src="{{asset('assets/img/star.svg')}}" alt="review"></figure>
                              <h3>Tom Holand</h3>
                           </div>
                        </div>
                        <!-- Slider pre and next arrow -->
                        <a class="carousel-control-prev text-white" href="#testimonialCarousel" role="button" data-slide="prev">
                        <i class=""><img src="{{asset('assets/img/arw-left.svg')}}" alt="arrow"></i>
                        </a>
                        <a class="carousel-control-next text-white" href="#testimonialCarousel" role="button" data-slide="next">
                        <i class=""><img src="{{asset('assets/img/arrow-right.svg')}}" alt="arrow"></i>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </div>
</section>
@endsection
@section('script')
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
@endsection