<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8" />
      <meta
         content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"
         name="viewport"
         />
      <link rel="shortcut icon" href="../assets/img/favicon.png" type="image/x-icon">
      <title>Admin</title>
      <link rel="stylesheet" href="{{asset('admin/modules/bootstrap/css/bootstrap.min.css')}}" />
      <link rel="stylesheet" href="{{asset('admin/modules/ionicons/css/ionicons.min.css')}}"  />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" />
      <link rel="stylesheet" href="{{asset('admin/modules/datatables/datatables.min.css')}}">
      <link rel="stylesheet" href="{{asset('admin/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{asset('admin/modules/summernote/summernote-lite.css')}}" />
      <link rel="stylesheet" href="{{asset('admin/modules/flag-icon-css/css/flag-icon.min.css')}}" />
      <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />

      <link rel="stylesheet" href="{{asset('admin/css/demo.css')}}" />
      <link rel="stylesheet" href="{{asset('admin/css/style.css')}}" />
   </head>
   <body>
      <div id="app">
      <div class="main-wrapper">
      <nav class="navbar navbar-expand-lg main-navbar">
         <div class="mr-auto">
            <ul class="navbar-nav mr-3">
               <li>
                  <a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="ion ion-navicon-round"></i></a>
               </li>
               <li> <a href="channel-partner" class="nav-link nav-link-lg"><i class="i-Add-UserStar text-20 cursor-pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Users"></i></a></li>
               <li> <a href="template" class="nav-link nav-link-lg"><i class="i-Speach-Bubble-3 text-20 cursor-pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Send Mass-SMS"></i></a></li>
               <li> <a href="template" class="nav-link nav-link-lg"><i class="i-Email text-20 mobile-hide cursor-pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Send Mass-Email"></i></a></li>
               <li> <a href="#" class="nav-link nav-link-lg"><i class="i-Upload-Window text-20 mobile-hide cursor-pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Run A Report"></i></a></li>
               <li> <a href="schedule-report" class="nav-link nav-link-lg"><i class="i-Calendar-3 text-20 mobile-hide cursor-pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Schedule Export"></i></a></li>
            </ul>
         </div>
         
   
         <ul class="navbar-nav navbar-right">
            <li class="dropdown">
               <a
                  href="#"
                  data-toggle="dropdown"
                  class="nav-link dropdown-toggle nav-link-lg"
                  >
                  <i class="ion ion-android-person person-round"></i>
                 
               </a
                  >
               <div class="dropdown-menu dropdown-menu-right">
                  <a href="profile" class="dropdown-item has-icon">
                  Company Profile
                  </a>
                  <a href="change-password" class="dropdown-item has-icon">
                  Change Password
                  </a>
                  <a href="/login" class="dropdown-item lg-bottom">Logout</a>
               </div>
            </li>
           
         </ul>

      </nav>
      <div class="main-sidebar">
         <aside id="sidebar-wrapper">
          
            <div class="sidebar-brand">
               <div class="sidebar-user-picture pt-4 pb-5">
                  <img alt="image" src="{{asset('admin/img/logo.png')}}" />
               </div>
            </div>
            <ul class="sidebar-menu">
             

 <li>
                  <a href="#" class="has-dropdown"
                     ><svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg" class="mr-2">
                     <path d="M21 19V17C21 15.1362 19.7252 13.5701 18 13.126M14.5 1.29076C15.9659 1.88415 17 3.32131 17 5C17 6.67869 15.9659 8.11585 14.5 8.70924M16 19C16 17.1362 16 16.2044 15.6955 15.4693C15.2895 14.4892 14.5108 13.7105 13.5307 13.3045C12.7956 13 11.8638 13 10 13H7C5.13623 13 4.20435 13 3.46927 13.3045C2.48915 13.7105 1.71046 14.4892 1.30448 15.4693C1 16.2044 1 17.1362 1 19M12.5 5C12.5 7.20914 10.7091 9 8.5 9C6.29086 9 4.5 7.20914 4.5 5C4.5 2.79086 6.29086 1 8.5 1C10.7091 1 12.5 2.79086 12.5 5Z" stroke="#161B1B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                     </svg>
                     <span>Profiles</span></a
                     >
                  <ul class="menu-dropdown">
                     <li>
                        <a href="admin-dashboard">Creditor</a>
                     </li>
                     <li>
                        <a href="channel-partner">Channel Partners</a>
                     </li>
                     
                  </ul>
               </li>

               <li class="">
                  <a href="payment"
                     ><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" class="mr-2">
<path d="M8.5 14.6667C8.5 15.9553 9.54467 17 10.8333 17H13C14.3807 17 15.5 15.8807 15.5 14.5C15.5 13.1193 14.3807 12 13 12H11C9.61929 12 8.5 10.8807 8.5 9.5C8.5 8.11929 9.61929 7 11 7H13.1667C14.4553 7 15.5 8.04467 15.5 9.33333M12 5.5V7M12 17V18.5M22 12C22 17.5228 17.5228 22 12 22C6.47715 22 2 17.5228 2 12C2 6.47715 6.47715 2 12 2C17.5228 2 22 6.47715 22 12Z" stroke="#161B1B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
</svg> <span>Payment</span></a
                     >
               </li>
               <li>
                  <a href="#" class="has-dropdown"
                     ><svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg" class="mr-2">
                     <path d="M21 19V17C21 15.1362 19.7252 13.5701 18 13.126M14.5 1.29076C15.9659 1.88415 17 3.32131 17 5C17 6.67869 15.9659 8.11585 14.5 8.70924M16 19C16 17.1362 16 16.2044 15.6955 15.4693C15.2895 14.4892 14.5108 13.7105 13.5307 13.3045C12.7956 13 11.8638 13 10 13H7C5.13623 13 4.20435 13 3.46927 13.3045C2.48915 13.7105 1.71046 14.4892 1.30448 15.4693C1 16.2044 1 17.1362 1 19M12.5 5C12.5 7.20914 10.7091 9 8.5 9C6.29086 9 4.5 7.20914 4.5 5C4.5 2.79086 6.29086 1 8.5 1C10.7091 1 12.5 2.79086 12.5 5Z" stroke="#161B1B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                     </svg>
                     <span>Communication</span></a
                     >
                  <ul class="menu-dropdown">
                     <li>
                        <a href="template">Template</a>
                     </li>
                     <li>
                        <a href="configure">Configure</a>
                     </li>
                      <li>
                        <a href="schedule">Schedule</a>
                     </li>
                     <li>
                        <a href="campaign">Campaign</a>
                     </li>
                     
                  </ul>
               </li>

                <li>
                  <a href="#" class="has-dropdown"
                     ><svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg" class="mr-2">
                     <path d="M21 19V17C21 15.1362 19.7252 13.5701 18 13.126M14.5 1.29076C15.9659 1.88415 17 3.32131 17 5C17 6.67869 15.9659 8.11585 14.5 8.70924M16 19C16 17.1362 16 16.2044 15.6955 15.4693C15.2895 14.4892 14.5108 13.7105 13.5307 13.3045C12.7956 13 11.8638 13 10 13H7C5.13623 13 4.20435 13 3.46927 13.3045C2.48915 13.7105 1.71046 14.4892 1.30448 15.4693C1 16.2044 1 17.1362 1 19M12.5 5C12.5 7.20914 10.7091 9 8.5 9C6.29086 9 4.5 7.20914 4.5 5C4.5 2.79086 6.29086 1 8.5 1C10.7091 1 12.5 2.79086 12.5 5Z" stroke="#161B1B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                     </svg>
                     <span>Auto Communication</span></a
                     >
                  <ul class="menu-dropdown">
                      <li>
                        <a href="template2">Template</a>
                     </li>
                     <li>
                        <a href="configure2">Configure</a>
                     </li>
                      <li>
                        <a href="schedule2">Schedule</a>
                     </li>
                     <li>
                        <a href="schedule-campaign">Campaign</a>
                     </li>
                     
                  </ul>
               </li>

               <li class="">
                  <a href="report"
                     ><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" class="mr-2">
                     <path d="M8 15V17M12 11V17M16 7V17M7.8 21H16.2C17.8802 21 18.7202 21 19.362 20.673C19.9265 20.3854 20.3854 19.9265 20.673 19.362C21 18.7202 21 17.8802 21 16.2V7.8C21 6.11984 21 5.27976 20.673 4.63803C20.3854 4.07354 19.9265 3.6146 19.362 3.32698C18.7202 3 17.8802 3 16.2 3H7.8C6.11984 3 5.27976 3 4.63803 3.32698C4.07354 3.6146 3.6146 4.07354 3.32698 4.63803C3 5.27976 3 6.11984 3 7.8V16.2C3 17.8802 3 18.7202 3.32698 19.362C3.6146 19.9265 4.07354 20.3854 4.63803 20.673C5.27976 21 6.11984 21 7.8 21Z" stroke="#161B1B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                     </svg> <span>Reporting</span></a
                     >
               </li>
               <li class="">
                  <a href="creditor-billing"
                     ><svg width="18" height="22" viewBox="0 0 18 22" fill="none" xmlns="http://www.w3.org/2000/svg" class="mr-2">
                     <path d="M11 1.26953V5.40007C11 5.96012 11 6.24015 11.109 6.45406C11.2049 6.64222 11.3578 6.7952 11.546 6.89108C11.7599 7.00007 12.0399 7.00007 12.6 7.00007H16.7305M5 14V17M13 12V17M9 9.5V17M17 8.98822V16.2C17 17.8802 17 18.7202 16.673 19.362C16.3854 19.9265 15.9265 20.3854 15.362 20.673C14.7202 21 13.8802 21 12.2 21H5.8C4.11984 21 3.27976 21 2.63803 20.673C2.07354 20.3854 1.6146 19.9265 1.32698 19.362C1 18.7202 1 17.8802 1 16.2V5.8C1 4.11984 1 3.27976 1.32698 2.63803C1.6146 2.07354 2.07354 1.6146 2.63803 1.32698C3.27976 1 4.11984 1 5.8 1H9.01178C9.74555 1 10.1124 1 10.4577 1.08289C10.7638 1.15638 11.0564 1.27759 11.3249 1.44208C11.6276 1.6276 11.887 1.88703 12.4059 2.40589L15.5941 5.59411C16.113 6.11297 16.3724 6.3724 16.5579 6.67515C16.7224 6.94356 16.8436 7.2362 16.9171 7.5423C17 7.88757 17 8.25445 17 8.98822Z" stroke="#161B1B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                     </svg>
                     <span>Creditor Billing</span></a
                                          >
               </li>
              <li class="">
                  <a href="consumer"
                     >
                     <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" class="mr-2">
                     <path d="M3 20C5.33579 17.5226 8.50702 16 12 16C15.493 16 18.6642 17.5226 21 20M16.5 7.5C16.5 9.98528 14.4853 12 12 12C9.51472 12 7.5 9.98528 7.5 7.5C7.5 5.01472 9.51472 3 12 3C14.4853 3 16.5 5.01472 16.5 7.5Z" stroke="#161B1B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                     </svg>
                     <span>Consumers</span></a
                                          >
               </li>
           
            </ul>
         </aside>
      </div>
      @yield('admin-content')
      @yield('script')
   </body>
</html>