<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon">
    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" />
     <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/date-calender.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>You-Negotiate</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-white menu-bar">
        <div class="container">
            <a class="navbar-brand" href="{{url('/')}}"><img alt="Logo" src="{{asset('assets/img/logo.png')}}"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/')}}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('how-it-work')}}">How it works</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{url('cre-network')}}">Creditors Network</a>
                    </li>
                </ul>
                <div class="form-inline">
                    <a href="login" class="btn border-btn mr-2">I’m a consumer</a>
                    <a href="#" class="btn border-btn">I’m a creditor</a>
                <ul class="sl-nav-ul ml-3">
                    <li><b>Eng</b> <i class="fa fa-angle-down" aria-hidden="true"></i>
                        <div class="triangle"></div>
                        <ul>
                        
                        <li><span>English</span></li>
                        <li> <span class="active">Spanish</span></li>
                        </ul>
                    </li>
                </ul>
                    
                </div>
            </div>
        </div>
    </nav>

    @yield('content')




<footer>
    <section class="footer-top">
        <div class="container text-right">
            <ul>
               
                <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
            </ul>
        </div>
    </section>

    <section class="footer-mid">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div><a href="#"><img alt="Logo" src="{{asset('assets/img/logo2.png')}}"></a></div>
                    <div class="row nav-list-footer">
                         <div class="col-sm-12 col-md-6">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><a href="how-it-work">How it Works</a></li>
                                <li><a href="cre-network">Creditor Network</a></li>
                                <li><a href="contact">Contact</a></li>
                            </ul>
                         </div>
                          <div class="col-sm-12 col-md-6">
                            <ul>
                                <li><a href="faq">FAQ</a></li>
                                <li><a href="security">Security</a></li>
                                <li><a href="terms">Terms & Conditions</a></li>
                                <li><a href="privacy">Privacy Policy</a></li>
                            </ul>
                         </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-8 mt-3 nego-ftr">
                    <div class="mb-5"><a href="/"><img alt="Logo" width="150" src="{{asset('assets/img/logo.png')}}"></a></div>

                    <p>
                        YouNegotiate® offers a free service to all consumers in order to resolve debts and pay bills in a single place through a simple process that doesn’t require you to speak to a collection agent or a creditor. We believe in the need to fix the consumer debt problem and that our platform is the solution to help consumers understand their personal debt situation and be able to easily make or change payments at any time.
                    </p>
                    <p>
                        By accessing and using this page you agree to the <a href="terms"> Terms and Conditions</a>.
                    </p>


                </div>
            </div>
        </div>
    </section>

    <section class="footer-btm">
        <div class="container">Copyright © 2020 All Rights Reserved. YouNegotiate</div>
    </section>
</footer>
    @yield('script')

</body>
</html>
