<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8" />
      <meta
         content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"
         name="viewport"
         />
      <link rel="shortcut icon" href="../assets/img/favicon.png" type="image/x-icon">
      <title>Dashboard</title>
      <link rel="stylesheet" href="{{asset('dist/modules/bootstrap/css/bootstrap.min.css')}}" />
      <link rel="stylesheet" href="{{asset('dist/modules/ionicons/css/ionicons.min.css')}}"  />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" />
      <link rel="stylesheet" href="{{asset('dist/modules/datatables/datatables.min.css')}}">
      <link rel="stylesheet" href="{{asset('dist/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css')}}">
      <link rel="stylesheet" href="{{asset('dist/modules/summernote/summernote-lite.css')}}" />
      <link rel="stylesheet" href="{{asset('dist/modules/flag-icon-css/css/flag-icon.min.css')}}" />
      <link rel="stylesheet" href="{{asset('dist/css/demo.css')}}" />
      <link rel="stylesheet" href="{{asset('dist/css/style.css')}}" />
   </head>
   <body>
      <div id="app">
      <div class="main-wrapper">
      <nav class="navbar navbar-expand-lg main-navbar">
         <div class="mr-auto">
            <ul class="navbar-nav mr-3">
               <li>
                  <a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="ion ion-navicon-round"></i></a>
               </li>
               <li> <a href="create-user" class="nav-link nav-link-lg"><i class="i-Add-UserStar text-20 cursor-pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Users"></i></a></li>
               <li> <a href="communication-template" class="nav-link nav-link-lg"><i class="i-Speach-Bubble-3 text-20 cursor-pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Send Mass-SMS"></i></a></li>
               <li> <a href="communication-template" class="nav-link nav-link-lg"><i class="i-Email text-20 mobile-hide cursor-pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Send Mass-Email"></i></a></li>
               <li> <a href="genarate-report" class="nav-link nav-link-lg"><i class="i-Upload-Window text-20 mobile-hide cursor-pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Run A Report"></i></a></li>
               <li> <a href="report-now" class="nav-link nav-link-lg"><i class="i-Calendar-3 text-20 mobile-hide cursor-pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Schedule Export"></i></a></li>
            </ul>
         </div>
         
         <form class="form-inline">
            <div class="search-element">
               <input
                  class="form-control"
                  type="search"
                  placeholder="Search"
                  aria-label="Search"
                  />
               <button class="btn" type="submit">
               <img alt="search" src="{{asset('dist/img/search.svg')}}">
               </button>
            </div>
         </form>
         <ul class="navbar-nav navbar-right">
            <li class="dropdown">
               <a
                  href="#"
                  data-toggle="dropdown"
                  class="nav-link dropdown-toggle nav-link-lg"
                  >
                  <i class="ion ion-android-person person-round"></i>
                  <div class="d-sm-none d-lg-inline-block mt-0">
                     Donna Weaver
                  </div>
               </a
                  >
               <div class="dropdown-menu dropdown-menu-right">
                  <a href="profile" class="dropdown-item has-icon">
                  Company Profile
                  </a>
                  <a href="change-password" class="dropdown-item has-icon">
                  Change Password
                  </a>
                  <a href="/login" class="dropdown-item lg-bottom">Logout</a>
               </div>
            </li>
            <li class="dropdown dd-flex">
               <a
                  href="#"
                  data-toggle="dropdown"
                  class="nav-link dropdown-toggle nav-link-lg fl-we-60"
                  >
                  <div class="d-lg-inline-block">
                     ENG
                  </div>
               </a
                  >
               <div class="dropdown-menu dropdown-menu-right">
                  <a href="#" class="dropdown-item has-icon">
                  ENG (English)
                  </a>
                  <a href="#" class="dropdown-item has-icon">
                  ESP (Spanish)
                  </a>
               </div>
            </li>
            <li><a href="#" class="full-icon m-mob-nan"><i class="i-Full-Screen header-icon"></i></a></li>
         </ul>

      </nav>
      <div class="main-sidebar">
         <aside id="sidebar-wrapper">
            <!-- <div class="sidebar-brand">
               <a href="index.html">Stisla Lite</a>
               </div> -->
            <div class="sidebar-brand">
               <div class="sidebar-user-picture pt-4 pb-5">
                  <img alt="image" src="{{asset('dist/img/logo.png')}}" />
               </div>
            </div>
            <ul class="sidebar-menu">
               <li class="">
                  <a href="admin-dashboard"
                     ><i class="i-Monitor-5 text-20 mr-2"></i> <span>Dashboard</span></a
                     >
               </li>
               <li class="">
                  <a href="my-customer"
                     ><i class="i-Repeat-3 text-20 mr-2"></i> <span>Customer Offers</span></a
                     >
               </li>
               <li>
                  <a href="#" class="has-dropdown"
                     ><i class="i-Sync text-20 mr-2"></i> <span>Import & Export</span></a
                     >
                  <ul class="menu-dropdown">
                     <li>
                        <a href="create-master"
                           ><i class="nav-icon text-14b i-Shuffle-21"></i> Map Import File Layout</a
                           >
                     </li>
                     <li>
                        <a href="customer-date"
                           ><i class="nav-icon text-14b i-Upload1"></i> Upload a File</a>
                     </li>
                     <li>
                        <a href="fontawesome.html" class="has-dropdown">
                        <i class="i-Bar-Chart text-20 mr-2"></i> Export / Reporting</a>
                        <ul class="menu-dropdown" style="">
                           <li>
                              <a href="genarate-report" class="text-14">
                              <i class="nav-icon text-14b i-Download1"></i>
                              <span class="item-name text-13a text-nowrap">Generate a Report</span>
                              </a>
                           </li>
                           <li>
                              <a href="report-now" class="text-14">
                              <i class="nav-icon text-14b i-Download-from-Cloud"></i>
                              <span class="item-name text-13a text-nowrap">Schedule a Report</span>
                              </a>
                           </li>
                           <li>
                              <a href="ad-hoc" class="text-14">
                              <i class="i-Add-Window text-20 mr-2"></i>
                              <span class="item-name text-13a"> Ad-hoc to Generate Report</span>
                              </a>
                           </li>
                           <li>
                              <a href="profile-permission" class="text-14">
                              <i class="nav-icon text-14b i-Files"></i>
                              <span class="item-name text-13a text-nowrap">Profile Permissions</span>
                              </a>
                           </li>
                        </ul>
                     </li>
                  </ul>
               </li>
               <li>
                  <a href="set-terms"
                     ><i class="i-Sync text-20 mr-2"></i> <span>Pay Terms</span></a
                     >
               </li>
               <li>
                  <a href="#" class="has-dropdown"
                     ><i class="i-Wifi text-20 mr-2"></i> <span>Communication</span></a
                     >
                  <ul class="menu-dropdown">
                     <li>
                        <a href="dunning"
                           ><i class="nav-icon text-14b i-Mail-Send"></i> Dunning(RegF)</a
                           >
                     </li>
                     <li>
                        <a href="communication-template"
                           ><i class="nav-icon text-14b i-Receipt"></i> Templates
                        </a>
                     </li>
                     <li>
                        <a href="customer-group"
                           ><i class="nav-icon text-14b i-Conference"></i> Customer Groups
                        </a>
                     </li>
                     <li>
                        <a href="schedule-camp"
                           ><i class="nav-icon text-14b i-Consulting"></i> Schedule Campaign
                        </a>
                     </li>
                     <li>
                        <a href="campaign-tracker"
                           ><i class="nav-icon text-14b i-Inbox-Full"></i> Campaign Tracker
                        </a>
                     </li>
                     <li>
                        <a href="eletter-tracker"
                           ><i class="nav-icon text-14b i-Mail-Send"></i> Eletter Tracker
                        </a>
                     </li>
                  </ul>
               </li>
               <li>
                  <a href="#" class="has-dropdown"
                     ><i class="i-Gear text-20 mr-2"></i> <span>Settings</span></a
                     >
                  <ul class="menu-dropdown">
                     <li>
                        <a href="manage-sub"
                           ><i class="nav-icon text-14b i-Receipt-4"></i> Sub Accounts</a
                           >
                     </li>
                     <li>
                        <a href="#" class="has-dropdown"
                           ><i class="i-Communication-Tower-2 text-20 mr-2"></i> Auto Communications</a
                           >
                           <ul class="menu-dropdown">
                              <li>
                                 <a href="schedule-camp2"
                                    ><i class="nav-icon text-14b i-Receipt"></i> campaign</a
                                    >
                              </li>
                              <li>
                                 <a href="campaign-tracker"
                                    ><i class="nav-icon text-14b i-Inbox-Full"></i> Campaign Tracker</a
                                    >
                              </li>
                           </ul>
                     </li>
                     <li>
                        <a href="all-customer"
                           ><i class="nav-icon text-14b i-Yes"></i> Customer Activity</a
                           >
                     </li>
                     <li>
                        <a href="terms-condition"
                           ><i class="nav-icon text-14b i-Receipt-4"></i> Terms & conditions</a
                           >
                     </li>
                     <li>
                        <a href="my-logo"
                           ><i class="i-Internet text-14b mr-2"></i> My Logo & Links</a
                           >
                     </li>
                     <li>
                        <a href="profile"
                           ><i class="nav-icon text-14b i-Favorite-Window"></i> My Company Profile</a
                           >
                     </li>
                     <li>
                        <a href="current-master-terms"
                           ><i class="i-Receipt-3 text-20 mr-2"></i> Terms on the Fly</a
                           >
                     </li>
                     <li>
                        <a href="create-user"
                           ><i class="nav-icon text-14b i-Add-UserStar"></i> Users</a
                           >
                     </li>
                  </ul>
               </li>
            </ul>
         </aside>
      </div>
      @yield('dashboard-content')
      @yield('script')
   </body>
</html>