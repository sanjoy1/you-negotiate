@extends('layout.app-header') @section('content')
<section class="white-common">
    <div class="container">
        <h2 class="h2-text text-center mb-5">My Profile</h2>
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-3">
                <div class="faq-nav">
                    <ul>
                        <li>
                            <a href="edit-profile">My Profile Details <i class="fa fa-angle-right"></i></a>
                        </li>
                        <li>
                            <a href="notification" class="active">Notification Preference <i class="fa fa-angle-right"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-12 col-md-8 col-lg-8 offset-lg-1">
                <div class="faq-content-box swich-content">
                    <div class="toggle-style">
                        <input type="checkbox" id="switch" />
                        <label for="switch">Toggle</label>
                    </div>
                    <h3>Text Communication</h3>
                    <p>I Authorize receiving commnications & managing my account via text on my profile mobile phone.</p>
                </div>

                <div class="faq-content-box swich-content">
                    <div class="toggle-style">
                        <input type="checkbox" id="switch2" />
                        <label for="switch2">Toggle</label>
                    </div>
                    <h3>Email Communication</h3>
                    <p>I Authorize receiving commnications & managing my account via text on my profile mobile phone.</p>

                    <div class="form-style1">
                        <form>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label class="mb-2">Email</label>
                                    <input type="text" class="form-control" value="dcooper@gmail.com" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="faq-content-box swich-content">
                    <div class="toggle-style">
                        <input type="checkbox" id="switch3" />
                        <label for="switch3">Toggle</label>
                    </div>
                    <h3>Mobile phonecalls</h3>
                    <p>I Authorize receiving calls on my profile mobile phone between the TCPA hours of 8am-9pm (local time zone).</p>

                    <div class="form-style1">
                        <form>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label class="mb-2">Phone</label>
                                    <input type="text" class="form-control" value="4346663345" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="faq-content-box swich-content">
                    <div class="toggle-style">
                        <input type="checkbox" id="switch4" />
                        <label for="switch4">Toggle</label>
                    </div>
                    <h3>Landline Phonecalls</h3>
                    <p>I Authorize receiving calls on my profile mobile phone between the TCPA hours of 8am-9pm (local time zone).</p>
                </div>

                <div class="faq-content-box swich-content">
                    <div class="toggle-style">
                        <input type="checkbox" id="switch5" />
                        <label for="switch5">Toggle</label>
                    </div>
                    <h3>USPS Communication</h3>
                    <p>
                        I prefer to receive written communications to my profile address (if you prefer to receive communications via text or email above, keep this off and you’ll receive a communication link to access in you YouNegotiate
                        Account. Go to My Accounts (link to My Accounts Page)
                    </p>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-blue">Update</button>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection @section('script')
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>

<script>
    $(document).ready(function () {
        let passwordInput = document.getElementById("txtPassword"),
            toggle = document.getElementById("btnToggle"),
            icon = document.getElementById("eyeIcon");

        function togglePassword() {
            if (passwordInput.type === "password") {
                passwordInput.type = "text";
                icon.classList.add("fa-eye-slash");
                //toggle.innerHTML = 'hide';
            } else {
                passwordInput.type = "password";
                icon.classList.remove("fa-eye-slash");
                //toggle.innerHTML = 'show';
            }
        }

        function checkInput() {}

        toggle.addEventListener("click", togglePassword, false);
        passwordInput.addEventListener("keyup", checkInput, false);
    });
</script>
@endsection
