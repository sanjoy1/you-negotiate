@extends('layout.app')
@section('content')
<section class="login-pnl">
   <div class="container">
      <div class="row align-center">
         <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="login-card form-style1">
               <h3>Sign up and join the revolution!</h3>
               <form>
                  <div class="form-group">
                     <label>Company Name</label>
                     <input type="text" class="form-control">
                  </div>
                  <div class="form-group">
                     <label>Your Name</label>
                     <input type="text" class="form-control">
                  </div>
                  <div class="form-group">
                     <label>Email Address</label>
                     <input type="email" class="form-control">
                  </div>
                  <div class="form-group">
                     <label class="flex-between">Password</label>
                     <div class="grp-area">
                        <input type="password" id="txtPassword" class="form-control" name="txtPassword" />
                        <button type="button" id="btnToggle" class="toggle-view"><i id="eyeIcon"
                           class="fa fa-eye"></i></button>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="flex-between">Confirm Password</label>
                     <div class="grp-area">
                        <input type="password" id="txtPassword2" class="form-control" name="txtPassword2" />
                        <button type="button" id="btnToggle2" class="toggle-view"><i id="eyeIcon2"
                           class="fa fa-eye"></i></button>
                     </div>
                  </div>
                  <button type="submit" class="btn btn-blue w-100 mb-4">Join YouNegotiate</button>
                  <p class="dnt-acount">Already have an account? <a href="login">Login</a></p>
               </form>
            </div>
         </div>
         <div class="col-sm-12 col-md-6 col-lg-8 pl-5">
            <div class="iframe-video pl-5">
               <iframe width="560" height="400" src="https://www.youtube.com/embed/6GGEP0Dn0is" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
@section('script')
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
<script>
   $(document).ready(function () {
       let passwordInput = document.getElementById("txtPassword"),
           toggle = document.getElementById("btnToggle"),
           icon = document.getElementById("eyeIcon");
   
       function togglePassword() {
           if (passwordInput.type === "password") {
           passwordInput.type = "text";
           icon.classList.add("fa-eye-slash");
           //toggle.innerHTML = 'hide';
           } else {
           passwordInput.type = "password";
           icon.classList.remove("fa-eye-slash");
           //toggle.innerHTML = 'show';
           }
       }
   
       function checkInput() {}
   
       toggle.addEventListener("click", togglePassword, false);
       passwordInput.addEventListener("keyup", checkInput, false);
   });
   
   $(document).ready(function () {
       let passwordInput = document.getElementById("txtPassword2"),
           toggle = document.getElementById("btnToggle2"),
           icon = document.getElementById("eyeIcon2");
   
       function togglePassword() {
           if (passwordInput.type === "password") {
           passwordInput.type = "text";
           icon.classList.add("fa-eye-slash");
           //toggle.innerHTML = 'hide';
           } else {
           passwordInput.type = "password";
           icon.classList.remove("fa-eye-slash");
           //toggle.innerHTML = 'show';
           }
       }
   
       function checkInput() {}
   
       toggle.addEventListener("click", togglePassword, false);
       passwordInput.addEventListener("keyup", checkInput, false);
   });
</script>
@endsection