@extends('layout.app')
@section('content')
<section class="login-pnl">
   <div class="container">
      <div class="row align-center align-items-center">
         <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="login-card form-style1">
               <h3>Reset Password</h3>
               <form>
                  <div class="form-group">
                     <label>Email Address</label>
                     <input type="email" class="form-control">
                  </div>
                  <button type="submit" class="btn btn-blue w-100 mb-4">Send reset link</button>
                  <p class="dnt-acount">Want to go back to login? <a href="login">Click here</a></p>
               </form>
            </div>
         </div>
         <div class="col-sm-12 col-md-6 col-lg-8 pl-5">
            <div class="iframe-video pl-5">
               <iframe width="560" height="400" src="https://www.youtube.com/embed/6GGEP0Dn0is" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
@section('script')
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
@endsection