<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web - admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/dashboard/admin-dashboard', function () {return view('dashboard.admin-dashboard');});
