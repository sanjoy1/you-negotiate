<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Consumer panel
Route::get('/', function () {return view('home');});
Route::get('/how-it-work', function () {return view('how-it-work');});
Route::get('/cre-network', function () {return view('cre-network');});
Route::get('/contact', function () {return view('contact');});
Route::get('/faq', function () {return view('faq');});
Route::get('/security', function () {return view('security');});
Route::get('/terms', function () {return view('terms');});
Route::get('/privacy', function () {return view('privacy');});

//Consumer Login
Route::get('/login', function () {return view('login');});
Route::get('/register', function () {return view('register');});
Route::get('/reset', function () {return view('reset');});
Route::get('/change-password', function () {return view('change-password');});

//Consumer after login
Route::get('/ceomail', function () {return view('ceomail');});
Route::get('/choose', function () {return view('choose');});
Route::get('/choose1', function () {return view('choose1');});
Route::get('/choose2', function () {return view('choose2');});
Route::get('/choose3', function () {return view('choose3');});
Route::get('/ceomail2', function () {return view('ceomail2');});
Route::get('/account-table', function () {return view('account-table');});
Route::get('/edit-profile', function () {return view('edit-profile');});
Route::get('/notification', function () {return view('notification');});
Route::get('/save-profile', function () {return view('save-profile');});

//Creditor admin
Route::get('/dashboard/admin-dashboard', function () {return view('dashboard.admin-dashboard');});
Route::get('/dashboard/current-customer', function () {return view('dashboard.current-customer');});
Route::get('/dashboard/customer-payment', function () {return view('dashboard.customer-payment');});
Route::get('/dashboard/30-days', function () {return view('dashboard.30-days');});
Route::get('/dashboard/my-customer', function () {return view('dashboard.my-customer');});
Route::get('/dashboard/consumer-profile', function () {return view('dashboard.consumer-profile');});
Route::get('/dashboard/send-email', function () {return view('dashboard.send-email');});
Route::get('/dashboard/send-sms', function () {return view('dashboard.send-sms');});
Route::get('/dashboard/genarate-report', function () {return view('dashboard.genarate-report');});
Route::get('/dashboard/report-now', function () {return view('dashboard.report-now');});
Route::get('/dashboard/create-master', function () {return view('dashboard.create-master');});
Route::get('/dashboard/customer-date', function () {return view('dashboard.customer-date');});
Route::get('/dashboard/communication-template', function () {return view('dashboard.communication-template');});
Route::get('/dashboard/create-user', function () {return view('dashboard.create-user');});
Route::get('/dashboard/set-terms', function () {return view('dashboard.set-terms');});
Route::get('/dashboard/ad-hoc', function () {return view('dashboard.ad-hoc');});
Route::get('/dashboard/profile-permission', function () {return view('dashboard.profile-permission');});
Route::get('/dashboard/dunning', function () {return view('dashboard.dunning');});
Route::get('/dashboard/schedule-camp', function () {return view('dashboard.schedule-camp');});
Route::get('/dashboard/camp-tracker', function () {return view('dashboard.camp-tracker');});
Route::get('/dashboard/eletter-tracker', function () {return view('dashboard.eletter-tracker');});
Route::get('/dashboard/all-customer', function () {return view('dashboard.all-customer');});
Route::get('/dashboard/terms-condition', function () {return view('dashboard.terms-condition');});
Route::get('/dashboard/current-master-terms', function () {return view('dashboard.current-master-terms');});
Route::get('/dashboard/customer-group', function () {return view('dashboard.customer-group');});
Route::get('/dashboard/transaction', function () {return view('dashboard.transaction');});
Route::get('/dashboard/deactive-customer', function () {return view('dashboard.deactive-customer');});
Route::get('/dashboard/unsubscribed-list', function () {return view('dashboard.unsubscribed-list');});
Route::get('/dashboard/communication-history', function () {return view('dashboard.communication-history');});
Route::get('/dashboard/cumulative-report', function () {return view('dashboard.cumulative-report');});
Route::get('/dashboard/invoice-report', function () {return view('dashboard.invoice-report');});
Route::get('/dashboard/dispute-consumers', function () {return view('dashboard.dispute-consumers');});
Route::get('/dashboard/campaign-tracker', function () {return view('dashboard.campaign-tracker');});
Route::get('/dashboard/profile', function () {return view('dashboard.profile');});
Route::get('/dashboard/bank-details', function () {return view('dashboard.bank-details');});
Route::get('/dashboard/master-account', function () {return view('dashboard.master-account');});
Route::get('/dashboard/contact-details', function () {return view('dashboard.contact-details');});
Route::get('/dashboard/change-password', function () {return view('dashboard.change-password');});
Route::get('/dashboard/offer-details', function () {return view('dashboard.offer-details');});
Route::get('/dashboard/counter-offer', function () {return view('dashboard.counter-offer');});
Route::get('/dashboard/profile-permission2', function () {return view('dashboard.profile-permission2');});
Route::get('/dashboard/my-logo', function () {return view('dashboard.my-logo');});
Route::get('/dashboard/counter-offer2', function () {return view('dashboard.counter-offer2');});
Route::get('/dashboard/schedule-camp2', function () {return view('dashboard.schedule-camp2');});
Route::get('/dashboard/manage-sub', function () {return view('dashboard.manage-sub');});
Route::get('/dashboard/negotiation-terms', function () {return view('dashboard.negotiation-terms');});
Route::get('/dashboard/marchent-account', function () {return view('dashboard.marchent-account');});
Route::get('/dashboard/tact-details', function () {return view('dashboard.tact-details');});



//admin

Route::get('/admin/admin-dashboard', function () {return view('admin.admin-dashboard');});
Route::get('/admin/channel-partner', function () {return view('admin.channel-partner');});
Route::get('/admin/payment', function () {return view('admin.payment');});
Route::get('/admin/template', function () {return view('admin.template');});
Route::get('/admin/configure', function () {return view('admin.configure');});
Route::get('/admin/schedule', function () {return view('admin.schedule');});
Route::get('/admin/campaign', function () {return view('admin.campaign');});
Route::get('/admin/report', function () {return view('admin.report');});
Route::get('/admin/consumer', function () {return view('admin.consumer');});
Route::get('/admin/creditor-billing', function () {return view('admin.creditor-billing');});
Route::get('/admin/manage-creditors', function () {return view('admin.manage-creditors');});
Route::get('/admin/profile', function () {return view('admin.profile');});
Route::get('/admin/bank-details', function () {return view('admin.bank-details');});
Route::get('/admin/financial-profile', function () {return view('admin.financial-profile');});
Route::get('/admin/paymentsetup', function () {return view('admin.paymentsetup');});
Route::get('/admin/my-logo', function () {return view('admin.my-logo');});
Route::get('/admin/contact-profile', function () {return view('admin.contact-profile');});
Route::get('/admin/change-password', function () {return view('admin.change-password');});
Route::get('/admin/schedule-report', function () {return view('admin.schedule-report');});
Route::get('/admin/schedule-campaign', function () {return view('admin.schedule-campaign');});
Route::get('/admin/schedule2', function () {return view('admin.schedule2');});
Route::get('/admin/configure2', function () {return view('admin.configure2');});
Route::get('/admin/template2', function () {return view('admin.template2');});
Route::get('/admin/basic-info', function () {return view('admin.basic-info');});
Route::get('/admin/pay-history', function () {return view('admin.pay-history');});
